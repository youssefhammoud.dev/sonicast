﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using WeatherApp.Entities;

namespace WeatherApp
{
    public class CityDbInitializer
    {
        private static CityContext context;
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var serviceScope = serviceProvider.CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<CityContext>();
                InitializeSchedules(context);
            }
        }
        private static void InitializeSchedules(CityContext context)
        {
            if(!context.CityEntities.Any())
            {
                CityEntity user_01 = new CityEntity { CityName = "Tokyo",Latitude = "35.685", Longitude = "139.7514",Country= "Japan" };
                CityEntity user_02 = new CityEntity { CityName = "New York", Latitude = "40.6943", Longitude = "-73.9249", Country = "United States" };
                CityEntity user_03 = new CityEntity { CityName = "Mexico City", Latitude = "19.4424", Longitude = "-99.131", Country = "Mexico" };

                context.CityEntities.Add(user_01); 
                context.CityEntities.Add(user_02);
                context.CityEntities.Add(user_03);

                context.SaveChanges();
            }
            context.SaveChanges();
        }
    }
}
