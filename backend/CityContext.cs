﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using WeatherApp.Entities;

namespace WeatherApp
{
    public class CityContext : DbContext
    {

        public DbSet<CityEntity> CityEntities { get; set; }

        public CityContext(DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }


            modelBuilder.Entity<CityEntity>()
                .ToTable("CityEntitie");

            modelBuilder.Entity<CityEntity>()
                .Property(s => s.CityName)
                .IsRequired();

            modelBuilder.Entity<CityEntity>()
                .Property(s => s.Country)
                .IsRequired();

            modelBuilder.Entity<CityEntity>()
                .Property(s => s.Latitude)
                .IsRequired();

            modelBuilder.Entity<CityEntity>()
                 .Property(s => s.Longitude)
                 .IsRequired();

            //modelBuilder.Entity<Schedule>()
            //    .Property(s => s.DateCreated)
            //    .HasDefaultValue(DateTime.Now);

            //modelBuilder.Entity<Schedule>()
            //    .Property(s => s.DateUpdated)
            //    .HasDefaultValue(DateTime.Now);

            //modelBuilder.Entity<Schedule>()
            //    .Property(s => s.Type)
            //    .HasDefaultValue(ScheduleType.Work);

            //modelBuilder.Entity<Schedule>()
            //    .Property(s => s.Status)
            //    .HasDefaultValue(ScheduleStatus.Valid);

            //modelBuilder.Entity<Schedule>()
            //    .HasOne(s => s.Creator)
            //    .WithMany(c => c.SchedulesCreated);

            //modelBuilder.Entity<User>()
            //  .ToTable("User");

            //modelBuilder.Entity<User>()
            //    .Property(u => u.Name)
            //    .HasMaxLength(100)
            //    .IsRequired();

            //modelBuilder.Entity<Attendee>()
            //  .ToTable("Attendee");

            //modelBuilder.Entity<Attendee>()
            //    .HasOne(a => a.User)
            //    .WithMany(u => u.SchedulesAttended)
            //    .HasForeignKey(a => a.UserId);

            //modelBuilder.Entity<Attendee>()
            //    .HasOne(a => a.Schedule)
            //    .WithMany(s => s.Attendees)
            //.HasForeignKey(a => a.ScheduleId);

        }
    }
}
