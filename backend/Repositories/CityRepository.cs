﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WeatherApp.Abstract;
using WeatherApp.Entities;

namespace WeatherApp.Repositories
{
    public class CityRepository : EntityBaseRepository<CityEntity>, ICityRepository
    {
        public CityRepository(CityContext context)
            : base(context)
        { }
    }
}
