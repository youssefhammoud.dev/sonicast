﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WeatherApp.Entities;

namespace WeatherApp.Abstract
{
    public interface ICityRepository : IEntityBaseRepository<CityEntity> { }

}
