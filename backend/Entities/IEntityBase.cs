﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WeatherApp.Entities
{
    public interface IEntityBase
    {
        int Id { get; set; }
    }
}
