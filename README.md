# Sonicast

--- 

## Description
Sonicast, A project to demonstrate Gitlab CI/CD for a simple application based in microservice architecture. We build and deploy our application to Azure Kubernetes Service (AKS). AKS and its dependent resources are created from Azure CLI.
Technically, it's an application written in php, html, css, js and the C# which use the dotnet core framework.

A project to demonstrate:

- Manually Containerising our microservice using docker-compose
- Creation of Azure Kubernetes Service (AKS) & Connection to Gitlab CI/CD
- Manually Deploying an Application on AKS
- Automated build & deployment using Gitlab CI/CD
- Troubleshooting

 

## Whats including in this repository
We composed our application into many services: backend, frontend and database:

- The backend service is a sample web API that extend the functionality of the web server.
- The frontend service is a sample web Application that contain a screen to share the detailed weather reports, also contain information about precipitation, wind speed, relative humidity, atmospheric pressure, and other things as well.
- The databse service store all the data related to world cities and their coordinates.

## Containerising our microservice
- [I - Delivery with docker in production mode](docs/microservice-containerising.md)

#### Creation of Azure Kubernetes Service (AKS) & Connection to Gitlab CI/CD
- [II - Creating an AKS Cluster Using the Azure CLI](docs/creating_aks_via_azure_cli.md)
- [III - Connecting Gitlab to AKS](docs/connecting_gitlab_to_aks.md)

#### Manually Deploying an Application on AKS
- [IV - Manually Deploying the frontend Application to AKS](docs/deploying_to_aks.md)

#### Automated build & deployment using Gitlab CI/CD
- [V - Using Gitlab CI/CD to build an Image](docs/gitlab_ci_build_image.md)
- [VI - Using Gitlab CI/CD to deploy an Image](docs/gitlab_ci_deploy_image.md)

#### Demonstrating Continuous Delivery
- [VII - Demonstrating Continuous Delivery](docs/continuous_delivery.md)
