CREATE DATABASE CitiesDb;
GO

USE CitiesDb;
GO

CREATE TABLE CityEntitie
(
    Id INT IDENTITY(1,1) PRIMARY KEY,
    CityName NVARCHAR(150) NOT NULL,
    Latitude  NVARCHAR(150) NOT NULL,
    Longitude NVARCHAR(150) NOT NULL,
    Country NVARCHAR(150) NOT NULL
)
           
INSERT INTO CityEntitie VALUES('George Town','5.4136','100.3294','Malaysia')

INSERT INTO CityEntitie VALUES('Mashhad','36.27','59.57','Iran')

INSERT INTO CityEntitie VALUES('Damascus','33.5','36.3','Syria')

INSERT INTO CityEntitie VALUES('Daegu','35.8668','128.607','Korea')

INSERT INTO CityEntitie VALUES('Nagpur','21.17','79.09','India')

INSERT INTO CityEntitie VALUES('Jinxi','40.7503','120.83','China')

INSERT INTO CityEntitie VALUES('Paris','48.8667','2.3333','France')

INSERT INTO CityEntitie VALUES('Shijianzhuang','38.05','114.48','China')

INSERT INTO CityEntitie VALUES('Tunis','36.8028','10.1797','Tunisia')

INSERT INTO CityEntitie VALUES('Vienna','48.2','16.3666','Austria')

INSERT INTO CityEntitie VALUES('Jilin','43.85','126.55','China')

INSERT INTO CityEntitie VALUES('Omdurman','15.6167','32.48','Sudan')

INSERT INTO CityEntitie VALUES('Bandung','-6.95','107.57','Indonesia')

INSERT INTO CityEntitie VALUES('Bekasi','-6.2173','106.9723','Indonesia')

INSERT INTO CityEntitie VALUES('Mannheim','49.5004','8.47','Germany')

INSERT INTO CityEntitie VALUES('Nanchang','28.68','115.88','China')

INSERT INTO CityEntitie VALUES('Wenzhou','28.02','120.6501','China')

INSERT INTO CityEntitie VALUES('Queens','40.7498','-73.7976','United States')

INSERT INTO CityEntitie VALUES('Vancouver','49.2734','-123.1216','Canada')

INSERT INTO CityEntitie VALUES('Birmingham','52.475','-1.92','United Kingdom')

INSERT INTO CityEntitie VALUES('Cali','3.4','-76.5','Colombia')

INSERT INTO CityEntitie VALUES('Naples','40.84','14.245','Italy')

INSERT INTO CityEntitie VALUES('Sendai','38.2871','141.0217','Japan')

INSERT INTO CityEntitie VALUES('Manchester','53.5004','-2.248','United Kingdom')

INSERT INTO CityEntitie VALUES('Puebla','19.05','-98.2','Mexico')

INSERT INTO CityEntitie VALUES('Tripoli','32.8925','13.18','Libya')

INSERT INTO CityEntitie VALUES('Tashkent','41.3117','69.2949','Uzbekistan')

INSERT INTO CityEntitie VALUES('Nanchong','30.7804','106.13','China')

INSERT INTO CityEntitie VALUES('Havana','23.132','-82.3642','Cuba')

INSERT INTO CityEntitie VALUES('Baltimore','39.3051','-76.6144','United States')

INSERT INTO CityEntitie VALUES('Belém','-1.45','-48.48','Brazil')

INSERT INTO CityEntitie VALUES('Nanning','22.82','108.32','China')

INSERT INTO CityEntitie VALUES('Patna','25.625','85.13','India')

INSERT INTO CityEntitie VALUES('Santo Domingo','18.4701','-69.9001','Dominican Republic')

INSERT INTO CityEntitie VALUES('?r?mqi','43.805','87.575','China')

INSERT INTO CityEntitie VALUES('Zaozhuang','34.88','117.57','China')

INSERT INTO CityEntitie VALUES('Baku','40.3953','49.8622','Azerbaijan')

INSERT INTO CityEntitie VALUES('Accra','5.55','-0.2167','Ghana')

INSERT INTO CityEntitie VALUES('Yantai','37.5304','121.4','China')

INSERT INTO CityEntitie VALUES('Medan','3.58','98.65','Indonesia')

INSERT INTO CityEntitie VALUES('Santa Cruz','-17.7539','-63.226','Bolivia')

INSERT INTO CityEntitie VALUES('Xuzhou','34.28','117.18','China')

INSERT INTO CityEntitie VALUES('Riverside','33.9381','-117.3948','United States')

INSERT INTO CityEntitie VALUES('Linyi','35.08','118.33','China')

INSERT INTO CityEntitie VALUES('Saint Louis','38.6358','-90.2451','United States')

INSERT INTO CityEntitie VALUES('Las Vegas','36.2291','-115.2607','United States')

INSERT INTO CityEntitie VALUES('Maracaibo','10.73','-71.66','Venezuela')

INSERT INTO CityEntitie VALUES('Kuwait','29.3697','47.9783','Kuwait')

INSERT INTO CityEntitie VALUES('Ad Damman','26.4282','50.0997','Saudi Arabia')

INSERT INTO CityEntitie VALUES('Portland','45.5372','-122.65','United States')

INSERT INTO CityEntitie VALUES('Haikou','20.05','110.32','China')

INSERT INTO CityEntitie VALUES('Hiroshima','34.3878','132.4429','Japan')

INSERT INTO CityEntitie VALUES('Baotou','40.6522','109.822','China')

INSERT INTO CityEntitie VALUES('Hefei','31.85','117.28','China')

INSERT INTO CityEntitie VALUES('Indore','22.7151','75.865','India')

INSERT INTO CityEntitie VALUES('Goiânia','-16.72','-49.3','Brazil')

INSERT INTO CityEntitie VALUES('Sanaa','15.3547','44.2066','Yemen')

INSERT INTO CityEntitie VALUES('San Antonio','29.4722','-98.5247','United States')

INSERT INTO CityEntitie VALUES('Port-au-Prince','18.541','-72.336','Haiti')

INSERT INTO CityEntitie VALUES('Haiphong','20.83','106.6801','Vietnam')

INSERT INTO CityEntitie VALUES('Suzhou','33.6361','116.9789','China')

INSERT INTO CityEntitie VALUES('Nanyang','33.0004','112.53','China')

INSERT INTO CityEntitie VALUES('Bucharest','44.4334','26.0999','Romania')

INSERT INTO CityEntitie VALUES('Ningbo','29.88','121.55','China')

INSERT INTO CityEntitie VALUES('Douala','4.0604','9.71','Cameroon')

INSERT INTO CityEntitie VALUES('Tangshan','39.6243','118.1944','China')

INSERT INTO CityEntitie VALUES('Tainan','23','120.2','Taiwan')

INSERT INTO CityEntitie VALUES('Datong','40.08','113.3','China')

INSERT INTO CityEntitie VALUES('Asunci?n','-25.2964','-57.6415','Paraguay')

INSERT INTO CityEntitie VALUES('Saidu','34.75','72.35','Pakistan')

INSERT INTO CityEntitie VALUES('Brisbane','-27.455','153.0351','Australia')

INSERT INTO CityEntitie VALUES('Rawalpindi','33.6','73.04','Pakistan')

INSERT INTO CityEntitie VALUES('Sacramento','38.5667','-121.4683','United States')

INSERT INTO CityEntitie VALUES('Beirut','33.872','35.5097','Lebanon')

INSERT INTO CityEntitie VALUES('San Jose','37.3018','-121.8485','United States')

INSERT INTO CityEntitie VALUES('Minsk','53.9','27.5666','Belarus')

INSERT INTO CityEntitie VALUES('Kyoto','35.03','135.75','Japan')

INSERT INTO CityEntitie VALUES('Barranquilla','10.96','-74.8','Colombia')

INSERT INTO CityEntitie VALUES('Orlando','28.4788','-81.342','United States')

INSERT INTO CityEntitie VALUES('Valencia','10.23','-67.98','Venezuela')

INSERT INTO CityEntitie VALUES('Shuyang','34.1299','118.7734','China')

INSERT INTO CityEntitie VALUES('Hamburg','53.55','10','Germany')

INSERT INTO CityEntitie VALUES('Vadodara','22.31','73.18','India')

INSERT INTO CityEntitie VALUES('Manaus','-3.1','-60','Brazil')

INSERT INTO CityEntitie VALUES('Shangqiu','34.4504','115.65','China')

INSERT INTO CityEntitie VALUES('Wuxi','31.58','120.3','China')

INSERT INTO CityEntitie VALUES('Palembang','-2.98','104.75','Indonesia')

INSERT INTO CityEntitie VALUES('Brussels','50.8333','4.3333','Belgium')

INSERT INTO CityEntitie VALUES('Essen','51.45','7.0166','Germany')

INSERT INTO CityEntitie VALUES('Cleveland','41.4767','-81.6805','United States')

INSERT INTO CityEntitie VALUES('Bhopal','23.25','77.41','India')

INSERT INTO CityEntitie VALUES('Hohhot','40.82','111.66','China')

INSERT INTO CityEntitie VALUES('Pittsburgh','40.4396','-79.9763','United States')

INSERT INTO CityEntitie VALUES('Luoyang','34.68','112.4701','China')

INSERT INTO CityEntitie VALUES('Santos','-23.9537','-46.3329','Brazil')

INSERT INTO CityEntitie VALUES('Jianmen','30.6501','113.16','China')

INSERT INTO CityEntitie VALUES('Warsaw','52.25','21','Poland')

INSERT INTO CityEntitie VALUES('Rabat','34.0253','-6.8361','Morocco')

INSERT INTO CityEntitie VALUES('Vit?ria','-20.324','-40.366','Brazil')

INSERT INTO CityEntitie VALUES('Quito','-0.215','-78.5001','Ecuador')

INSERT INTO CityEntitie VALUES('Antananarivo','-18.9166','47.5166','Madagascar')

INSERT INTO CityEntitie VALUES('Coimbatore','11','76.95','India')

INSERT INTO CityEntitie VALUES('Daqing','46.58','125','China')

INSERT INTO CityEntitie VALUES('Luan','31.7503','116.48','China')

INSERT INTO CityEntitie VALUES('Wanzhou','30.82','108.4','China')

INSERT INTO CityEntitie VALUES('Budapest','47.5','19.0833','Hungary')

INSERT INTO CityEntitie VALUES('Turin','45.0704','7.67','Italy')

INSERT INTO CityEntitie VALUES('Suzhou','31.3005','120.62','China')

INSERT INTO CityEntitie VALUES('Ludhiana','30.9278','75.8723','India')

INSERT INTO CityEntitie VALUES('Cincinnati','39.1412','-84.5059','United States')

INSERT INTO CityEntitie VALUES('Kumasi','6.69','-1.63','Ghana')

INSERT INTO CityEntitie VALUES('Manhattan','40.7834','-73.9662','United States')

INSERT INTO CityEntitie VALUES('Qiqihar','47.345','123.99','China')

INSERT INTO CityEntitie VALUES('Anshan','41.115','122.94','China')

INSERT INTO CityEntitie VALUES('Austin','30.3006','-97.7517','United States')

INSERT INTO CityEntitie VALUES('Zhongli','24.965','121.2168','Taiwan')

INSERT INTO CityEntitie VALUES('Handan','36.58','114.48','China')

INSERT INTO CityEntitie VALUES('Taian','36.2','117.1201','China')

INSERT INTO CityEntitie VALUES('Isfahan','32.7','51.7','Iran')

INSERT INTO CityEntitie VALUES('Kansas City','39.1239','-94.5541','United States')

INSERT INTO CityEntitie VALUES('Yaounde','3.8667','11.5167','Cameroon')

INSERT INTO CityEntitie VALUES('Shantou','23.37','116.67','China')

INSERT INTO CityEntitie VALUES('Agra','27.1704','78.015','India')

INSERT INTO CityEntitie VALUES('La Paz','-16.498','-68.15','Bolivia')

INSERT INTO CityEntitie VALUES('Zhanjiang','21.2','110.38','China')

INSERT INTO CityEntitie VALUES('Kalyan','19.2502','73.1602','India')

INSERT INTO CityEntitie VALUES('Abuja','9.0833','7.5333','Nigeria')

INSERT INTO CityEntitie VALUES('Harare','-17.8178','31.0447','Zimbabwe')

INSERT INTO CityEntitie VALUES('Indianapolis','39.7771','-86.1458','United States')

INSERT INTO CityEntitie VALUES('Xiantao','30.3704','113.44','China')

INSERT INTO CityEntitie VALUES('Tijuana','32.5','-117.08','Mexico')

INSERT INTO CityEntitie VALUES('Khulna','22.84','89.56','Bangladesh')

INSERT INTO CityEntitie VALUES('Weifang','36.7204','119.1001','China')

INSERT INTO CityEntitie VALUES('Santiago','19.5','-70.67','Dominican Republic')

INSERT INTO CityEntitie VALUES('Xinyang','32.1304','114.07','China')

INSERT INTO CityEntitie VALUES('Luzhou','28.88','105.38','China')

INSERT INTO CityEntitie VALUES('Perth','-31.955','115.84','Australia')

INSERT INTO CityEntitie VALUES('Toluca','19.3304','-99.67','Mexico')

INSERT INTO CityEntitie VALUES('Leeds','53.83','-1.58','United Kingdom')

INSERT INTO CityEntitie VALUES('Vishakhapatnam','17.73','83.305','India')

INSERT INTO CityEntitie VALUES('K?be','34.68','135.17','Japan')

INSERT INTO CityEntitie VALUES('Columbus','39.986','-82.9852','United States')

INSERT INTO CityEntitie VALUES('Multan','30.2','71.455','Pakistan')

INSERT INTO CityEntitie VALUES('Kochi','10.015','76.2239','India')

INSERT INTO CityEntitie VALUES('Gujranwala','32.1604','74.185','Pakistan')

INSERT INTO CityEntitie VALUES('Montevideo','-34.858','-56.1711','Uruguay')

INSERT INTO CityEntitie VALUES('Niter?i','-22.9','-43.1','Brazil')

INSERT INTO CityEntitie VALUES('Ganzhou','25.92','114.95','China')

INSERT INTO CityEntitie VALUES('Florence','43.78','11.25','Italy')

INSERT INTO CityEntitie VALUES('Liuzhou','24.28','109.25','China')

INSERT INTO CityEntitie VALUES('Bamako','12.65','-8','Mali')

INSERT INTO CityEntitie VALUES('Conakry','9.5315','-13.6802','Guinea')

INSERT INTO CityEntitie VALUES('Bursa','40.2','29.07','Turkey')

INSERT INTO CityEntitie VALUES('Le?n','21.15','-101.7','Mexico')

INSERT INTO CityEntitie VALUES('Virginia Beach','36.7335','-76.0435','United States')

INSERT INTO CityEntitie VALUES('Nasik','20.0004','73.78','India')

INSERT INTO CityEntitie VALUES('Fushun','41.8654','123.87','China')

INSERT INTO CityEntitie VALUES('Changde','29.03','111.68','China')

INSERT INTO CityEntitie VALUES('Daejeon','36.3355','127.425','Korea')

INSERT INTO CityEntitie VALUES('Charlotte','35.2079','-80.8303','United States')

INSERT INTO CityEntitie VALUES('Neijiang','29.5804','105.05','China')

INSERT INTO CityEntitie VALUES('Phnom Penh','11.55','104.9166','Cambodia')

INSERT INTO CityEntitie VALUES('Quanzhou','24.9','118.58','China')

INSERT INTO CityEntitie VALUES('Kharkiv','50','36.25','Ukraine')

INSERT INTO CityEntitie VALUES('Hyderabad','25.38','68.375','Pakistan')

INSERT INTO CityEntitie VALUES('Bronx','40.8501','-73.8662','United States')

INSERT INTO CityEntitie VALUES('Lomé','6.1319','1.2228','Togo')

INSERT INTO CityEntitie VALUES('C?rdoba','-31.4','-64.1823','Argentina')

INSERT INTO CityEntitie VALUES('Huainan','32.63','116.98','China')

INSERT INTO CityEntitie VALUES('Doha','25.2866','51.533','Qatar')

INSERT INTO CityEntitie VALUES('Kuala Lumpur','3.1667','101.7','Malaysia')

INSERT INTO CityEntitie VALUES('Maputo','-25.9553','32.5892','Mozambique')

INSERT INTO CityEntitie VALUES('Kaduna','10.52','7.44','Nigeria')

INSERT INTO CityEntitie VALUES('Gwangju','35.171','126.9104','Korea')

INSERT INTO CityEntitie VALUES('Kawasaki','35.53','139.705','Japan')

INSERT INTO CityEntitie VALUES('San Salvador','13.71','-89.203','El Salvador')

INSERT INTO CityEntitie VALUES('Suining','30.5333','105.5333','China')

INSERT INTO CityEntitie VALUES('Lyon','45.77','4.83','France')

INSERT INTO CityEntitie VALUES('Karaj','35.8004','50.97','Iran')

INSERT INTO CityEntitie VALUES('Kampala','0.3167','32.5833','Uganda')

INSERT INTO CityEntitie VALUES('Tabriz','38.0863','46.3012','Iran')

INSERT INTO CityEntitie VALUES('The Hague','52.08','4.27','Netherlands')

INSERT INTO CityEntitie VALUES('Davao','7.11','125.63','Philippines')

INSERT INTO CityEntitie VALUES('Marseille','43.29','5.375','France')

INSERT INTO CityEntitie VALUES('Meerut','29.0004','77.7','India')

INSERT INTO CityEntitie VALUES('Mianyang','31.47','104.77','China')

INSERT INTO CityEntitie VALUES('Semarang','-6.9666','110.42','Indonesia')

INSERT INTO CityEntitie VALUES('Faridabad','28.4333','77.3167','India')

INSERT INTO CityEntitie VALUES('Novosibirsk','55.03','82.96','Russia')

INSERT INTO CityEntitie VALUES('Makkah','21.43','39.82','Saudi Arabia')

INSERT INTO CityEntitie VALUES('Dubai','25.23','55.28','United Arab Emirates')

INSERT INTO CityEntitie VALUES('Milwaukee','43.064','-87.9669','United States')

INSERT INTO CityEntitie VALUES('Auckland','-36.8481','174.763','New Zealand')

INSERT INTO CityEntitie VALUES('Maanshan','31.7304','118.48','China')

INSERT INTO CityEntitie VALUES('Brazzaville','-4.2592','15.2847','Congo (Brazzaville)')

INSERT INTO CityEntitie VALUES('Lubumbashi','-11.68','27.48','Congo (Kinshasa)')

INSERT INTO CityEntitie VALUES('Yiyang','28.6004','112.33','China')

INSERT INTO CityEntitie VALUES('Varanasi','25.33','83','India')

INSERT INTO CityEntitie VALUES('Ciudad Ju?rez','31.6904','-106.49','Mexico')

INSERT INTO CityEntitie VALUES('Ghaziabad','28.6604','77.4084','India')

INSERT INTO CityEntitie VALUES('Pretoria','-25.7069','28.2294','South Africa')

INSERT INTO CityEntitie VALUES('Heze','35.23','115.45','China')

INSERT INTO CityEntitie VALUES('Porto','41.15','-8.62','Portugal')

INSERT INTO CityEntitie VALUES('Lusaka','-15.4166','28.2833','Zambia')

INSERT INTO CityEntitie VALUES('Asansol','23.6833','86.9833','India')

INSERT INTO CityEntitie VALUES('Changzhou','31.78','119.97','China')

INSERT INTO CityEntitie VALUES('Mosul','36.345','43.145','Iraq')

INSERT INTO CityEntitie VALUES('Yekaterinburg','56.85','60.6','Russia')

INSERT INTO CityEntitie VALUES('Peshawar','34.005','71.535','Pakistan')

INSERT INTO CityEntitie VALUES('Mandalay','21.97','96.085','Burma')

INSERT INTO CityEntitie VALUES('Jamshedpur','22.7875','86.1975','India')

INSERT INTO CityEntitie VALUES('Mbuji-Mayi','-6.15','23.6','Congo (Kinshasa)')

INSERT INTO CityEntitie VALUES('Madurai','9.92','78.12','India')

INSERT INTO CityEntitie VALUES('Adana','36.995','35.32','Turkey')

INSERT INTO CityEntitie VALUES('Sheffield','53.3667','-1.5','United Kingdom')

INSERT INTO CityEntitie VALUES('Jabalpur','23.1751','79.9551','India')

INSERT INTO CityEntitie VALUES('San José','9.935','-84.0841','Costa Rica')

INSERT INTO CityEntitie VALUES('Panama City','8.968','-79.533','Panama')

INSERT INTO CityEntitie VALUES('Nizhny Novgorod','56.333','44.0001','Russia')

INSERT INTO CityEntitie VALUES('Chifeng','42.27','118.95','China')

INSERT INTO CityEntitie VALUES('Duisburg','51.43','6.75','Germany')

INSERT INTO CityEntitie VALUES('Munich','48.1299','11.575','Germany')

INSERT INTO CityEntitie VALUES('Stockholm','59.3508','18.0973','Sweden')

INSERT INTO CityEntitie VALUES('Huaiyin','33.58','119.03','China')

INSERT INTO CityEntitie VALUES('Ujungpandang','-5.14','119.432','Indonesia')

INSERT INTO CityEntitie VALUES('Rajkot','22.31','70.8','India')

INSERT INTO CityEntitie VALUES('Dhanbad','23.8004','86.42','India')

INSERT INTO CityEntitie VALUES('Mudangiang','44.575','129.59','China')

INSERT INTO CityEntitie VALUES('Geneva','46.21','6.14','Switzerland')

INSERT INTO CityEntitie VALUES('Shiraz','29.63','52.57','Iran')

INSERT INTO CityEntitie VALUES('Huzhou','30.8704','120.1','China')

INSERT INTO CityEntitie VALUES('Tianshui','34.6','105.92','China')

INSERT INTO CityEntitie VALUES('Lupanshui','26.5944','104.8333','China')

INSERT INTO CityEntitie VALUES('D?sseldorf','51.2204','6.78','Germany')

INSERT INTO CityEntitie VALUES('Maoming','21.9204','110.87','China')

INSERT INTO CityEntitie VALUES('Seville','37.405','-5.98','Spain')

INSERT INTO CityEntitie VALUES('Amritsar','31.64','74.87','India')

INSERT INTO CityEntitie VALUES('Vila Velha','-20.3676','-40.318','Brazil')

INSERT INTO CityEntitie VALUES('Vila Velha','3.2167','-51.2167','Brazil')

INSERT INTO CityEntitie VALUES('Almaty','43.325','76.915','Kazakhstan')

INSERT INTO CityEntitie VALUES('Providence','41.8229','-71.4186','United States')

INSERT INTO CityEntitie VALUES('Warangal','18.01','79.58','India')

INSERT INTO CityEntitie VALUES('Rosario','-32.9511','-60.6663','Argentina')

INSERT INTO CityEntitie VALUES('Allahabad','25.455','81.84','India')

INSERT INTO CityEntitie VALUES('Benin City','6.3405','5.62','Nigeria')

INSERT INTO CityEntitie VALUES('Macei?','-9.62','-35.73','Brazil')

INSERT INTO CityEntitie VALUES('Jining','35.4004','116.55','China')

INSERT INTO CityEntitie VALUES('Sofia','42.6833','23.3167','Bulgaria')

INSERT INTO CityEntitie VALUES('Abbottabad','34.1495','73.1995','Pakistan')

INSERT INTO CityEntitie VALUES('Banghazi','32.1167','20.0667','Libya')

INSERT INTO CityEntitie VALUES('Cilacap','-7.7188','109.0154','Indonesia')

INSERT INTO CityEntitie VALUES('Prague','50.0833','14.466','Czechia')

INSERT INTO CityEntitie VALUES('Glasgow','55.8744','-4.2507','United Kingdom')

INSERT INTO CityEntitie VALUES('Leshan','29.5671','103.7333','China')

INSERT INTO CityEntitie VALUES('Jacksonville','30.3322','-81.6749','United States')

INSERT INTO CityEntitie VALUES('Ouagadougou','12.3703','-1.5247','Burkina Faso')

INSERT INTO CityEntitie VALUES('Adelaide','-34.935','138.6','Australia')

INSERT INTO CityEntitie VALUES('Ottawa','45.4167','-75.7','Canada')

INSERT INTO CityEntitie VALUES('Shangrao','28.4704','117.97','China')

INSERT INTO CityEntitie VALUES('Torre?n','25.5701','-103.42','Mexico')

INSERT INTO CityEntitie VALUES('Srinagar','34.1','74.815','India')

INSERT INTO CityEntitie VALUES('Samara','53.195','50.1513','Russia')

INSERT INTO CityEntitie VALUES('Vijayawada','16.52','80.63','India')

INSERT INTO CityEntitie VALUES('Omsk','54.99','73.4','Russia')

INSERT INTO CityEntitie VALUES('Newcastle','-32.8453','151.815','Australia')

INSERT INTO CityEntitie VALUES('Yulin','22.63','110.15','China')

INSERT INTO CityEntitie VALUES('Nampo','38.7669','125.4524','Korea')

INSERT INTO CityEntitie VALUES('Xianyang','34.3456','108.7147','China')

INSERT INTO CityEntitie VALUES('Cagayan de Oro','8.4508','124.6853','Philippines')

INSERT INTO CityEntitie VALUES('Can Tho','10.05','105.77','Vietnam')

INSERT INTO CityEntitie VALUES('Barquisimeto','10.05','-69.3','Venezuela')

INSERT INTO CityEntitie VALUES('Kazan','55.7499','49.1263','Russia')

INSERT INTO CityEntitie VALUES('Helsinki','60.1756','24.9341','Finland')

INSERT INTO CityEntitie VALUES('Aurangabad','19.8957','75.3203','India')

INSERT INTO CityEntitie VALUES('Calgary','51.083','-114.08','Canada')

INSERT INTO CityEntitie VALUES('Nezahualcoyotl','19.41','-99.03','Mexico')

INSERT INTO CityEntitie VALUES('Z?rich','47.38','8.55','Switzerland')

INSERT INTO CityEntitie VALUES('Baoding','38.8704','115.48','China')

INSERT INTO CityEntitie VALUES('Zigong','29.4','104.78','China')

INSERT INTO CityEntitie VALUES('Sharjah','25.3714','55.4065','United Arab Emirates')

INSERT INTO CityEntitie VALUES('Yerevan','40.1812','44.5136','Armenia')

INSERT INTO CityEntitie VALUES('Mogadishu','2.0667','45.3667','Somalia')

INSERT INTO CityEntitie VALUES('Huambo','-12.75','15.76','Angola')

INSERT INTO CityEntitie VALUES('Ankang','32.68','109.02','China')

INSERT INTO CityEntitie VALUES('Tbilisi','41.725','44.7908','Georgia')

INSERT INTO CityEntitie VALUES('Ikare','7.5304','5.76','Nigeria')

INSERT INTO CityEntitie VALUES('Belgrade','44.8186','20.468','Serbia')

INSERT INTO CityEntitie VALUES('Salt Lake City','40.7774','-111.9301','United States')

INSERT INTO CityEntitie VALUES('Bhilai','21.2167','81.4333','India')

INSERT INTO CityEntitie VALUES('Jinhua','29.12','119.65','China')

INSERT INTO CityEntitie VALUES('Chelyabinsk','55.155','61.4387','Russia')

INSERT INTO CityEntitie VALUES('Natal','-5.78','-35.24','Brazil')

INSERT INTO CityEntitie VALUES('Dushanbe','38.56','68.7739','Tajikistan')

INSERT INTO CityEntitie VALUES('K?benhavn','55.6786','12.5635','Denmark')

INSERT INTO CityEntitie VALUES('Changwon','35.2191','128.5836','Korea')

INSERT INTO CityEntitie VALUES('Zhuzhou','27.83','113.15','China')

INSERT INTO CityEntitie VALUES('Suwon','37.2578','127.0109','Korea')

INSERT INTO CityEntitie VALUES('Nashville','36.1714','-86.7844','United States')

INSERT INTO CityEntitie VALUES('Vereeniging','-26.6496','27.96','South Africa')

INSERT INTO CityEntitie VALUES('Xiangfan','32.02','112.13','China')

INSERT INTO CityEntitie VALUES('Memphis','35.1047','-89.9773','United States')

INSERT INTO CityEntitie VALUES('Ulsan','35.5467','129.317','Korea')

INSERT INTO CityEntitie VALUES('Zhucheng','35.99','119.3801','China')

INSERT INTO CityEntitie VALUES('Amman','31.95','35.9333','Jordan')

INSERT INTO CityEntitie VALUES('Richmond','37.5295','-77.4756','United States')

INSERT INTO CityEntitie VALUES('Dublin','53.3331','-6.2489','Ireland')

INSERT INTO CityEntitie VALUES('Edmonton','53.55','-113.5','Canada')

INSERT INTO CityEntitie VALUES('Sholapur','17.6704','75.9','India')

INSERT INTO CityEntitie VALUES('Rostov','47.2346','39.7127','Russia')

INSERT INTO CityEntitie VALUES('Dnipro','48.48','35','Ukraine')

INSERT INTO CityEntitie VALUES('Xining','36.62','101.77','China')

INSERT INTO CityEntitie VALUES('Zhangjiakou','40.83','114.93','China')

INSERT INTO CityEntitie VALUES('Gaziantep','37.075','37.385','Turkey')

INSERT INTO CityEntitie VALUES('Lille','50.65','3.08','France')

INSERT INTO CityEntitie VALUES('Ranchi','23.37','85.33','India')

INSERT INTO CityEntitie VALUES('Monrovia','6.3106','-10.8048','Liberia')

INSERT INTO CityEntitie VALUES('S?o Lu?s','-2.516','-44.266','Brazil')

INSERT INTO CityEntitie VALUES('Amsterdam','52.35','4.9166','Netherlands')

INSERT INTO CityEntitie VALUES('Jerusalem','31.7784','35.2066','Israel')

INSERT INTO CityEntitie VALUES('New Orleans','30.0687','-89.9288','United States')

INSERT INTO CityEntitie VALUES('Guatemala','14.6211','-90.527','Guatemala')

INSERT INTO CityEntitie VALUES('Florian?polis','-27.58','-48.52','Brazil')

INSERT INTO CityEntitie VALUES('Zhuhai','22.2769','113.5678','China')

INSERT INTO CityEntitie VALUES('Port Elizabeth','-33.97','25.6','South Africa')

INSERT INTO CityEntitie VALUES('Port Harcourt','4.81','7.01','Nigeria')

INSERT INTO CityEntitie VALUES('Jiamusi','46.83','130.35','China')

INSERT INTO CityEntitie VALUES('Raleigh','35.8323','-78.6439','United States')

INSERT INTO CityEntitie VALUES('Ufa','54.79','56.04','Russia')

INSERT INTO CityEntitie VALUES('Hengyang','26.88','112.59','China')

INSERT INTO CityEntitie VALUES('Benxi','41.3304','123.75','China')

INSERT INTO CityEntitie VALUES('Louisville','38.1662','-85.6488','United States')

INSERT INTO CityEntitie VALUES('Haifa','32.8204','34.98','Israel')

INSERT INTO CityEntitie VALUES('Medina','24.5','39.58','Saudi Arabia')

INSERT INTO CityEntitie VALUES('Bucaramanga','7.1301','-73.1259','Colombia')

INSERT INTO CityEntitie VALUES('Maracay','10.2469','-67.5958','Venezuela')

INSERT INTO CityEntitie VALUES('Rotterdam','51.92','4.48','Netherlands')

INSERT INTO CityEntitie VALUES('Hims','34.73','36.72','Syria')

INSERT INTO CityEntitie VALUES('Cologne','50.93','6.95','Germany')

INSERT INTO CityEntitie VALUES('Qinhuangdao','39.9304','119.62','China')

INSERT INTO CityEntitie VALUES('Fez','34.0546','-5.0004','Morocco')

INSERT INTO CityEntitie VALUES('Aden','12.7797','45.0095','Yemen')

INSERT INTO CityEntitie VALUES('Da Nang','16.06','108.25','Vietnam')

INSERT INTO CityEntitie VALUES('Cochabamba','-17.41','-66.17','Bolivia')

INSERT INTO CityEntitie VALUES('Yongzhou','26.2304','111.62','China')

INSERT INTO CityEntitie VALUES('Baoshan','25.12','99.15','China')

INSERT INTO CityEntitie VALUES('Kitaky?sh?','33.8704','130.82','Japan')

INSERT INTO CityEntitie VALUES('Perm','58','56.25','Russia')

INSERT INTO CityEntitie VALUES('Ahvaz','31.28','48.72','Iran')

INSERT INTO CityEntitie VALUES('Jodhpur','26.2918','73.0168','India')

INSERT INTO CityEntitie VALUES('San Luis Potos?','22.17','-101','Mexico')

INSERT INTO CityEntitie VALUES('Odessa','46.49','30.71','Ukraine')

INSERT INTO CityEntitie VALUES('Yinchuan','38.468','106.273','China')

INSERT INTO CityEntitie VALUES('Ndjamena','12.1131','15.0491','Chad')

INSERT INTO CityEntitie VALUES('Donetsk','48','37.83','Ukraine')

INSERT INTO CityEntitie VALUES('Joinville','-26.32','-48.8399','Brazil')

INSERT INTO CityEntitie VALUES('Jiaxing','30.7704','120.75','China')

INSERT INTO CityEntitie VALUES('Guilin','25.28','110.28','China')

INSERT INTO CityEntitie VALUES('Dahuk','36.8667','43','Iraq')

INSERT INTO CityEntitie VALUES('Volgograd','48.71','44.5','Russia')

INSERT INTO CityEntitie VALUES('Guwahati','26.16','91.77','India')

INSERT INTO CityEntitie VALUES('Yichun','27.8333','114.4','China')

INSERT INTO CityEntitie VALUES('Yangquan','37.87','113.57','China')

INSERT INTO CityEntitie VALUES('Natal','-6.9838','-60.2699','Brazil')

INSERT INTO CityEntitie VALUES('Chandigarh','30.72','76.78','India')

INSERT INTO CityEntitie VALUES('Gwalior','26.23','78.1801','India')

INSERT INTO CityEntitie VALUES('Hamamatsu','34.7181','137.7327','Japan')

INSERT INTO CityEntitie VALUES('Qom','34.65','50.95','Iran')

INSERT INTO CityEntitie VALUES('Mérida','20.9666','-89.6166','Mexico')

INSERT INTO CityEntitie VALUES('Jixi','45.3','130.97','China')

INSERT INTO CityEntitie VALUES('Xinyi','34.38','118.35','China')

INSERT INTO CityEntitie VALUES('Querétaro','20.63','-100.38','Mexico')

INSERT INTO CityEntitie VALUES('Pingxiang','27.62','113.85','China')

INSERT INTO CityEntitie VALUES('Kelang','3.0204','101.55','Malaysia')

INSERT INTO CityEntitie VALUES('Jo?o Pessoa','-7.1011','-34.8761','Brazil')

INSERT INTO CityEntitie VALUES('Jinzhou','41.1204','121.1','China')

INSERT INTO CityEntitie VALUES('Oklahoma City','35.4676','-97.5136','United States')

INSERT INTO CityEntitie VALUES('Salerno','40.6804','14.7699','Italy')

INSERT INTO CityEntitie VALUES('Thiruvananthapuram','8.5','76.95','India')

INSERT INTO CityEntitie VALUES('Kozhikode','11.2504','75.77','India')

INSERT INTO CityEntitie VALUES('Ogbomosho','8.13','4.24','Nigeria')

INSERT INTO CityEntitie VALUES('Tiruchirappalli','10.81','78.69','India')

INSERT INTO CityEntitie VALUES('General Santos','6.1108','125.1747','Philippines')

INSERT INTO CityEntitie VALUES('Hue','16.47','107.58','Vietnam')

INSERT INTO CityEntitie VALUES('Bacolod','10.6317','122.9817','Philippines')

INSERT INTO CityEntitie VALUES('Nantong','32.0304','120.825','China')

INSERT INTO CityEntitie VALUES('Tegucigalpa','14.102','-87.2175','Honduras')

INSERT INTO CityEntitie VALUES('Foshan','23.0301','113.12','China')

INSERT INTO CityEntitie VALUES('Songnam','37.4386','127.1378','Korea')

INSERT INTO CityEntitie VALUES('Bridgeport','41.1909','-73.1958','United States')

INSERT INTO CityEntitie VALUES('Kingston','17.9771','-76.7674','Jamaica')

INSERT INTO CityEntitie VALUES('Naypyidaw','19.7666','96.1186','Burma')

INSERT INTO CityEntitie VALUES('Nice','43.715','7.265','France')

INSERT INTO CityEntitie VALUES('Buffalo','42.9017','-78.8487','United States')

INSERT INTO CityEntitie VALUES('Irbil','36.179','44.0086','Iraq')

INSERT INTO CityEntitie VALUES('Krasnoyarsk','56.014','92.866','Russia')

INSERT INTO CityEntitie VALUES('Djibouti','11.595','43.148','Djibouti')

INSERT INTO CityEntitie VALUES('Olinda','-8','-34.85','Brazil')

INSERT INTO CityEntitie VALUES('Managua','12.153','-86.2685','Nicaragua')

INSERT INTO CityEntitie VALUES('Antwerpen','51.2204','4.415','Belgium')

INSERT INTO CityEntitie VALUES('Konya','37.875','32.475','Turkey')

INSERT INTO CityEntitie VALUES('Bogor','-6.57','106.75','Indonesia')

INSERT INTO CityEntitie VALUES('Niamey','13.5167','2.1167','Niger')

INSERT INTO CityEntitie VALUES('Hartford','41.7661','-72.6834','United States')

INSERT INTO CityEntitie VALUES('Xinyu','27.8','114.93','China')

INSERT INTO CityEntitie VALUES('Huaibei','33.9504','116.75','China')

INSERT INTO CityEntitie VALUES('Teresina','-5.095','-42.78','Brazil')

INSERT INTO CityEntitie VALUES('Naha','26.2072','127.673','Japan')

INSERT INTO CityEntitie VALUES('Xinxiang','35.3204','113.87','China')

INSERT INTO CityEntitie VALUES('Goyang','37.6527','126.8372','Korea')

INSERT INTO CityEntitie VALUES('Yibin','28.77','104.57','China')

INSERT INTO CityEntitie VALUES('Aba','5.1004','7.35','Nigeria')

INSERT INTO CityEntitie VALUES('Maiduguri','11.85','13.16','Nigeria')

INSERT INTO CityEntitie VALUES('Tirana','41.3275','19.8189','Albania')

INSERT INTO CityEntitie VALUES('Kathmandu','27.7167','85.3166','Nepal')

INSERT INTO CityEntitie VALUES('Az Zarqa','32.07','36.1','Jordan')

INSERT INTO CityEntitie VALUES('Tarsus','36.9204','34.88','Turkey')

INSERT INTO CityEntitie VALUES('Bengbu','32.95','117.33','China')

INSERT INTO CityEntitie VALUES('Mendoza','-32.8833','-68.8166','Argentina')

INSERT INTO CityEntitie VALUES('Hubli','15.36','75.125','India')

INSERT INTO CityEntitie VALUES('Concepci?n','-36.83','-73.05','Chile')

INSERT INTO CityEntitie VALUES('Zaria','11.08','7.71','Nigeria')

INSERT INTO CityEntitie VALUES('Anyang','36.08','114.35','China')

INSERT INTO CityEntitie VALUES('Cartagena','10.3997','-75.5144','Colombia')

INSERT INTO CityEntitie VALUES('Mysore','12.31','76.66','India')

INSERT INTO CityEntitie VALUES('Ulaanbaatar','47.9167','106.9166','Mongolia')

INSERT INTO CityEntitie VALUES('Mexicali','32.65','-115.48','Mexico')

INSERT INTO CityEntitie VALUES('Tongliao','43.62','122.27','China')

INSERT INTO CityEntitie VALUES('Newcastle','55.0004','-1.6','United Kingdom')

INSERT INTO CityEntitie VALUES('Mombasa','-4.04','39.6899','Kenya')

INSERT INTO CityEntitie VALUES('Novo Hamburgo','-29.7096','-51.14','Brazil')

INSERT INTO CityEntitie VALUES('Callao','-12.07','-77.135','Peru')

INSERT INTO CityEntitie VALUES('Bilbao','43.25','-2.93','Spain')

INSERT INTO CityEntitie VALUES('Johor Bahru','1.48','103.73','Malaysia')

INSERT INTO CityEntitie VALUES('Yichang','30.7','111.28','China')

INSERT INTO CityEntitie VALUES('Raipur','21.235','81.635','India')

INSERT INTO CityEntitie VALUES('Fort Worth','32.7814','-97.3473','United States')

INSERT INTO CityEntitie VALUES('Salem','11.67','78.1801','India')

INSERT INTO CityEntitie VALUES('Yangjiang','21.8504','111.97','China')

INSERT INTO CityEntitie VALUES('Marrakesh','31.63','-8','Morocco')

INSERT INTO CityEntitie VALUES('Kaifeng','34.85','114.35','China')

INSERT INTO CityEntitie VALUES('Dandong','40.1436','124.3936','China')

INSERT INTO CityEntitie VALUES('Basra','30.5135','47.8136','Iraq')

INSERT INTO CityEntitie VALUES('Aguascalientes','21.8795','-102.2904','Mexico')

INSERT INTO CityEntitie VALUES('Tucson','32.1546','-110.8782','United States')

INSERT INTO CityEntitie VALUES('Okayama','34.672','133.9171','Japan')

INSERT INTO CityEntitie VALUES('Xuanzhou','30.9525','118.7553','China')

INSERT INTO CityEntitie VALUES('Puch on','37.4989','126.7831','Korea')

INSERT INTO CityEntitie VALUES('Rizhao','35.4304','119.45','China')

INSERT INTO CityEntitie VALUES('Bandar Lampung','-5.43','105.27','Indonesia')

INSERT INTO CityEntitie VALUES('Palermo','38.125','13.35','Italy')

INSERT INTO CityEntitie VALUES('Cardiff','51.5','-3.225','United Kingdom')

INSERT INTO CityEntitie VALUES('Kigali','-1.9536','30.0605','Rwanda')

INSERT INTO CityEntitie VALUES('Tampico','22.3','-97.87','Mexico')

INSERT INTO CityEntitie VALUES('Jiaozuo','35.25','113.22','China')

INSERT INTO CityEntitie VALUES('Padang','-0.96','100.36','Indonesia')

INSERT INTO CityEntitie VALUES('Jullundur','31.3349','75.569','India')

INSERT INTO CityEntitie VALUES('Valpara?so','-33.0478','-71.621','Chile')

INSERT INTO CityEntitie VALUES('Zhenjiang','32.22','119.43','China')

INSERT INTO CityEntitie VALUES('Zunyi','27.7','106.92','China')

INSERT INTO CityEntitie VALUES('Anshun','26.2504','105.93','China')

INSERT INTO CityEntitie VALUES('Pingdingshan','33.7304','113.3','China')

INSERT INTO CityEntitie VALUES('Toulouse','43.62','1.4499','France')

INSERT INTO CityEntitie VALUES('El Paso','31.8479','-106.4309','United States')

INSERT INTO CityEntitie VALUES('Nova Iguaçu','-22.74','-43.47','Brazil')

INSERT INTO CityEntitie VALUES('Voronezh','51.73','39.27','Russia')

INSERT INTO CityEntitie VALUES('Bhubaneshwar','20.2704','85.8274','India')

INSERT INTO CityEntitie VALUES('Saratov','51.58','46.03','Russia')

INSERT INTO CityEntitie VALUES('Yuci','37.6804','112.73','China')

INSERT INTO CityEntitie VALUES('Yancheng','33.3856','120.1253','China')

INSERT INTO CityEntitie VALUES('Bishkek','42.8731','74.5852','Kyrgyzstan')

INSERT INTO CityEntitie VALUES('Oslo','59.9167','10.75','Norway')

INSERT INTO CityEntitie VALUES('Cuernavaca','18.9211','-99.24','Mexico')

INSERT INTO CityEntitie VALUES('Linfen','36.0803','111.52','China')

INSERT INTO CityEntitie VALUES('Honolulu','21.3294','-157.846','United States')

INSERT INTO CityEntitie VALUES('Bangui','4.3666','18.5583','Central African Republic')

INSERT INTO CityEntitie VALUES('Warri','5.52','5.76','Nigeria')

INSERT INTO CityEntitie VALUES('Tucum?n','-26.816','-65.2166','Argentina')

INSERT INTO CityEntitie VALUES('Basel','47.5804','7.59','Switzerland')

INSERT INTO CityEntitie VALUES('Kermanshah','34.38','47.06','Iran')

INSERT INTO CityEntitie VALUES('Thessaloniki','40.6961','22.885','Greece')

INSERT INTO CityEntitie VALUES('Omaha','41.2628','-96.0495','United States')

INSERT INTO CityEntitie VALUES('Freetown','8.47','-13.2342','Sierra Leone')

INSERT INTO CityEntitie VALUES('Kota','25.18','75.835','India')

INSERT INTO CityEntitie VALUES('Braga','41.555','-8.4213','Portugal')

INSERT INTO CityEntitie VALUES('Jhansi','25.453','78.5575','India')

INSERT INTO CityEntitie VALUES('Yueyang','29.3801','113.1','China')

INSERT INTO CityEntitie VALUES('Nottingham','52.9703','-1.17','United Kingdom')

INSERT INTO CityEntitie VALUES('Agadir','30.44','-9.62','Morocco')

INSERT INTO CityEntitie VALUES('Butterworth','5.4171','100.4','Malaysia')

INSERT INTO CityEntitie VALUES('Bareilly','28.3454','79.42','India')

INSERT INTO CityEntitie VALUES('Jos','9.93','8.89','Nigeria')

INSERT INTO CityEntitie VALUES('Xingyi','25.0904','104.89','China')

INSERT INTO CityEntitie VALUES('Arequipa','-16.42','-71.53','Peru')

INSERT INTO CityEntitie VALUES('Cebu','10.32','123.9001','Philippines')

INSERT INTO CityEntitie VALUES('Liverpool','53.416','-2.918','United Kingdom')

INSERT INTO CityEntitie VALUES('Rajshahi','24.375','88.605','Bangladesh')

INSERT INTO CityEntitie VALUES('Langfang','39.5204','116.68','China')

INSERT INTO CityEntitie VALUES('Wuhu','31.3504','118.37','China')

INSERT INTO CityEntitie VALUES('Culiac?n','24.83','-107.38','Mexico')

INSERT INTO CityEntitie VALUES('Zhaotang','27.3204','103.72','China')

INSERT INTO CityEntitie VALUES('Valencia','39.485','-0.4','Spain')

INSERT INTO CityEntitie VALUES('Cuiab?','-15.5696','-56.085','Brazil')

INSERT INTO CityEntitie VALUES('Lingyuan','41.24','119.4011','China')

INSERT INTO CityEntitie VALUES('Qui Nhon','13.78','109.18','Vietnam')

INSERT INTO CityEntitie VALUES('Malang','-7.98','112.61','Indonesia')

INSERT INTO CityEntitie VALUES('Aligarh','27.8922','78.0618','India')

INSERT INTO CityEntitie VALUES('Lvov','49.835','24.03','Ukraine')

INSERT INTO CityEntitie VALUES('Bordeaux','44.85','-0.595','France')

INSERT INTO CityEntitie VALUES('McAllen','26.2325','-98.2467','United States')

INSERT INTO CityEntitie VALUES('Baoji','34.38','107.15','China')

INSERT INTO CityEntitie VALUES('Pekanbaru','0.565','101.425','Indonesia')

INSERT INTO CityEntitie VALUES('Oran','35.71','-0.62','Algeria')

INSERT INTO CityEntitie VALUES('Yingkow','40.6703','122.28','China')

INSERT INTO CityEntitie VALUES('Bhiwandi','19.35','73.13','India')

INSERT INTO CityEntitie VALUES('Liaoyang','41.28','123.18','China')

INSERT INTO CityEntitie VALUES('Chihuahua','28.645','-106.085','Mexico')

INSERT INTO CityEntitie VALUES('Jammu','32.7118','74.8467','India')

INSERT INTO CityEntitie VALUES('Malacca','2.2064','102.2465','Malaysia')

INSERT INTO CityEntitie VALUES('Zaporizhzhya','47.8573','35.1768','Ukraine')

INSERT INTO CityEntitie VALUES('Moradabad','28.8418','78.7568','India')

INSERT INTO CityEntitie VALUES('Antalya','36.89','30.7','Turkey')

INSERT INTO CityEntitie VALUES('Al Hudaydah','14.7979','42.953','Yemen')

INSERT INTO CityEntitie VALUES('Islamabad','33.7','73.1666','Pakistan')

INSERT INTO CityEntitie VALUES('Campo Grande','-20.45','-54.6166','Brazil')

INSERT INTO CityEntitie VALUES('Shaoxing','30.0004','120.57','China')

INSERT INTO CityEntitie VALUES('Yichun','47.6999','128.9','China')

INSERT INTO CityEntitie VALUES('Mangalore','12.9','74.85','India')

INSERT INTO CityEntitie VALUES('Wuppertal','51.25','7.17','Germany')

INSERT INTO CityEntitie VALUES('Cheongju','36.6439','127.5012','Korea')

INSERT INTO CityEntitie VALUES('Zamboanga','6.92','122.08','Philippines')

INSERT INTO CityEntitie VALUES('Hamhung','39.9101','127.5454','Korea')

INSERT INTO CityEntitie VALUES('Ilorin','8.49','4.55','Nigeria')

INSERT INTO CityEntitie VALUES('Fuyang','30.0533','119.9519','China')

INSERT INTO CityEntitie VALUES('Saarbr?cken','49.2504','6.97','Germany')

INSERT INTO CityEntitie VALUES('Fuxin','42.0105','121.66','China')

INSERT INTO CityEntitie VALUES('Shiyan','32.57','110.78','China')

INSERT INTO CityEntitie VALUES('Quetta','30.22','67.025','Pakistan')

INSERT INTO CityEntitie VALUES('Trujillo','-8.12','-79.02','Peru')

INSERT INTO CityEntitie VALUES('Kananga','-5.89','22.4','Congo (Kinshasa)')

INSERT INTO CityEntitie VALUES('Trabzon','40.98','39.72','Turkey')

INSERT INTO CityEntitie VALUES('Cotonou','6.4','2.52','Benin')

INSERT INTO CityEntitie VALUES('Jincheng','35.5004','112.83','China')

INSERT INTO CityEntitie VALUES('Albuquerque','35.1053','-106.6464','United States')

INSERT INTO CityEntitie VALUES('??d?','51.775','19.4514','Poland')

INSERT INTO CityEntitie VALUES('Krak?w','50.06','19.96','Poland')

INSERT INTO CityEntitie VALUES('Vientiane','17.9667','102.6','Laos')

INSERT INTO CityEntitie VALUES('Saltillo','25.42','-101.005','Mexico')

INSERT INTO CityEntitie VALUES('S?o José dos Campos','-23.2','-45.8799','Brazil')

INSERT INTO CityEntitie VALUES('Hungnam','39.8231','127.6232','Korea')

INSERT INTO CityEntitie VALUES('Taizz','13.6045','44.0394','Yemen')

INSERT INTO CityEntitie VALUES('Pietermaritzburg','-29.61','30.39','South Africa')

INSERT INTO CityEntitie VALUES('Tangier','35.7473','-5.8327','Morocco')

INSERT INTO CityEntitie VALUES('Changhua','24.0734','120.5134','Taiwan')

INSERT INTO CityEntitie VALUES('Hsinchu','24.8168','120.9767','Taiwan')

INSERT INTO CityEntitie VALUES('Fargona','40.39','71.78','Uzbekistan')

INSERT INTO CityEntitie VALUES('Namangan','41','71.67','Uzbekistan')

INSERT INTO CityEntitie VALUES('Kolhapur','16.7','74.22','India')

INSERT INTO CityEntitie VALUES('Liège','50.63','5.58','Belgium')

INSERT INTO CityEntitie VALUES('Ciudad Guayana','8.37','-62.62','Venezuela')

INSERT INTO CityEntitie VALUES('Birmingham','33.5276','-86.7988','United States')

INSERT INTO CityEntitie VALUES('Hegang','47.4','130.37','China')

INSERT INTO CityEntitie VALUES('Riga','56.95','24.1','Latvia')

INSERT INTO CityEntitie VALUES('Nouakchott','18.0864','-15.9753','Mauritania')

INSERT INTO CityEntitie VALUES('Naga','13.6192','123.1814','Philippines')

INSERT INTO CityEntitie VALUES('Gda?sk','54.36','18.64','Poland')

INSERT INTO CityEntitie VALUES('Ansan','37.3481','126.8595','Korea')

INSERT INTO CityEntitie VALUES('N?rnberg','49.45','11.08','Germany')

INSERT INTO CityEntitie VALUES('Oyo','7.8504','3.93','Nigeria')

INSERT INTO CityEntitie VALUES('Muscat','23.6133','58.5933','Oman')

INSERT INTO CityEntitie VALUES('Amravati','20.95','77.77','India')

INSERT INTO CityEntitie VALUES('Denpasar','-8.65','115.22','Indonesia')

INSERT INTO CityEntitie VALUES('Sokoto','13.06','5.24','Nigeria')

INSERT INTO CityEntitie VALUES('Beihai','21.4804','109.1','China')

INSERT INTO CityEntitie VALUES('Ashgabat','37.95','58.3833','Turkmenistan')

INSERT INTO CityEntitie VALUES('Bremen','53.08','8.8','Germany')

INSERT INTO CityEntitie VALUES('As Sulaymaniyah','35.5613','45.4309','Iraq')

INSERT INTO CityEntitie VALUES('Zagreb','45.8','16','Croatia')

INSERT INTO CityEntitie VALUES('Hannover','52.367','9.7167','Germany')

INSERT INTO CityEntitie VALUES('C?cuta','7.92','-72.52','Colombia')

INSERT INTO CityEntitie VALUES('Hamilton','43.25','-79.83','Canada')

INSERT INTO CityEntitie VALUES('Moshi','-3.3396','37.34','Tanzania')

INSERT INTO CityEntitie VALUES('Shaoguan','24.8','113.58','China')

INSERT INTO CityEntitie VALUES('Kumamoto','32.8009','130.7006','Japan')

INSERT INTO CityEntitie VALUES('Dayton','39.7797','-84.1998','United States')

INSERT INTO CityEntitie VALUES('Lianyungang','34.6004','119.17','China')

INSERT INTO CityEntitie VALUES('Acapulco','16.85','-99.916','Mexico')

INSERT INTO CityEntitie VALUES('Kandahar','31.61','65.6949','Afghanistan')

INSERT INTO CityEntitie VALUES('Dehra Dun','30.3204','78.05','India')

INSERT INTO CityEntitie VALUES('Rochester','43.168','-77.6162','United States')

INSERT INTO CityEntitie VALUES('Jeonju','35.8314','127.1404','Korea')

INSERT INTO CityEntitie VALUES('Samarqand','39.67','66.945','Uzbekistan')

INSERT INTO CityEntitie VALUES('Qingyuan','23.7004','113.0301','China')

INSERT INTO CityEntitie VALUES('Sarasota','27.3391','-82.5439','United States')

INSERT INTO CityEntitie VALUES('Changzhi','36.1839','113.1053','China')

INSERT INTO CityEntitie VALUES('Tolyatti','53.4804','49.53','Russia')

INSERT INTO CityEntitie VALUES('Jaboatao','-8.11','-35.02','Brazil')

INSERT INTO CityEntitie VALUES('Shizuoka','34.9858','138.3854','Japan')

INSERT INTO CityEntitie VALUES('Bulawayo','-20.17','28.58','Zimbabwe')

INSERT INTO CityEntitie VALUES('Soledad','10.92','-74.77','Colombia')

INSERT INTO CityEntitie VALUES('Fresno','36.7834','-119.7941','United States')

INSERT INTO CityEntitie VALUES('Meknes','33.9004','-5.56','Morocco')

INSERT INTO CityEntitie VALUES('Sarajevo','43.85','18.383','Bosnia And Herzegovina')

INSERT INTO CityEntitie VALUES('La Plata','-34.9096','-57.96','Argentina')

INSERT INTO CityEntitie VALUES('Malegaon','20.5604','74.525','India')

INSERT INTO CityEntitie VALUES('Enugu','6.45','7.5','Nigeria')

INSERT INTO CityEntitie VALUES('Chi?in?u','47.005','28.8577','Moldova')

INSERT INTO CityEntitie VALUES('Huangshi','30.22','115.1','China')

INSERT INTO CityEntitie VALUES('Aracaju','-10.9','-37.12','Brazil')

INSERT INTO CityEntitie VALUES('Allentown','40.5961','-75.4756','United States')

INSERT INTO CityEntitie VALUES('Bonn','50.7205','7.08','Germany')

INSERT INTO CityEntitie VALUES('San Pedro Sula','15.5','-88.03','Honduras')

INSERT INTO CityEntitie VALUES('Nellore','14.44','79.9899','India')

INSERT INTO CityEntitie VALUES('Catania','37.5','15.08','Italy')

INSERT INTO CityEntitie VALUES('Gorakhpur','26.7504','83.38','India')

INSERT INTO CityEntitie VALUES('Ipoh','4.6','101.065','Malaysia')

INSERT INTO CityEntitie VALUES('Chongjin','41.7846','129.79','Korea')

INSERT INTO CityEntitie VALUES('Tulsa','36.1284','-95.9042','United States')

INSERT INTO CityEntitie VALUES('Utsunomiya','36.55','139.87','Japan')

INSERT INTO CityEntitie VALUES('Puyang','35.7004','114.98','China')

INSERT INTO CityEntitie VALUES('An Najaf','32.0003','44.3354','Iraq')

INSERT INTO CityEntitie VALUES('S?o José dos Pinhais','-25.57','-49.18','Brazil')

INSERT INTO CityEntitie VALUES('Santo André','-23.6528','-46.5278','Brazil')

INSERT INTO CityEntitie VALUES('Bytom','50.35','18.91','Poland')

INSERT INTO CityEntitie VALUES('Pointe-Noire','-4.77','11.88','Congo (Brazzaville)')

INSERT INTO CityEntitie VALUES('At Taif','21.2622','40.3823','Saudi Arabia')

INSERT INTO CityEntitie VALUES('Ismaïlia','30.5903','32.26','Egypt')

INSERT INTO CityEntitie VALUES('Concord','37.9722','-122.0016','United States')

INSERT INTO CityEntitie VALUES('Shimoga','13.9304','75.56','India')

INSERT INTO CityEntitie VALUES('Biên H?a','10.97','106.8301','Vietnam')

INSERT INTO CityEntitie VALUES('Zhanyi','25.6005','103.8166','China')

INSERT INTO CityEntitie VALUES('Kryvyy Rih','47.9283','33.345','Ukraine')

INSERT INTO CityEntitie VALUES('Andijon','40.79','72.34','Uzbekistan')

INSERT INTO CityEntitie VALUES('Tiruppur','11.0804','77.33','India')

INSERT INTO CityEntitie VALUES('Irbid','32.55','35.85','Jordan')

INSERT INTO CityEntitie VALUES('Krasnodar','45.02','39','Russia')

INSERT INTO CityEntitie VALUES('Zaragoza','41.65','-0.89','Spain')

INSERT INTO CityEntitie VALUES('Genoa','44.41','8.93','Italy')

INSERT INTO CityEntitie VALUES('Lilongwe','-13.9833','33.7833','Malawi')

INSERT INTO CityEntitie VALUES('Diyarbak?r','37.9204','40.23','Turkey')

INSERT INTO CityEntitie VALUES('Morelia','19.7334','-101.1895','Mexico')

INSERT INTO CityEntitie VALUES('Ulyanovsk','54.33','48.41','Russia')

INSERT INTO CityEntitie VALUES('Utrecht','52.1003','5.12','Netherlands')

INSERT INTO CityEntitie VALUES('Kikwit','-5.03','18.85','Congo (Kinshasa)')

INSERT INTO CityEntitie VALUES('Al Hufuf','25.3487','49.5856','Saudi Arabia')

INSERT INTO CityEntitie VALUES('Yogyakarta','-7.78','110.375','Indonesia')

INSERT INTO CityEntitie VALUES('Wroc?aw','51.1104','17.03','Poland')

INSERT INTO CityEntitie VALUES('Winnipeg','49.883','-97.166','Canada')

INSERT INTO CityEntitie VALUES('Izhevsk','56.85','53.23','Russia')

INSERT INTO CityEntitie VALUES('Cape Coral','26.6444','-81.9956','United States')

INSERT INTO CityEntitie VALUES('Springfield','42.1155','-72.5395','United States')

INSERT INTO CityEntitie VALUES('Zhuozhou','39.5401','115.79','China')

INSERT INTO CityEntitie VALUES('Raurkela','22.2304','84.83','India')

INSERT INTO CityEntitie VALUES('Québec','46.84','-71.2456','Canada')

INSERT INTO CityEntitie VALUES('Pozna?','52.4058','16.8999','Poland')

INSERT INTO CityEntitie VALUES('Colorado Springs','38.8674','-104.7606','United States')

INSERT INTO CityEntitie VALUES('Bur Said','31.26','32.29','Egypt')

INSERT INTO CityEntitie VALUES('Nanded','19.17','77.3','India')

INSERT INTO CityEntitie VALUES('Bannu','32.989','70.5986','Pakistan')

INSERT INTO CityEntitie VALUES('Asmara','15.3333','38.9333','Eritrea')

INSERT INTO CityEntitie VALUES('Southend-on-Sea','51.55','0.72','United Kingdom')

INSERT INTO CityEntitie VALUES('Dresden','51.05','13.75','Germany')

INSERT INTO CityEntitie VALUES('Wiesbaden','50.0804','8.25','Germany')

INSERT INTO CityEntitie VALUES('Charleston','32.8137','-79.9643','United States')

INSERT INTO CityEntitie VALUES('Changping','40.2248','116.1944','China')

INSERT INTO CityEntitie VALUES('Palu','-0.907','119.833','Indonesia')

INSERT INTO CityEntitie VALUES('Taizhou','32.4904','119.9','China')

INSERT INTO CityEntitie VALUES('Xiangtai','37.05','114.5','China')

INSERT INTO CityEntitie VALUES('Samsun','41.28','36.3437','Turkey')

INSERT INTO CityEntitie VALUES('Luxor','25.7','32.65','Egypt')

INSERT INTO CityEntitie VALUES('Belgaum','15.865','74.505','India')

INSERT INTO CityEntitie VALUES('Pontianak','-0.03','109.32','Indonesia')

INSERT INTO CityEntitie VALUES('Yaroslavl','57.62','39.87','Russia')

INSERT INTO CityEntitie VALUES('Constantine','36.36','6.5999','Algeria')

INSERT INTO CityEntitie VALUES('Bandjarmasin','-3.33','114.5801','Indonesia')

INSERT INTO CityEntitie VALUES('Abu Dhabi','24.4667','54.3666','United Arab Emirates')

INSERT INTO CityEntitie VALUES('Grand Rapids','42.9615','-85.6557','United States')

INSERT INTO CityEntitie VALUES('Kirkuk','35.4722','44.3923','Iraq')

INSERT INTO CityEntitie VALUES('Sangli','16.8604','74.575','India')

INSERT INTO CityEntitie VALUES('Barcelona','10.1304','-64.72','Venezuela')

INSERT INTO CityEntitie VALUES('Mission Viejo','33.6095','-117.6551','United States')

INSERT INTO CityEntitie VALUES('Canoas','-29.92','-51.18','Brazil')

INSERT INTO CityEntitie VALUES('El Mansura','31.0504','31.38','Egypt')

INSERT INTO CityEntitie VALUES('Sohag','26.5504','31.7','Egypt')

INSERT INTO CityEntitie VALUES('Barnaul','53.355','83.745','Russia')

INSERT INTO CityEntitie VALUES('Zahedan','29.5','60.83','Iran')

INSERT INTO CityEntitie VALUES('Jalalabad','34.4415','70.4361','Afghanistan')

INSERT INTO CityEntitie VALUES('Albany','42.6664','-73.7987','United States')

INSERT INTO CityEntitie VALUES('Chiclayo','-6.7629','-79.8366','Peru')

INSERT INTO CityEntitie VALUES('Hermosillo','29.0989','-110.9541','Mexico')

INSERT INTO CityEntitie VALUES('Port Louis','-20.1666','57.5','Mauritius')

INSERT INTO CityEntitie VALUES('Chandrapur','19.97','79.3','India')

INSERT INTO CityEntitie VALUES('Al Hillah','23.4895','46.7564','Saudi Arabia')

INSERT INTO CityEntitie VALUES('Al Hillah','32.4721','44.4217','Iraq')

INSERT INTO CityEntitie VALUES('Rasht','37.3','49.63','Iran')

INSERT INTO CityEntitie VALUES('Nagano','36.65','138.17','Japan')

INSERT INTO CityEntitie VALUES('Vinh','18.7','105.68','Vietnam')

INSERT INTO CityEntitie VALUES('Abeokuta','7.1604','3.35','Nigeria')

INSERT INTO CityEntitie VALUES('Kayseri','38.735','35.49','Turkey')

INSERT INTO CityEntitie VALUES('Samarinda','-0.5','117.15','Indonesia')

INSERT INTO CityEntitie VALUES('Ajmer','26.45','74.64','India')

INSERT INTO CityEntitie VALUES('Dortmund','51.53','7.45','Germany')

INSERT INTO CityEntitie VALUES('Vladivostok','43.13','131.91','Russia')

INSERT INTO CityEntitie VALUES('Irkutsk','52.32','104.245','Russia')

INSERT INTO CityEntitie VALUES('Knoxville','35.9692','-83.9495','United States')

INSERT INTO CityEntitie VALUES('Blantyre','-15.79','34.9899','Malawi')

INSERT INTO CityEntitie VALUES('Baton Rouge','30.4423','-91.1314','United States')

INSERT INTO CityEntitie VALUES('Anqing','30.5','117.05','China')

INSERT INTO CityEntitie VALUES('Cuttack','20.47','85.8899','India')

INSERT INTO CityEntitie VALUES('Hachi?ji','35.6577','139.3261','Japan')

INSERT INTO CityEntitie VALUES('Khabarovsk','48.455','135.12','Russia')

INSERT INTO CityEntitie VALUES('Veracruz','19.1773','-96.16','Mexico')

INSERT INTO CityEntitie VALUES('Kisangani','0.52','25.22','Congo (Kinshasa)')

INSERT INTO CityEntitie VALUES('Libreville','0.3854','9.458','Gabon')

INSERT INTO CityEntitie VALUES('Kerman','30.3','57.08','Iran')

INSERT INTO CityEntitie VALUES('Urmia','37.53','45','Iran')

INSERT INTO CityEntitie VALUES('Bikaner','28.0304','73.3299','India')

INSERT INTO CityEntitie VALUES('Quetzaltenango','14.83','-91.52','Guatemala')

INSERT INTO CityEntitie VALUES('Bakersfield','35.353','-119.036','United States')

INSERT INTO CityEntitie VALUES('Ogden','41.228','-111.9677','United States')

INSERT INTO CityEntitie VALUES('Shihezi','44.3','86.0299','China')

INSERT INTO CityEntitie VALUES('Kuching','1.53','110.33','Malaysia')

INSERT INTO CityEntitie VALUES('Shuozhou','39.3004','112.42','China')

INSERT INTO CityEntitie VALUES('Niigata','37.92','139.04','Japan')

INSERT INTO CityEntitie VALUES('Pereira','4.8104','-75.68','Colombia')

INSERT INTO CityEntitie VALUES('Macau','22.203','113.545','Macau')

INSERT INTO CityEntitie VALUES('New Haven','41.3112','-72.9245','United States')

INSERT INTO CityEntitie VALUES('Bouaké','7.69','-5.03','Côte DIvoire')

INSERT INTO CityEntitie VALUES('Columbia','34.037','-80.9042','United States')

INSERT INTO CityEntitie VALUES('Akron','41.0802','-81.5219','United States')

INSERT INTO CityEntitie VALUES('Binjai','3.6204','98.5001','Indonesia')

INSERT INTO CityEntitie VALUES('Manama','26.2361','50.5831','Bahrain')

INSERT INTO CityEntitie VALUES('Uberlândia','-18.9','-48.28','Brazil')

INSERT INTO CityEntitie VALUES('Sorocaba','-23.49','-47.47','Brazil')

INSERT INTO CityEntitie VALUES('Tongling','30.9504','117.78','China')

INSERT INTO CityEntitie VALUES('Weihai','37.5','122.1','China')

INSERT INTO CityEntitie VALUES('Mar del Plata','-38','-57.58','Argentina')

INSERT INTO CityEntitie VALUES('Santiago de Cuba','20.025','-75.8213','Cuba')

INSERT INTO CityEntitie VALUES('Siping','43.17','124.33','China')

INSERT INTO CityEntitie VALUES('Kagoshima','31.586','130.5611','Japan')

INSERT INTO CityEntitie VALUES('Surakarta','-7.565','110.825','Indonesia')

INSERT INTO CityEntitie VALUES('Makhachkala','42.98','47.5','Russia')

INSERT INTO CityEntitie VALUES('Bhavnagar','21.7784','72.13','India')

INSERT INTO CityEntitie VALUES('Uyo','5.008','7.85','Nigeria')

INSERT INTO CityEntitie VALUES('Bristol','51.45','-2.5833','United Kingdom')

INSERT INTO CityEntitie VALUES('Bahawalpur','29.39','71.675','Pakistan')

INSERT INTO CityEntitie VALUES('Kenitra','34.2704','-6.58','Morocco')

INSERT INTO CityEntitie VALUES('Ribeir?o Preto','-21.17','-47.83','Brazil')

INSERT INTO CityEntitie VALUES('Kanazawa','36.56','136.64','Japan')

INSERT INTO CityEntitie VALUES('Orenburg','51.78','55.11','Russia')

INSERT INTO CityEntitie VALUES('M?laga','36.7204','-4.42','Spain')

INSERT INTO CityEntitie VALUES('Tabuk','28.3838','36.555','Saudi Arabia')

INSERT INTO CityEntitie VALUES('Puerto la Cruz','10.17','-64.68','Venezuela')

INSERT INTO CityEntitie VALUES('Jiujiang','29.73','115.98','China')

INSERT INTO CityEntitie VALUES('Hisar','29.17','75.725','India')

INSERT INTO CityEntitie VALUES('Kashgar','39.4763','75.9699','China')

INSERT INTO CityEntitie VALUES('Matola','-25.9696','32.46','Mozambique')

INSERT INTO CityEntitie VALUES('Bilaspur','22.0904','82.16','India')

INSERT INTO CityEntitie VALUES('Sargodha','32.0854','72.675','Pakistan')

INSERT INTO CityEntitie VALUES('Leipzig','51.3354','12.41','Germany')

INSERT INTO CityEntitie VALUES('Vilnius','54.6834','25.3166','Lithuania')

INSERT INTO CityEntitie VALUES('Tirunelveli','8.7304','77.69','India')

INSERT INTO CityEntitie VALUES('Canc?n','21.17','-86.83','Mexico')

INSERT INTO CityEntitie VALUES('Yangzhou','32.4','119.43','China')

INSERT INTO CityEntitie VALUES('Novokuznetsk','53.75','87.115','Russia')

INSERT INTO CityEntitie VALUES('Al Ladhiqiyah','35.54','35.78','Syria')

INSERT INTO CityEntitie VALUES('Matamoros','25.88','-97.5','Mexico')

INSERT INTO CityEntitie VALUES('G?teborg','57.75','12','Sweden')

INSERT INTO CityEntitie VALUES('?tsu','35.0064','135.8674','Japan')

INSERT INTO CityEntitie VALUES('Tomsk','56.495','84.975','Russia')

INSERT INTO CityEntitie VALUES('Linxia','35.6','103.2','China')

INSERT INTO CityEntitie VALUES('Matsuyama','33.8455','132.7658','Japan')

INSERT INTO CityEntitie VALUES('Rouen','49.4304','1.08','France')

INSERT INTO CityEntitie VALUES('Jiangmen','22.5804','113.08','China')

INSERT INTO CityEntitie VALUES('Oaxaca','17.0827','-96.6699','Mexico')

INSERT INTO CityEntitie VALUES('Beira','-19.82','34.87','Mozambique')

INSERT INTO CityEntitie VALUES('Guntur','16.33','80.45','India')

INSERT INTO CityEntitie VALUES('Trablous','34.42','35.87','Lebanon')

INSERT INTO CityEntitie VALUES('Hamadan','34.796','48.515','Iran')

INSERT INTO CityEntitie VALUES('Cangzhou','38.3204','116.87','China')

INSERT INTO CityEntitie VALUES('Kota Kinabalu','5.98','116.11','Malaysia')

INSERT INTO CityEntitie VALUES('Gold Coast','-28.0815','153.4482','Australia')

INSERT INTO CityEntitie VALUES('Jian','27.1304','115','China')

INSERT INTO CityEntitie VALUES('Londrina','-23.3','-51.18','Brazil')

INSERT INTO CityEntitie VALUES('Ryazan','54.62','39.72','Russia')

INSERT INTO CityEntitie VALUES('Shashi','30.32','112.23','China')

INSERT INTO CityEntitie VALUES('Bello','6.33','-75.57','Colombia')

INSERT INTO CityEntitie VALUES('Tyumen','57.14','65.53','Russia')

INSERT INTO CityEntitie VALUES('Lipetsk','52.62','39.64','Russia')

INSERT INTO CityEntitie VALUES('Siliguri','26.7204','88.455','India')

INSERT INTO CityEntitie VALUES('Eski?ehir','39.795','30.53','Turkey')

INSERT INTO CityEntitie VALUES('Banda Aceh','5.55','95.32','Indonesia')

INSERT INTO CityEntitie VALUES('Ujjain','23.1904','75.79','India')

INSERT INTO CityEntitie VALUES('Salta','-24.7834','-65.4166','Argentina')

INSERT INTO CityEntitie VALUES('Penza','53.18','45','Russia')

INSERT INTO CityEntitie VALUES('Blida','36.4203','2.83','Algeria')

INSERT INTO CityEntitie VALUES('Mykolayiv','46.9677','31.9843','Ukraine')

INSERT INTO CityEntitie VALUES('Karbala','32.6149','44.0245','Iraq')

INSERT INTO CityEntitie VALUES('Suez','30.005','32.5499','Egypt')

INSERT INTO CityEntitie VALUES('Gliwice','50.3304','18.67','Poland')

INSERT INTO CityEntitie VALUES('Bukittinggi','-0.3031','100.3615','Indonesia')

INSERT INTO CityEntitie VALUES('Liaoyuan','42.9','125.13','China')

INSERT INTO CityEntitie VALUES('Kota Baharu','6.12','102.23','Malaysia')

INSERT INTO CityEntitie VALUES('Jundia?','-23.2','-46.88','Brazil')

INSERT INTO CityEntitie VALUES('Edinburgh','55.9483','-3.2191','United Kingdom')

INSERT INTO CityEntitie VALUES('Tlaxcala','19.32','-98.23','Mexico')

INSERT INTO CityEntitie VALUES('Provo','40.2458','-111.6457','United States')

INSERT INTO CityEntitie VALUES('Arak','34.0804','49.7','Iran')

INSERT INTO CityEntitie VALUES('Davangere','14.47','75.92','India')

INSERT INTO CityEntitie VALUES('Vi?a del Mar','-33.03','-71.54','Chile')

INSERT INTO CityEntitie VALUES('Pingtung','22.6817','120.4817','Taiwan')

INSERT INTO CityEntitie VALUES('Annaba','36.92','7.76','Algeria')

INSERT INTO CityEntitie VALUES('Akola','20.71','77.01','India')

INSERT INTO CityEntitie VALUES('Brighton','50.8303','-0.17','United Kingdom')

INSERT INTO CityEntitie VALUES('Astrakhan','46.3487','48.055','Russia')

INSERT INTO CityEntitie VALUES('Bradford','53.8','-1.75','United Kingdom')

INSERT INTO CityEntitie VALUES('Bari','41.1142','16.8728','Italy')

INSERT INTO CityEntitie VALUES('Mazatl?n','29.0171','-110.1333','Mexico')

INSERT INTO CityEntitie VALUES('Awka','6.2104','7.07','Nigeria')

INSERT INTO CityEntitie VALUES('San Lorenzo','-25.34','-57.52','Paraguay')

INSERT INTO CityEntitie VALUES('Taoyuan','24.9889','121.3111','Taiwan')

INSERT INTO CityEntitie VALUES('Chiayi','23.4755','120.4351','Taiwan')

INSERT INTO CityEntitie VALUES('Keelung','25.1333','121.7333','Taiwan')

INSERT INTO CityEntitie VALUES('Th?i Nguyên','21.6','105.83','Vietnam')

INSERT INTO CityEntitie VALUES('Shuangyashan','46.6704','131.35','China')

INSERT INTO CityEntitie VALUES('El Minya','28.09','30.75','Egypt')

INSERT INTO CityEntitie VALUES('Damanhûr','31.0504','30.47','Egypt')

INSERT INTO CityEntitie VALUES('Pasuruan','-7.6296','112.9','Indonesia')

INSERT INTO CityEntitie VALUES('Tsu','34.7171','136.5167','Japan')

INSERT INTO CityEntitie VALUES('Mataram','-8.5795','116.135','Indonesia')

INSERT INTO CityEntitie VALUES('Macap?','0.033','-51.05','Brazil')

INSERT INTO CityEntitie VALUES('Worcester','42.2705','-71.8079','United States')

INSERT INTO CityEntitie VALUES('Reynosa','26.08','-98.3','Mexico')

INSERT INTO CityEntitie VALUES('Shahrisabz','39.0618','66.8315','Uzbekistan')

INSERT INTO CityEntitie VALUES('Mesa','33.4017','-111.7181','United States')

INSERT INTO CityEntitie VALUES('Douma','33.5833','36.4','Syria')

INSERT INTO CityEntitie VALUES('Skopje','42','21.4335','Macedonia')

INSERT INTO CityEntitie VALUES('Mwanza','-2.52','32.93','Tanzania')

INSERT INTO CityEntitie VALUES('Wuwei','37.928','102.641','China')

INSERT INTO CityEntitie VALUES('Palm Bay','27.9861','-80.6628','United States')

INSERT INTO CityEntitie VALUES('Port Sudan','19.6158','37.2164','Sudan')

INSERT INTO CityEntitie VALUES('Santa Fe','-31.6239','-60.69','Argentina')

INSERT INTO CityEntitie VALUES('Tula','54.2','37.6299','Russia')

INSERT INTO CityEntitie VALUES('Beni Suef','29.0804','31.09','Egypt')

INSERT INTO CityEntitie VALUES('Yanji','42.8823','129.5128','China')

INSERT INTO CityEntitie VALUES('Toledo','41.6639','-83.5822','United States')

INSERT INTO CityEntitie VALUES('Bologna','44.5004','11.34','Italy')

INSERT INTO CityEntitie VALUES('Saharanpur','29.97','77.55','India')

INSERT INTO CityEntitie VALUES('Murrieta','33.5719','-117.1909','United States')

INSERT INTO CityEntitie VALUES('Gulbarga','17.35','76.82','India')

INSERT INTO CityEntitie VALUES('Bhatpara','22.8504','88.52','India')

INSERT INTO CityEntitie VALUES('Wichita','37.6897','-97.3442','United States')

INSERT INTO CityEntitie VALUES('Ife','7.4804','4.56','Nigeria')

INSERT INTO CityEntitie VALUES('Feira de Santana','-12.25','-38.97','Brazil')

INSERT INTO CityEntitie VALUES('Shah Alam','3.0667','101.55','Malaysia')

INSERT INTO CityEntitie VALUES('Mariupol','47.0962','37.5562','Ukraine')

INSERT INTO CityEntitie VALUES('Des Moines','41.5725','-93.6104','United States')

INSERT INTO CityEntitie VALUES('Tuxtla Gutiérrez','16.75','-93.15','Mexico')

INSERT INTO CityEntitie VALUES('Herat','34.33','62.17','Afghanistan')

INSERT INTO CityEntitie VALUES('Homyel','52.43','31','Belarus')

INSERT INTO CityEntitie VALUES('Zhaoqing','23.0504','112.45','China')

INSERT INTO CityEntitie VALUES('Americana','-22.7499','-47.33','Brazil')

INSERT INTO CityEntitie VALUES('Dhule','20.9','74.77','India')

INSERT INTO CityEntitie VALUES('Ostrava','49.8304','18.25','Czechia')

INSERT INTO CityEntitie VALUES('Yazd','31.9201','54.37','Iran')

INSERT INTO CityEntitie VALUES('Sialkote','32.52','74.56','Pakistan')

INSERT INTO CityEntitie VALUES('Kemerovo','55.34','86.09','Russia')

INSERT INTO CityEntitie VALUES('Nazret','8.55','39.27','Ethiopia')

INSERT INTO CityEntitie VALUES('Staten Island','40.5834','-74.1496','United States')

INSERT INTO CityEntitie VALUES('Jiaojing','28.6804','121.45','China')

INSERT INTO CityEntitie VALUES('Chaoyang','41.5504','120.42','China')

INSERT INTO CityEntitie VALUES('Juiz de Fora','-21.77','-43.375','Brazil')

INSERT INTO CityEntitie VALUES('Udaipur','24.6','73.73','India')

INSERT INTO CityEntitie VALUES('Long Beach','33.8059','-118.161','United States')

INSERT INTO CityEntitie VALUES('Greenville','34.8363','-82.365','United States')

INSERT INTO CityEntitie VALUES('?zmit','40.776','29.9306','Turkey')

INSERT INTO CityEntitie VALUES('Piraeus','37.95','23.7','Greece')

INSERT INTO CityEntitie VALUES('Shymkent','42.32','69.595','Kazakhstan')

INSERT INTO CityEntitie VALUES('Iligan','8.1712','124.2154','Philippines')

INSERT INTO CityEntitie VALUES('Qazvin','36.27','50','Iran')

INSERT INTO CityEntitie VALUES('Bloemfontein','-29.12','26.2299','South Africa')

INSERT INTO CityEntitie VALUES('Calabar','4.9604','8.33','Nigeria')

INSERT INTO CityEntitie VALUES('Malatya','38.3704','38.3','Turkey')

INSERT INTO CityEntitie VALUES('Panzhihua','26.55','101.73','China')

INSERT INTO CityEntitie VALUES('Bandar-e-Abbas','27.2041','56.2721','Iran')

INSERT INTO CityEntitie VALUES('Naberezhnyye Chelny','55.7','52.3199','Russia')

INSERT INTO CityEntitie VALUES('Hamah','35.1503','36.73','Syria')

INSERT INTO CityEntitie VALUES('Cranbourne','-38.0996','145.2834','Australia')

INSERT INTO CityEntitie VALUES('Iquitos','-3.75','-73.25','Peru')

INSERT INTO CityEntitie VALUES('Mazar-e Sharif','36.7','67.1','Afghanistan')

INSERT INTO CityEntitie VALUES('Leicester','52.63','-1.1332','United Kingdom')

INSERT INTO CityEntitie VALUES('Kirov','58.5901','49.67','Russia')

INSERT INTO CityEntitie VALUES('Jingdezhen','29.2704','117.18','China')

INSERT INTO CityEntitie VALUES('Durango','24.0311','-104.67','Mexico')

INSERT INTO CityEntitie VALUES('Jambi','-1.59','103.61','Indonesia')

INSERT INTO CityEntitie VALUES('Volta Redonda','-22.5196','-44.095','Brazil')

INSERT INTO CityEntitie VALUES('Hengshui','37.72','115.7','China')

INSERT INTO CityEntitie VALUES('Sfax','34.75','10.72','Tunisia')

INSERT INTO CityEntitie VALUES('Sunderland','54.92','-1.38','United Kingdom')

INSERT INTO CityEntitie VALUES('Xalapa','19.53','-96.92','Mexico')

INSERT INTO CityEntitie VALUES('Luhansk','48.5698','39.3344','Ukraine')

INSERT INTO CityEntitie VALUES('Manado','1.48','124.85','Indonesia')

INSERT INTO CityEntitie VALUES('Qaraghandy','49.885','73.115','Kazakhstan')

INSERT INTO CityEntitie VALUES('An Nasiriyah','31.0429','46.2676','Iraq')

INSERT INTO CityEntitie VALUES('Oshawa','43.88','-78.85','Canada')

INSERT INTO CityEntitie VALUES('Qitaihe','45.8','130.85','China')

INSERT INTO CityEntitie VALUES('Belfast','54.6','-5.96','United Kingdom')

INSERT INTO CityEntitie VALUES('?anl?urfa','37.17','38.795','Turkey')

INSERT INTO CityEntitie VALUES('Chengde','40.9604','117.93','China')

INSERT INTO CityEntitie VALUES('Xuchang','34.0204','113.82','China')

INSERT INTO CityEntitie VALUES('Chlef','36.1704','1.32','Algeria')

INSERT INTO CityEntitie VALUES('?ita','33.2432','131.5979','Japan')

INSERT INTO CityEntitie VALUES('Baguio City','16.43','120.5699','Philippines')

INSERT INTO CityEntitie VALUES('San Juan','-31.55','-68.52','Argentina')

INSERT INTO CityEntitie VALUES('Cheboksary','56.13','47.25','Russia')

INSERT INTO CityEntitie VALUES('Ado Ekiti','7.6304','5.22','Nigeria')

INSERT INTO CityEntitie VALUES('Balikpapan','-1.25','116.83','Indonesia')

INSERT INTO CityEntitie VALUES('Bellary','15.15','76.915','India')

INSERT INTO CityEntitie VALUES('Bamenda','5.96','10.15','Cameroon')

INSERT INTO CityEntitie VALUES('Gent','51.03','3.7','Belgium')

INSERT INTO CityEntitie VALUES('Tokushima','34.0674','134.5525','Japan')

INSERT INTO CityEntitie VALUES('Little Rock','34.7255','-92.3581','United States')

INSERT INTO CityEntitie VALUES('Wuzhou','23.48','111.32','China')

INSERT INTO CityEntitie VALUES('Portsmouth','50.8003','-1.08','United Kingdom')

INSERT INTO CityEntitie VALUES('Harrisburg','40.2752','-76.8843','United States')

INSERT INTO CityEntitie VALUES('Cabimas','10.43','-71.45','Venezuela')

INSERT INTO CityEntitie VALUES('Foz do Iguaçu','-25.5235','-54.53','Brazil')

INSERT INTO CityEntitie VALUES('Denton','33.2176','-97.1421','United States')

INSERT INTO CityEntitie VALUES('Wakayama','34.2231','135.1677','Japan')

INSERT INTO CityEntitie VALUES('Strasbourg','48.58','7.75','France')

INSERT INTO CityEntitie VALUES('Madison','43.0808','-89.3922','United States')

INSERT INTO CityEntitie VALUES('Mawlamyine','16.5004','97.67','Burma')

INSERT INTO CityEntitie VALUES('San Crist?bal','7.77','-72.25','Venezuela')

INSERT INTO CityEntitie VALUES('Nantes','47.2104','-1.59','France')

INSERT INTO CityEntitie VALUES('Khujand','40.29','69.6199','Tajikistan')

INSERT INTO CityEntitie VALUES('Guangyuan','32.43','105.87','China')

INSERT INTO CityEntitie VALUES('Khomeini Shahr','32.7004','51.47','Iran')

INSERT INTO CityEntitie VALUES('Garoua','9.3','13.39','Cameroon')

INSERT INTO CityEntitie VALUES('Bukavu','-2.51','28.84','Congo (Kinshasa)')

INSERT INTO CityEntitie VALUES('Tuticorin','8.82','78.13','India')

INSERT INTO CityEntitie VALUES('Nagasaki','32.765','129.885','Japan')

INSERT INTO CityEntitie VALUES('Pohang','36.0209','129.3715','Korea')

INSERT INTO CityEntitie VALUES('Kaliningrad','54.7','20.4973','Russia')

INSERT INTO CityEntitie VALUES('Likasi','-10.97','26.78','Congo (Kinshasa)')

INSERT INTO CityEntitie VALUES('Reno','39.5497','-119.8483','United States')

INSERT INTO CityEntitie VALUES('Spanish Town','17.9833','-76.95','Jamaica')

INSERT INTO CityEntitie VALUES('Port Saint Lucie','27.2796','-80.3883','United States')

INSERT INTO CityEntitie VALUES('San Luis','-33.3','-66.35','Argentina')

INSERT INTO CityEntitie VALUES('Katsina','12.9904','7.6','Nigeria')

INSERT INTO CityEntitie VALUES('Welkom','-27.97','26.73','South Africa')

INSERT INTO CityEntitie VALUES('Santa Marta','11.2472','-74.2017','Colombia')

INSERT INTO CityEntitie VALUES('Villahermosa','18','-92.9','Mexico')

INSERT INTO CityEntitie VALUES('Bryansk','53.26','34.43','Russia')

INSERT INTO CityEntitie VALUES('Bournemouth','50.73','-1.9','United Kingdom')

INSERT INTO CityEntitie VALUES('Bengkulu','-3.8','102.27','Indonesia')

INSERT INTO CityEntitie VALUES('Heidelberg','49.42','8.7','Germany')

INSERT INTO CityEntitie VALUES('Oakland','37.7903','-122.2165','United States')

INSERT INTO CityEntitie VALUES('Kurnool','15.83','78.03','India')

INSERT INTO CityEntitie VALUES('Chaozhou','23.68','116.63','China')

INSERT INTO CityEntitie VALUES('Batangas','13.7817','121.0217','Philippines')

INSERT INTO CityEntitie VALUES('Bratislava','48.15','17.117','Slovakia')

INSERT INTO CityEntitie VALUES('Gaya','24.8','85','India')

INSERT INTO CityEntitie VALUES('Ibagué','4.4389','-75.2322','Colombia')

INSERT INTO CityEntitie VALUES('Ivanovo','57.01','41.01','Russia')

INSERT INTO CityEntitie VALUES('Erzurum','39.9204','41.29','Turkey')

INSERT INTO CityEntitie VALUES('Akure','7.2504','5.2','Nigeria')

INSERT INTO CityEntitie VALUES('Asyut','27.19','31.1799','Egypt')

INSERT INTO CityEntitie VALUES('Kolwezi','-10.7167','25.4724','Congo (Kinshasa)')

INSERT INTO CityEntitie VALUES('Sukkur','27.7136','68.8486','Pakistan')

INSERT INTO CityEntitie VALUES('Luohe','33.57','114.03','China')

INSERT INTO CityEntitie VALUES('Campina Grande','-7.23','-35.88','Brazil')

INSERT INTO CityEntitie VALUES('Kitchener','43.45','-80.5','Canada')

INSERT INTO CityEntitie VALUES('Winston-Salem','36.1029','-80.261','United States')

INSERT INTO CityEntitie VALUES('Middlesbrough','54.5804','-1.23','United Kingdom')

INSERT INTO CityEntitie VALUES('Meizhou','24.3005','116.12','China')

INSERT INTO CityEntitie VALUES('Ardabil','38.25','48.3','Iran')

INSERT INTO CityEntitie VALUES('Magnitogorsk','53.4227','58.98','Russia')

INSERT INTO CityEntitie VALUES('Gifu','35.4231','136.7628','Japan')

INSERT INTO CityEntitie VALUES('Huancayo','-12.08','-75.2','Peru')

INSERT INTO CityEntitie VALUES('Nha Trang','12.25','109.17','Vietnam')

INSERT INTO CityEntitie VALUES('Matur?n','9.75','-63.17','Venezuela')

INSERT INTO CityEntitie VALUES('Xuanhua','40.5944','115.0243','China')

INSERT INTO CityEntitie VALUES('Kursk','51.74','36.19','Russia')

INSERT INTO CityEntitie VALUES('Oujda','34.69','-1.91','Morocco')

INSERT INTO CityEntitie VALUES('Metz','49.1203','6.18','France')

INSERT INTO CityEntitie VALUES('Al Ayn','24.2305','55.74','United Arab Emirates')

INSERT INTO CityEntitie VALUES('Jeju','33.5101','126.5219','Korea')

INSERT INTO CityEntitie VALUES('Oshogbo','7.7704','4.56','Nigeria')

INSERT INTO CityEntitie VALUES('Indio','33.7346','-116.2346','United States')

INSERT INTO CityEntitie VALUES('Ipatinga','-19.4796','-42.52','Brazil')

INSERT INTO CityEntitie VALUES('Szczecin','53.4204','14.53','Poland')

INSERT INTO CityEntitie VALUES('Durham','35.9797','-78.9037','United States')

INSERT INTO CityEntitie VALUES('Syracuse','43.0409','-76.1438','United States')

INSERT INTO CityEntitie VALUES('Chattanooga','35.0657','-85.2488','United States')

INSERT INTO CityEntitie VALUES('Murcia','37.98','-1.13','Spain')

INSERT INTO CityEntitie VALUES('Kitwe','-12.81','28.22','Zambia')

INSERT INTO CityEntitie VALUES('Tanta','30.7904','31','Egypt')

INSERT INTO CityEntitie VALUES('Lancaster','40.0421','-76.3012','United States')

INSERT INTO CityEntitie VALUES('Zanzibar','-6.16','39.2','Tanzania')

INSERT INTO CityEntitie VALUES('Taubaté','-23.0195','-45.56','Brazil')

INSERT INTO CityEntitie VALUES('Yining','43.9','81.35','China')

INSERT INTO CityEntitie VALUES('Bissau','11.865','-15.5984','Guinea-Bissau')

INSERT INTO CityEntitie VALUES('Pasay City','14.5504','121','Philippines')

INSERT INTO CityEntitie VALUES('Spokane','47.6671','-117.433','United States')

INSERT INTO CityEntitie VALUES('Mbale','1.0904','34.17','Uganda')

INSERT INTO CityEntitie VALUES('Palm Coast','29.5389','-81.246','United States')

INSERT INTO CityEntitie VALUES('Kassala','15.46','36.39','Sudan')

INSERT INTO CityEntitie VALUES('Sunchon','39.4236','125.939','Korea')

INSERT INTO CityEntitie VALUES('Tver','56.86','35.89','Russia')

INSERT INTO CityEntitie VALUES('Surgut','61.2599','73.425','Russia')

INSERT INTO CityEntitie VALUES('Jingmen','31.0304','112.1','China')

INSERT INTO CityEntitie VALUES('Sikar','27.6104','75.14','India')

INSERT INTO CityEntitie VALUES('Tumkur','13.33','77.1','India')

INSERT INTO CityEntitie VALUES('G?mez Palacio','25.5701','-103.5','Mexico')

INSERT INTO CityEntitie VALUES('Buraydah','26.3664','43.9628','Saudi Arabia')

INSERT INTO CityEntitie VALUES('Khmelnytskyy','49.4249','27.0015','Ukraine')

INSERT INTO CityEntitie VALUES('Eindhoven','51.43','5.5','Netherlands')

INSERT INTO CityEntitie VALUES('Chiang Mai','18.8','98.98','Thailand')

INSERT INTO CityEntitie VALUES('Piura','-5.21','-80.63','Peru')

INSERT INTO CityEntitie VALUES('Horlivka','48.2996','38.0547','Ukraine')

INSERT INTO CityEntitie VALUES('Arlington','32.6998','-97.125','United States')

INSERT INTO CityEntitie VALUES('Ndola','-12.9999','28.65','Zambia')

INSERT INTO CityEntitie VALUES('Yuxi','24.38','102.57','China')

INSERT INTO CityEntitie VALUES('Bonita Springs','26.3559','-81.7861','United States')

INSERT INTO CityEntitie VALUES('Poughkeepsie','41.6949','-73.921','United States')

INSERT INTO CityEntitie VALUES('Kisumu','-0.09','34.75','Kenya')

INSERT INTO CityEntitie VALUES('Stockton','37.9766','-121.3112','United States')

INSERT INTO CityEntitie VALUES('Kollam','8.9004','76.57','India')

INSERT INTO CityEntitie VALUES('Tallinn','59.4339','24.728','Estonia')

INSERT INTO CityEntitie VALUES('Wellington','-41.3','174.7833','New Zealand')

INSERT INTO CityEntitie VALUES('El Obeid','13.1833','30.2167','Sudan')

INSERT INTO CityEntitie VALUES('Niyala','12.06','24.89','Sudan')

INSERT INTO CityEntitie VALUES('Sandakan','5.843','118.108','Malaysia')

INSERT INTO CityEntitie VALUES('Ahmednagar','19.1104','74.75','India')

INSERT INTO CityEntitie VALUES('Osh','40.5404','72.79','Kyrgyzstan')

INSERT INTO CityEntitie VALUES('Stoke','53.0004','-2.18','United Kingdom')

INSERT INTO CityEntitie VALUES('Bhilwara','25.3504','74.635','India')

INSERT INTO CityEntitie VALUES('Oxnard','34.1962','-119.1819','United States')

INSERT INTO CityEntitie VALUES('Comilla','23.4704','91.17','Bangladesh')

INSERT INTO CityEntitie VALUES('Augusta','33.3645','-82.0708','United States')

INSERT INTO CityEntitie VALUES('Scranton','41.4044','-75.6649','United States')

INSERT INTO CityEntitie VALUES('Samut Prakan','13.6069','100.6115','Thailand')

INSERT INTO CityEntitie VALUES('Grenoble','45.1804','5.72','France')

INSERT INTO CityEntitie VALUES('Nampula','-15.136','39.293','Mozambique')

INSERT INTO CityEntitie VALUES('Nizamabad','18.6704','78.1','India')

INSERT INTO CityEntitie VALUES('Granada?','37.165','-3.585','Spain')

INSERT INTO CityEntitie VALUES('Brno','49.2004','16.61','Czechia')

INSERT INTO CityEntitie VALUES('Coventry','52.4204','-1.5','United Kingdom')

INSERT INTO CityEntitie VALUES('Alajuela','10.02','-84.23','Costa Rica')

INSERT INTO CityEntitie VALUES('Iloilo','10.705','122.545','Philippines')

INSERT INTO CityEntitie VALUES('Campos','-21.75','-41.32','Brazil')

INSERT INTO CityEntitie VALUES('Resistencia','-27.46','-58.99','Argentina')

INSERT INTO CityEntitie VALUES('Baicheng','45.62','122.82','China')

INSERT INTO CityEntitie VALUES('Qarshi','38.8704','65.8','Uzbekistan')

INSERT INTO CityEntitie VALUES('Misratah','32.38','15.1','Libya')

INSERT INTO CityEntitie VALUES('Hail','27.5236','41.7001','Saudi Arabia')

INSERT INTO CityEntitie VALUES('Boise','43.6006','-116.2316','United States')

INSERT INTO CityEntitie VALUES('Béjaïa','36.7604','5.07','Algeria')

INSERT INTO CityEntitie VALUES('Southampton','50.9','-1.4','United Kingdom')

INSERT INTO CityEntitie VALUES('Celaya','20.53','-100.8','Mexico')

INSERT INTO CityEntitie VALUES('Pasto','1.2136','-77.2811','Colombia')

INSERT INTO CityEntitie VALUES('Modesto','37.6374','-121.0028','United States')

INSERT INTO CityEntitie VALUES('Caxias do Sul','-29.18','-51.17','Brazil')

INSERT INTO CityEntitie VALUES('Nizhny Tagil','57.92','59.975','Russia')

INSERT INTO CityEntitie VALUES('Xichang','27.88','102.3','China')

INSERT INTO CityEntitie VALUES('Dezhou','37.4504','116.3','China')

INSERT INTO CityEntitie VALUES('Vigo','42.22','-8.73','Spain')

INSERT INTO CityEntitie VALUES('Las Palmas','28.1','-15.43','Spain')

INSERT INTO CityEntitie VALUES('Parbhani','19.2704','76.76','India')

INSERT INTO CityEntitie VALUES('Karlsruhe','49','8.4','Germany')

INSERT INTO CityEntitie VALUES('Zhoukou','33.6304','114.63','China')

INSERT INTO CityEntitie VALUES('Makiyivka','48.0297','37.9746','Ukraine')

INSERT INTO CityEntitie VALUES('Putian','25.4303','119.02','China')

INSERT INTO CityEntitie VALUES('Kahramanmara?','37.61','36.945','Turkey')

INSERT INTO CityEntitie VALUES('Manizales','5.06','-75.52','Colombia')

INSERT INTO CityEntitie VALUES('Palma','39.5743','2.6542','Spain')

INSERT INTO CityEntitie VALUES('Manukau','-37','174.885','New Zealand')

INSERT INTO CityEntitie VALUES('Shillong','25.5705','91.88','India')

INSERT INTO CityEntitie VALUES('Khorramabad','33.4804','48.35','Iran')

INSERT INTO CityEntitie VALUES('Villavicencio','4.1533','-73.635','Colombia')

INSERT INTO CityEntitie VALUES('S?o José do Rio Preto','-20.7996','-49.39','Brazil')

INSERT INTO CityEntitie VALUES('Kaunas','54.9504','23.88','Lithuania')

INSERT INTO CityEntitie VALUES('Latur','18.4004','76.57','India')

INSERT INTO CityEntitie VALUES('Kissimmee','28.3042','-81.4164','United States')

INSERT INTO CityEntitie VALUES('Youngstown','41.0993','-80.6463','United States')

INSERT INTO CityEntitie VALUES('Shache','38.4261','77.25','China')

INSERT INTO CityEntitie VALUES('Seremban','2.7105','101.94','Malaysia')

INSERT INTO CityEntitie VALUES('Denizli','37.7704','29.08','Turkey')

INSERT INTO CityEntitie VALUES('Van','38.4954','43.4','Turkey')

INSERT INTO CityEntitie VALUES('La Coru?a','43.33','-8.42','Spain')

INSERT INTO CityEntitie VALUES('Abadan','30.3307','48.2797','Iran')

INSERT INTO CityEntitie VALUES('Quzhou','28.9704','118.87','China')

INSERT INTO CityEntitie VALUES('Rajapalaiyam','9.4204','77.58','India')

INSERT INTO CityEntitie VALUES('Reading','51.47','-0.98','United Kingdom')

INSERT INTO CityEntitie VALUES('Najran','17.5065','44.1316','Saudi Arabia')

INSERT INTO CityEntitie VALUES('Mahilyow','53.8985','30.3247','Belarus')

INSERT INTO CityEntitie VALUES('Al-Qatif','26.5196','50.0115','Saudi Arabia')

INSERT INTO CityEntitie VALUES('Valletta','35.8997','14.5147','Malta')

INSERT INTO CityEntitie VALUES('Mazatl?n','23.2211','-106.42','Mexico')

INSERT INTO CityEntitie VALUES('Longyan','25.1804','117.03','China')

INSERT INTO CityEntitie VALUES('Aurora','39.7084','-104.7274','United States')

INSERT INTO CityEntitie VALUES('Bydgoszcz','53.1204','18.01','Poland')

INSERT INTO CityEntitie VALUES('Kuantan','3.83','103.32','Malaysia')

INSERT INTO CityEntitie VALUES('Gorontalo','0.55','123.07','Indonesia')

INSERT INTO CityEntitie VALUES('Nakuru','-0.28','36.07','Kenya')

INSERT INTO CityEntitie VALUES('Vladikavkaz','43.0504','44.67','Russia')

INSERT INTO CityEntitie VALUES('Larkana','27.5618','68.2068','Pakistan')

INSERT INTO CityEntitie VALUES('Christchurch','-43.535','172.63','New Zealand')

INSERT INTO CityEntitie VALUES('Stavropol','45.05','41.98','Russia')

INSERT INTO CityEntitie VALUES('Sanya','18.2591','109.504','China')

INSERT INTO CityEntitie VALUES('Bhagalpur','25.23','86.98','India')

INSERT INTO CityEntitie VALUES('Maseru','-29.3167','27.4833','Lesotho')

INSERT INTO CityEntitie VALUES('Sheikhu Pura','31.72','73.99','Pakistan')

INSERT INTO CityEntitie VALUES('Cusco','-13.525','-71.9722','Peru')

INSERT INTO CityEntitie VALUES('Tamale','9.4004','-0.84','Ghana')

INSERT INTO CityEntitie VALUES('Ulan Ude','51.825','107.625','Russia')

INSERT INTO CityEntitie VALUES('Bobo Dioulasso','11.18','-4.29','Burkina Faso')

INSERT INTO CityEntitie VALUES('Lublin','51.2504','22.5727','Poland')

INSERT INTO CityEntitie VALUES('Halifax','44.65','-63.6','Canada')

INSERT INTO CityEntitie VALUES('Augsburg','48.35','10.9','Germany')

INSERT INTO CityEntitie VALUES('Sungai Petani','5.6497','100.4793','Malaysia')

INSERT INTO CityEntitie VALUES('Ad Diwaniyah','31.9889','44.924','Iraq')

INSERT INTO CityEntitie VALUES('Taraz','42.9','71.365','Kazakhstan')

INSERT INTO CityEntitie VALUES('Toulon','43.1342','5.9188','France')

INSERT INTO CityEntitie VALUES('Zanjan','36.67','48.5','Iran')

INSERT INTO CityEntitie VALUES('San Sebasti?n','43.3204','-1.98','Spain')

INSERT INTO CityEntitie VALUES('Iwaki','37.0553','140.89','Japan')

INSERT INTO CityEntitie VALUES('Encarnaci?n','-27.3472','-55.8739','Paraguay')

INSERT INTO CityEntitie VALUES('Posadas','-27.3578','-55.8851','Argentina')

INSERT INTO CityEntitie VALUES('Fuyu','45.1804','124.82','China')

INSERT INTO CityEntitie VALUES('Cà Mau','9.1774','105.15','Vietnam')

INSERT INTO CityEntitie VALUES('Asahikawa','43.755','142.38','Japan')

INSERT INTO CityEntitie VALUES('Mirput Khas','25.5318','69.0118','Pakistan')

INSERT INTO CityEntitie VALUES('Archangel','64.575','40.545','Russia')

INSERT INTO CityEntitie VALUES('Wafangdian','39.6259','121.996','China')

INSERT INTO CityEntitie VALUES('Ambon','-3.7167','128.2','Indonesia')

INSERT INTO CityEntitie VALUES('Orizaba','18.85','-97.13','Mexico')

INSERT INTO CityEntitie VALUES('Longxi','35.0476','104.6394','China')

INSERT INTO CityEntitie VALUES('Santiago del Estero','-27.7833','-64.2667','Argentina')

INSERT INTO CityEntitie VALUES('Mito','36.3704','140.48','Japan')

INSERT INTO CityEntitie VALUES('Safi','32.32','-9.24','Morocco')

INSERT INTO CityEntitie VALUES('Eldoret','0.52','35.27','Kenya')

INSERT INTO CityEntitie VALUES('Rahim Yar Khan','28.4202','70.2952','Pakistan')

INSERT INTO CityEntitie VALUES('Neiva','2.931','-75.3302','Colombia')

INSERT INTO CityEntitie VALUES('Anaheim','33.839','-117.8572','United States')

INSERT INTO CityEntitie VALUES('Vinnytsya','49.2254','28.4816','Ukraine')

INSERT INTO CityEntitie VALUES('Hualien','23.9837','121.6','Taiwan')

INSERT INTO CityEntitie VALUES('Kuala Terengganu','5.3304','103.12','Malaysia')

INSERT INTO CityEntitie VALUES('Qoqon','40.5404','70.94','Uzbekistan')

INSERT INTO CityEntitie VALUES('Long Xuyen','10.3804','105.42','Vietnam')

INSERT INTO CityEntitie VALUES('Vi?t Tr?','21.3304','105.43','Vietnam')

INSERT INTO CityEntitie VALUES('Yumen','39.83','97.73','China')

INSERT INTO CityEntitie VALUES('Haarlem','52.3804','4.63','Netherlands')

INSERT INTO CityEntitie VALUES('Buon Me Thuot','12.667','108.05','Vietnam')

INSERT INTO CityEntitie VALUES('Chimbote','-9.07','-78.57','Peru')

INSERT INTO CityEntitie VALUES('Muzaffarnagar','29.485','77.695','India')

INSERT INTO CityEntitie VALUES('Nuevo Laredo','27.5','-99.55','Mexico')

INSERT INTO CityEntitie VALUES('Lancaster','34.6934','-118.1753','United States')

INSERT INTO CityEntitie VALUES('Sanandaj','35.3','47.02','Iran')

INSERT INTO CityEntitie VALUES('Linz','48.3192','14.2888','Austria')

INSERT INTO CityEntitie VALUES('Camag?ey','21.3808','-77.9169','Cuba')

INSERT INTO CityEntitie VALUES('Verona','45.4404','10.99','Italy')

INSERT INTO CityEntitie VALUES('Victorville','34.5277','-117.3537','United States')

INSERT INTO CityEntitie VALUES('London','42.97','-81.25','Canada')

INSERT INTO CityEntitie VALUES('Astana','51.1811','71.4278','Kazakhstan')

INSERT INTO CityEntitie VALUES('Merida','8.4','-71.13','Venezuela')

INSERT INTO CityEntitie VALUES('Charleroi','50.4204','4.45','Belgium')

INSERT INTO CityEntitie VALUES('Belgorod','50.63','36.5999','Russia')

INSERT INTO CityEntitie VALUES('Kosti','13.17','32.66','Sudan')

INSERT INTO CityEntitie VALUES('Al Amarah','31.8416','47.1512','Iraq')

INSERT INTO CityEntitie VALUES('Maebashi','36.3927','139.0727','Japan')

INSERT INTO CityEntitie VALUES('Pensacola','30.4427','-87.1886','United States')

INSERT INTO CityEntitie VALUES('Kurgan','55.46','65.345','Russia')

INSERT INTO CityEntitie VALUES('Kohat','33.6027','71.4327','Pakistan')

INSERT INTO CityEntitie VALUES('Vitsyebsk','55.1887','30.1853','Belarus')

INSERT INTO CityEntitie VALUES('Piracicaba','-22.71','-47.64','Brazil')

INSERT INTO CityEntitie VALUES('Yeosu','34.7368','127.7458','Korea')

INSERT INTO CityEntitie VALUES('Fayetteville','36.0713','-94.166','United States')

INSERT INTO CityEntitie VALUES('Corpus Christi','27.7173','-97.3822','United States')

INSERT INTO CityEntitie VALUES('Jhang','31.2804','72.325','Pakistan')

INSERT INTO CityEntitie VALUES('Pematangsiantar','2.9614','99.0615','Indonesia')

INSERT INTO CityEntitie VALUES('Arusha','-3.36','36.67','Tanzania')

INSERT INTO CityEntitie VALUES('Corrientes','-27.49','-58.81','Argentina')

INSERT INTO CityEntitie VALUES('K?riyama','37.41','140.38','Japan')

INSERT INTO CityEntitie VALUES('Plovdiv','42.154','24.754','Bulgaria')

INSERT INTO CityEntitie VALUES('Chitungwiza','-18','31.1','Zimbabwe')

INSERT INTO CityEntitie VALUES('Aksu','41.15','80.25','China')

INSERT INTO CityEntitie VALUES('Yaan','29.9804','103.08','China')

INSERT INTO CityEntitie VALUES('Tieling','42.3004','123.82','China')

INSERT INTO CityEntitie VALUES('Irapuato','20.67','-101.5','Mexico')

INSERT INTO CityEntitie VALUES('Kaluga','54.5204','36.27','Russia')

INSERT INTO CityEntitie VALUES('East London','-32.97','27.87','South Africa')

INSERT INTO CityEntitie VALUES('Jackson','32.3163','-90.2124','United States')

INSERT INTO CityEntitie VALUES('Kaesong','37.964','126.5644','Korea')

INSERT INTO CityEntitie VALUES('Ciudad Bol?var','8.1','-63.6','Venezuela')

INSERT INTO CityEntitie VALUES('Kawagoe','35.9177','139.4911','Japan')

INSERT INTO CityEntitie VALUES('Lalitpur','27.6666','85.3333','Nepal')

INSERT INTO CityEntitie VALUES('Phan Thiet','10.9337','108.1001','Vietnam')

INSERT INTO CityEntitie VALUES('Alor Setar','6.1133','100.3729','Malaysia')

INSERT INTO CityEntitie VALUES('Santa Cruz de Tenerife','28.47','-16.25','Spain')

INSERT INTO CityEntitie VALUES('Gij?n','43.53','-5.67','Spain')

INSERT INTO CityEntitie VALUES('Greensboro','36.0956','-79.8269','United States')

INSERT INTO CityEntitie VALUES('K?chi','33.5624','133.5375','Japan')

INSERT INTO CityEntitie VALUES('Flint','43.0236','-83.6922','United States')

INSERT INTO CityEntitie VALUES('Bauru','-22.33','-49.08','Brazil')

INSERT INTO CityEntitie VALUES('Nakhon Ratchasima','15','102.1','Thailand')

INSERT INTO CityEntitie VALUES('Orel','52.97','36.07','Russia')

INSERT INTO CityEntitie VALUES('Takamatsu','34.3447','134.0448','Japan')

INSERT INTO CityEntitie VALUES('Santa Ana','33.7366','-117.8819','United States')

INSERT INTO CityEntitie VALUES('Muzaffarpur','26.1204','85.3799','India')

INSERT INTO CityEntitie VALUES('Beni','0.4904','29.45','Congo (Kinshasa)')

INSERT INTO CityEntitie VALUES('Toyama','36.7','137.23','Japan')

INSERT INTO CityEntitie VALUES('Hangu','39.232','117.777','China')

INSERT INTO CityEntitie VALUES('Medani','14.4','33.52','Sudan')

INSERT INTO CityEntitie VALUES('Montes Claros','-16.72','-43.86','Brazil')

INSERT INTO CityEntitie VALUES('Bielefeld','52.03','8.53','Germany')

INSERT INTO CityEntitie VALUES('Bujumbura','-3.3761','29.36','Burundi')

INSERT INTO CityEntitie VALUES('Heyuan','23.7304','114.68','China')

INSERT INTO CityEntitie VALUES('Mathura','27.5','77.67','India')

INSERT INTO CityEntitie VALUES('Mymensingh','24.7504','90.38','Bangladesh')

INSERT INTO CityEntitie VALUES('Baishan','41.9','126.43','China')

INSERT INTO CityEntitie VALUES('Patiala','30.3204','76.385','India')

INSERT INTO CityEntitie VALUES('Wonsan','39.1605','127.4308','Korea')

INSERT INTO CityEntitie VALUES('Pavlodar','52.3','76.95','Kazakhstan')

INSERT INTO CityEntitie VALUES('Fort Wayne','41.0885','-85.1436','United States')

INSERT INTO CityEntitie VALUES('Maring?','-23.4095','-51.93','Brazil')

INSERT INTO CityEntitie VALUES('Sagar','23.8504','78.75','India')

INSERT INTO CityEntitie VALUES('Canberra','-35.283','149.129','Australia')

INSERT INTO CityEntitie VALUES('Sochi','43.59','39.73','Russia')

INSERT INTO CityEntitie VALUES('Montpellier','43.6104','3.87','France')

INSERT INTO CityEntitie VALUES('Itaja?','-26.8996','-48.68','Brazil')

INSERT INTO CityEntitie VALUES('Sousse','35.83','10.625','Tunisia')

INSERT INTO CityEntitie VALUES('Ann Arbor','42.2755','-83.7312','United States')

INSERT INTO CityEntitie VALUES('Ia?i','47.1683','27.5749','Romania')

INSERT INTO CityEntitie VALUES('Brahmapur','19.32','84.8','India')

INSERT INTO CityEntitie VALUES('Birjand','32.88','59.2199','Iran')

INSERT INTO CityEntitie VALUES('Fayetteville','35.0846','-78.9776','United States')

INSERT INTO CityEntitie VALUES('Yoshkar Ola','56.6354','47.8749','Russia')

INSERT INTO CityEntitie VALUES('Miyazaki','31.9182','131.4184','Japan')

INSERT INTO CityEntitie VALUES('Volzhskiy','48.7948','44.7744','Russia')

INSERT INTO CityEntitie VALUES('Chenzhou','25.8004','113.0301','China')

INSERT INTO CityEntitie VALUES('Valladolid','41.65','-4.75','Spain')

INSERT INTO CityEntitie VALUES('Santa Rosa','38.4465','-122.706','United States')

INSERT INTO CityEntitie VALUES('Al Kut','32.4907','45.8304','Iraq')

INSERT INTO CityEntitie VALUES('C?rdoba','37.88','-4.77','Spain')

INSERT INTO CityEntitie VALUES('Smolensk','54.7827','32.0473','Russia')

INSERT INTO CityEntitie VALUES('Lansing','42.7142','-84.56','United States')

INSERT INTO CityEntitie VALUES('Ciudad del Este','-25.5167','-54.6161','Paraguay')

INSERT INTO CityEntitie VALUES('Pelotas','-31.75','-52.33','Brazil')

INSERT INTO CityEntitie VALUES('Podolsk','55.3804','37.5299','Russia')

INSERT INTO CityEntitie VALUES('Kherson','46.6325','32.6007','Ukraine')

INSERT INTO CityEntitie VALUES('Shahjahanpur','27.8804','79.905','India')

INSERT INTO CityEntitie VALUES('Itu','-23.26','-47.3','Brazil')

INSERT INTO CityEntitie VALUES('Legazpi','13.17','123.75','Philippines')

INSERT INTO CityEntitie VALUES('Akita','39.71','140.09','Japan')

INSERT INTO CityEntitie VALUES('Maroua','10.5956','14.3247','Cameroon')

INSERT INTO CityEntitie VALUES('An?polis','-16.3196','-48.96','Brazil')

INSERT INTO CityEntitie VALUES('Pachuca','20.1704','-98.73','Mexico')

INSERT INTO CityEntitie VALUES('Bukhara','39.78','64.43','Uzbekistan')

INSERT INTO CityEntitie VALUES('Murmansk','68.97','33.1','Russia')

INSERT INTO CityEntitie VALUES('Windsor','42.3333','-83.0333','Canada')

INSERT INTO CityEntitie VALUES('Holgu?n','20.8872','-76.2631','Cuba')

INSERT INTO CityEntitie VALUES('Oskemen','49.99','82.6149','Kazakhstan')

INSERT INTO CityEntitie VALUES('Tepic','21.5054','-104.88','Mexico')

INSERT INTO CityEntitie VALUES('Vladimir','56.13','40.4099','Russia')

INSERT INTO CityEntitie VALUES('Mobile','30.6783','-88.1162','United States')

INSERT INTO CityEntitie VALUES('Hat Yai','6.9964','100.4714','Thailand')

INSERT INTO CityEntitie VALUES('Poltava','49.574','34.5703','Ukraine')

INSERT INTO CityEntitie VALUES('New Delhi','28.6','77.2','India')

INSERT INTO CityEntitie VALUES('Cuman?','10.45','-64.18','Venezuela')

INSERT INTO CityEntitie VALUES('Hrodna','53.6779','23.8341','Belarus')

INSERT INTO CityEntitie VALUES('Rohtak','28.9','76.58','India')

INSERT INTO CityEntitie VALUES('El Faiyum','29.31','30.84','Egypt')

INSERT INTO CityEntitie VALUES('Cluj-Napoca','46.7884','23.5984','Romania')

INSERT INTO CityEntitie VALUES('Bauchi','10.3104','9.84','Nigeria')

INSERT INTO CityEntitie VALUES('Lexington','38.0423','-84.4587','United States')

INSERT INTO CityEntitie VALUES('Alicante','38.3512','-0.4836','Spain')

INSERT INTO CityEntitie VALUES('Dezful','32.3804','48.47','Iran')

INSERT INTO CityEntitie VALUES('Maiquet?a','10.6004','-66.97','Venezuela')

INSERT INTO CityEntitie VALUES('Armenia','4.5343','-75.6811','Colombia')

INSERT INTO CityEntitie VALUES('Timi?oara','45.7588','21.2234','Romania')

INSERT INTO CityEntitie VALUES('Ljubljana','46.0553','14.515','Slovenia')

INSERT INTO CityEntitie VALUES('Pescara','42.4554','14.2187','Italy')

INSERT INTO CityEntitie VALUES('Gdynia','54.5204','18.53','Poland')

INSERT INTO CityEntitie VALUES('Angeles','15.1451','120.5451','Philippines')

INSERT INTO CityEntitie VALUES('Ninde','26.6804','119.5301','China')

INSERT INTO CityEntitie VALUES('Aswan','24.0875','32.8989','Egypt')

INSERT INTO CityEntitie VALUES('Varna','43.2156','27.8953','Bulgaria')

INSERT INTO CityEntitie VALUES('Koblenz','50.3505','7.6','Germany')

INSERT INTO CityEntitie VALUES('Cherepovets','59.1404','37.91','Russia')

INSERT INTO CityEntitie VALUES('Qurghonteppa','37.8373','68.7713','Tajikistan')

INSERT INTO CityEntitie VALUES('Semey','50.435','80.275','Kazakhstan')

INSERT INTO CityEntitie VALUES('Gala?i','45.4559','28.0459','Romania')

INSERT INTO CityEntitie VALUES('Bra?ov','45.6475','25.6072','Romania')

INSERT INTO CityEntitie VALUES('Sambalpur','21.4704','83.9701','India')

INSERT INTO CityEntitie VALUES('Pucallpa','-8.3689','-74.535','Peru')

INSERT INTO CityEntitie VALUES('Antofagasta','-23.65','-70.4','Chile')

INSERT INTO CityEntitie VALUES('Huntsville','34.6989','-86.6414','United States')

INSERT INTO CityEntitie VALUES('Santa Clarita','34.4155','-118.4992','United States')

INSERT INTO CityEntitie VALUES('Asheville','35.5706','-82.5537','United States')

INSERT INTO CityEntitie VALUES('Sapele','5.8904','5.68','Nigeria')

INSERT INTO CityEntitie VALUES('Dayr az Zawr','35.3304','40.13','Syria')

INSERT INTO CityEntitie VALUES('Chita','52.055','113.465','Russia')

INSERT INTO CityEntitie VALUES('Valledupar','10.48','-73.25','Colombia')

INSERT INTO CityEntitie VALUES('Vit?ria da Conquista','-14.85','-40.84','Brazil')

INSERT INTO CityEntitie VALUES('Antsirabe','-19.85','47.0333','Madagascar')

INSERT INTO CityEntitie VALUES('Naltchik','43.4981','43.6179','Russia')

INSERT INTO CityEntitie VALUES('Chernihiv','51.5049','31.3015','Ukraine')

INSERT INTO CityEntitie VALUES('Ratlam','23.3504','75.03','India')

INSERT INTO CityEntitie VALUES('Saint Paul','44.9477','-93.104','United States')

INSERT INTO CityEntitie VALUES('Ksar El Kebir','35.0204','-5.91','Morocco')

INSERT INTO CityEntitie VALUES('Tawau','4.271','117.896','Malaysia')

INSERT INTO CityEntitie VALUES('Firozabad','27.15','78.3949','India')

INSERT INTO CityEntitie VALUES('Porto Velho','-8.75','-63.9','Brazil')

INSERT INTO CityEntitie VALUES('San Salvador de Jujuy','-24.1833','-65.3','Argentina')

INSERT INTO CityEntitie VALUES('Hatay','36.2304','36.12','Turkey')

INSERT INTO CityEntitie VALUES('Franca','-20.53','-47.39','Brazil')

INSERT INTO CityEntitie VALUES('Rajahmundry','17.0303','81.79','India')

INSERT INTO CityEntitie VALUES('Olongapo','14.8296','120.2828','Philippines')

INSERT INTO CityEntitie VALUES('Craiova','44.3263','23.8259','Romania')

INSERT INTO CityEntitie VALUES('Kom Ombo','24.47','32.95','Egypt')

INSERT INTO CityEntitie VALUES('Los Teques','10.42','-67.02','Venezuela')

INSERT INTO CityEntitie VALUES('Constan?a','44.2027','28.61','Romania')

INSERT INTO CityEntitie VALUES('Saransk','54.1704','45.18','Russia')

INSERT INTO CityEntitie VALUES('Mengzi','23.3619','103.4061','China')

INSERT INTO CityEntitie VALUES('Ganca','40.685','46.35','Azerbaijan')

INSERT INTO CityEntitie VALUES('Fort Collins','40.5479','-105.0658','United States')

INSERT INTO CityEntitie VALUES('Hakodate','41.795','140.74','Japan')

INSERT INTO CityEntitie VALUES('Antioch','37.9789','-121.7957','United States')

INSERT INTO CityEntitie VALUES('Chemnitz','50.83','12.92','Germany')

INSERT INTO CityEntitie VALUES('Henderson','36.0145','-115.0362','United States')

INSERT INTO CityEntitie VALUES('Kingston upon Hull','53.7504','-0.33','United Kingdom')

INSERT INTO CityEntitie VALUES('Batman','37.8904','41.14','Turkey')

INSERT INTO CityEntitie VALUES('Qena','26.1505','32.72','Egypt')

INSERT INTO CityEntitie VALUES('Barddhaman','23.2504','87.865','India')

INSERT INTO CityEntitie VALUES('Jinja','0.4404','33.1999','Uganda')

INSERT INTO CityEntitie VALUES('Gujrat','32.58','74.08','Pakistan')

INSERT INTO CityEntitie VALUES('Tambov','52.73','41.43','Russia')

INSERT INTO CityEntitie VALUES('Hami','42.827','93.515','China')

INSERT INTO CityEntitie VALUES('Mardan','34.2','72.04','Pakistan')

INSERT INTO CityEntitie VALUES('Bidar','17.9229','77.5175','India')

INSERT INTO CityEntitie VALUES('Qyzylorda','44.8','65.465','Kazakhstan')

INSERT INTO CityEntitie VALUES('R?ch Gi?','10.0154','105.0914','Vietnam')

INSERT INTO CityEntitie VALUES('S?c Tr?ng','9.6037','105.98','Vietnam')

INSERT INTO CityEntitie VALUES('Porto-Novo','6.4833','2.6166','Benin')

INSERT INTO CityEntitie VALUES('Curepipe','-20.3162','57.5166','Mauritius')

INSERT INTO CityEntitie VALUES('Kanggye','40.9732','126.6032','Korea')

INSERT INTO CityEntitie VALUES('Baqubah','33.7476','44.6573','Iraq')

INSERT INTO CityEntitie VALUES('Dumyat','31.4204','31.82','Egypt')

INSERT INTO CityEntitie VALUES('Jember','-8.1727','113.6873','Indonesia')

INSERT INTO CityEntitie VALUES('Al Mubarraz','25.4291','49.5659','Saudi Arabia')

INSERT INTO CityEntitie VALUES('Al Kharj','24.1556','47.312','Saudi Arabia')

INSERT INTO CityEntitie VALUES('Aomori','40.825','140.71','Japan')

INSERT INTO CityEntitie VALUES('Chernivtsi','48.3053','25.9216','Ukraine')

INSERT INTO CityEntitie VALUES('?skenderun','36.5804','36.17','Turkey')

INSERT INTO CityEntitie VALUES('Cherkasy','49.4347','32.0709','Ukraine')

INSERT INTO CityEntitie VALUES('Trenton','40.2237','-74.764','United States')

INSERT INTO CityEntitie VALUES('Bandar Seri Begawan','4.8833','114.9333','Brunei')

INSERT INTO CityEntitie VALUES('Rampur','28.8154','79.025','India')

INSERT INTO CityEntitie VALUES('Morioka','39.72','141.13','Japan')

INSERT INTO CityEntitie VALUES('Ar Ramadi','33.42','43.3','Iraq')

INSERT INTO CityEntitie VALUES('Port-of-Spain','10.652','-61.517','Trinidad And Tobago')

INSERT INTO CityEntitie VALUES('Vologda','59.21','39.92','Russia')

INSERT INTO CityEntitie VALUES('Sumy','50.9243','34.7809','Ukraine')

INSERT INTO CityEntitie VALUES('Swansea','51.63','-3.95','United Kingdom')

INSERT INTO CityEntitie VALUES('Fukushima','37.74','140.47','Japan')

INSERT INTO CityEntitie VALUES('Blumenau','-26.92','-49.09','Brazil')

INSERT INTO CityEntitie VALUES('Thiès','14.8104','-16.93','Senegal')

INSERT INTO CityEntitie VALUES('Kakinada','16.9675','82.2375','India')

INSERT INTO CityEntitie VALUES('Panipat','29.4004','76.97','India')

INSERT INTO CityEntitie VALUES('Makurdi','7.73','8.53','Nigeria')

INSERT INTO CityEntitie VALUES('Ponta Grossa','-25.09','-50.16','Brazil')

INSERT INTO CityEntitie VALUES('Minna','9.62','6.55','Nigeria')

INSERT INTO CityEntitie VALUES('Bia?ystok','53.1504','23.17','Poland')

INSERT INTO CityEntitie VALUES('Mbeya','-8.89','33.43','Tanzania')

INSERT INTO CityEntitie VALUES('Cagliari','39.2224','9.104','Italy')

INSERT INTO CityEntitie VALUES('Lakeland','28.0557','-81.9543','United States')

INSERT INTO CityEntitie VALUES('Khammam','17.2804','80.1601','India')

INSERT INTO CityEntitie VALUES('Bafoussam','5.4904','10.4099','Cameroon')

INSERT INTO CityEntitie VALUES('Kasur','31.1254','74.455','Pakistan')

INSERT INTO CityEntitie VALUES('Hulan Ergi','47.21','123.61','China')

INSERT INTO CityEntitie VALUES('Kassel','51.3','9.5','Germany')

INSERT INTO CityEntitie VALUES('Limeira','-22.5495','-47.4','Brazil')

INSERT INTO CityEntitie VALUES('Victoria','48.4333','-123.35','Canada')

INSERT INTO CityEntitie VALUES('Bhuj','23.2504','69.81','India')

INSERT INTO CityEntitie VALUES('Huizhou','23.08','114.4','China')

INSERT INTO CityEntitie VALUES('Karimnagar','18.4604','79.11','India')

INSERT INTO CityEntitie VALUES('Sinuiju','40.0859','124.4213','Korea')

INSERT INTO CityEntitie VALUES('Talcahuano','-36.7167','-73.1167','Chile')

INSERT INTO CityEntitie VALUES('Ciudad Obregon','27.4666','-109.925','Mexico')

INSERT INTO CityEntitie VALUES('Shreveport','32.4659','-93.7959','United States')

INSERT INTO CityEntitie VALUES('Davenport','41.5563','-90.6052','United States')

INSERT INTO CityEntitie VALUES('Coatzacoalcos','18.1204','-94.42','Mexico')

INSERT INTO CityEntitie VALUES('Springfield','37.1943','-93.2915','United States')

INSERT INTO CityEntitie VALUES('Tirupati','13.6504','79.42','India')

INSERT INTO CityEntitie VALUES('Cuenca','-2.9','-79','Ecuador')

INSERT INTO CityEntitie VALUES('Butembo','0.13','29.28','Congo (Kinshasa)')

INSERT INTO CityEntitie VALUES('Sekondi','4.9433','-1.704','Ghana')

INSERT INTO CityEntitie VALUES('Plano','33.0502','-96.7487','United States')

INSERT INTO CityEntitie VALUES('Petr?polis','-22.5095','-43.2','Brazil')

INSERT INTO CityEntitie VALUES('Hospet','15.2804','76.375','India')

INSERT INTO CityEntitie VALUES('Rangpur','25.75','89.28','Bangladesh')

INSERT INTO CityEntitie VALUES('Rockford','42.2598','-89.0641','United States')

INSERT INTO CityEntitie VALUES('Newark','40.7245','-74.1725','United States')

INSERT INTO CityEntitie VALUES('Lincoln','40.8098','-96.6802','United States')

INSERT INTO CityEntitie VALUES('Zagazig','30.5833','31.5167','Egypt')

INSERT INTO CityEntitie VALUES('Mandya','12.5704','76.92','India')

INSERT INTO CityEntitie VALUES('Round Lake Beach','42.3791','-88.0811','United States')

INSERT INTO CityEntitie VALUES('Bago','17.32','96.515','Burma')

INSERT INTO CityEntitie VALUES('Barinas','8.6','-70.25','Venezuela')

INSERT INTO CityEntitie VALUES('Port Moresby','-9.4647','147.1925','Papua New Guinea')

INSERT INTO CityEntitie VALUES('Iksan','35.941','126.9454','Korea')

INSERT INTO CityEntitie VALUES('Alwar','27.5454','76.6049','India')

INSERT INTO CityEntitie VALUES('Cadiz','10.9587','123.3086','Philippines')

INSERT INTO CityEntitie VALUES('C?diz','36.535','-6.225','Spain')

INSERT INTO CityEntitie VALUES('Aizawl','23.7104','92.72','India')

INSERT INTO CityEntitie VALUES('Kupang','-10.1787','123.583','Indonesia')

INSERT INTO CityEntitie VALUES('Tongchuan','35.08','109.03','China')

INSERT INTO CityEntitie VALUES('Zhytomyr','50.2456','28.6622','Ukraine')

INSERT INTO CityEntitie VALUES('Jining','41.03','113.08','China')

INSERT INTO CityEntitie VALUES('Bah?a Blanca','-38.74','-62.265','Argentina')

INSERT INTO CityEntitie VALUES('Cap-Haïtien','19.7592','-72.2125','Haiti')

INSERT INTO CityEntitie VALUES('Ambato','-1.2696','-78.62','Ecuador')

INSERT INTO CityEntitie VALUES('South Bend','41.6771','-86.2692','United States')

INSERT INTO CityEntitie VALUES('Gorgan','36.8303','54.48','Iran')

INSERT INTO CityEntitie VALUES('Batna','35.57','6.17','Algeria')

INSERT INTO CityEntitie VALUES('Tacna','-18','-70.25','Peru')

INSERT INTO CityEntitie VALUES('Savannah','32.0282','-81.1786','United States')

INSERT INTO CityEntitie VALUES('Tacloban','11.2504','125','Philippines')

INSERT INTO CityEntitie VALUES('Xinzhou','38.4104','112.72','China')

INSERT INTO CityEntitie VALUES('Cotabato','7.2169','124.2484','Philippines')

INSERT INTO CityEntitie VALUES('Rize','41.0208','40.5219','Turkey')

INSERT INTO CityEntitie VALUES('Ica','-14.068','-75.7255','Peru')

INSERT INTO CityEntitie VALUES('Sumqayt','40.58','49.63','Azerbaijan')

INSERT INTO CityEntitie VALUES('Taganrog','47.23','38.92','Russia')

INSERT INTO CityEntitie VALUES('Kaolack','14.15','-16.1','Senegal')

INSERT INTO CityEntitie VALUES('Kostroma','57.77','40.94','Russia')

INSERT INTO CityEntitie VALUES('Irvine','33.6772','-117.7738','United States')

INSERT INTO CityEntitie VALUES('Owo','7.2004','5.59','Nigeria')

INSERT INTO CityEntitie VALUES('Sukabumi','-6.9096','106.9','Indonesia')

INSERT INTO CityEntitie VALUES('Komsomolsk na Amure','50.555','137.02','Russia')

INSERT INTO CityEntitie VALUES('Prokopyevsk','53.9','86.71','Russia')

INSERT INTO CityEntitie VALUES('Bern','46.9167','7.467','Switzerland')

INSERT INTO CityEntitie VALUES('Monter?a','8.7575','-75.89','Colombia')

INSERT INTO CityEntitie VALUES('Mbandaka','0.04','18.26','Congo (Kinshasa)')

INSERT INTO CityEntitie VALUES('Ciudad Victoria','23.72','-99.13','Mexico')

INSERT INTO CityEntitie VALUES('Sétif','36.18','5.4','Algeria')

INSERT INTO CityEntitie VALUES('Pamplona','42.82','-1.65','Spain')

INSERT INTO CityEntitie VALUES('Jinshi','29.6321','111.8517','China')

INSERT INTO CityEntitie VALUES('Ubon Ratchathani','15.25','104.83','Thailand')

INSERT INTO CityEntitie VALUES('Crato','-7.4639','-63.04','Brazil')

INSERT INTO CityEntitie VALUES('Crato','-7.2296','-39.42','Brazil')

INSERT INTO CityEntitie VALUES('Guant?namo','20.1453','-75.2061','Cuba')

INSERT INTO CityEntitie VALUES('Blackpool','53.8304','-3.05','United Kingdom')

INSERT INTO CityEntitie VALUES('Majene','-3.5336','118.966','Indonesia')

INSERT INTO CityEntitie VALUES('Yamagata','38.2705','140.32','Japan')

INSERT INTO CityEntitie VALUES('Pakalongan','-6.8796','109.67','Indonesia')

INSERT INTO CityEntitie VALUES('Elâz??','38.68','39.23','Turkey')

INSERT INTO CityEntitie VALUES('Sari','36.5504','53.1','Iran')

INSERT INTO CityEntitie VALUES('Canton','40.8076','-81.3677','United States')

INSERT INTO CityEntitie VALUES('Tasikmalaya','-7.3254','108.2147','Indonesia')

INSERT INTO CityEntitie VALUES('Bijapur','16.8354','75.71','India')

INSERT INTO CityEntitie VALUES('Venice','45.4387','12.335','Italy')

INSERT INTO CityEntitie VALUES('Jersey City','40.7161','-74.0682','United States')

INSERT INTO CityEntitie VALUES('Chula Vista','32.6281','-117.0145','United States')

INSERT INTO CityEntitie VALUES('Gombe','10.2904','11.17','Nigeria')

INSERT INTO CityEntitie VALUES('M?nster','51.9704','7.62','Germany')

INSERT INTO CityEntitie VALUES('Thohoyandou','-22.95','30.48','South Africa')

INSERT INTO CityEntitie VALUES('Kiel','54.3304','10.13','Germany')

INSERT INTO CityEntitie VALUES('Malm?','55.5833','13.0333','Sweden')

INSERT INTO CityEntitie VALUES('Nancy','48.6837','6.2','France')

INSERT INTO CityEntitie VALUES('Mokpo','34.8068','126.3958','Korea')

INSERT INTO CityEntitie VALUES('Windhoek','-22.57','17.0835','Namibia')

INSERT INTO CityEntitie VALUES('Tebingtinggi','3.3304','99.13','Indonesia')

INSERT INTO CityEntitie VALUES('Yanbu al Bahr','24.0943','38.0493','Saudi Arabia')

INSERT INTO CityEntitie VALUES('Eugene','44.0563','-123.1173','United States')

INSERT INTO CityEntitie VALUES('Tshikapa','-6.41','20.77','Congo (Kinshasa)')

INSERT INTO CityEntitie VALUES('Reading','40.34','-75.9266','United States')

INSERT INTO CityEntitie VALUES('Sterlitamak','53.63','55.96','Russia')

INSERT INTO CityEntitie VALUES('Padangsidempuan','1.3887','99.2734','Indonesia')

INSERT INTO CityEntitie VALUES('Myeik','12.4541','98.6115','Burma')

INSERT INTO CityEntitie VALUES('Temuco','-38.73','-72.58','Chile')

INSERT INTO CityEntitie VALUES('Lafayette','30.2083','-92.0322','United States')

INSERT INTO CityEntitie VALUES('Lausanne','46.5304','6.65','Switzerland')

INSERT INTO CityEntitie VALUES('Saint-?tienne','45.4304','4.38','France')

INSERT INTO CityEntitie VALUES('Petrozavodsk','61.85','34.28','Russia')

INSERT INTO CityEntitie VALUES('Imphal','24.8','93.95','India')

INSERT INTO CityEntitie VALUES('Umuahia','5.532','7.486','Nigeria')

INSERT INTO CityEntitie VALUES('Uruapan','19.4204','-102.07','Mexico')

INSERT INTO CityEntitie VALUES('Georgetown','6.802','-58.167','Guyana')

INSERT INTO CityEntitie VALUES('Sivas','39.7454','37.035','Turkey')

INSERT INTO CityEntitie VALUES('Tubruq','32.08','23.96','Libya')

INSERT INTO CityEntitie VALUES('Saint Petersburg','27.793','-82.6652','United States')

INSERT INTO CityEntitie VALUES('Graz','47.0778','15.41','Austria')

INSERT INTO CityEntitie VALUES('Kindu','-2.9639','25.91','Congo (Kinshasa)')

INSERT INTO CityEntitie VALUES('Lubbock','33.5643','-101.8871','United States')

INSERT INTO CityEntitie VALUES('Aqtobe','50.28','57.17','Kazakhstan')

INSERT INTO CityEntitie VALUES('Paran?','-31.7333','-60.5333','Argentina')

INSERT INTO CityEntitie VALUES('Az Aubayr','30.3892','47.708','Iraq')

INSERT INTO CityEntitie VALUES('Peoria','40.752','-89.6155','United States')

INSERT INTO CityEntitie VALUES('Hotan','37.0997','79.9269','China')

INSERT INTO CityEntitie VALUES('Cabo Frio','-22.89','-42.04','Brazil')

INSERT INTO CityEntitie VALUES('Bal?kesir','39.6504','27.89','Turkey')

INSERT INTO CityEntitie VALUES('Zhuanghe','39.6823','122.9619','China')

INSERT INTO CityEntitie VALUES('Sincelejo','9.2904','-75.38','Colombia')

INSERT INTO CityEntitie VALUES('Petrolina','-9.38','-40.51','Brazil')

INSERT INTO CityEntitie VALUES('Acarigua','9.5804','-69.2','Venezuela')

INSERT INTO CityEntitie VALUES('Wollongong','-34.4154','150.89','Australia')

INSERT INTO CityEntitie VALUES('Uberaba','-19.78','-47.95','Brazil')

INSERT INTO CityEntitie VALUES('Myrtle Beach','33.7096','-78.8843','United States')

INSERT INTO CityEntitie VALUES('Laredo','27.5616','-99.487','United States')

INSERT INTO CityEntitie VALUES('Adapazar?','40.8','30.415','Turkey')

INSERT INTO CityEntitie VALUES('Konibodom','40.2922','70.4272','Tajikistan')

INSERT INTO CityEntitie VALUES('Salem','44.9232','-123.0245','United States')

INSERT INTO CityEntitie VALUES('Kondoz','36.728','68.8725','Afghanistan')

INSERT INTO CityEntitie VALUES('Tampere','61.5','23.75','Finland')

INSERT INTO CityEntitie VALUES('Columbus','32.51','-84.8771','United States')

INSERT INTO CityEntitie VALUES('Popay?n','2.42','-76.61','Colombia')

INSERT INTO CityEntitie VALUES('Nonthaburi','13.8337','100.4833','Thailand')

INSERT INTO CityEntitie VALUES('Al Mukalla','14.5412','49.1259','Yemen')

INSERT INTO CityEntitie VALUES('Rio Branco','-9.9666','-67.8','Brazil')

INSERT INTO CityEntitie VALUES('Etawah','26.7855','79.015','India')

INSERT INTO CityEntitie VALUES('Mendefera','14.886','38.8163','Eritrea')

INSERT INTO CityEntitie VALUES('Cascavel','-24.9596','-53.46','Brazil')

INSERT INTO CityEntitie VALUES('Ondo','7.0904','4.84','Nigeria')

INSERT INTO CityEntitie VALUES('Chimoio','-19.12','33.47','Mozambique')

INSERT INTO CityEntitie VALUES('Ensenada','31.87','-116.62','Mexico')

INSERT INTO CityEntitie VALUES('Dzerzhinsk','56.2504','43.46','Russia')

INSERT INTO CityEntitie VALUES('Da Lat','11.9304','108.42','Vietnam')

INSERT INTO CityEntitie VALUES('Montgomery','32.347','-86.2663','United States')

INSERT INTO CityEntitie VALUES('Damaturu','11.749','11.966','Nigeria')

INSERT INTO CityEntitie VALUES('Raichur','16.2104','77.355','India')

INSERT INTO CityEntitie VALUES('Daloa','6.89','-6.45','Côte DIvoire')

INSERT INTO CityEntitie VALUES('Rivne','50.6166','26.2528','Ukraine')

INSERT INTO CityEntitie VALUES('Turpan','42.9354','89.165','China')

INSERT INTO CityEntitie VALUES('Freiburg','48.0004','7.8699','Germany')

INSERT INTO CityEntitie VALUES('Poza Rica de Hidalgo','20.5504','-97.47','Mexico')

INSERT INTO CityEntitie VALUES('Chuxiong','25.0364','101.5456','China')

INSERT INTO CityEntitie VALUES('Cirebon','-6.7333','108.5666','Indonesia')

INSERT INTO CityEntitie VALUES('Paramaribo','5.835','-55.167','Suriname')

INSERT INTO CityEntitie VALUES('Fort-de-France','14.6104','-61.08','Martinique')

INSERT INTO CityEntitie VALUES('Pathankot','32.2703','75.72','India')

INSERT INTO CityEntitie VALUES('Chandler','33.2827','-111.8516','United States')

INSERT INTO CityEntitie VALUES('Anchorage','61.1508','-149.1091','United States')

INSERT INTO CityEntitie VALUES('Tallahassee','30.455','-84.2526','United States')

INSERT INTO CityEntitie VALUES('Chirala','15.8604','80.34','India')

INSERT INTO CityEntitie VALUES('Buenaventura','3.8724','-77.0505','Colombia')

INSERT INTO CityEntitie VALUES('El Fasher','13.63','25.35','Sudan')

INSERT INTO CityEntitie VALUES('Dire Dawa','9.59','41.86','Ethiopia')

INSERT INTO CityEntitie VALUES('Suihua','46.6304','126.98','China')

INSERT INTO CityEntitie VALUES('Messina','38.2005','15.55','Italy')

INSERT INTO CityEntitie VALUES('Borujerd','33.92','48.8','Iran')

INSERT INTO CityEntitie VALUES('Linhai','28.85','121.12','China')

INSERT INTO CityEntitie VALUES('Khon Kaen','16.42','102.83','Thailand')

INSERT INTO CityEntitie VALUES('Morogoro','-6.82','37.66','Tanzania')

INSERT INTO CityEntitie VALUES('Governador Valadares','-18.87','-41.97','Brazil')

INSERT INTO CityEntitie VALUES('Sonipat','29','77.02','India')

INSERT INTO CityEntitie VALUES('Santa Clara','22.4','-79.9667','Cuba')

INSERT INTO CityEntitie VALUES('Iwo','7.63','4.18','Nigeria')

INSERT INTO CityEntitie VALUES('Concord','35.3933','-80.636','United States')

INSERT INTO CityEntitie VALUES('Jaffna','9.675','80.005','Sri Lanka')

INSERT INTO CityEntitie VALUES('Zumpango','19.8104','-99.11','Mexico')

INSERT INTO CityEntitie VALUES('Ni?','43.3304','21.9','Serbia')

INSERT INTO CityEntitie VALUES('Arua','3.0204','30.9','Uganda')

INSERT INTO CityEntitie VALUES('Qu?ng Ng?i','15.1504','108.83','Vietnam')

INSERT INTO CityEntitie VALUES('Luzern','47.0504','8.28','Switzerland')

INSERT INTO CityEntitie VALUES('Laiyang','36.9684','120.7084','China')

INSERT INTO CityEntitie VALUES('Kashan','33.9804','51.58','Iran')

INSERT INTO CityEntitie VALUES('Como','45.81','9.08','Italy')

INSERT INTO CityEntitie VALUES('Caserta','41.06','14.3374','Italy')

INSERT INTO CityEntitie VALUES('Scottsdale','33.6872','-111.865','United States')

INSERT INTO CityEntitie VALUES('V?rzea Grande','-15.65','-56.14','Brazil')

INSERT INTO CityEntitie VALUES('Kirovohrad','48.5041','32.2603','Ukraine')

INSERT INTO CityEntitie VALUES('Melun','48.5333','2.6666','France')

INSERT INTO CityEntitie VALUES('Mutare','-18.97','32.65','Zimbabwe')

INSERT INTO CityEntitie VALUES('Santa Maria','-29.6833','-53.8','Brazil')

INSERT INTO CityEntitie VALUES('Hafar al Batin','28.4337','45.9601','Saudi Arabia')

INSERT INTO CityEntitie VALUES('Los Mochis','25.79','-109','Mexico')

INSERT INTO CityEntitie VALUES('Jalal Abad','40.9429','73.0025','Kyrgyzstan')

INSERT INTO CityEntitie VALUES('Vung Tau','10.3554','107.085','Vietnam')

INSERT INTO CityEntitie VALUES('Fukui','36.0704','136.22','Japan')

INSERT INTO CityEntitie VALUES('Singkawang','0.912','108.9655','Indonesia')

INSERT INTO CityEntitie VALUES('Miskolc','48.1004','20.78','Hungary')

INSERT INTO CityEntitie VALUES('Killeen','31.0754','-97.7293','United States')

INSERT INTO CityEntitie VALUES('Plymouth','50.3854','-4.16','United Kingdom')

INSERT INTO CityEntitie VALUES('Udon Thani','17.4048','102.7893','Thailand')

INSERT INTO CityEntitie VALUES('Kattaqorgon','39.9007','66.2608','Uzbekistan')

INSERT INTO CityEntitie VALUES('Orsk','51.21','58.6273','Russia')

INSERT INTO CityEntitie VALUES('San Bernardo','-33.6','-70.7','Chile')

INSERT INTO CityEntitie VALUES('Glendale','33.5796','-112.2258','United States')

INSERT INTO CityEntitie VALUES('Oruro','-17.98','-67.13','Bolivia')

INSERT INTO CityEntitie VALUES('Bratsk','56.157','101.615','Russia')

INSERT INTO CityEntitie VALUES('Wilmington','34.21','-77.886','United States')

INSERT INTO CityEntitie VALUES('Matadi','-5.8166','13.45','Congo (Kinshasa)')

INSERT INTO CityEntitie VALUES('Mirzapur','25.1454','82.57','India')

INSERT INTO CityEntitie VALUES('Shimonoseki','33.9654','130.9454','Japan')

INSERT INTO CityEntitie VALUES('Rzesz?w','50.0705','22','Poland')

INSERT INTO CityEntitie VALUES('Juliaca','-15.5','-70.14','Peru')

INSERT INTO CityEntitie VALUES('Qinzhou','21.9504','108.62','China')

INSERT INTO CityEntitie VALUES('Winter Haven','28.0122','-81.7018','United States')

INSERT INTO CityEntitie VALUES('Nizhenvartovsk','60.935','76.58','Russia')

INSERT INTO CityEntitie VALUES('Ternopil','49.536','25.5821','Ukraine')

INSERT INTO CityEntitie VALUES('Braunschweig','52.25','10.5','Germany')

INSERT INTO CityEntitie VALUES('Norfolk','36.8945','-76.259','United States')

INSERT INTO CityEntitie VALUES('Hebi','35.9504','114.22','China')

INSERT INTO CityEntitie VALUES('Thu Dau Mot','10.9691','106.6527','Vietnam')

INSERT INTO CityEntitie VALUES('Gunsan','35.9818','126.716','Korea')

INSERT INTO CityEntitie VALUES('Jessore','23.1704','89.2','Bangladesh')

INSERT INTO CityEntitie VALUES('Manisa','38.6304','27.44','Turkey')

INSERT INTO CityEntitie VALUES('Wonju','37.3551','127.9396','Korea')

INSERT INTO CityEntitie VALUES('Nam ??nh','20.42','106.2','Vietnam')

INSERT INTO CityEntitie VALUES('Angarsk','52.56','103.92','Russia')

INSERT INTO CityEntitie VALUES('North Las Vegas','36.288','-115.0901','United States')

INSERT INTO CityEntitie VALUES('Hapur','28.7437','77.7628','India')

INSERT INTO CityEntitie VALUES('Gilbert','33.3103','-111.7463','United States')

INSERT INTO CityEntitie VALUES('Le Havre','49.505','0.105','France')

INSERT INTO CityEntitie VALUES('Caruaru','-8.28','-35.98','Brazil')

INSERT INTO CityEntitie VALUES('Neuquén','-38.95','-68.06','Argentina')

INSERT INTO CityEntitie VALUES('Ulanhot','46.08','122.08','China')

INSERT INTO CityEntitie VALUES('Novorossiysk','44.73','37.7699','Russia')

INSERT INTO CityEntitie VALUES('Tehuacan','18.4504','-97.38','Mexico')

INSERT INTO CityEntitie VALUES('Atlantic City','39.3797','-74.4527','United States')

INSERT INTO CityEntitie VALUES('Ivano-Frankivsk','48.9348','24.7094','Ukraine')

INSERT INTO CityEntitie VALUES('Linchuan','27.9703','116.36','China')

INSERT INTO CityEntitie VALUES('San Pablo','14.0696','121.3226','Philippines')

INSERT INTO CityEntitie VALUES('Chesapeake','36.6778','-76.3024','United States')

INSERT INTO CityEntitie VALUES('Irving','32.8584','-96.9702','United States')

INSERT INTO CityEntitie VALUES('Hialeah','25.8696','-80.3045','United States')

INSERT INTO CityEntitie VALUES('Hachinohe','40.51','141.54','Japan')

INSERT INTO CityEntitie VALUES('Garland','32.91','-96.6305','United States')

INSERT INTO CityEntitie VALUES('F?rth','49.47','11','Germany')

INSERT INTO CityEntitie VALUES('As Sib','23.6802','58.1825','Oman')

INSERT INTO CityEntitie VALUES('?rhus','56.1572','10.2107','Denmark')

INSERT INTO CityEntitie VALUES('Sasebo','33.1631','129.7177','Japan')

INSERT INTO CityEntitie VALUES('Al Jubayl','27.0046','49.646','Saudi Arabia')

INSERT INTO CityEntitie VALUES('Taiping','4.865','100.72','Malaysia')

INSERT INTO CityEntitie VALUES('Pathein','16.7704','94.75','Burma')

INSERT INTO CityEntitie VALUES('Tegal','-6.8696','109.12','Indonesia')

INSERT INTO CityEntitie VALUES('Zicheng','30.3004','111.5','China')

INSERT INTO CityEntitie VALUES('Sylhet','24.9036','91.8736','Bangladesh')

INSERT INTO CityEntitie VALUES('Oostanay','53.2209','63.6283','Kazakhstan')

INSERT INTO CityEntitie VALUES('Ko?ice','48.7304','21.25','Slovakia')

INSERT INTO CityEntitie VALUES('Monclova','26.9','-101.42','Mexico')

INSERT INTO CityEntitie VALUES('Tours','47.3804','0.6999','France')

INSERT INTO CityEntitie VALUES('Dera Ghazi Khan','30.0604','70.6351','Pakistan')

INSERT INTO CityEntitie VALUES('Springs','-26.2696','28.43','South Africa')

INSERT INTO CityEntitie VALUES('Luton','51.8804','-0.42','United Kingdom')

INSERT INTO CityEntitie VALUES('Sahiwal','30.6717','73.1118','Pakistan')

INSERT INTO CityEntitie VALUES('Oviedo','43.3505','-5.83','Spain')

INSERT INTO CityEntitie VALUES('Yakutsk','62.035','129.735','Russia')

INSERT INTO CityEntitie VALUES('L?beck','53.8704','10.67','Germany')

INSERT INTO CityEntitie VALUES('Jizzax','40.1004','67.83','Uzbekistan')

INSERT INTO CityEntitie VALUES('Palmas','-10.2377','-48.2878','Brazil')

INSERT INTO CityEntitie VALUES('Boa Vista','2.8161','-60.666','Brazil')

INSERT INTO CityEntitie VALUES('Kediri','-7.7896','112','Indonesia')

INSERT INTO CityEntitie VALUES('McKinney','33.2016','-96.6669','United States')

INSERT INTO CityEntitie VALUES('Fremont','37.5265','-121.9852','United States')

INSERT INTO CityEntitie VALUES('Kismaayo','-0.3566','42.5183','Somalia')

INSERT INTO CityEntitie VALUES('Ibb','13.9759','44.1709','Yemen')

INSERT INTO CityEntitie VALUES('T?rkmenabat','39.11','63.58','Turkmenistan')

INSERT INTO CityEntitie VALUES('Punto Fijo','11.72','-70.21','Venezuela')

INSERT INTO CityEntitie VALUES('Porbandar','21.67','69.67','India')

INSERT INTO CityEntitie VALUES('Visalia','36.3273','-119.3264','United States')

INSERT INTO CityEntitie VALUES('Zacatecas','22.7704','-102.58','Mexico')

INSERT INTO CityEntitie VALUES('Santa Ana','13.9946','-89.5598','El Salvador')

INSERT INTO CityEntitie VALUES('Santa Ana','-13.76','-65.58','Bolivia')

INSERT INTO CityEntitie VALUES('Singaraja','-8.1152','115.0944','Indonesia')

INSERT INTO CityEntitie VALUES('Dili','-8.5594','125.5795','Timor-Leste')

INSERT INTO CityEntitie VALUES('Nizhnekamsk','55.6404','51.82','Russia')

INSERT INTO CityEntitie VALUES('York','39.9651','-76.7315','United States')

INSERT INTO CityEntitie VALUES('Zabol','31.0215','61.4815','Iran')

INSERT INTO CityEntitie VALUES('Rancagua','-34.17','-70.74','Chile')

INSERT INTO CityEntitie VALUES('Clermont-Ferrand','45.78','3.08','France')

INSERT INTO CityEntitie VALUES('Kennewick','46.1979','-119.1732','United States')

INSERT INTO CityEntitie VALUES('Kremenchuk','49.0835','33.4296','Ukraine')

INSERT INTO CityEntitie VALUES('Maradi','13.4916','7.0964','Niger')

INSERT INTO CityEntitie VALUES('Ploie?ti','44.9469','26.0365','Romania')

INSERT INTO CityEntitie VALUES('Nakhon Si Thammarat','8.4','99.97','Thailand')

INSERT INTO CityEntitie VALUES('Hetauda','27.4167','85.0334','Nepal')

INSERT INTO CityEntitie VALUES('Evansville','37.9882','-87.5339','United States')

INSERT INTO CityEntitie VALUES('Saidpur','25.8004','89','Bangladesh')

INSERT INTO CityEntitie VALUES('Al Fallujah','33.3477','43.7773','Iraq')

INSERT INTO CityEntitie VALUES('Shishou','29.7004','112.4','China')

INSERT INTO CityEntitie VALUES('Ngaoundéré','7.3204','13.58','Cameroon')

INSERT INTO CityEntitie VALUES('Debrecen','47.5305','21.63','Hungary')

INSERT INTO CityEntitie VALUES('Osnabr?ck','52.2804','8.05','Germany')

INSERT INTO CityEntitie VALUES('Nashua','42.7491','-71.491','United States')

INSERT INTO CityEntitie VALUES('Hailar','49.2','119.7','China')

INSERT INTO CityEntitie VALUES('Syktyvkar','61.66','50.82','Russia')

INSERT INTO CityEntitie VALUES('Zhangye','38.93','100.45','China')

INSERT INTO CityEntitie VALUES('Ilhéus','-14.78','-39.05','Brazil')

INSERT INTO CityEntitie VALUES('Santarém','-2.4333','-54.7','Brazil')

INSERT INTO CityEntitie VALUES('Zinder','13.8','8.9833','Niger')

INSERT INTO CityEntitie VALUES('Nukus','42.47','59.615','Uzbekistan')

INSERT INTO CityEntitie VALUES('Magdeburg','52.1304','11.62','Germany')

INSERT INTO CityEntitie VALUES('Tlimcen','34.8904','-1.32','Algeria')

INSERT INTO CityEntitie VALUES('Noginsk','55.8704','38.48','Russia')

INSERT INTO CityEntitie VALUES('Noginsk','64.4833','91.2333','Russia')

INSERT INTO CityEntitie VALUES('Nawabshah','26.2454','68.4','Pakistan')

INSERT INTO CityEntitie VALUES('Bharatpur','27.2504','77.5','India')

INSERT INTO CityEntitie VALUES('Uitenhage','-33.7596','25.39','South Africa')

INSERT INTO CityEntitie VALUES('Beipiao','41.81','120.76','China')

INSERT INTO CityEntitie VALUES('Er Rachidia','31.9404','-4.45','Morocco')

INSERT INTO CityEntitie VALUES('Petropavlovsk','54.88','69.22','Kazakhstan')

INSERT INTO CityEntitie VALUES('Miri','4.3999','113.9845','Malaysia')

INSERT INTO CityEntitie VALUES('Nassau','25.0834','-77.35','Bahamas')

INSERT INTO CityEntitie VALUES('Brownsville','25.998','-97.4565','United States')

INSERT INTO CityEntitie VALUES('Iquique','-20.25','-70.13','Chile')

INSERT INTO CityEntitie VALUES('Hyeson','41.3927','128.1927','Korea')

INSERT INTO CityEntitie VALUES('Pondicherry','11.935','79.83','India')

INSERT INTO CityEntitie VALUES('Quillacollo','-17.4','-66.28','Bolivia')

INSERT INTO CityEntitie VALUES('Starsy Oskol','51.3004','37.84','Russia')

INSERT INTO CityEntitie VALUES('Liaocheng','36.4304','115.97','China')

INSERT INTO CityEntitie VALUES('Gusau','12.1704','6.66','Nigeria')

INSERT INTO CityEntitie VALUES('Tanjungpinang','0.9168','104.4714','Indonesia')

INSERT INTO CityEntitie VALUES('Chon Buri','13.4004','101','Thailand')

INSERT INTO CityEntitie VALUES('Chuncheon','37.8747','127.7342','Korea')

INSERT INTO CityEntitie VALUES('Monywa','22.105','95.15','Burma')

INSERT INTO CityEntitie VALUES('Sabzewar','36.22','57.63','Iran')

INSERT INTO CityEntitie VALUES('Groznyy','43.3187','45.6987','Russia')

INSERT INTO CityEntitie VALUES('Kolpino','59.73','30.65','Russia')

INSERT INTO CityEntitie VALUES('Sikasso','11.3204','-5.68','Mali')

INSERT INTO CityEntitie VALUES('Mubi','10.2703','13.27','Nigeria')

INSERT INTO CityEntitie VALUES('Novi Sad','45.2504','19.8499','Serbia')

INSERT INTO CityEntitie VALUES('Juazeiro do Norte','-7.21','-39.32','Brazil')

INSERT INTO CityEntitie VALUES('Nyanza','-2.3496','29.74','Rwanda')

INSERT INTO CityEntitie VALUES('Skikda','36.8804','6.9','Algeria')

INSERT INTO CityEntitie VALUES('Karnal','29.6804','76.97','India')

INSERT INTO CityEntitie VALUES('B?c Liêu','9.2804','105.72','Vietnam')

INSERT INTO CityEntitie VALUES('Matsumoto','36.2404','137.97','Japan')

INSERT INTO CityEntitie VALUES('Tanga','-5.07','39.09','Tanzania')

INSERT INTO CityEntitie VALUES('Sucre','-19.041','-65.2595','Bolivia')

INSERT INTO CityEntitie VALUES('Nacala','-14.5186','40.715','Mozambique')

INSERT INTO CityEntitie VALUES('Vitoria','42.85','-2.67','Spain')

INSERT INTO CityEntitie VALUES('Nagercoil','8.1804','77.43','India')

INSERT INTO CityEntitie VALUES('Nicosia','35.1667','33.3666','Cyprus')

INSERT INTO CityEntitie VALUES('Haeju','38.0394','125.7144','Korea')

INSERT INTO CityEntitie VALUES('Ad?yaman','37.7704','38.2799','Turkey')

INSERT INTO CityEntitie VALUES('Okara','30.8104','73.45','Pakistan')

INSERT INTO CityEntitie VALUES('Narayanganj','23.6204','90.5','Bangladesh')

INSERT INTO CityEntitie VALUES('Colima','19.23','-103.72','Mexico')

INSERT INTO CityEntitie VALUES('Appleton','44.2774','-88.3894','United States')

INSERT INTO CityEntitie VALUES('Hancheng','35.4704','110.43','China')

INSERT INTO CityEntitie VALUES('Arar','30.99','41.0207','Saudi Arabia')

INSERT INTO CityEntitie VALUES('Itabuna','-14.7896','-39.28','Brazil')

INSERT INTO CityEntitie VALUES('Banja Luka','44.7804','17.18','Bosnia And Herzegovina')

INSERT INTO CityEntitie VALUES('Gueckedou','8.554','-10.151','Guinea')

INSERT INTO CityEntitie VALUES('Neyshabur','36.22','58.82','Iran')

INSERT INTO CityEntitie VALUES('Amol','36.4713','52.3633','Iran')

INSERT INTO CityEntitie VALUES('Tapachula','14.9','-92.27','Mexico')

INSERT INTO CityEntitie VALUES('Formosa','-26.1728','-58.1828','Argentina')

INSERT INTO CityEntitie VALUES('Gulfport','30.4271','-89.0703','United States')

INSERT INTO CityEntitie VALUES('Shakhty','47.7204','40.27','Russia')

INSERT INTO CityEntitie VALUES('Blagoveshchensk','50.2666','127.5333','Russia')

INSERT INTO CityEntitie VALUES('C?rdoba','18.9204','-96.92','Mexico')

INSERT INTO CityEntitie VALUES('Babruysk','53.1266','29.1928','Belarus')

INSERT INTO CityEntitie VALUES('Cabanatuan','15.5021','120.9617','Philippines')

INSERT INTO CityEntitie VALUES('Polokwane','-23.89','29.45','South Africa')

INSERT INTO CityEntitie VALUES('Lhasa','29.645','91.1','China')

INSERT INTO CityEntitie VALUES('Thanjavur','10.7704','79.15','India')

INSERT INTO CityEntitie VALUES('Gabès','33.9004','10.1','Tunisia')

INSERT INTO CityEntitie VALUES('Baghlan','36.1393','68.6993','Afghanistan')

INSERT INTO CityEntitie VALUES('Thousand Oaks','34.1914','-118.8755','United States')

INSERT INTO CityEntitie VALUES('Velikiy Novgorod','58.5','31.33','Russia')

INSERT INTO CityEntitie VALUES('Wuhai','39.6647','106.8122','China')

INSERT INTO CityEntitie VALUES('Dodoma','-6.1833','35.75','Tanzania')

INSERT INTO CityEntitie VALUES('Bertoua','4.5804','13.68','Cameroon')

INSERT INTO CityEntitie VALUES('Imperatriz','-5.52','-47.49','Brazil')

INSERT INTO CityEntitie VALUES('Potsdam','52.4004','13.07','Germany')

INSERT INTO CityEntitie VALUES('San Pedro de Macor?s','18.4504','-69.3','Dominican Republic')

INSERT INTO CityEntitie VALUES('Mallawi','27.7304','30.84','Egypt')

INSERT INTO CityEntitie VALUES('Sibolga','1.75','98.8','Indonesia')

INSERT INTO CityEntitie VALUES('Orléans','47.9004','1.9','France')

INSERT INTO CityEntitie VALUES('Denow','38.2772','67.8872','Uzbekistan')

INSERT INTO CityEntitie VALUES('Avondale','33.3858','-112.3236','United States')

INSERT INTO CityEntitie VALUES('Colombo','6.932','79.8578','Sri Lanka')

INSERT INTO CityEntitie VALUES('San Bernardino','34.1412','-117.2936','United States')

INSERT INTO CityEntitie VALUES('Gilgit','35.9171','74.3','Pakistan')

INSERT INTO CityEntitie VALUES('Rybinsk','58.0503','38.82','Russia')

INSERT INTO CityEntitie VALUES('Groningen','53.2204','6.58','Netherlands')

INSERT INTO CityEntitie VALUES('Bremerton','47.5436','-122.7122','United States')

INSERT INTO CityEntitie VALUES('Roanoke','37.2785','-79.958','United States')

INSERT INTO CityEntitie VALUES('Trieste','45.6504','13.8','Italy')

INSERT INTO CityEntitie VALUES('Kielce','50.8904','20.66','Poland')

INSERT INTO CityEntitie VALUES('Mulhouse','47.7504','7.35','France')

INSERT INTO CityEntitie VALUES('Biysk','52.5341','85.18','Russia')

INSERT INTO CityEntitie VALUES('Hickory','35.7427','-81.3233','United States')

INSERT INTO CityEntitie VALUES('Owerri','5.493','7.026','Nigeria')

INSERT INTO CityEntitie VALUES('Zamora','19.9804','-102.28','Mexico')

INSERT INTO CityEntitie VALUES('Navoi','40.1104','65.355','Uzbekistan')

INSERT INTO CityEntitie VALUES('Split','43.5204','16.47','Croatia')

INSERT INTO CityEntitie VALUES('Kalamazoo','42.2749','-85.5881','United States')

INSERT INTO CityEntitie VALUES('Oradea','47.05','21.92','Romania')

INSERT INTO CityEntitie VALUES('Lutsk','50.7472','25.3334','Ukraine')

INSERT INTO CityEntitie VALUES('Portoviejo','-1.06','-80.46','Ecuador')

INSERT INTO CityEntitie VALUES('Bergen','60.391','5.3245','Norway')

INSERT INTO CityEntitie VALUES('Br?ila','45.292','27.969','Romania')

INSERT INTO CityEntitie VALUES('Tacoma','47.2431','-122.4531','United States')

INSERT INTO CityEntitie VALUES('Nanping','26.6304','118.17','China')

INSERT INTO CityEntitie VALUES('Machala','-3.26','-79.96','Ecuador')

INSERT INTO CityEntitie VALUES('Limbe','4.0304','9.19','Cameroon')

INSERT INTO CityEntitie VALUES('Mar?lia','-22.21','-49.95','Brazil')

INSERT INTO CityEntitie VALUES('Fontana','34.0968','-117.4599','United States')

INSERT INTO CityEntitie VALUES('K?r?kkale','39.8504','33.53','Turkey')

INSERT INTO CityEntitie VALUES('Abha','18.2301','42.5001','Saudi Arabia')

INSERT INTO CityEntitie VALUES('San-Pedro','4.7704','-6.64','Côte DIvoire')

INSERT INTO CityEntitie VALUES('Toamasina','-18.1818','49.405','Madagascar')

INSERT INTO CityEntitie VALUES('Pali','25.7904','73.3299','India')

INSERT INTO CityEntitie VALUES('Th?i B?nh','20.4503','106.333','Vietnam')

INSERT INTO CityEntitie VALUES('Oral','51.2711','51.335','Kazakhstan')

INSERT INTO CityEntitie VALUES('Presidente Prudente','-22.12','-51.39','Brazil')

INSERT INTO CityEntitie VALUES('Sanming','26.23','117.58','China')

INSERT INTO CityEntitie VALUES('Rennes','48.1','-1.67','France')

INSERT INTO CityEntitie VALUES('Ijebu Ode','6.8204','3.92','Nigeria')

INSERT INTO CityEntitie VALUES('Tema','5.6404','0.01','Ghana')

INSERT INTO CityEntitie VALUES('Santander','43.3805','-3.8','Spain')

INSERT INTO CityEntitie VALUES('Green Bay','44.515','-87.9896','United States')

INSERT INTO CityEntitie VALUES('Sidi bel Abbes','35.1903','-0.64','Algeria')

INSERT INTO CityEntitie VALUES('Bojnurd','37.47','57.32','Iran')

INSERT INTO CityEntitie VALUES('La Romana','18.417','-68.9666','Dominican Republic')

INSERT INTO CityEntitie VALUES('Gaborone','-24.6463','25.9119','Botswana')

INSERT INTO CityEntitie VALUES('Hajjah','15.6917','43.6021','Yemen')

INSERT INTO CityEntitie VALUES('Waitakere','-36.8524','174.5543','New Zealand')

INSERT INTO CityEntitie VALUES('Lobito','-12.37','13.5412','Angola')

INSERT INTO CityEntitie VALUES('College Station','30.5852','-96.296','United States')

INSERT INTO CityEntitie VALUES('Pingliang','35.5304','106.6801','China')

INSERT INTO CityEntitie VALUES('Moreno Valley','33.9244','-117.2045','United States')

INSERT INTO CityEntitie VALUES('Laoag','18.1988','120.5936','Philippines')

INSERT INTO CityEntitie VALUES('Bahir Dar','11.6001','37.3833','Ethiopia')

INSERT INTO CityEntitie VALUES('Yamoussoukro','6.8184','-5.2755','Côte DIvoire')

INSERT INTO CityEntitie VALUES('Beer Sheva','31.25','34.83','Israel')

INSERT INTO CityEntitie VALUES('Korla','41.73','86.15','China')

INSERT INTO CityEntitie VALUES('Salzburg','47.8105','13.04','Austria')

INSERT INTO CityEntitie VALUES('Kalemie','-5.9333','29.2','Congo (Kinshasa)')

INSERT INTO CityEntitie VALUES('Amarillo','35.1988','-101.8308','United States')

INSERT INTO CityEntitie VALUES('Portland','43.6773','-70.2715','United States')

INSERT INTO CityEntitie VALUES('North Shore','-36.7913','174.7758','New Zealand')

INSERT INTO CityEntitie VALUES('Jiutai','44.1447','125.8443','China')

INSERT INTO CityEntitie VALUES('Campeche','19.83','-90.5','Mexico')

INSERT INTO CityEntitie VALUES('Puqi','29.7204','113.88','China')

INSERT INTO CityEntitie VALUES('S?o Carlos','-22.02','-47.89','Brazil')

INSERT INTO CityEntitie VALUES('Fargo','46.8653','-96.8292','United States')

INSERT INTO CityEntitie VALUES('Funchal','32.65','-16.88','Portugal')

INSERT INTO CityEntitie VALUES('Crici?ma','-28.68','-49.39','Brazil')

INSERT INTO CityEntitie VALUES('Lampang','18.2916','99.4813','Thailand')

INSERT INTO CityEntitie VALUES('Santa Barbara','34.4285','-119.7202','United States')

INSERT INTO CityEntitie VALUES('Col?n','9.365','-79.875','Panama')

INSERT INTO CityEntitie VALUES('Norwich','41.5495','-72.0882','United States')

INSERT INTO CityEntitie VALUES('Salalah','17.0255','54.0852','Oman')

INSERT INTO CityEntitie VALUES('Sibu','2.303','111.843','Malaysia')

INSERT INTO CityEntitie VALUES('Las Tunas','20.9601','-76.9544','Cuba')

INSERT INTO CityEntitie VALUES('Lianxian','24.7815','112.3825','China')

INSERT INTO CityEntitie VALUES('Guaratinguet?','-22.82','-45.19','Brazil')

INSERT INTO CityEntitie VALUES('Pisa','43.7205','10.4','Italy')

INSERT INTO CityEntitie VALUES('Agartala','23.8354','91.28','India')

INSERT INTO CityEntitie VALUES('Erfurt','50.9701','11.03','Germany')

INSERT INTO CityEntitie VALUES('Timon','-5.115','-42.845','Brazil')

INSERT INTO CityEntitie VALUES('Edéa','3.8005','10.12','Cameroon')

INSERT INTO CityEntitie VALUES('Rostock','54.0704','12.15','Germany')

INSERT INTO CityEntitie VALUES('Glendale','34.1818','-118.2468','United States')

INSERT INTO CityEntitie VALUES('Ongole','15.5604','80.05','India')

INSERT INTO CityEntitie VALUES('Djougou','9.7004','1.68','Benin')

INSERT INTO CityEntitie VALUES('Khvoy','38.5304','44.97','Iran')

INSERT INTO CityEntitie VALUES('Mossor?','-5.19','-37.34','Brazil')

INSERT INTO CityEntitie VALUES('Barisal','22.7004','90.375','Bangladesh')

INSERT INTO CityEntitie VALUES('Biskra','34.86','5.73','Algeria')

INSERT INTO CityEntitie VALUES('Angren','41.0304','70.1549','Uzbekistan')

INSERT INTO CityEntitie VALUES('Taranto','40.5084','17.23','Italy')

INSERT INTO CityEntitie VALUES('Yonkers','40.9466','-73.8674','United States')

INSERT INTO CityEntitie VALUES('Pskov','57.83','28.3299','Russia')

INSERT INTO CityEntitie VALUES('Al Khums','32.6604','14.26','Libya')

INSERT INTO CityEntitie VALUES('Minatitl?n','17.9805','-94.53','Mexico')

INSERT INTO CityEntitie VALUES('Huntington Beach','33.696','-118.0025','United States')

INSERT INTO CityEntitie VALUES('Chiniot','31.72','72.98','Pakistan')

INSERT INTO CityEntitie VALUES('Pingzhen','24.9439','121.2161','Taiwan')

INSERT INTO CityEntitie VALUES('San Miguel','13.4833','-88.1833','El Salvador')

INSERT INTO CityEntitie VALUES('Lashkar Gah','31.583','64.36','Afghanistan')

INSERT INTO CityEntitie VALUES('Da?oguz','41.84','59.965','Turkmenistan')

INSERT INTO CityEntitie VALUES('Sete Lagoas','-19.4496','-44.25','Brazil')

INSERT INTO CityEntitie VALUES('Gedaref','14.04','35.38','Sudan')

INSERT INTO CityEntitie VALUES('Cartagena','37.6004','-0.98','Spain')

INSERT INTO CityEntitie VALUES('Puri','19.8204','85.9','India')

INSERT INTO CityEntitie VALUES('Aurora','41.7638','-88.2901','United States')

INSERT INTO CityEntitie VALUES('Dindigul','10.38','78','India')

INSERT INTO CityEntitie VALUES('Haldia','22.0257','88.0583','India')

INSERT INTO CityEntitie VALUES('Kasama','-10.1996','31.1799','Zambia')

INSERT INTO CityEntitie VALUES('Moratuwa','6.7804','79.88','Sri Lanka')

INSERT INTO CityEntitie VALUES('Az Zawiyah','32.7604','12.72','Libya')

INSERT INTO CityEntitie VALUES('Nema','16.6171','-7.25','Mauritania')

INSERT INTO CityEntitie VALUES('Pokhara','28.264','83.972','Nepal')

INSERT INTO CityEntitie VALUES('Zhubei','24.8333','121.0119','Taiwan')

INSERT INTO CityEntitie VALUES('Rivera','-30.8996','-55.56','Uruguay')

INSERT INTO CityEntitie VALUES('Bergamo','45.7004','9.67','Italy')

INSERT INTO CityEntitie VALUES('Meymaneh','35.9302','64.7701','Afghanistan')

INSERT INTO CityEntitie VALUES('Gainesville','29.6808','-82.3455','United States')

INSERT INTO CityEntitie VALUES('Bac?u','46.5784','26.9196','Romania')

INSERT INTO CityEntitie VALUES('Taza','34.2204','-4.02','Morocco')

INSERT INTO CityEntitie VALUES('Batu Pahat','1.8504','102.9333','Malaysia')

INSERT INTO CityEntitie VALUES('Balakovo','52.03','47.8','Russia')

INSERT INTO CityEntitie VALUES('Armavir','45.0004','41.13','Russia')

INSERT INTO CityEntitie VALUES('Rashid','31.4604','30.39','Egypt')

INSERT INTO CityEntitie VALUES('Bila Tserkva','49.7743','30.1309','Ukraine')

INSERT INTO CityEntitie VALUES('Saskatoon','52.17','-106.67','Canada')

INSERT INTO CityEntitie VALUES('Ayd?n','37.85','27.85','Turkey')

INSERT INTO CityEntitie VALUES('Huangyan','28.65','121.25','China')

INSERT INTO CityEntitie VALUES('Bulandshahr','28.4104','77.8484','India')

INSERT INTO CityEntitie VALUES('Kushiro','42.975','144.3747','Japan')

INSERT INTO CityEntitie VALUES('Purnia','25.7854','87.48','India')

INSERT INTO CityEntitie VALUES('Martapura','-3.4135','114.8365','Indonesia')

INSERT INTO CityEntitie VALUES('Gemena','3.26','19.77','Congo (Kinshasa)')

INSERT INTO CityEntitie VALUES('Thanh H?a','19.82','105.8','Vietnam')

INSERT INTO CityEntitie VALUES('Mbanza-Ngungu','-5.2496','14.86','Congo (Kinshasa)')

INSERT INTO CityEntitie VALUES('K?fu','35.6504','138.5833','Japan')

INSERT INTO CityEntitie VALUES('Salamanca','20.5704','-101.2','Mexico')

INSERT INTO CityEntitie VALUES('Talca','-35.455','-71.67','Chile')

INSERT INTO CityEntitie VALUES('Proddatur','14.7504','78.57','India')

INSERT INTO CityEntitie VALUES('El Tigre','8.8903','-64.26','Venezuela')

INSERT INTO CityEntitie VALUES('Gurgaon','28.45','77.02','India')

INSERT INTO CityEntitie VALUES('Burhanpur','21.3004','76.13','India')

INSERT INTO CityEntitie VALUES('Kure','34.251','132.5656','Japan')

INSERT INTO CityEntitie VALUES('Cartago','9.87','-83.93','Costa Rica')

INSERT INTO CityEntitie VALUES('Porlamar','10.9604','-63.85','Venezuela')

INSERT INTO CityEntitie VALUES('Reims','49.2504','4.03','France')

INSERT INTO CityEntitie VALUES('Olympia','47.0418','-122.8959','United States')

INSERT INTO CityEntitie VALUES('Engels','51.5004','46.12','Russia')

INSERT INTO CityEntitie VALUES('North Port','27.0577','-82.1976','United States')

INSERT INTO CityEntitie VALUES('Burgas','42.5146','27.4746','Bulgaria')

INSERT INTO CityEntitie VALUES('Gandhinagar','23.3004','72.6399','India')

INSERT INTO CityEntitie VALUES('Iskandar','41.5507','69.6807','Uzbekistan')

INSERT INTO CityEntitie VALUES('Nagaoka','37.4504','138.86','Japan')

INSERT INTO CityEntitie VALUES('Coro','11.42','-69.68','Venezuela')

INSERT INTO CityEntitie VALUES('Brikama','13.2804','-16.6599','Gambia')

INSERT INTO CityEntitie VALUES('Severodvinsk','64.57','39.83','Russia')

INSERT INTO CityEntitie VALUES('Al Jahra','29.3375','47.6581','Kuwait')

INSERT INTO CityEntitie VALUES('Huntington','38.4109','-82.4344','United States')

INSERT INTO CityEntitie VALUES('Deltona','28.905','-81.2136','United States')

INSERT INTO CityEntitie VALUES('Bongor','10.2859','15.3872','Chad')

INSERT INTO CityEntitie VALUES('Grand Prairie','32.6869','-97.0209','United States')

INSERT INTO CityEntitie VALUES('Divin?polis','-20.1495','-44.9','Brazil')

INSERT INTO CityEntitie VALUES('Salinas','36.6881','-121.6316','United States')

INSERT INTO CityEntitie VALUES('Tarakan','3.3','117.633','Indonesia')

INSERT INTO CityEntitie VALUES('Machilipatnam','16.2004','81.18','India')

INSERT INTO CityEntitie VALUES('Oxford','51.7704','-1.25','United Kingdom')

INSERT INTO CityEntitie VALUES('Ciudad Madero','22.3189','-97.8361','Mexico')

INSERT INTO CityEntitie VALUES('Bayamo','20.3795','-76.6433','Cuba')

INSERT INTO CityEntitie VALUES('Klaip?da','55.7204','21.1199','Lithuania')

INSERT INTO CityEntitie VALUES('Dunhua','43.3505','128.22','China')

INSERT INTO CityEntitie VALUES('Ziguinchor','12.59','-16.29','Senegal')

INSERT INTO CityEntitie VALUES('Waterbury','41.5583','-73.0361','United States')

INSERT INTO CityEntitie VALUES('Barrancabermeja','7.09','-73.85','Colombia')

INSERT INTO CityEntitie VALUES('Zlatoust','55.175','59.65','Russia')

INSERT INTO CityEntitie VALUES('Muar','2.0337','102.5666','Malaysia')

INSERT INTO CityEntitie VALUES('Overland Park','38.887','-94.687','United States')

INSERT INTO CityEntitie VALUES('Dhamar','14.5575','44.3875','Yemen')

INSERT INTO CityEntitie VALUES('Valera','9.32','-70.62','Venezuela')

INSERT INTO CityEntitie VALUES('Bridgetown','13.102','-59.6165','Barbados')

INSERT INTO CityEntitie VALUES('Bhiwani','28.81','76.125','India')

INSERT INTO CityEntitie VALUES('Norwich','52.6304','1.3','United Kingdom')

INSERT INTO CityEntitie VALUES('Butuan','8.9495','125.5436','Philippines')

INSERT INTO CityEntitie VALUES('Caen','49.1838','-0.35','France')

INSERT INTO CityEntitie VALUES('St.-Denis','-20.8789','55.4481','Reunion')

INSERT INTO CityEntitie VALUES('Sadiqabad','28.3006','70.1302','Pakistan')

INSERT INTO CityEntitie VALUES('La Paz','24.14','-110.32','Mexico')

INSERT INTO CityEntitie VALUES('S?n Tây','21.1382','105.505','Vietnam')

INSERT INTO CityEntitie VALUES('Aberdeen','57.1704','-2.08','United Kingdom')

INSERT INTO CityEntitie VALUES('Syzran','53.17','48.48','Russia')

INSERT INTO CityEntitie VALUES('??ng H?i','17.4833','106.6','Vietnam')

INSERT INTO CityEntitie VALUES('Mwene-Ditu','-7','23.44','Congo (Kinshasa)')

INSERT INTO CityEntitie VALUES('Kabwe','-14.44','28.45','Zambia')

INSERT INTO CityEntitie VALUES('Quelimane','-17.88','36.89','Mozambique')

INSERT INTO CityEntitie VALUES('Cork','51.8986','-8.4958','Ireland')

INSERT INTO CityEntitie VALUES('Parakou','9.34','2.62','Benin')

INSERT INTO CityEntitie VALUES('Catamarca','-28.47','-65.78','Argentina')

INSERT INTO CityEntitie VALUES('Nandyal','15.5204','78.48','India')

INSERT INTO CityEntitie VALUES('Waco','31.5597','-97.1883','United States')

INSERT INTO CityEntitie VALUES('Angers','47.48','-0.53','France')

INSERT INTO CityEntitie VALUES('Mostaganem','35.9404','0.09','Algeria')

INSERT INTO CityEntitie VALUES('Erie','42.1168','-80.0733','United States')

INSERT INTO CityEntitie VALUES('Paarl','-33.6996','18.96','South Africa')

INSERT INTO CityEntitie VALUES('Rio Grande','-32.0495','-52.12','Brazil')

INSERT INTO CityEntitie VALUES('Szeged','46.2504','20.15','Hungary')

INSERT INTO CityEntitie VALUES('Arapiraca','-9.75','-36.67','Brazil')

INSERT INTO CityEntitie VALUES('Ternate','0.793','127.363','Indonesia')

INSERT INTO CityEntitie VALUES('Petropavlovsk Kamchatskiy','53.062','158.623','Russia')

INSERT INTO CityEntitie VALUES('Kimchaek','40.6723','129.2027','Korea')

INSERT INTO CityEntitie VALUES('Puerto Vallarta','20.6771','-105.245','Mexico')

INSERT INTO CityEntitie VALUES('Gibraltar','36.1324','-5.3781','Gibraltar')

INSERT INTO CityEntitie VALUES('Cachoeiro de Itapemirim','-20.85','-41.13','Brazil')

INSERT INTO CityEntitie VALUES('Pinar del Rio','22.4175','-83.6981','Cuba')

INSERT INTO CityEntitie VALUES('Manpo','41.1454','126.2958','Korea')

INSERT INTO CityEntitie VALUES('Olsztyn','53.8','20.48','Poland')

INSERT INTO CityEntitie VALUES('San Cristobal de Las Casas','16.75','-92.6334','Mexico')

INSERT INTO CityEntitie VALUES('Cienfuegos','22.1444','-80.4403','Cuba')

INSERT INTO CityEntitie VALUES('Shendi','16.6805','33.42','Sudan')

INSERT INTO CityEntitie VALUES('Pécs','46.0804','18.22','Hungary')

INSERT INTO CityEntitie VALUES('Cedar Rapids','41.9667','-91.6782','United States')

INSERT INTO CityEntitie VALUES('Marbella','36.5166','-4.8833','Spain')

INSERT INTO CityEntitie VALUES('Madiun','-7.6346','111.515','Indonesia')

INSERT INTO CityEntitie VALUES('Ad Damaz?n','11.7704','34.35','Sudan')

INSERT INTO CityEntitie VALUES('Arica','-18.5','-70.29','Chile')

INSERT INTO CityEntitie VALUES('Hagerstown','39.6401','-77.7217','United States')

INSERT INTO CityEntitie VALUES('Jamaame','0.0722','42.7506','Somalia')

INSERT INTO CityEntitie VALUES('Tempe','33.3881','-111.9318','United States')

INSERT INTO CityEntitie VALUES('K?tahya','39.42','29.93','Turkey')

INSERT INTO CityEntitie VALUES('Yei','4.0904','30.68','South Sudan')

INSERT INTO CityEntitie VALUES('Mainz','49.9825','8.2732','Germany')

INSERT INTO CityEntitie VALUES('Ruse','43.8537','25.9733','Bulgaria')

INSERT INTO CityEntitie VALUES('Tiarat','35.3804','1.32','Algeria')

INSERT INTO CityEntitie VALUES('Fianarantsoa','-21.4333','47.0833','Madagascar')

INSERT INTO CityEntitie VALUES('Kramatorsk','48.7194','37.5344','Ukraine')

INSERT INTO CityEntitie VALUES('Kutaisi','42.25','42.73','Georgia')

INSERT INTO CityEntitie VALUES('Tarlac','15.4838','120.5834','Philippines')

INSERT INTO CityEntitie VALUES('Dese','11.13','39.63','Ethiopia')

INSERT INTO CityEntitie VALUES('Corum','40.52','34.95','Turkey')

INSERT INTO CityEntitie VALUES('Gweru','-19.45','29.82','Zimbabwe')

INSERT INTO CityEntitie VALUES('Matar?','41.54','2.45','Spain')

INSERT INTO CityEntitie VALUES('Spartanburg','34.9437','-81.9257','United States')

INSERT INTO CityEntitie VALUES('Manta','-0.98','-80.73','Ecuador')

INSERT INTO CityEntitie VALUES('Bhusawal','21.02','75.83','India')

INSERT INTO CityEntitie VALUES('Shibin el Kom','30.592','30.9','Egypt')

INSERT INTO CityEntitie VALUES('Clarksville','36.5696','-87.3428','United States')

INSERT INTO CityEntitie VALUES('Atyrau','47.1127','51.92','Kazakhstan')

INSERT INTO CityEntitie VALUES('Pinrang','-3.7857','119.6522','Indonesia')

INSERT INTO CityEntitie VALUES('Kamensk Uralskiy','56.4205','61.935','Russia')

INSERT INTO CityEntitie VALUES('Chilpancingo','17.55','-99.5','Mexico')

INSERT INTO CityEntitie VALUES('Biratnagar','26.4837','87.2833','Nepal')

INSERT INTO CityEntitie VALUES('Bahraich','27.6204','81.6699','India')

INSERT INTO CityEntitie VALUES('Barrie','44.3838','-79.7','Canada')

INSERT INTO CityEntitie VALUES('Chingola','-12.5396','27.85','Zambia')

INSERT INTO CityEntitie VALUES('Tonk','26.1505','75.79','India')

INSERT INTO CityEntitie VALUES('Probolinggo','-7.7496','113.15','Indonesia')

INSERT INTO CityEntitie VALUES('Sirsa','29.4904','75.03','India')

INSERT INTO CityEntitie VALUES('Anda','46.4','125.32','China')

INSERT INTO CityEntitie VALUES('Gastonia','35.2494','-81.1855','United States')

INSERT INTO CityEntitie VALUES('Hinthada','17.6483','95.4679','Burma')

INSERT INTO CityEntitie VALUES('Gangneung','37.7559','128.8962','Korea')

INSERT INTO CityEntitie VALUES('Castello','39.9704','-0.05','Spain')

INSERT INTO CityEntitie VALUES('Funtua','11.5204','7.32','Nigeria')

INSERT INTO CityEntitie VALUES('El Jadida','33.2604','-8.51','Morocco')

INSERT INTO CityEntitie VALUES('Reggio di Calabria','38.115','15.6414','Italy')

INSERT INTO CityEntitie VALUES('Zuwarah','32.9344','12.0791','Libya')

INSERT INTO CityEntitie VALUES('Lorain','41.4409','-82.184','United States')

INSERT INTO CityEntitie VALUES('Sioux Falls','43.5397','-96.7322','United States')

INSERT INTO CityEntitie VALUES('Rio Claro','-22.41','-47.56','Brazil')

INSERT INTO CityEntitie VALUES('Tangail','24.25','89.92','Bangladesh')

INSERT INTO CityEntitie VALUES('Obuasi','6.1904','-1.66','Ghana')

INSERT INTO CityEntitie VALUES('Fort Lauderdale','26.1412','-80.1464','United States')

INSERT INTO CityEntitie VALUES('Zhaodong','46.0804','125.98','China')

INSERT INTO CityEntitie VALUES('Balkh','36.7501','66.8997','Afghanistan')

INSERT INTO CityEntitie VALUES('Potos?','-19.5696','-65.75','Bolivia')

INSERT INTO CityEntitie VALUES('Phan Rang','11.567','108.9833','Vietnam')

INSERT INTO CityEntitie VALUES('Varamin','35.3166','51.6466','Iran')

INSERT INTO CityEntitie VALUES('Passo Fundo','-28.25','-52.42','Brazil')

INSERT INTO CityEntitie VALUES('Almer?a','36.8303','-2.43','Spain')

INSERT INTO CityEntitie VALUES('Newport News','37.1051','-76.5185','United States')

INSERT INTO CityEntitie VALUES('Vizianagaram','18.1204','83.5','India')

INSERT INTO CityEntitie VALUES('Sittwe','20.14','92.88','Burma')

INSERT INTO CityEntitie VALUES('Klerksdorp','-26.88','26.62','South Africa')

INSERT INTO CityEntitie VALUES('Si Racha','13.159','100.9287','Thailand')

INSERT INTO CityEntitie VALUES('Mary','37.6','61.8333','Turkmenistan')

INSERT INTO CityEntitie VALUES('El Arish','31.1249','33.8006','Egypt')

INSERT INTO CityEntitie VALUES('Boma','-5.83','13.05','Congo (Kinshasa)')

INSERT INTO CityEntitie VALUES('Sangolqu?','-0.31','-78.46','Ecuador')

INSERT INTO CityEntitie VALUES('Nogales','31.305','-110.945','Mexico')

INSERT INTO CityEntitie VALUES('High Point','35.9902','-79.9938','United States')

INSERT INTO CityEntitie VALUES('Teziutl?n','19.8204','-97.36','Mexico')

INSERT INTO CityEntitie VALUES('Korhogo','9.46','-5.64','Côte DIvoire')

INSERT INTO CityEntitie VALUES('Termiz','37.2329','67.2729','Uzbekistan')

INSERT INTO CityEntitie VALUES('Ar Raqqah','35.9304','39.02','Syria')

INSERT INTO CityEntitie VALUES('Santa Cruz','36.9788','-122.0346','United States')

INSERT INTO CityEntitie VALUES('El Oued','33.3704','6.86','Algeria')

INSERT INTO CityEntitie VALUES('Rancho Cucamonga','34.1247','-117.5664','United States')

INSERT INTO CityEntitie VALUES('Treviso','45.67','12.24','Italy')

INSERT INTO CityEntitie VALUES('Frisco','33.1553','-96.8218','United States')

INSERT INTO CityEntitie VALUES('Danbury','41.4016','-73.471','United States')

INSERT INTO CityEntitie VALUES('Vellore','12.9204','79.15','India')

INSERT INTO CityEntitie VALUES('Kadugli','11.01','29.7','Sudan')

INSERT INTO CityEntitie VALUES('Hemet','33.7341','-116.9969','United States')

INSERT INTO CityEntitie VALUES('Ny?regyh?za','47.9653','21.7187','Hungary')

INSERT INTO CityEntitie VALUES('Alappuzha','9.5004','76.37','India')

INSERT INTO CityEntitie VALUES('Malayer','34.32','48.85','Iran')

INSERT INTO CityEntitie VALUES('Letpadan','17.7819','95.7415','Burma')

INSERT INTO CityEntitie VALUES('Yuzhno Sakhalinsk','46.965','142.74','Russia')

INSERT INTO CityEntitie VALUES('Ouargla','31.97','5.34','Algeria')

INSERT INTO CityEntitie VALUES('Oceanside','33.2247','-117.3083','United States')

INSERT INTO CityEntitie VALUES('Regina','50.45','-104.617','Canada')

INSERT INTO CityEntitie VALUES('Salatiga','-7.3095','110.4901','Indonesia')

INSERT INTO CityEntitie VALUES('Turku','60.4539','22.255','Finland')

INSERT INTO CityEntitie VALUES('Ontario','34.0393','-117.6064','United States')

INSERT INTO CityEntitie VALUES('Vancouver','45.6352','-122.597','United States')

INSERT INTO CityEntitie VALUES('Saveh','35.0218','50.3314','Iran')

INSERT INTO CityEntitie VALUES('Modena','44.65','10.92','Italy')

INSERT INTO CityEntitie VALUES('Les Cayes','18.2004','-73.75','Haiti')

INSERT INTO CityEntitie VALUES('Suva','-18.133','178.4417','Fiji')

INSERT INTO CityEntitie VALUES('Bose','23.8997','106.6133','China')

INSERT INTO CityEntitie VALUES('Entebbe','0.0604','32.46','Uganda')

INSERT INTO CityEntitie VALUES('Yangmei','24.9167','121.15','Taiwan')

INSERT INTO CityEntitie VALUES('Taitung','22.7554','121.14','Taiwan')

INSERT INTO CityEntitie VALUES('Sirjan','29.47','55.73','Iran')

INSERT INTO CityEntitie VALUES('Harar','9.32','42.15','Ethiopia')

INSERT INTO CityEntitie VALUES('As Samawah','31.3099','45.2803','Iraq')

INSERT INTO CityEntitie VALUES('Hirosaki','40.57','140.47','Japan')

INSERT INTO CityEntitie VALUES('Tomakomai','42.6504','141.55','Japan')

INSERT INTO CityEntitie VALUES('Puerto Montt','-41.47','-72.93','Chile')

INSERT INTO CityEntitie VALUES('George','-33.95','22.45','South Africa')

INSERT INTO CityEntitie VALUES('Bo','7.97','-11.74','Sierra Leone')

INSERT INTO CityEntitie VALUES('Garden Grove','33.7787','-117.9601','United States')

INSERT INTO CityEntitie VALUES('Puerto Cabello','10.4704','-68.17','Venezuela')

INSERT INTO CityEntitie VALUES('Saida','33.563','35.3688','Lebanon')

INSERT INTO CityEntitie VALUES('Obihiro','42.9304','143.17','Japan')

INSERT INTO CityEntitie VALUES('Nampa','43.5843','-116.5626','United States')

INSERT INTO CityEntitie VALUES('Bida','9.0804','6.01','Nigeria')

INSERT INTO CityEntitie VALUES('Vallejo','38.1133','-122.2359','United States')

INSERT INTO CityEntitie VALUES('Simla','31.1','77.1666','India')

INSERT INTO CityEntitie VALUES('Stavanger','58.97','5.68','Norway')

INSERT INTO CityEntitie VALUES('Esmeraldas','0.9304','-79.67','Ecuador')

INSERT INTO CityEntitie VALUES('Bata','1.87','9.77','Equatorial Guinea')

INSERT INTO CityEntitie VALUES('Ulm','48.4004','10','Germany')

INSERT INTO CityEntitie VALUES('Hong Gai','20.9604','107.1','Vietnam')

INSERT INTO CityEntitie VALUES('Lishui','28.4504','119.9','China')

INSERT INTO CityEntitie VALUES('Riobamba','-1.67','-78.65','Ecuador')

INSERT INTO CityEntitie VALUES('Banyuwangi','-8.195','114.3696','Indonesia')

INSERT INTO CityEntitie VALUES('Isparta','37.77','30.53','Turkey')

INSERT INTO CityEntitie VALUES('Weinan','34.5004','109.5001','China')

INSERT INTO CityEntitie VALUES('Bade','24.9575','121.2989','Taiwan')

INSERT INTO CityEntitie VALUES('Nova Friburgo','-22.26','-42.54','Brazil')

INSERT INTO CityEntitie VALUES('Elk Grove','38.416','-121.384','United States')

INSERT INTO CityEntitie VALUES('Tébessa','35.4104','8.12','Algeria')

INSERT INTO CityEntitie VALUES('Kragujevac','44.02','20.92','Serbia')

INSERT INTO CityEntitie VALUES('Pite?ti','44.8563','24.8758','Romania')

INSERT INTO CityEntitie VALUES('Rijeka','45.33','14.45','Croatia')

INSERT INTO CityEntitie VALUES('Djelfa','34.68','3.25','Algeria')

INSERT INTO CityEntitie VALUES('Pembroke Pines','26.0128','-80.3387','United States')

INSERT INTO CityEntitie VALUES('Temirtau','50.065','72.965','Kazakhstan')

INSERT INTO CityEntitie VALUES('Uvira','-3.37','29.14','Congo (Kinshasa)')

INSERT INTO CityEntitie VALUES('Burgos','42.3504','-3.68','Spain')

INSERT INTO CityEntitie VALUES('Takaoka','36.67','137','Japan')

INSERT INTO CityEntitie VALUES('Araçatuba','-21.21','-50.45','Brazil')

INSERT INTO CityEntitie VALUES('Fuyang','32.9004','115.82','China')

INSERT INTO CityEntitie VALUES('?a?ak','43.8897','20.3301','Serbia')

INSERT INTO CityEntitie VALUES('Dijon','47.3304','5.03','France')

INSERT INTO CityEntitie VALUES('Keluang','2.0383','103.3179','Malaysia')

INSERT INTO CityEntitie VALUES('Nîmes','43.8304','4.35','France')

INSERT INTO CityEntitie VALUES('Al Marj','32.5005','20.83','Libya')

INSERT INTO CityEntitie VALUES('Barra Mansa','-22.56','-44.17','Brazil')

INSERT INTO CityEntitie VALUES('Jayapura','-2.533','140.7','Indonesia')

INSERT INTO CityEntitie VALUES('Sarh','9.15','18.39','Chad')

INSERT INTO CityEntitie VALUES('Medinipur','22.3304','87.15','India')

INSERT INTO CityEntitie VALUES('Arad','46.17','21.32','Romania')

INSERT INTO CityEntitie VALUES('Bandar-e Bushehr','28.92','50.83','Iran')

INSERT INTO CityEntitie VALUES('Sonsonate','13.72','-89.73','El Salvador')

INSERT INTO CityEntitie VALUES('Baranavichy','53.1368','26.0134','Belarus')

INSERT INTO CityEntitie VALUES('Atbara','17.71','33.98','Sudan')

INSERT INTO CityEntitie VALUES('W?rzburg','49.8004','9.95','Germany')

INSERT INTO CityEntitie VALUES('Hindupur','13.7804','77.49','India')

INSERT INTO CityEntitie VALUES('Medford','42.3372','-122.8537','United States')

INSERT INTO CityEntitie VALUES('Peoria','33.7844','-112.2989','United States')

INSERT INTO CityEntitie VALUES('Nzérékoré','7.76','-8.83','Guinea')

INSERT INTO CityEntitie VALUES('Baramula','34.2004','74.35','India')

INSERT INTO CityEntitie VALUES('Jamalpur','24.9004','89.95','Bangladesh')

INSERT INTO CityEntitie VALUES('Chirchiq','41.455','69.56','Uzbekistan')

INSERT INTO CityEntitie VALUES('Corona','33.862','-117.5644','United States')

INSERT INTO CityEntitie VALUES('Berezniki','59.42','56.76','Russia')

INSERT INTO CityEntitie VALUES('Volgodonsk','47.51','42.1599','Russia')

INSERT INTO CityEntitie VALUES('Ormac','11.0643','124.6075','Philippines')

INSERT INTO CityEntitie VALUES('Miass','54.9954','60.0949','Russia')

INSERT INTO CityEntitie VALUES('Oldenburg','53.13','8.22','Germany')

INSERT INTO CityEntitie VALUES('Abakan','53.7037','91.445','Russia')

INSERT INTO CityEntitie VALUES('Murfreesboro','35.8493','-86.4098','United States')

INSERT INTO CityEntitie VALUES('Benha','30.4667','31.1833','Egypt')

INSERT INTO CityEntitie VALUES('Novocherkassk','47.42','40.08','Russia')

INSERT INTO CityEntitie VALUES('Gonaïves','19.4504','-72.6832','Haiti')

INSERT INTO CityEntitie VALUES('Marysville','48.0809','-122.1558','United States')

INSERT INTO CityEntitie VALUES('Fatehpur','25.8804','80.8','India')

INSERT INTO CityEntitie VALUES('Dibrugarh','27.4833','94.9','India')

INSERT INTO CityEntitie VALUES('Ayacucho','-13.175','-74.22','Peru')

INSERT INTO CityEntitie VALUES('Reykjav?k','64.15','-21.95','Iceland')

INSERT INTO CityEntitie VALUES('Marab?','-5.35','-49.116','Brazil')

INSERT INTO CityEntitie VALUES('Ocumare del Tuy','10.12','-66.78','Venezuela')

INSERT INTO CityEntitie VALUES('San Fernando','10.2805','-61.4594','Trinidad And Tobago')

INSERT INTO CityEntitie VALUES('Parma','44.8104','10.32','Italy')

INSERT INTO CityEntitie VALUES('Rustenburg','-25.65','27.24','South Africa')

INSERT INTO CityEntitie VALUES('Cary','35.7814','-78.8167','United States')

INSERT INTO CityEntitie VALUES('Norilsk','69.34','88.225','Russia')

INSERT INTO CityEntitie VALUES('Carpina','-7.84','-35.26','Brazil')

INSERT INTO CityEntitie VALUES('Tulu?','4.0904','-76.21','Colombia')

INSERT INTO CityEntitie VALUES('Livingstone','-17.86','25.86','Zambia')

INSERT INTO CityEntitie VALUES('Hà T?nh','18.3338','105.9','Vietnam')

INSERT INTO CityEntitie VALUES('Kendari','-3.9553','122.5973','Indonesia')

INSERT INTO CityEntitie VALUES('Kimberley','-28.7468','24.77','South Africa')

INSERT INTO CityEntitie VALUES('Elbasan','41.1215','20.0838','Albania')

INSERT INTO CityEntitie VALUES('Nantou','23.9167','120.6833','Taiwan')

INSERT INTO CityEntitie VALUES('Fredericksburg','38.2992','-77.4872','United States')

INSERT INTO CityEntitie VALUES('Serang','-6.11','106.1496','Indonesia')

INSERT INTO CityEntitie VALUES('Ocala','29.1803','-82.1495','United States')

INSERT INTO CityEntitie VALUES('Lajes','-27.8096','-50.31','Brazil')

INSERT INTO CityEntitie VALUES('Le?n','12.4356','-86.8794','Nicaragua')

INSERT INTO CityEntitie VALUES('Sitapur','27.63','80.75','India')

INSERT INTO CityEntitie VALUES('Zenica','44.22','17.92','Bosnia And Herzegovina')

INSERT INTO CityEntitie VALUES('Regensburg','49.0204','12.12','Germany')

INSERT INTO CityEntitie VALUES('Kigoma','-4.8796','29.61','Tanzania')

INSERT INTO CityEntitie VALUES('Pizen','49.7404','13.36','Czechia')

INSERT INTO CityEntitie VALUES('Phitsanulok','16.8283','100.2729','Thailand')

INSERT INTO CityEntitie VALUES('Myingyan','21.4618','95.3914','Burma')

INSERT INTO CityEntitie VALUES('Dagupan','16.0479','120.3408','Philippines')

INSERT INTO CityEntitie VALUES('Patra','38.23','21.73','Greece')

INSERT INTO CityEntitie VALUES('Ingolstadt','48.7704','11.45','Germany')

INSERT INTO CityEntitie VALUES('La Vega','19.2166','-70.5166','Dominican Republic')

INSERT INTO CityEntitie VALUES('Mostar','43.3505','17.82','Bosnia And Herzegovina')

INSERT INTO CityEntitie VALUES('Navsari','20.8504','72.92','India')

INSERT INTO CityEntitie VALUES('Geneina','13.45','22.44','Sudan')

INSERT INTO CityEntitie VALUES('Tartus','34.8846','35.8866','Syria')

INSERT INTO CityEntitie VALUES('Simao','22.7807','100.9782','China')

INSERT INTO CityEntitie VALUES('La Rioja','-29.41','-66.85','Argentina')

INSERT INTO CityEntitie VALUES('Lecce','40.3604','18.15','Italy')

INSERT INTO CityEntitie VALUES('Mahabad','36.7704','45.72','Iran')

INSERT INTO CityEntitie VALUES('Salamanca','40.9704','-5.67','Spain')

INSERT INTO CityEntitie VALUES('Dourados','-22.23','-54.81','Brazil')

INSERT INTO CityEntitie VALUES('Ash Shatrah','31.4175','46.1772','Iraq')

INSERT INTO CityEntitie VALUES('Settat','33.0104','-7.62','Morocco')

INSERT INTO CityEntitie VALUES('Budaun','28.03','79.09','India')

INSERT INTO CityEntitie VALUES('Tunja','5.5504','-73.37','Colombia')

INSERT INTO CityEntitie VALUES('Coquimbo','-29.9529','-71.3436','Chile')

INSERT INTO CityEntitie VALUES('Rubtsovsk','51.52','81.21','Russia')

INSERT INTO CityEntitie VALUES('Bhisho','-32.87','27.39','South Africa')

INSERT INTO CityEntitie VALUES('Geelong','-38.1675','144.3956','Australia')

INSERT INTO CityEntitie VALUES('Ras al Khaymah','25.7915','55.9428','United Arab Emirates')

INSERT INTO CityEntitie VALUES('Sullana','-4.8896','-80.68','Peru')

INSERT INTO CityEntitie VALUES('Malakal','9.5369','31.656','South Sudan')

INSERT INTO CityEntitie VALUES('Manchester','42.9848','-71.4447','United States')

INSERT INTO CityEntitie VALUES('Bumba','2.19','22.46','Congo (Kinshasa)')

INSERT INTO CityEntitie VALUES('Hayward','37.6328','-122.0772','United States')

INSERT INTO CityEntitie VALUES('Xiaogan','30.9204','113.9','China')

INSERT INTO CityEntitie VALUES('Xapeco','-27.1','-52.64','Brazil')

INSERT INTO CityEntitie VALUES('Muskegon','43.2281','-86.2562','United States')

INSERT INTO CityEntitie VALUES('Taunggyi','20.782','97.038','Burma')

INSERT INTO CityEntitie VALUES('L?zaro C?rdenas','17.9587','-102.2','Mexico')

INSERT INTO CityEntitie VALUES('Alexandria','38.8185','-77.0861','United States')

INSERT INTO CityEntitie VALUES('Gonbad-e Kavus','37.2518','55.1715','Iran')

INSERT INTO CityEntitie VALUES('Salavat','53.3703','55.93','Russia')

INSERT INTO CityEntitie VALUES('Valdivia','-39.795','-73.245','Chile')

INSERT INTO CityEntitie VALUES('Pagadian','7.853','123.507','Philippines')

INSERT INTO CityEntitie VALUES('Nakhodka','42.8374','132.8874','Russia')

INSERT INTO CityEntitie VALUES('Nakhodka','67.7504','77.52','Russia')

INSERT INTO CityEntitie VALUES('Springfield','39.7712','-89.6539','United States')

INSERT INTO CityEntitie VALUES('Lafayette','40.3991','-86.8594','United States')

INSERT INTO CityEntitie VALUES('Tarija','-21.5167','-64.75','Bolivia')

INSERT INTO CityEntitie VALUES('Zarzis','33.5104','11.1','Tunisia')

INSERT INTO CityEntitie VALUES('Punta del Este','-34.97','-54.95','Uruguay')

INSERT INTO CityEntitie VALUES('York','53.9704','-1.08','United Kingdom')

INSERT INTO CityEntitie VALUES('Cuddalore','11.7204','79.77','India')

INSERT INTO CityEntitie VALUES('Rio Largo','-9.48','-35.84','Brazil')

INSERT INTO CityEntitie VALUES('Samarra','34.194','43.875','Iraq')

INSERT INTO CityEntitie VALUES('Maykop','44.61','40.12','Russia')

INSERT INTO CityEntitie VALUES('Abéché','13.84','20.83','Chad')

INSERT INTO CityEntitie VALUES('Barreiras','-12.14','-45','Brazil')

INSERT INTO CityEntitie VALUES('Odense','55.4004','10.3833','Denmark')

INSERT INTO CityEntitie VALUES('Hu?nuco','-9.92','-76.24','Peru')

INSERT INTO CityEntitie VALUES('Hoshiarpur','31.52','75.98','India')

INSERT INTO CityEntitie VALUES('Albacete','39.0003','-1.87','Spain')

INSERT INTO CityEntitie VALUES('Melitopol','46.8378','35.3775','Ukraine')

INSERT INTO CityEntitie VALUES('Sobral','-3.69','-40.35','Brazil')

INSERT INTO CityEntitie VALUES('Port Arthur','29.8554','-93.9264','United States')

INSERT INTO CityEntitie VALUES('Sudbury','46.5','-80.9666','Canada')

INSERT INTO CityEntitie VALUES('Surat Thani','9.1501','99.3401','Thailand')

INSERT INTO CityEntitie VALUES('Palmdale','34.5944','-118.1057','United States')

INSERT INTO CityEntitie VALUES('Hurghada','27.23','33.83','Egypt')

INSERT INTO CityEntitie VALUES('Isiro','2.76','27.62','Congo (Kinshasa)')

INSERT INTO CityEntitie VALUES('Puerto Princesa','9.7543','118.7444','Philippines')

INSERT INTO CityEntitie VALUES('Kashmar','35.1814','58.4515','Iran')

INSERT INTO CityEntitie VALUES('Ussuriysk','43.8','132.02','Russia')

INSERT INTO CityEntitie VALUES('Rustavi','41.5704','45.05','Georgia')

INSERT INTO CityEntitie VALUES('Tiraspol','46.8531','29.64','Moldova')

INSERT INTO CityEntitie VALUES('Afyon','38.7504','30.55','Turkey')

INSERT INTO CityEntitie VALUES('Zonguldak','41.4304','31.78','Turkey')

INSERT INTO CityEntitie VALUES('Matsue','35.467','133.0666','Japan')

INSERT INTO CityEntitie VALUES('Champaign','40.1144','-88.2735','United States')

INSERT INTO CityEntitie VALUES('Moundou','8.55','16.09','Chad')

INSERT INTO CityEntitie VALUES('Dali','25.7','100.18','China')

INSERT INTO CityEntitie VALUES('Koforidua','6.0904','-0.26','Ghana')

INSERT INTO CityEntitie VALUES('Uroteppa','39.9219','69.0015','Tajikistan')

INSERT INTO CityEntitie VALUES('Livorno','43.5511','10.3023','Italy')

INSERT INTO CityEntitie VALUES('Gainesville','34.2901','-83.8301','United States')

INSERT INTO CityEntitie VALUES('Gondar','12.61','37.46','Ethiopia')

INSERT INTO CityEntitie VALUES('Ciudad del Carmen','18.6537','-91.8245','Mexico')

INSERT INTO CityEntitie VALUES('Malabo','3.75','8.7833','Equatorial Guinea')

INSERT INTO CityEntitie VALUES('Yulin','38.2833','109.7333','China')

INSERT INTO CityEntitie VALUES('Shkodër','42.0685','19.5188','Albania')

INSERT INTO CityEntitie VALUES('Muroran','42.35','140.98','Japan')

INSERT INTO CityEntitie VALUES('Sibiu','45.7971','24.1371','Romania')

INSERT INTO CityEntitie VALUES('Batumi','41.6','41.63','Georgia')

INSERT INTO CityEntitie VALUES('Barysaw','54.226','28.4922','Belarus')

INSERT INTO CityEntitie VALUES('Gyeongju','35.8428','129.2117','Korea')

INSERT INTO CityEntitie VALUES('Innsbruck','47.2804','11.41','Austria')

INSERT INTO CityEntitie VALUES('Foggia','41.4605','15.56','Italy')

INSERT INTO CityEntitie VALUES('Ordu','41.0004','37.8699','Turkey')

INSERT INTO CityEntitie VALUES('Kanchipuram','12.8337','79.7167','India')

INSERT INTO CityEntitie VALUES('Frederick','39.4335','-77.4157','United States')

INSERT INTO CityEntitie VALUES('Lakewood','39.6978','-105.1168','United States')

INSERT INTO CityEntitie VALUES('Sariwon','38.507','125.762','Korea')

INSERT INTO CityEntitie VALUES('Beian','48.239','126.482','China')

INSERT INTO CityEntitie VALUES('Battambang','13.1','103.2','Cambodia')

INSERT INTO CityEntitie VALUES('Guangshui','31.6204','114','China')

INSERT INTO CityEntitie VALUES('Middelburg','-25.7596','29.47','South Africa')

INSERT INTO CityEntitie VALUES('Mahajanga','-15.67','46.345','Madagascar')

INSERT INTO CityEntitie VALUES('La Serena','-29.9','-71.25','Chile')

INSERT INTO CityEntitie VALUES('Chetumal','18.5','-88.3','Mexico')

INSERT INTO CityEntitie VALUES('Gandajika','-6.7396','23.96','Congo (Kinshasa)')

INSERT INTO CityEntitie VALUES('Pathum Thani','14.0171','100.5333','Thailand')

INSERT INTO CityEntitie VALUES('Cairns','-16.8878','145.7633','Australia')

INSERT INTO CityEntitie VALUES('Kovrov','56.3604','41.33','Russia')

INSERT INTO CityEntitie VALUES('Tuscaloosa','33.2349','-87.5267','United States')

INSERT INTO CityEntitie VALUES('Osorno','-40.57','-73.16','Chile')

INSERT INTO CityEntitie VALUES('Tottori','35.5004','134.2333','Japan')

INSERT INTO CityEntitie VALUES('San Crist?bal','18.416','-70.109','Dominican Republic')

INSERT INTO CityEntitie VALUES('Lemosos','34.6754','33.0333','Cyprus')

INSERT INTO CityEntitie VALUES('Moanda','-5.9229','12.355','Congo (Kinshasa)')

INSERT INTO CityEntitie VALUES('R?o Cuarto','-33.13','-64.35','Argentina')

INSERT INTO CityEntitie VALUES('Masjed Soleyman','31.98','49.2999','Iran')

INSERT INTO CityEntitie VALUES('Sunnyvale','37.3835','-122.0257','United States')

INSERT INTO CityEntitie VALUES('Lake Charles','30.203','-93.215','United States')

INSERT INTO CityEntitie VALUES('Hollywood','26.0293','-80.1678','United States')

INSERT INTO CityEntitie VALUES('Mufulira','-12.55','28.26','Zambia')

INSERT INTO CityEntitie VALUES('Pasadena','29.6584','-95.1499','United States')

INSERT INTO CityEntitie VALUES('Chincha Alta','-13.4196','-76.14','Peru')

INSERT INTO CityEntitie VALUES('Faizabad','26.7504','82.17','India')

INSERT INTO CityEntitie VALUES('Pomona','34.0585','-117.7625','United States')

INSERT INTO CityEntitie VALUES('Kansas City','39.1234','-94.7443','United States')

INSERT INTO CityEntitie VALUES('Dingzhou','38.5004','115','China')

INSERT INTO CityEntitie VALUES('Rondon?polis','-16.4695','-54.64','Brazil')

INSERT INTO CityEntitie VALUES('Ilam','33.6304','46.43','Iran')

INSERT INTO CityEntitie VALUES('U?ak','38.6804','29.42','Turkey')

INSERT INTO CityEntitie VALUES('Nusaybin','37.075','41.2184','Turkey')

INSERT INTO CityEntitie VALUES('San Francisco de Macor?s','19.3','-70.25','Dominican Republic')

INSERT INTO CityEntitie VALUES('Zab?d','14.1951','43.3155','Yemen')

INSERT INTO CityEntitie VALUES('Silchar','24.7904','92.79','India')

INSERT INTO CityEntitie VALUES('Wenshan','23.3724','104.2497','China')

INSERT INTO CityEntitie VALUES('Novokuybishevsk','53.12','49.9199','Russia')

INSERT INTO CityEntitie VALUES('Mudon','16.2618','97.7215','Burma')

INSERT INTO CityEntitie VALUES('Limoges','45.83','1.25','France')

INSERT INTO CityEntitie VALUES('Deyang','31.1333','104.4','China')

INSERT INTO CityEntitie VALUES('Samalut','28.3004','30.71','Egypt')

INSERT INTO CityEntitie VALUES('Luanshya','-13.1333','28.4','Zambia')

INSERT INTO CityEntitie VALUES('Escondido','33.1347','-117.0722','United States')

INSERT INTO CityEntitie VALUES('El Manaqil','14.2504','32.98','Sudan')

INSERT INTO CityEntitie VALUES('Abbotsford','49.0504','-122.3','Canada')

INSERT INTO CityEntitie VALUES('Guanare','9.05','-69.75','Venezuela')

INSERT INTO CityEntitie VALUES('Bintulu','3.1664','113.036','Malaysia')

INSERT INTO CityEntitie VALUES('Dundee','56.4704','-3','United Kingdom')

INSERT INTO CityEntitie VALUES('Nazareth','32.704','35.2955','Israel')

INSERT INTO CityEntitie VALUES('Maragheh','37.4204','46.22','Iran')

INSERT INTO CityEntitie VALUES('B?l?i','47.7591','27.9053','Moldova')

INSERT INTO CityEntitie VALUES('Suhar','24.362','56.7344','Oman')

INSERT INTO CityEntitie VALUES('Benguela','-12.5783','13.4072','Angola')

INSERT INTO CityEntitie VALUES('Guarapuava','-25.38','-51.48','Brazil')

INSERT INTO CityEntitie VALUES('Uzhgorod','48.63','22.25','Ukraine')

INSERT INTO CityEntitie VALUES('Binghamton','42.1014','-75.9093','United States')

INSERT INTO CityEntitie VALUES('Kitale','1.0305','34.9899','Kenya')

INSERT INTO CityEntitie VALUES('Chill?n','-36.6','-72.106','Chile')

INSERT INTO CityEntitie VALUES('Urgentch','41.56','60.64','Uzbekistan')

INSERT INTO CityEntitie VALUES('Vryheid','-27.76','30.79','South Africa')

INSERT INTO CityEntitie VALUES('Pattani','6.864','101.25','Thailand')

INSERT INTO CityEntitie VALUES('Miaoli','24.57','120.82','Taiwan')

INSERT INTO CityEntitie VALUES('Yilan','24.75','121.75','Taiwan')

INSERT INTO CityEntitie VALUES('Tororo','0.7104','34.17','Uganda')

INSERT INTO CityEntitie VALUES('Msila','35.7','4.545','Algeria')

INSERT INTO CityEntitie VALUES('Keren','15.6804','38.45','Eritrea')

INSERT INTO CityEntitie VALUES('Cao L?nh','10.467','105.636','Vietnam')

INSERT INTO CityEntitie VALUES('Khiwa','41.3911','60.3557','Uzbekistan')

INSERT INTO CityEntitie VALUES('Carora','10.19','-70.08','Venezuela')

INSERT INTO CityEntitie VALUES('Warner Robins','32.598','-83.6528','United States')

INSERT INTO CityEntitie VALUES('New Bedford','41.6697','-70.9428','United States')

INSERT INTO CityEntitie VALUES('Tirgu Mures','46.5582','24.5578','Romania')

INSERT INTO CityEntitie VALUES('Perugia','43.1104','12.39','Italy')

INSERT INTO CityEntitie VALUES('Gejiu','23.38','103.1501','China')

INSERT INTO CityEntitie VALUES('Topeka','39.0346','-95.6956','United States')

INSERT INTO CityEntitie VALUES('Beaumont','30.0849','-94.1451','United States')

INSERT INTO CityEntitie VALUES('Paterson','40.9147','-74.1624','United States')

INSERT INTO CityEntitie VALUES('Joliet','41.5193','-88.1501','United States')

INSERT INTO CityEntitie VALUES('Gyumri','40.7894','43.8475','Armenia')

INSERT INTO CityEntitie VALUES('Palangkaraya','-2.2096','113.91','Indonesia')

INSERT INTO CityEntitie VALUES('Jiayuguan','39.82','98.3','China')

INSERT INTO CityEntitie VALUES('Hamilton','-37.7783','175.2896','New Zealand')

INSERT INTO CityEntitie VALUES('Leesburg','28.7674','-81.8981','United States')

INSERT INTO CityEntitie VALUES('Yala','6.5505','101.2851','Thailand')

INSERT INTO CityEntitie VALUES('Elkhart','41.6913','-85.9628','United States')

INSERT INTO CityEntitie VALUES('Diourbel','14.6604','-16.24','Senegal')

INSERT INTO CityEntitie VALUES('L?ng S?n','21.846','106.757','Vietnam')

INSERT INTO CityEntitie VALUES('Jijel','36.822','5.766','Algeria')

INSERT INTO CityEntitie VALUES('Trang','7.5634','99.608','Thailand')

INSERT INTO CityEntitie VALUES('Odessa','31.8831','-102.3407','United States')

INSERT INTO CityEntitie VALUES('Turbat','25.9918','63.0718','Pakistan')

INSERT INTO CityEntitie VALUES('Médéa','36.2704','2.77','Algeria')

INSERT INTO CityEntitie VALUES('Kolomna','55.08','38.785','Russia')

INSERT INTO CityEntitie VALUES('Naperville','41.7483','-88.1657','United States')

INSERT INTO CityEntitie VALUES('Fairfield','38.2594','-122.0319','United States')

INSERT INTO CityEntitie VALUES('San Juan del R?o','20.38','-100','Mexico')

INSERT INTO CityEntitie VALUES('Mangyshlak','43.6905','51.1417','Kazakhstan')

INSERT INTO CityEntitie VALUES('El Progreso','14.85','-90.0167','Guatemala')

INSERT INTO CityEntitie VALUES('Trondheim','63.4167','10.4167','Norway')

INSERT INTO CityEntitie VALUES('Man','7.4004','-7.55','Côte DIvoire')

INSERT INTO CityEntitie VALUES('Wamba','2.1404','27.99','Congo (Kinshasa)')

INSERT INTO CityEntitie VALUES('Gulu','2.78','32.28','Uganda')

INSERT INTO CityEntitie VALUES('Aix-en-Provence','43.52','5.45','France')

INSERT INTO CityEntitie VALUES('Willemstad','12.2004','-69.02','Curaçao')

INSERT INTO CityEntitie VALUES('Gharyan','32.1704','13.02','Libya')

INSERT INTO CityEntitie VALUES('Ambala','30.32','76.82','India')

INSERT INTO CityEntitie VALUES('Cam Ranh','11.902','109.2207','Vietnam')

INSERT INTO CityEntitie VALUES('Torrance','33.8346','-118.3417','United States')

INSERT INTO CityEntitie VALUES('Ibarra','0.3604','-78.13','Ecuador')

INSERT INTO CityEntitie VALUES('Matanzas','23.0415','-81.5775','Cuba')

INSERT INTO CityEntitie VALUES('Perpignan','42.7','2.9','France')

INSERT INTO CityEntitie VALUES('Poços de Caldas','-21.78','-46.57','Brazil')

INSERT INTO CityEntitie VALUES('Tabora','-5.02','32.8','Tanzania')

INSERT INTO CityEntitie VALUES('Dara','32.625','36.105','Syria')

INSERT INTO CityEntitie VALUES('Brugge','51.2204','3.23','Belgium')

INSERT INTO CityEntitie VALUES('Cabo de Santo Agostinho','-8.29','-35.03','Brazil')

INSERT INTO CityEntitie VALUES('Dawei','14.098','98.195','Burma')

INSERT INTO CityEntitie VALUES('Hanzhong','33.13','107.03','China')

INSERT INTO CityEntitie VALUES('La Ceiba','15.7631','-86.797','Honduras')

INSERT INTO CityEntitie VALUES('Krishnanagar','23.3803','88.53','India')

INSERT INTO CityEntitie VALUES('Podgorica','42.466','19.2663','Montenegro')

INSERT INTO CityEntitie VALUES('Ayutthaya','14.3588','100.5684','Thailand')

INSERT INTO CityEntitie VALUES('Quchan','37.1118','58.5015','Iran')

INSERT INTO CityEntitie VALUES('Pointe-à-Pitre','16.2415','-61.533','Guadeloupe')

INSERT INTO CityEntitie VALUES('Biarritz','43.4733','-1.5616','France')

INSERT INTO CityEntitie VALUES('Los Angeles','-37.46','-72.36','Chile')

INSERT INTO CityEntitie VALUES('Panama City','30.1997','-85.6003','United States')

INSERT INTO CityEntitie VALUES('Concordia','-31.3896','-58.03','Argentina')

INSERT INTO CityEntitie VALUES('Machakos','-1.5095','37.26','Kenya')

INSERT INTO CityEntitie VALUES('Brest','48.3904','-4.495','France')

INSERT INTO CityEntitie VALUES('Kolar','13.1337','78.1334','India')

INSERT INTO CityEntitie VALUES('Qairouan','35.6804','10.1','Tunisia')

INSERT INTO CityEntitie VALUES('Le Mans','48.0004','0.1','France')

INSERT INTO CityEntitie VALUES('Tall Afar','36.376','42.4497','Iraq')

INSERT INTO CityEntitie VALUES('Bellevue','47.5953','-122.1551','United States')

INSERT INTO CityEntitie VALUES('Kumba','4.6404','9.44','Cameroon')

INSERT INTO CityEntitie VALUES('Jinchang','38.4957','102.1739','China')

INSERT INTO CityEntitie VALUES('Tuzla','44.5505','18.68','Bosnia And Herzegovina')

INSERT INTO CityEntitie VALUES('Székesfehérv?r','47.1947','18.4081','Hungary')

INSERT INTO CityEntitie VALUES('Midland','32.0249','-102.1137','United States')

INSERT INTO CityEntitie VALUES('Huelva','37.2504','-6.9299','Spain')

INSERT INTO CityEntitie VALUES('Sarnia','42.9666','-82.4','Canada')

INSERT INTO CityEntitie VALUES('Goma','-1.6788','29.2218','Congo (Kinshasa)')

INSERT INTO CityEntitie VALUES('Merced','37.3058','-120.4778','United States')

INSERT INTO CityEntitie VALUES('Tizi-Ouzou','36.8','4.0333','Algeria')

INSERT INTO CityEntitie VALUES('Kafr el Sheikh','31.109','30.936','Egypt')

INSERT INTO CityEntitie VALUES('Mesquite','32.7622','-96.5889','United States')

INSERT INTO CityEntitie VALUES('Ajdabiya','30.77','20.22','Libya')

INSERT INTO CityEntitie VALUES('Otaru','43.1887','140.9783','Japan')

INSERT INTO CityEntitie VALUES('Ipswich','52.0703','1.17','United Kingdom')

INSERT INTO CityEntitie VALUES('Logro?o','42.4704','-2.43','Spain')

INSERT INTO CityEntitie VALUES('Stara Zagora','42.4231','25.6227','Bulgaria')

INSERT INTO CityEntitie VALUES('Béchar','31.6111','-2.23','Algeria')

INSERT INTO CityEntitie VALUES('Kenema','7.8804','-11.19','Sierra Leone')

INSERT INTO CityEntitie VALUES('Amiens','49.9004','2.3','France')

INSERT INTO CityEntitie VALUES('Calama','-22.45','-68.92','Chile')

INSERT INTO CityEntitie VALUES('Macaé','-22.38','-41.79','Brazil')

INSERT INTO CityEntitie VALUES('Cape Coast','5.1104','-1.25','Ghana')

INSERT INTO CityEntitie VALUES('Houma','29.5799','-90.7058','United States')

INSERT INTO CityEntitie VALUES('Play Ku','13.9833','108','Vietnam')

INSERT INTO CityEntitie VALUES('Pyatigorsk','44.08','43.09','Russia')

INSERT INTO CityEntitie VALUES('Charleston','38.3484','-81.6323','United States')

INSERT INTO CityEntitie VALUES('Cajamarca','-7.15','-78.53','Peru')

INSERT INTO CityEntitie VALUES('Pasadena','34.1598','-118.139','United States')

INSERT INTO CityEntitie VALUES('Arzamas','55.4','43.8','Russia')

INSERT INTO CityEntitie VALUES('Massawa','15.6101','39.45','Eritrea')

INSERT INTO CityEntitie VALUES('Durrës','41.3178','19.4482','Albania')

INSERT INTO CityEntitie VALUES('Nawabganj','24.5804','88.35','Bangladesh')

INSERT INTO CityEntitie VALUES('Pueblo','38.2701','-104.6131','United States')

INSERT INTO CityEntitie VALUES('Saïda','34.8404','0.14','Algeria')

INSERT INTO CityEntitie VALUES('Ciego de ?vila','21.84','-78.7619','Cuba')

INSERT INTO CityEntitie VALUES('Altay','47.8666','88.1166','China')

INSERT INTO CityEntitie VALUES('Jieshou','33.2504','115.35','China')

INSERT INTO CityEntitie VALUES('Chongju','39.6813','125.2163','Korea')

INSERT INTO CityEntitie VALUES('Arnhem','51.988','5.923','Netherlands')

INSERT INTO CityEntitie VALUES('Phuket','7.8765','98.3815','Thailand')

INSERT INTO CityEntitie VALUES('Nongan','44.4304','125.1701','China')

INSERT INTO CityEntitie VALUES('Melilla','35.3','-2.95','Spain')

INSERT INTO CityEntitie VALUES('Tyler','32.3184','-95.3065','United States')

INSERT INTO CityEntitie VALUES('Paranagu?','-25.5279','-48.5345','Brazil')

INSERT INTO CityEntitie VALUES('Ghazni','33.5633','68.4178','Afghanistan')

INSERT INTO CityEntitie VALUES('Comodoro Rivadavia','-45.87','-67.5','Argentina')

INSERT INTO CityEntitie VALUES('As Salt','32.0392','35.7272','Jordan')

INSERT INTO CityEntitie VALUES('Labé','11.32','-12.3','Guinea')

INSERT INTO CityEntitie VALUES('Orange','33.8038','-117.8219','United States')

INSERT INTO CityEntitie VALUES('Almetyevsk','54.9004','52.3199','Russia')

INSERT INTO CityEntitie VALUES('Fullerton','33.8841','-117.9279','United States')

INSERT INTO CityEntitie VALUES('Miramar','25.9773','-80.3358','United States')

INSERT INTO CityEntitie VALUES('Lhokseumawe','5.1914','97.1415','Indonesia')

INSERT INTO CityEntitie VALUES('Orekhovo-Zuevo','55.82','38.98','Russia')

INSERT INTO CityEntitie VALUES('Santa Maria','34.9333','-120.4432','United States')

INSERT INTO CityEntitie VALUES('Athens','33.9508','-83.3689','United States')

INSERT INTO CityEntitie VALUES('Peterborough','52.5804','-0.25','United Kingdom')

INSERT INTO CityEntitie VALUES('Badajoz','38.8804','-6.97','Spain')

INSERT INTO CityEntitie VALUES('Dunhuang','40.1427','94.662','China')

INSERT INTO CityEntitie VALUES('Bordj Bou Arréridj','36.059','4.63','Algeria')

INSERT INTO CityEntitie VALUES('Columbia','38.9478','-92.3258','United States')

INSERT INTO CityEntitie VALUES('Bizerte','37.2904','9.855','Tunisia')

INSERT INTO CityEntitie VALUES('Sherbrooke','45.4','-71.9','Canada')

INSERT INTO CityEntitie VALUES('Piedras Negras','28.7076','-100.5317','Mexico')

INSERT INTO CityEntitie VALUES('G?ttingen','51.5204','9.92','Germany')

INSERT INTO CityEntitie VALUES('Kumbakonam','10.9805','79.4','India')

INSERT INTO CityEntitie VALUES('Myitkyina','25.3596','97.3928','Burma')

INSERT INTO CityEntitie VALUES('Chinandega','12.6304','-87.13','Nicaragua')

INSERT INTO CityEntitie VALUES('Townsville','-19.25','146.77','Australia')

INSERT INTO CityEntitie VALUES('Yuma','32.5991','-114.5488','United States')

INSERT INTO CityEntitie VALUES('Tiruvannamalai','12.2604','79.1','India')

INSERT INTO CityEntitie VALUES('Zalantun','48','122.72','China')

INSERT INTO CityEntitie VALUES('Parna?ba','-2.91','-41.77','Brazil')

INSERT INTO CityEntitie VALUES('Pabna','24.0004','89.25','Bangladesh')

INSERT INTO CityEntitie VALUES('Umtata','-31.58','28.79','South Africa')

INSERT INTO CityEntitie VALUES('Bremerhaven','53.5504','8.58','Germany')

INSERT INTO CityEntitie VALUES('Tan An','10.5337','106.4167','Vietnam')

INSERT INTO CityEntitie VALUES('Olathe','38.8838','-94.8196','United States')

INSERT INTO CityEntitie VALUES('Castanhal','-1.2896','-47.93','Brazil')

INSERT INTO CityEntitie VALUES('Cerro de Pasco','-10.69','-76.27','Peru')

INSERT INTO CityEntitie VALUES('Iraklio','35.325','25.1305','Greece')

INSERT INTO CityEntitie VALUES('Thornton','39.9205','-104.9443','United States')

INSERT INTO CityEntitie VALUES('Harlingen','26.1917','-97.6977','United States')

INSERT INTO CityEntitie VALUES('Oulu','65','25.47','Finland')

INSERT INTO CityEntitie VALUES('Grand Junction','39.0878','-108.5673','United States')

INSERT INTO CityEntitie VALUES('Shizuishan','39.2333','106.769','China')

INSERT INTO CityEntitie VALUES('Baia Mare','47.6595','23.5791','Romania')

INSERT INTO CityEntitie VALUES('Le?n','42.58','-5.57','Spain')

INSERT INTO CityEntitie VALUES('West Valley City','40.6889','-112.0115','United States')

INSERT INTO CityEntitie VALUES('Gy?r','47.7004','17.63','Hungary')

INSERT INTO CityEntitie VALUES('Carrollton','32.9886','-96.9','United States')

INSERT INTO CityEntitie VALUES('Serpukhov','54.9304','37.43','Russia')

INSERT INTO CityEntitie VALUES('Jequié','-13.85','-40.08','Brazil')

INSERT INTO CityEntitie VALUES('C?m Ph?','21.0404','107.32','Vietnam')

INSERT INTO CityEntitie VALUES('Lira','2.2604','32.89','Uganda')

INSERT INTO CityEntitie VALUES('Roseville','38.7691','-121.3178','United States')

INSERT INTO CityEntitie VALUES('Pyay','18.8165','95.2114','Burma')

INSERT INTO CityEntitie VALUES('Orsha','54.5153','30.4215','Belarus')

INSERT INTO CityEntitie VALUES('May Pen','17.9666','-77.2333','Jamaica')

INSERT INTO CityEntitie VALUES('Warren','42.4934','-83.027','United States')

INSERT INTO CityEntitie VALUES('Souk Ahras','36.2904','7.95','Algeria')

INSERT INTO CityEntitie VALUES('Bloomington','40.4757','-88.9705','United States')

INSERT INTO CityEntitie VALUES('Zakho','37.1445','42.6872','Iraq')

INSERT INTO CityEntitie VALUES('Cartago','4.75','-75.91','Colombia')

INSERT INTO CityEntitie VALUES('Hampton','37.0551','-76.3629','United States')

INSERT INTO CityEntitie VALUES('Caxias','-4.833','-43.35','Brazil')

INSERT INTO CityEntitie VALUES('Ravenna','44.4204','12.22','Italy')

INSERT INTO CityEntitie VALUES('s-Hertogenbosch','51.6833','5.3167','Netherlands')

INSERT INTO CityEntitie VALUES('Nevinnomyssk','44.6201','41.95','Russia')

INSERT INTO CityEntitie VALUES('Surprise','33.6802','-112.4525','United States')

INSERT INTO CityEntitie VALUES('Las Cruces','32.3265','-106.7893','United States')

INSERT INTO CityEntitie VALUES('Touggourt','33.1','6.06','Algeria')

INSERT INTO CityEntitie VALUES('Greeley','40.415','-104.7696','United States')

INSERT INTO CityEntitie VALUES('Pervouralsk','56.91','59.955','Russia')

INSERT INTO CityEntitie VALUES('Yunxian','32.8082','110.8136','China')

INSERT INTO CityEntitie VALUES('?iauliai','55.9386','23.325','Lithuania')

INSERT INTO CityEntitie VALUES('Birganj','27.0004','84.8666','Nepal')

INSERT INTO CityEntitie VALUES('Yakima','46.5923','-120.5496','United States')

INSERT INTO CityEntitie VALUES('Riohacha','11.5403','-72.91','Colombia')

INSERT INTO CityEntitie VALUES('Musoma','-1.4896','33.8','Tanzania')

INSERT INTO CityEntitie VALUES('Uppsala','59.8601','17.64','Sweden')

INSERT INTO CityEntitie VALUES('Awasa','7.06','38.477','Ethiopia')

INSERT INTO CityEntitie VALUES('Coral Springs','26.2701','-80.2592','United States')

INSERT INTO CityEntitie VALUES('Namibe','-15.19','12.16','Angola')

INSERT INTO CityEntitie VALUES('Kislovodsk','43.91','42.72','Russia')

INSERT INTO CityEntitie VALUES('Sterling Heights','42.5809','-83.0305','United States')

INSERT INTO CityEntitie VALUES('San Luis','16.2','-89.44','Guatemala')

INSERT INTO CityEntitie VALUES('Blitar','-8.0696','112.15','Indonesia')

INSERT INTO CityEntitie VALUES('Mauldin','34.7864','-82.2996','United States')

INSERT INTO CityEntitie VALUES('Dimitrovgrad','54.2504','49.56','Russia')

INSERT INTO CityEntitie VALUES('Pindamonhangaba','-22.92','-45.47','Brazil')

INSERT INTO CityEntitie VALUES('Shahrud','36.4229','54.9629','Iran')

INSERT INTO CityEntitie VALUES('Papeete','-17.5334','-149.5667','French Polynesia')

INSERT INTO CityEntitie VALUES('St. Johns','47.585','-52.681','Canada')

INSERT INTO CityEntitie VALUES('Racine','42.7274','-87.8135','United States')

INSERT INTO CityEntitie VALUES('Opole','50.685','17.9313','Poland')

INSERT INTO CityEntitie VALUES('Tra Vinh','9.934','106.334','Vietnam')

INSERT INTO CityEntitie VALUES('Murom','55.5704','42.04','Russia')

INSERT INTO CityEntitie VALUES('Ciénaga','11.0104','-74.25','Colombia')

INSERT INTO CityEntitie VALUES('Lae','-6.733','146.99','Papua New Guinea')

INSERT INTO CityEntitie VALUES('Pilibhit','28.64','79.81','India')

INSERT INTO CityEntitie VALUES('Ferrara','44.8504','11.6099','Italy')

INSERT INTO CityEntitie VALUES('Novomoskovsk','54.09','38.22','Russia')

INSERT INTO CityEntitie VALUES('Buz?u','45.1565','26.8065','Romania')

INSERT INTO CityEntitie VALUES('Stamford','41.1036','-73.5583','United States')

INSERT INTO CityEntitie VALUES('Pinsk','52.1279','26.0941','Belarus')

INSERT INTO CityEntitie VALUES('Shuangcheng','45.3503','126.28','China')

INSERT INTO CityEntitie VALUES('Abohar','30.1204','74.29','India')

INSERT INTO CityEntitie VALUES('Ninh B?nh','20.2543','105.975','Vietnam')

INSERT INTO CityEntitie VALUES('Nikopol','47.5666','34.4062','Ukraine')

INSERT INTO CityEntitie VALUES('Florencia','1.6104','-75.62','Colombia')

INSERT INTO CityEntitie VALUES('Girardot','4.31','-74.81','Colombia')

INSERT INTO CityEntitie VALUES('Elizabeth','40.6657','-74.1912','United States')

INSERT INTO CityEntitie VALUES('Jaragu? do Sul','-26.48','-49.1','Brazil')

INSERT INTO CityEntitie VALUES('Sennar','13.55','33.6','Sudan')

INSERT INTO CityEntitie VALUES('Masaya','11.969','-86.095','Nicaragua')

INSERT INTO CityEntitie VALUES('Baydhabo','3.12','43.65','Somalia')

INSERT INTO CityEntitie VALUES('Tokat','40.306','36.563','Turkey')

INSERT INTO CityEntitie VALUES('Erzincan','39.7526','39.4928','Turkey')

INSERT INTO CityEntitie VALUES('Andong','36.5659','128.725','Korea')

INSERT INTO CityEntitie VALUES('Tete','-16.17','33.58','Mozambique')

INSERT INTO CityEntitie VALUES('Copiap?','-27.36','-70.34','Chile')

INSERT INTO CityEntitie VALUES('Batticaloa','7.717','81.7','Sri Lanka')

INSERT INTO CityEntitie VALUES('Shar e Kord','32.321','50.854','Iran')

INSERT INTO CityEntitie VALUES('Kokshetau','53.3','69.42','Kazakhstan')

INSERT INTO CityEntitie VALUES('Idlib','35.9297','36.6317','Syria')

INSERT INTO CityEntitie VALUES('Xai-Xai','-25.04','33.64','Mozambique')

INSERT INTO CityEntitie VALUES('Kamina','-8.73','25.01','Congo (Kinshasa)')

INSERT INTO CityEntitie VALUES('Larissa','39.6304','22.42','Greece')

INSERT INTO CityEntitie VALUES('Moroni','-11.7042','43.2402','Comoros')

INSERT INTO CityEntitie VALUES('Kamyshin','50.0804','45.4','Russia')

INSERT INTO CityEntitie VALUES('Karab?k','41.2','32.6','Turkey')

INSERT INTO CityEntitie VALUES('Cambridge','52.2004','0.1166','United Kingdom')

INSERT INTO CityEntitie VALUES('Kent','47.3887','-122.2128','United States')

INSERT INTO CityEntitie VALUES('Besançon','47.23','6.03','France')

INSERT INTO CityEntitie VALUES('Greenville','35.5957','-77.3764','United States')

INSERT INTO CityEntitie VALUES('Sakakah','30','40.1333','Saudi Arabia')

INSERT INTO CityEntitie VALUES('Jima','7.68','36.83','Ethiopia')

INSERT INTO CityEntitie VALUES('Girga','26.3304','31.88','Egypt')

INSERT INTO CityEntitie VALUES('Manzanillo','20.3438','-77.1166','Cuba')

INSERT INTO CityEntitie VALUES('Surt','31.21','16.59','Libya')

INSERT INTO CityEntitie VALUES('Al Hasakah','36.4833','40.75','Syria')

INSERT INTO CityEntitie VALUES('Darnah','32.7648','22.6391','Libya')

INSERT INTO CityEntitie VALUES('Ekibastuz','51.73','75.3199','Kazakhstan')

INSERT INTO CityEntitie VALUES('Divo','5.839','-5.36','Côte DIvoire')

INSERT INTO CityEntitie VALUES('Simi Valley','34.2663','-118.749','United States')

INSERT INTO CityEntitie VALUES('San Nicolas','-33.33','-60.24','Argentina')

INSERT INTO CityEntitie VALUES('Bragança Paulista','-22.95','-46.55','Brazil')

INSERT INTO CityEntitie VALUES('Worcester','-33.64','19.4399','South Africa')

INSERT INTO CityEntitie VALUES('Tukuyu','-9.2496','33.64','Tanzania')

INSERT INTO CityEntitie VALUES('Port Blair','11.667','92.736','India')

INSERT INTO CityEntitie VALUES('Elbl?g','54.19','19.4027','Poland')

INSERT INTO CityEntitie VALUES('Paneve?ys','55.74','24.37','Lithuania')

INSERT INTO CityEntitie VALUES('Wau','7.7','27.99','South Sudan')

INSERT INTO CityEntitie VALUES('Burlington','36.0762','-79.4687','United States')

INSERT INTO CityEntitie VALUES('Alipur Duar','26.4837','89.5667','India')

INSERT INTO CityEntitie VALUES('Lafia','8.4904','8.52','Nigeria')

INSERT INTO CityEntitie VALUES('Santa Clara','37.3646','-121.9679','United States')

INSERT INTO CityEntitie VALUES('Sancti Sp?ritus','21.9301','-79.4425','Cuba')

INSERT INTO CityEntitie VALUES('Pakokku','21.332','95.0866','Burma')

INSERT INTO CityEntitie VALUES('Hathras','27.6','78.05','India')

INSERT INTO CityEntitie VALUES('Neftekamsk','56.0835','54.2631','Russia')

INSERT INTO CityEntitie VALUES('Gafsa','34.4204','8.78','Tunisia')

INSERT INTO CityEntitie VALUES('Johnson City','36.3406','-82.3804','United States')

INSERT INTO CityEntitie VALUES('Sogamoso','5.72','-72.94','Colombia')

INSERT INTO CityEntitie VALUES('Edirne','41.6704','26.57','Turkey')

INSERT INTO CityEntitie VALUES('Songea','-10.68','35.65','Tanzania')

INSERT INTO CityEntitie VALUES('Tây Ninh','11.323','106.147','Vietnam')

INSERT INTO CityEntitie VALUES('Loja','-3.99','-79.21','Ecuador')

INSERT INTO CityEntitie VALUES('Macheng','31.18','115.03','China')

INSERT INTO CityEntitie VALUES('Car?pano','10.67','-63.23','Venezuela')

INSERT INTO CityEntitie VALUES('Tarragona','41.1204','1.25','Spain')

INSERT INTO CityEntitie VALUES('Itapetininga','-23.59','-48.04','Brazil')

INSERT INTO CityEntitie VALUES('Tengchong','25.0333','98.4666','China')

INSERT INTO CityEntitie VALUES('Boulder','40.0249','-105.2523','United States')

INSERT INTO CityEntitie VALUES('Bellingham','48.7543','-122.4687','United States')

INSERT INTO CityEntitie VALUES('Montego Bay','18.4667','-77.9167','Jamaica')

INSERT INTO CityEntitie VALUES('Magway','20.1445','94.9196','Burma')

INSERT INTO CityEntitie VALUES('Pangkalpinang','-2.08','106.15','Indonesia')

INSERT INTO CityEntitie VALUES('Malanje','-9.54','16.34','Angola')

INSERT INTO CityEntitie VALUES('Gashua','12.8705','11.04','Nigeria')

INSERT INTO CityEntitie VALUES('Leeuwarden','53.2504','5.7834','Netherlands')

INSERT INTO CityEntitie VALUES('Lubango','-14.91','13.49','Angola')

INSERT INTO CityEntitie VALUES('Fort Smith','35.3494','-94.3695','United States')

INSERT INTO CityEntitie VALUES('Sorong','-0.8554','131.285','Indonesia')

INSERT INTO CityEntitie VALUES('Kumbo','6.2204','10.68','Cameroon')

INSERT INTO CityEntitie VALUES('Ghardaia','32.49','3.67','Algeria')

INSERT INTO CityEntitie VALUES('Kelowna','49.9','-119.4833','Canada')

INSERT INTO CityEntitie VALUES('Lynchburg','37.4003','-79.1908','United States')

INSERT INTO CityEntitie VALUES('Marv Dasht','29.8014','52.8215','Iran')

INSERT INTO CityEntitie VALUES('Kenosha','42.5863','-87.8759','United States')

INSERT INTO CityEntitie VALUES('Semnan','35.5548','53.3743','Iran')

INSERT INTO CityEntitie VALUES('Nueva San Salvador','13.674','-89.29','El Salvador')

INSERT INTO CityEntitie VALUES('Zl?n','49.2304','17.65','Czechia')

INSERT INTO CityEntitie VALUES('Fasa','28.9718','53.6715','Iran')

INSERT INTO CityEntitie VALUES('Lysychansk','48.9204','38.4274','Ukraine')

INSERT INTO CityEntitie VALUES('Drohobych','49.3444','23.4994','Ukraine')

INSERT INTO CityEntitie VALUES('M? Tho','10.3504','106.35','Vietnam')

INSERT INTO CityEntitie VALUES('Laiwu','36.2004','117.66','China')

INSERT INTO CityEntitie VALUES('Alagoinhas','-12.14','-38.43','Brazil')

INSERT INTO CityEntitie VALUES('Rudny','52.9527','63.13','Kazakhstan')

INSERT INTO CityEntitie VALUES('South Lyon','42.4614','-83.6526','United States')

INSERT INTO CityEntitie VALUES('Higuey','18.616','-68.708','Dominican Republic')

INSERT INTO CityEntitie VALUES('Round Rock','30.5252','-97.6659','United States')

INSERT INTO CityEntitie VALUES('Potchefstroom','-26.6996','27.1','South Africa')

INSERT INTO CityEntitie VALUES('Siracusa','37.0704','15.29','Italy')

INSERT INTO CityEntitie VALUES('Guelma','36.466','7.428','Algeria')

INSERT INTO CityEntitie VALUES('Uruguaiana','-29.7696','-57.09','Brazil')

INSERT INTO CityEntitie VALUES('Indramayu','-6.3356','108.319','Indonesia')

INSERT INTO CityEntitie VALUES('Gagnoa','6.1504','-5.88','Côte DIvoire')

INSERT INTO CityEntitie VALUES('Porto Seguro','-16.4296','-39.08','Brazil')

INSERT INTO CityEntitie VALUES('Jiaohe','43.7163','127.346','China')

INSERT INTO CityEntitie VALUES('Ségou','13.44','-6.26','Mali')

INSERT INTO CityEntitie VALUES('Pati','-6.7415','111.0347','Indonesia')

INSERT INTO CityEntitie VALUES('San Fernando de Apure','7.9','-67.4699','Venezuela')

INSERT INTO CityEntitie VALUES('Brits','-25.6296','27.78','South Africa')

INSERT INTO CityEntitie VALUES('Maina','13.4692','144.7332','Guam')

INSERT INTO CityEntitie VALUES('Maastricht','50.853','5.677','Netherlands')

INSERT INTO CityEntitie VALUES('Berkeley','37.8723','-122.276','United States')

INSERT INTO CityEntitie VALUES('Tekirda?','40.9909','27.51','Turkey')

INSERT INTO CityEntitie VALUES('Aalborg','57.0337','9.9166','Denmark')

INSERT INTO CityEntitie VALUES('Barbacena','-21.22','-43.77','Brazil')

INSERT INTO CityEntitie VALUES('Tauranga','-37.6964','176.1536','New Zealand')

INSERT INTO CityEntitie VALUES('Nancha','47.1364','129.2859','China')

INSERT INTO CityEntitie VALUES('Olmaliq','40.8504','69.595','Uzbekistan')

INSERT INTO CityEntitie VALUES('Xilinhot','43.9443','116.0443','China')

INSERT INTO CityEntitie VALUES('Billings','45.7889','-108.5503','United States')

INSERT INTO CityEntitie VALUES('Sassari','40.73','8.57','Italy')

INSERT INTO CityEntitie VALUES('Karaman','37.1815','33.215','Turkey')

INSERT INTO CityEntitie VALUES('Redding','40.5698','-122.365','United States')

INSERT INTO CityEntitie VALUES('Yuba City','39.1357','-121.6381','United States')

INSERT INTO CityEntitie VALUES('Manbij','36.5266','37.9563','Syria')

INSERT INTO CityEntitie VALUES('Duluth','46.7757','-92.1392','United States')

INSERT INTO CityEntitie VALUES('Larache','35.2004','-6.16','Morocco')

INSERT INTO CityEntitie VALUES('Huanghua','38.3704','117.33','China')

INSERT INTO CityEntitie VALUES('Pearland','29.5585','-95.3215','United States')

INSERT INTO CityEntitie VALUES('Puerto Plata','19.7902','-70.6902','Dominican Republic')

INSERT INTO CityEntitie VALUES('Maxixe','-23.866','35.3886','Mozambique')

INSERT INTO CityEntitie VALUES('Trois-Rivières','46.35','-72.5499','Canada')

INSERT INTO CityEntitie VALUES('Leominster','42.5209','-71.7717','United States')

INSERT INTO CityEntitie VALUES('Saginaw','43.4199','-83.9501','United States')

INSERT INTO CityEntitie VALUES('Ja?','-22.2896','-48.57','Brazil')

INSERT INTO CityEntitie VALUES('Tetovo','42.0092','20.9701','Macedonia')

INSERT INTO CityEntitie VALUES('Iowa City','41.6559','-91.5304','United States')

INSERT INTO CityEntitie VALUES('Udine','46.07','13.24','Italy')

INSERT INTO CityEntitie VALUES('Arvada','39.8321','-105.151','United States')

INSERT INTO CityEntitie VALUES('Pleven','43.4238','24.6134','Bulgaria')

INSERT INTO CityEntitie VALUES('Zielona G?ra','51.9504','15.5','Poland')

INSERT INTO CityEntitie VALUES('Anuradhapura','8.35','80.3833','Sri Lanka')

INSERT INTO CityEntitie VALUES('Qomsheh','32.0115','51.8597','Iran')

INSERT INTO CityEntitie VALUES('Tongren','27.6804','109.13','China')

INSERT INTO CityEntitie VALUES('Berdyansk','46.7568','36.7868','Ukraine')

INSERT INTO CityEntitie VALUES('Bandundu','-3.31','17.38','Congo (Kinshasa)')

INSERT INTO CityEntitie VALUES('Bama','11.5204','13.69','Nigeria')

INSERT INTO CityEntitie VALUES('Seaside','36.6224','-121.8191','United States')

INSERT INTO CityEntitie VALUES('Ourense','42.33','-7.87','Spain')

INSERT INTO CityEntitie VALUES('Kingsport','36.522','-82.5453','United States')

INSERT INTO CityEntitie VALUES('Set?bal','38.53','-8.9','Portugal')

INSERT INTO CityEntitie VALUES('Nakhon Pathom','13.818','100.064','Thailand')

INSERT INTO CityEntitie VALUES('Sokodé','8.9905','1.15','Togo')

INSERT INTO CityEntitie VALUES('Agadez','16.9959','7.9828','Niger')

INSERT INTO CityEntitie VALUES('Jalingo','8.9004','11.36','Nigeria')

INSERT INTO CityEntitie VALUES('Dunedin','-45.8854','170.491','New Zealand')

INSERT INTO CityEntitie VALUES('Achinsk','56.27','90.5','Russia')

INSERT INTO CityEntitie VALUES('Anaco','9.44','-64.46','Venezuela')

INSERT INTO CityEntitie VALUES('Punta Arenas','-53.165','-70.94','Chile')

INSERT INTO CityEntitie VALUES('San Mart?n','-33.07','-68.49','Argentina')

INSERT INTO CityEntitie VALUES('Independence','39.0871','-94.3503','United States')

INSERT INTO CityEntitie VALUES('Langsa','4.6736','97.9664','Indonesia')

INSERT INTO CityEntitie VALUES('Vyborg','60.7039','28.7549','Russia')

INSERT INTO CityEntitie VALUES('Calabozo','8.9304','-67.44','Venezuela')

INSERT INTO CityEntitie VALUES('Chiang Rai','19.9119','99.8265','Thailand')

INSERT INTO CityEntitie VALUES('Nkongsamba','4.9604','9.94','Cameroon')

INSERT INTO CityEntitie VALUES('Kindia','10.06','-12.87','Guinea')

INSERT INTO CityEntitie VALUES('Gao','16.2666','-0.05','Mali')

INSERT INTO CityEntitie VALUES('Port-Gentil','-0.72','8.78','Gabon')

INSERT INTO CityEntitie VALUES('Richardson','32.9717','-96.7092','United States')

INSERT INTO CityEntitie VALUES('Rochester','44.0151','-92.4778','United States')

INSERT INTO CityEntitie VALUES('Huanren','41.2563','125.346','China')

INSERT INTO CityEntitie VALUES('Taonan','45.3304','122.78','China')

INSERT INTO CityEntitie VALUES('Puno','-15.8329','-70.0333','Peru')

INSERT INTO CityEntitie VALUES('Jaén','37.7704','-3.8','Spain')

INSERT INTO CityEntitie VALUES('Nizhyn','51.0541','31.8903','Ukraine')

INSERT INTO CityEntitie VALUES('Yakeshi','49.2804','120.73','China')

INSERT INTO CityEntitie VALUES('Boké','10.94','-14.3','Guinea')

INSERT INTO CityEntitie VALUES('Gilroy','37.0047','-121.5843','United States')

INSERT INTO CityEntitie VALUES('Cherkessk','44.2904','42.06','Russia')

INSERT INTO CityEntitie VALUES('El Monte','34.074','-118.0291','United States')

INSERT INTO CityEntitie VALUES('Navajoa','27.0819','-109.4546','Mexico')

INSERT INTO CityEntitie VALUES('Rock Hill','34.9413','-81.025','United States')

INSERT INTO CityEntitie VALUES('Tahoua','14.9','5.2599','Niger')

INSERT INTO CityEntitie VALUES('Yelets','52.58','38.5','Russia')

INSERT INTO CityEntitie VALUES('Buizhou','37.3704','118.02','China')

INSERT INTO CityEntitie VALUES('Sri Jawewardenepura Kotte','6.9','79.95','Sri Lanka')

INSERT INTO CityEntitie VALUES('Inhambane','-23.858','35.3398','Mozambique')

INSERT INTO CityEntitie VALUES('Clearwater','27.9786','-82.7622','United States')

INSERT INTO CityEntitie VALUES('Monroe','32.5184','-92.0774','United States')

INSERT INTO CityEntitie VALUES('Dover','39.1603','-75.5203','United States')

INSERT INTO CityEntitie VALUES('Carlsbad','33.1246','-117.2837','United States')

INSERT INTO CityEntitie VALUES('Toliara','-23.3568','43.69','Madagascar')

INSERT INTO CityEntitie VALUES('Goulimine','28.98','-10.07','Morocco')

INSERT INTO CityEntitie VALUES('Pouso Alegre','-22.22','-45.94','Brazil')

INSERT INTO CityEntitie VALUES('Kulob','37.9212','69.7757','Tajikistan')

INSERT INTO CityEntitie VALUES('Nabeul','36.4603','10.73','Tunisia')

INSERT INTO CityEntitie VALUES('Tuguegarao','17.6131','121.7269','Philippines')

INSERT INTO CityEntitie VALUES('Norman','35.2335','-97.3471','United States')

INSERT INTO CityEntitie VALUES('Ciudad Valles','21.98','-99.02','Mexico')

INSERT INTO CityEntitie VALUES('Abilene','32.4543','-99.7384','United States')

INSERT INTO CityEntitie VALUES('Bloomington','39.1637','-86.5256','United States')

INSERT INTO CityEntitie VALUES('Delicias','28.2','-105.5','Mexico')

INSERT INTO CityEntitie VALUES('Boto?ani','47.7484','26.6597','Romania')

INSERT INTO CityEntitie VALUES('Kamyanets-Podilskyy','48.6843','26.5809','Ukraine')

INSERT INTO CityEntitie VALUES('Texas City','29.4128','-94.9658','United States')

INSERT INTO CityEntitie VALUES('Giyon','8.5304','37.97','Ethiopia')

INSERT INTO CityEntitie VALUES('Shashemene','7.2004','38.59','Ethiopia')

INSERT INTO CityEntitie VALUES('Temecula','33.4928','-117.1314','United States')

INSERT INTO CityEntitie VALUES('Utica','43.0961','-75.226','United States')

INSERT INTO CityEntitie VALUES('Valparai','10.3204','76.97','India')

INSERT INTO CityEntitie VALUES('Kuito','-12.38','16.94','Angola')

INSERT INTO CityEntitie VALUES('Maribor','46.5405','15.65','Slovenia')

INSERT INTO CityEntitie VALUES('Saint Cloud','45.5339','-94.1719','United States')

INSERT INTO CityEntitie VALUES('Kingston','44.2337','-76.4833','Canada')

INSERT INTO CityEntitie VALUES('Siirt','37.944','41.933','Turkey')

INSERT INTO CityEntitie VALUES('Kankan','10.39','-9.31','Guinea')

INSERT INTO CityEntitie VALUES('Zhijiang','27.4409','109.678','China')

INSERT INTO CityEntitie VALUES('Saint George','37.0769','-113.577','United States')

INSERT INTO CityEntitie VALUES('West Jordan','40.6024','-112.0008','United States')

INSERT INTO CityEntitie VALUES('Kecskemét','46.9','19.7','Hungary')

INSERT INTO CityEntitie VALUES('Laghouat','33.81','2.88','Algeria')

INSERT INTO CityEntitie VALUES('Botucatu','-22.8796','-48.45','Brazil')

INSERT INTO CityEntitie VALUES('Costa Mesa','33.6667','-117.9135','United States')

INSERT INTO CityEntitie VALUES('Tobolsk','58.1998','68.2648','Russia')

INSERT INTO CityEntitie VALUES('Miami Gardens','25.9433','-80.2425','United States')

INSERT INTO CityEntitie VALUES('Curic?','-34.98','-71.24','Chile')

INSERT INTO CityEntitie VALUES('B?rum','59.9135','11.3472','Norway')

INSERT INTO CityEntitie VALUES('Cambridge','42.3758','-71.1184','United States')

INSERT INTO CityEntitie VALUES('Santa Cruz do Sul','-29.71','-52.44','Brazil')

INSERT INTO CityEntitie VALUES('Praia','14.9167','-23.5167','Cabo Verde')

INSERT INTO CityEntitie VALUES('Kipushi','-11.7596','27.25','Congo (Kinshasa)')

INSERT INTO CityEntitie VALUES('Kitami','43.8504','143.9','Japan')

INSERT INTO CityEntitie VALUES('Exeter','50.7004','-3.53','United Kingdom')

INSERT INTO CityEntitie VALUES('Downey','33.9379','-118.1312','United States')

INSERT INTO CityEntitie VALUES('Coeur dAlene','47.7039','-116.7933','United States')

INSERT INTO CityEntitie VALUES('Malé','4.1667','73.4999','Maldives')

INSERT INTO CityEntitie VALUES('Tokmak','42.8299','75.2846','Kyrgyzstan')

INSERT INTO CityEntitie VALUES('Westminster','39.8837','-105.0627','United States')

INSERT INTO CityEntitie VALUES('Nefteyugansk','61.0777','72.7027','Russia')

INSERT INTO CityEntitie VALUES('Shaowu','27.3004','117.5','China')

INSERT INTO CityEntitie VALUES('Mojokerto','-7.4696','112.43','Indonesia')

INSERT INTO CityEntitie VALUES('Satu Mare','47.792','22.885','Romania')

INSERT INTO CityEntitie VALUES('Elgin','42.0385','-88.3229','United States')

INSERT INTO CityEntitie VALUES('Guanajuato','21.0204','-101.28','Mexico')

INSERT INTO CityEntitie VALUES('El Centro','32.7873','-115.5579','United States')

INSERT INTO CityEntitie VALUES('Mazyr','52.046','29.2722','Belarus')

INSERT INTO CityEntitie VALUES('Iguala','18.37','-99.54','Mexico')

INSERT INTO CityEntitie VALUES('Waterloo','42.492','-92.3522','United States')

INSERT INTO CityEntitie VALUES('Juba','4.83','31.58','South Sudan')

INSERT INTO CityEntitie VALUES('Nakhon Sawan','15.7','100.07','Thailand')

INSERT INTO CityEntitie VALUES('Iringa','-7.7696','35.69','Tanzania')

INSERT INTO CityEntitie VALUES('Zwolle','52.524','6.097','Netherlands')

INSERT INTO CityEntitie VALUES('Kandy','7.28','80.67','Sri Lanka')

INSERT INTO CityEntitie VALUES('Conselheiro Lafaiete','-20.67','-43.79','Brazil')

INSERT INTO CityEntitie VALUES('Daugavpils','55.88','26.51','Latvia')

INSERT INTO CityEntitie VALUES('Kuqa','41.7277','82.9364','China')

INSERT INTO CityEntitie VALUES('Magelang','-7.4696','110.18','Indonesia')

INSERT INTO CityEntitie VALUES('Santa Rosa','-36.62','-64.3','Argentina')

INSERT INTO CityEntitie VALUES('Lowell','42.6389','-71.3217','United States')

INSERT INTO CityEntitie VALUES('Jawhar','2.767','45.5166','Somalia')

INSERT INTO CityEntitie VALUES('Gresham','45.5023','-122.4414','United States')

INSERT INTO CityEntitie VALUES('Algeciras','36.1267','-5.4665','Spain')

INSERT INTO CityEntitie VALUES('Nsukka','6.867','7.3834','Nigeria')

INSERT INTO CityEntitie VALUES('Nguru','12.8804','10.45','Nigeria')

INSERT INTO CityEntitie VALUES('North Charleston','32.9086','-80.0705','United States')

INSERT INTO CityEntitie VALUES('Balkanabat','39.5124','54.3649','Turkmenistan')

INSERT INTO CityEntitie VALUES('Manzanillo','19.0496','-104.3231','Mexico')

INSERT INTO CityEntitie VALUES('Pemba','-12.983','40.5323','Mozambique')

INSERT INTO CityEntitie VALUES('Volos','39.37','22.95','Greece')

INSERT INTO CityEntitie VALUES('Inglewood','33.9566','-118.3444','United States')

INSERT INTO CityEntitie VALUES('Noyabrsk','63.1665','75.6165','Russia')

INSERT INTO CityEntitie VALUES('Yaynangyoung','20.4615','94.881','Burma')

INSERT INTO CityEntitie VALUES('Manzini','-26.495','31.388','Swaziland')

INSERT INTO CityEntitie VALUES('Pompano Beach','26.2428','-80.1312','United States')

INSERT INTO CityEntitie VALUES('Centennial','39.5926','-104.8674','United States')

INSERT INTO CityEntitie VALUES('West Palm Beach','26.7467','-80.1314','United States')

INSERT INTO CityEntitie VALUES('Mzuzu','-11.46','34.02','Malawi')

INSERT INTO CityEntitie VALUES('Bouïra','36.3805','3.9','Algeria')

INSERT INTO CityEntitie VALUES('Garanhuns','-8.89','-36.5','Brazil')

INSERT INTO CityEntitie VALUES('Everett','47.9524','-122.167','United States')

INSERT INTO CityEntitie VALUES('Santa Fe','35.6619','-105.9819','United States')

INSERT INTO CityEntitie VALUES('Linqing','36.8504','115.68','China')

INSERT INTO CityEntitie VALUES('Richmond','37.9477','-122.339','United States')

INSERT INTO CityEntitie VALUES('David','8.4333','-82.4333','Panama')

INSERT INTO CityEntitie VALUES('Milagro','-2.1796','-79.6','Ecuador')

INSERT INTO CityEntitie VALUES('Bataysk','47.1368','39.7449','Russia')

INSERT INTO CityEntitie VALUES('Hailun','47.4504','126.93','China')

INSERT INTO CityEntitie VALUES('Lichinga','-13.3','35.24','Mozambique')

INSERT INTO CityEntitie VALUES('Kandi','11.1304','2.94','Benin')

INSERT INTO CityEntitie VALUES('Dali','34.7953','109.9378','China')

INSERT INTO CityEntitie VALUES('Clovis','36.8278','-119.6841','United States')

INSERT INTO CityEntitie VALUES('Tieli','46.9504','128.05','China')

INSERT INTO CityEntitie VALUES('Ipiales','0.8304','-77.65','Colombia')

INSERT INTO CityEntitie VALUES('Catanduva','-21.14','-48.98','Brazil')

INSERT INTO CityEntitie VALUES('Szombathely','47.2253','16.6287','Hungary')

INSERT INTO CityEntitie VALUES('Pottstown','40.2508','-75.6445','United States')

INSERT INTO CityEntitie VALUES('Heihe','50.25','127.446','China')

INSERT INTO CityEntitie VALUES('Siem Reap','13.3666','103.85','Cambodia')

INSERT INTO CityEntitie VALUES('Szolnok','47.1864','20.1794','Hungary')

INSERT INTO CityEntitie VALUES('Sergiyev Posad','56.33','38.17','Russia')

INSERT INTO CityEntitie VALUES('Tumbes','-3.57','-80.46','Peru')

INSERT INTO CityEntitie VALUES('San Rafael','-34.6','-68.3333','Argentina')

INSERT INTO CityEntitie VALUES('Matagalpa','12.9171','-85.9167','Nicaragua')

INSERT INTO CityEntitie VALUES('Tuxpam','20.9604','-97.41','Mexico')

INSERT INTO CityEntitie VALUES('Leninsk Kuznetsky','54.66','86.17','Russia')

INSERT INTO CityEntitie VALUES('Soubré','5.7904','-6.61','Côte DIvoire')

INSERT INTO CityEntitie VALUES('Jacksonville','34.7323','-77.3962','United States')

INSERT INTO CityEntitie VALUES('Karamay','45.5899','84.8599','China')

INSERT INTO CityEntitie VALUES('Kaposv?r','46.367','17.8','Hungary')

INSERT INTO CityEntitie VALUES('Mopti','14.49','-4.18','Mali')

INSERT INTO CityEntitie VALUES('Trincomalee','8.569','81.233','Sri Lanka')

INSERT INTO CityEntitie VALUES('Broken Arrow','36.0365','-95.7808','United States')

INSERT INTO CityEntitie VALUES('Burlington','44.4877','-73.2314','United States')

INSERT INTO CityEntitie VALUES('Nehe','48.49','124.88','China')

INSERT INTO CityEntitie VALUES('Kyzyl','51.7067','94.3831','Russia')

INSERT INTO CityEntitie VALUES('Mascara','35.4004','0.14','Algeria')

INSERT INTO CityEntitie VALUES('Oktyabrskiy','54.46','53.46','Russia')

INSERT INTO CityEntitie VALUES('Birnin Kebbi','12.4504','4.1999','Nigeria')

INSERT INTO CityEntitie VALUES('En Nuhud','12.6904','28.42','Sudan')

INSERT INTO CityEntitie VALUES('Estel?','13.09','-86.36','Nicaragua')

INSERT INTO CityEntitie VALUES('Barletta','41.32','16.27','Italy')

INSERT INTO CityEntitie VALUES('Trento','46.0804','11.12','Italy')

INSERT INTO CityEntitie VALUES('Escuintla','15.33','-92.63','Mexico')

INSERT INTO CityEntitie VALUES('Escuintla','14.3004','-90.78','Guatemala')

INSERT INTO CityEntitie VALUES('West Covina','34.0555','-117.9113','United States')

INSERT INTO CityEntitie VALUES('Kogon','39.7211','64.5458','Uzbekistan')

INSERT INTO CityEntitie VALUES('Rimnicu Vilcea','45.11','24.383','Romania')

INSERT INTO CityEntitie VALUES('Koszalin','54.2','16.1833','Poland')

INSERT INTO CityEntitie VALUES('Obninsk','55.0804','36.62','Russia')

INSERT INTO CityEntitie VALUES('Shinyanga','-3.6596','33.42','Tanzania')

INSERT INTO CityEntitie VALUES('Luxembourg','49.6117','6.13','Luxembourg')

INSERT INTO CityEntitie VALUES('Turlock','37.5053','-120.8588','United States')

INSERT INTO CityEntitie VALUES('V?ster?s','59.63','16.54','Sweden')

INSERT INTO CityEntitie VALUES('Chapayevsk','52.9743','49.7243','Russia')

INSERT INTO CityEntitie VALUES('Louangphrabang','19.8845','102.1416','Laos')

INSERT INTO CityEntitie VALUES('Ilebo','-4.3196','20.61','Congo (Kinshasa)')

INSERT INTO CityEntitie VALUES('Golmud','36.4166','94.8833','China')

INSERT INTO CityEntitie VALUES('Apucarana','-23.55','-51.47','Brazil')

INSERT INTO CityEntitie VALUES('Daly City','37.6863','-122.4684','United States')

INSERT INTO CityEntitie VALUES('Eau Claire','44.8203','-91.4951','United States')

INSERT INTO CityEntitie VALUES('Pardubice','50.0404','15.76','Czechia')

INSERT INTO CityEntitie VALUES('Ratchaburi','13.5419','99.8215','Thailand')

INSERT INTO CityEntitie VALUES('Elista','46.3287','44.2087','Russia')

INSERT INTO CityEntitie VALUES('Taungoo','18.9483','96.4179','Burma')

INSERT INTO CityEntitie VALUES('Hillsboro','45.5271','-122.9358','United States')

INSERT INTO CityEntitie VALUES('Kandalaksha','67.1643','32.4144','Russia')

INSERT INTO CityEntitie VALUES('Sandy Springs','33.9366','-84.3703','United States')

INSERT INTO CityEntitie VALUES('Douliou','23.7075','120.5439','Taiwan')

INSERT INTO CityEntitie VALUES('Coimbra','40.2004','-8.4167','Portugal')

INSERT INTO CityEntitie VALUES('Drobeta-Turnu Severin','44.6459','22.6659','Romania')

INSERT INTO CityEntitie VALUES('Longjiang','47.3404','123.18','China')

INSERT INTO CityEntitie VALUES('Namur','50.4704','4.87','Belgium')

INSERT INTO CityEntitie VALUES('Sioux City','42.4959','-96.3901','United States')

INSERT INTO CityEntitie VALUES('Chico','39.7574','-121.815','United States')

INSERT INTO CityEntitie VALUES('Novotroitsk','51.2','58.33','Russia')

INSERT INTO CityEntitie VALUES('Raba','-8.45','118.7666','Indonesia')

INSERT INTO CityEntitie VALUES('Bagé','-31.32','-54.1','Brazil')

INSERT INTO CityEntitie VALUES('Norwalk','33.9069','-118.0829','United States')

INSERT INTO CityEntitie VALUES('Salisbury','38.3755','-75.5867','United States')

INSERT INTO CityEntitie VALUES('Lewisville','33.0453','-96.9823','United States')

INSERT INTO CityEntitie VALUES('Derbent','42.0578','48.2774','Russia')

INSERT INTO CityEntitie VALUES('Suceava','47.6377','26.2593','Romania')

INSERT INTO CityEntitie VALUES('Annecy','45.9','6.1167','France')

INSERT INTO CityEntitie VALUES('Salto','-31.3903','-57.9687','Uruguay')

INSERT INTO CityEntitie VALUES('Azare','11.6804','10.19','Nigeria')

INSERT INTO CityEntitie VALUES('Lahad Datu','5.0464','118.336','Malaysia')

INSERT INTO CityEntitie VALUES('Sadah','16.9398','43.8498','Yemen')

INSERT INTO CityEntitie VALUES('Assab','13.01','42.73','Eritrea')

INSERT INTO CityEntitie VALUES('Kon Tum','14.3838','107.9833','Vietnam')

INSERT INTO CityEntitie VALUES('Fresnillo','23.1704','-102.86','Mexico')

INSERT INTO CityEntitie VALUES('Lugano','46.0004','8.9667','Switzerland')

INSERT INTO CityEntitie VALUES('Queenstown','-31.8996','26.88','South Africa')

INSERT INTO CityEntitie VALUES('Hoa Binh','20.8137','105.3383','Vietnam')

INSERT INTO CityEntitie VALUES('Granada?','11.9337','-85.95','Nicaragua')

INSERT INTO CityEntitie VALUES('Kongolo','-5.3795','26.98','Congo (Kinshasa)')

INSERT INTO CityEntitie VALUES('Jizan','16.9066','42.5566','Saudi Arabia')

INSERT INTO CityEntitie VALUES('Davie','26.079','-80.287','United States')

INSERT INTO CityEntitie VALUES('Foc?ani','45.6966','27.1865','Romania')

INSERT INTO CityEntitie VALUES('Linhares','-19.39','-40.05','Brazil')

INSERT INTO CityEntitie VALUES('Cottbus','51.7704','14.33','Germany')

INSERT INTO CityEntitie VALUES('Koutiala','12.3904','-5.47','Mali')

INSERT INTO CityEntitie VALUES('League City','29.4874','-95.1087','United States')

INSERT INTO CityEntitie VALUES('Prijedor','44.9804','16.7','Bosnia And Herzegovina')

INSERT INTO CityEntitie VALUES('Burbank','34.1879','-118.3234','United States')

INSERT INTO CityEntitie VALUES('San Mateo','37.5522','-122.3122','United States')

INSERT INTO CityEntitie VALUES('Jena','50.9304','11.58','Germany')

INSERT INTO CityEntitie VALUES('Gera','50.8704','12.07','Germany')

INSERT INTO CityEntitie VALUES('Torbat-e Jam','35.2233','60.6129','Iran')

INSERT INTO CityEntitie VALUES('Nong Khai','17.8733','102.7479','Thailand')

INSERT INTO CityEntitie VALUES('Brindisi','40.6403','17.93','Italy')

INSERT INTO CityEntitie VALUES('Mmabatho','-25.83','25.61','South Africa')

INSERT INTO CityEntitie VALUES('Zhezqazghan','47.78','67.77','Kazakhstan')

INSERT INTO CityEntitie VALUES('Tandil','-37.32','-59.15','Argentina')

INSERT INTO CityEntitie VALUES('San Antonio','-33.5995','-71.61','Chile')

INSERT INTO CityEntitie VALUES('Maumere','-8.6189','122.2123','Indonesia')

INSERT INTO CityEntitie VALUES('Albury','-36.06','146.92','Australia')

INSERT INTO CityEntitie VALUES('Al Qamishli','37.03','41.23','Syria')

INSERT INTO CityEntitie VALUES('Adigrat','14.2804','39.47','Ethiopia')

INSERT INTO CityEntitie VALUES('Abengourou','6.7304','-3.49','Côte DIvoire')

INSERT INTO CityEntitie VALUES('Kiselevsk','54','86.64','Russia')

INSERT INTO CityEntitie VALUES('Kroonstad','-27.66','27.21','South Africa')

INSERT INTO CityEntitie VALUES('EdDamer','17.59','33.96','Sudan')

INSERT INTO CityEntitie VALUES('Loubomo','-4.1796','12.67','Congo (Brazzaville)')

INSERT INTO CityEntitie VALUES('El Cajon','32.8017','-116.9604','United States')

INSERT INTO CityEntitie VALUES('Kilinochchi','9.4004','80.3999','Sri Lanka')

INSERT INTO CityEntitie VALUES('Ahar','38.4829','47.0629','Iran')

INSERT INTO CityEntitie VALUES('Biak','-1.1615','136.0485','Indonesia')

INSERT INTO CityEntitie VALUES('Gardiz','33.6001','69.2146','Afghanistan')

INSERT INTO CityEntitie VALUES('Rialto','34.1128','-117.3885','United States')

INSERT INTO CityEntitie VALUES('Konotop','51.2424','33.209','Ukraine')

INSERT INTO CityEntitie VALUES('Matruh','31.3504','27.23','Egypt')

INSERT INTO CityEntitie VALUES('Perabumulih','-3.4432','104.2315','Indonesia')

INSERT INTO CityEntitie VALUES('Guaymas','27.93','-110.89','Mexico')

INSERT INTO CityEntitie VALUES('Hidalgo del Parral','26.9334','-105.6666','Mexico')

INSERT INTO CityEntitie VALUES('Bend','44.0562','-121.3087','United States')

INSERT INTO CityEntitie VALUES('V?nh Long','10.256','105.964','Vietnam')

INSERT INTO CityEntitie VALUES('Velikiye Luki','56.32','30.52','Russia')

INSERT INTO CityEntitie VALUES('Palma Soriano','20.2172','-75.9988','Cuba')

INSERT INTO CityEntitie VALUES('Pedro Juan Caballero','-22.5446','-55.76','Paraguay')

INSERT INTO CityEntitie VALUES('Pakxe','15.1221','105.8183','Laos')

INSERT INTO CityEntitie VALUES('Roxas','11.5853','122.7511','Philippines')

INSERT INTO CityEntitie VALUES('Piatra-Neamt','46.94','26.383','Romania')

INSERT INTO CityEntitie VALUES('Grudzi?dz','53.4804','18.75','Poland')

INSERT INTO CityEntitie VALUES('Lower Hutt','-41.2037','174.9123','New Zealand')

INSERT INTO CityEntitie VALUES('Houma','35.62','111.21','China')

INSERT INTO CityEntitie VALUES('Ukhta','63.56','53.69','Russia')

INSERT INTO CityEntitie VALUES('Liberec','50.8','15.08','Czechia')

INSERT INTO CityEntitie VALUES('Charlottesville','38.0375','-78.4855','United States')

INSERT INTO CityEntitie VALUES('Bethal','-26.4696','29.45','South Africa')

INSERT INTO CityEntitie VALUES('La Crosse','43.8241','-91.2268','United States')

INSERT INTO CityEntitie VALUES('Temple','31.1076','-97.3894','United States')

INSERT INTO CityEntitie VALUES('Bontang','0.1333','117.5','Indonesia')

INSERT INTO CityEntitie VALUES('Teluk Intan','4.0119','101.0314','Malaysia')

INSERT INTO CityEntitie VALUES('Ibri','23.2254','56.517','Oman')

INSERT INTO CityEntitie VALUES('Dera Ismail Khan','31.829','70.8986','Pakistan')

INSERT INTO CityEntitie VALUES('Vista','33.1895','-117.2387','United States')

INSERT INTO CityEntitie VALUES('Kansk','56.19','95.71','Russia')

INSERT INTO CityEntitie VALUES('Renton','47.4758','-122.1905','United States')

INSERT INTO CityEntitie VALUES('Sarapul','56.4791','53.7987','Russia')

INSERT INTO CityEntitie VALUES('Maladzyechna','54.3188','26.8653','Belarus')

INSERT INTO CityEntitie VALUES('Olomouc','49.63','17.25','Czechia')

INSERT INTO CityEntitie VALUES('Barretos','-20.55','-48.58','Brazil')

INSERT INTO CityEntitie VALUES('Te?filo Otoni','-17.87','-41.5','Brazil')

INSERT INTO CityEntitie VALUES('Kilchu','40.9604','129.3204','Korea')

INSERT INTO CityEntitie VALUES('Duitama','5.8305','-73.02','Colombia')

INSERT INTO CityEntitie VALUES('Vanadzor','40.8128','44.4883','Armenia')

INSERT INTO CityEntitie VALUES('Tartu','58.3839','26.7099','Estonia')

INSERT INTO CityEntitie VALUES('Novara','45.45','8.62','Italy')

INSERT INTO CityEntitie VALUES('Sparks','39.5729','-119.7157','United States')

INSERT INTO CityEntitie VALUES('Holland','42.7677','-86.0984','United States')

INSERT INTO CityEntitie VALUES('Oum el Bouaghi','35.85','7.15','Algeria')

INSERT INTO CityEntitie VALUES('Solikamsk','59.67','56.75','Russia')

INSERT INTO CityEntitie VALUES('Vacaville','38.3592','-121.9686','United States')

INSERT INTO CityEntitie VALUES('Logan','41.7402','-111.8419','United States')

INSERT INTO CityEntitie VALUES('Allen','33.1088','-96.6735','United States')

INSERT INTO CityEntitie VALUES('Glazov','58.1232','52.6288','Russia')

INSERT INTO CityEntitie VALUES('Ancona','43.6004','13.4999','Italy')

INSERT INTO CityEntitie VALUES('Bukoba','-1.3196','31.8','Tanzania')

INSERT INTO CityEntitie VALUES('Kpalimé','6.9004','0.63','Togo')

INSERT INTO CityEntitie VALUES('Sakata','38.92','139.8501','Japan')

INSERT INTO CityEntitie VALUES('Magangué','9.23','-74.74','Colombia')

INSERT INTO CityEntitie VALUES('Ust-Ulimsk','57.99','102.6333','Russia')

INSERT INTO CityEntitie VALUES('Sabha','27.0333','14.4333','Libya')

INSERT INTO CityEntitie VALUES('Lida','53.8885','25.2846','Belarus')

INSERT INTO CityEntitie VALUES('Longview','32.5192','-94.7622','United States')

INSERT INTO CityEntitie VALUES('Arlit','18.82','7.33','Niger')

INSERT INTO CityEntitie VALUES('Subotica','46.07','19.68','Serbia')

INSERT INTO CityEntitie VALUES('Chamdo','31.1667','97.2333','China')

INSERT INTO CityEntitie VALUES('Isna','25.2904','32.5499','Egypt')

INSERT INTO CityEntitie VALUES('Meridian','43.6113','-116.3972','United States')

INSERT INTO CityEntitie VALUES('San Angelo','31.4426','-100.4501','United States')

INSERT INTO CityEntitie VALUES('Tsuruoka','38.7004','139.8302','Japan')

INSERT INTO CityEntitie VALUES('Chanthaburi','12.6133','102.0979','Thailand')

INSERT INTO CityEntitie VALUES('Tumen','42.97','129.8201','China')

INSERT INTO CityEntitie VALUES('Urgut','39.4007','67.2607','Uzbekistan')

INSERT INTO CityEntitie VALUES('Wichita Falls','33.9072','-98.5293','United States')

INSERT INTO CityEntitie VALUES('Zelenodolsk','55.8406','48.655','Russia')

INSERT INTO CityEntitie VALUES('Galle','6.03','80.24','Sri Lanka')

INSERT INTO CityEntitie VALUES('Novoshakhtinsk','47.77','39.92','Russia')

INSERT INTO CityEntitie VALUES('Choluteca','13.3007','-87.1908','Honduras')

INSERT INTO CityEntitie VALUES('Longmont','40.1691','-105.0996','United States')

INSERT INTO CityEntitie VALUES('Thunder Bay','48.4462','-89.275','Canada')

INSERT INTO CityEntitie VALUES('Thika','-1.0396','37.09','Kenya')

INSERT INTO CityEntitie VALUES('Bam','29.1077','58.362','Iran')

INSERT INTO CityEntitie VALUES('Fengzhen','40.4547','113.1443','China')

INSERT INTO CityEntitie VALUES('Kwekwe','-18.9296','29.8','Zimbabwe')

INSERT INTO CityEntitie VALUES('Tuapse','44.1148','39.0644','Russia')

INSERT INTO CityEntitie VALUES('Talara','-4.58','-81.28','Peru')

INSERT INTO CityEntitie VALUES('Giresun','40.913','38.39','Turkey')

INSERT INTO CityEntitie VALUES('Kericho','-0.3596','35.28','Kenya')

INSERT INTO CityEntitie VALUES('?eské Bud?jovice','48.98','14.46','Czechia')

INSERT INTO CityEntitie VALUES('Lahti','60.9939','25.6649','Finland')

INSERT INTO CityEntitie VALUES('Kontagora','10.4004','5.4699','Nigeria')

INSERT INTO CityEntitie VALUES('Passos','-20.71','-46.61','Brazil')

INSERT INTO CityEntitie VALUES('Manteca','37.7938','-121.227','United States')

INSERT INTO CityEntitie VALUES('Thimphu','27.473','89.639','Bhutan')

INSERT INTO CityEntitie VALUES('Votkinsk','57.0304','53.99','Russia')

INSERT INTO CityEntitie VALUES('?rebro','59.2803','15.22','Sweden')

INSERT INTO CityEntitie VALUES('Serov','59.615','60.585','Russia')

INSERT INTO CityEntitie VALUES('Apatzing?n','19.08','-102.35','Mexico')

INSERT INTO CityEntitie VALUES('Boca Raton','26.3749','-80.1077','United States')

INSERT INTO CityEntitie VALUES('Jyv?skyl?','62.2603','25.75','Finland')

INSERT INTO CityEntitie VALUES('Balashov','51.5535','43.1631','Russia')

INSERT INTO CityEntitie VALUES('Iseyin','7.97','3.59','Nigeria')

INSERT INTO CityEntitie VALUES('Flensburg','54.7837','9.4333','Germany')

INSERT INTO CityEntitie VALUES('Zheleznogorsk','52.3548','35.4044','Russia')

INSERT INTO CityEntitie VALUES('Spokane Valley','47.6625','-117.2346','United States')

INSERT INTO CityEntitie VALUES('Atlixco','18.9','-98.45','Mexico')

INSERT INTO CityEntitie VALUES('Orem','40.2983','-111.6993','United States')

INSERT INTO CityEntitie VALUES('Ub?','-21.1196','-42.95','Brazil')

INSERT INTO CityEntitie VALUES('Ozamis','8.1462','123.8444','Philippines')

INSERT INTO CityEntitie VALUES('Ourinhos','-22.97','-49.87','Brazil')

INSERT INTO CityEntitie VALUES('Middletown','39.5032','-84.3659','United States')

INSERT INTO CityEntitie VALUES('Slavonski Brod','45.1603','18.0156','Croatia')

INSERT INTO CityEntitie VALUES('Mangochi','-14.4596','35.27','Malawi')

INSERT INTO CityEntitie VALUES('LAriana','36.8667','10.2','Tunisia')

INSERT INTO CityEntitie VALUES('Compton','33.893','-118.2275','United States')

INSERT INTO CityEntitie VALUES('Idaho Falls','43.4868','-112.0363','United States')

INSERT INTO CityEntitie VALUES('Trindade','-16.65','-49.5','Brazil')

INSERT INTO CityEntitie VALUES('Beaverton','45.4779','-122.8168','United States')

INSERT INTO CityEntitie VALUES('Arapongas','-23.41','-51.43','Brazil')

INSERT INTO CityEntitie VALUES('Bismarck','46.814','-100.7695','United States')

INSERT INTO CityEntitie VALUES('Bafra','41.5682','35.9069','Turkey')

INSERT INTO CityEntitie VALUES('Az Zahran','26.2914','50.1583','Saudi Arabia')

INSERT INTO CityEntitie VALUES('Turkistan','43.3016','68.2549','Kazakhstan')

INSERT INTO CityEntitie VALUES('Lawrence','38.9597','-95.2641','United States')

INSERT INTO CityEntitie VALUES('San Carlos del Zulia','9.0104','-71.92','Venezuela')

INSERT INTO CityEntitie VALUES('Târgu Jiu','45.045','23.274','Romania')

INSERT INTO CityEntitie VALUES('Prescott Valley','34.5982','-112.3178','United States')

INSERT INTO CityEntitie VALUES('Shangzhi','45.2204','127.97','China')

INSERT INTO CityEntitie VALUES('Guasave','25.5705','-108.47','Mexico')

INSERT INTO CityEntitie VALUES('Shostka','51.8734','33.4797','Ukraine')

INSERT INTO CityEntitie VALUES('Yasuj','30.659','51.594','Iran')

INSERT INTO CityEntitie VALUES('Bunia','1.5604','30.24','Congo (Kinshasa)')

INSERT INTO CityEntitie VALUES('Link?ping','58.41','15.6299','Sweden')

INSERT INTO CityEntitie VALUES('Federal Way','47.309','-122.3359','United States')

INSERT INTO CityEntitie VALUES('Schwerin','53.6333','11.4167','Germany')

INSERT INTO CityEntitie VALUES('Bolu','40.7363','31.6061','Turkey')

INSERT INTO CityEntitie VALUES('Mtwara','-10.2696','40.19','Tanzania')

INSERT INTO CityEntitie VALUES('Araguari','-18.64','-48.2','Brazil')

INSERT INTO CityEntitie VALUES('Yên B?i','21.705','104.875','Vietnam')

INSERT INTO CityEntitie VALUES('Corumb?','-19.016','-57.65','Brazil')

INSERT INTO CityEntitie VALUES('Half Way Tree','18.0333','-76.8','Jamaica')

INSERT INTO CityEntitie VALUES('Sliven','42.6794','26.33','Bulgaria')

INSERT INTO CityEntitie VALUES('Kelo','9.3171','15.8','Chad')

INSERT INTO CityEntitie VALUES('San Marcos','33.1349','-117.1744','United States')

INSERT INTO CityEntitie VALUES('Rio Rancho','35.2872','-106.6981','United States')

INSERT INTO CityEntitie VALUES('Erechim','-27.63','-52.27','Brazil')

INSERT INTO CityEntitie VALUES('Yola','9.21','12.48','Nigeria')

INSERT INTO CityEntitie VALUES('Tracy','37.726','-121.444','United States')

INSERT INTO CityEntitie VALUES('Aurangabad','24.7704','84.38','India')

INSERT INTO CityEntitie VALUES('Sungaipenuh','-2.0631','101.3964','Indonesia')

INSERT INTO CityEntitie VALUES('Bolzano','46.5004','11.36','Italy')

INSERT INTO CityEntitie VALUES('Mekele','13.5','39.47','Ethiopia')

INSERT INTO CityEntitie VALUES('Brockton','42.0821','-71.0242','United States')

INSERT INTO CityEntitie VALUES('Kuznetsk','53.1204','46.6','Russia')

INSERT INTO CityEntitie VALUES('Guliston','40.4957','68.7907','Uzbekistan')

INSERT INTO CityEntitie VALUES('South Gate','33.9447','-118.1926','United States')

INSERT INTO CityEntitie VALUES('San Carlos de Bariloche','-41.15','-71.3','Argentina')

INSERT INTO CityEntitie VALUES('Magadan','59.575','150.81','Russia')

INSERT INTO CityEntitie VALUES('Boli','45.7564','130.5759','China')

INSERT INTO CityEntitie VALUES('Catanzaro','38.9004','16.6','Italy')

INSERT INTO CityEntitie VALUES('Hradec Kr?lové','50.206','15.812','Czechia')

INSERT INTO CityEntitie VALUES('Juazeiro','-9.42','-40.5','Brazil')

INSERT INTO CityEntitie VALUES('Osijek','45.5504','18.68','Croatia')

INSERT INTO CityEntitie VALUES('Al Aqabah','29.527','35.0777','Jordan')

INSERT INTO CityEntitie VALUES('Biu','10.6204','12.19','Nigeria')

INSERT INTO CityEntitie VALUES('Latacunga','-0.9296','-78.61','Ecuador')

INSERT INTO CityEntitie VALUES('Hesperia','34.3974','-117.3144','United States')

INSERT INTO CityEntitie VALUES('Dobrich','43.5851','27.84','Bulgaria')

INSERT INTO CityEntitie VALUES('Naxcivan','39.2092','45.4122','Azerbaijan')

INSERT INTO CityEntitie VALUES('Roswell','34.0391','-84.3513','United States')

INSERT INTO CityEntitie VALUES('Nowra','-34.8828','150.6','Australia')

INSERT INTO CityEntitie VALUES('Mandeville','30.375','-90.0906','United States')

INSERT INTO CityEntitie VALUES('Vineland','39.4653','-74.9981','United States')

INSERT INTO CityEntitie VALUES('Pre?ov','48.9997','21.2394','Slovakia')

INSERT INTO CityEntitie VALUES('Portsmouth','36.8468','-76.354','United States')

INSERT INTO CityEntitie VALUES('Dearborn','42.3127','-83.2129','United States')

INSERT INTO CityEntitie VALUES('K?r?ehir','39.142','34.171','Turkey')

INSERT INTO CityEntitie VALUES('Ponta Por?','-22.53','-55.73','Brazil')

INSERT INTO CityEntitie VALUES('Sunrise','26.1547','-80.2997','United States')

INSERT INTO CityEntitie VALUES('Bento Gonçalves','-29.1695','-51.52','Brazil')

INSERT INTO CityEntitie VALUES('Novy Urengoy','66.0833','76.6332','Russia')

INSERT INTO CityEntitie VALUES('Quincy','42.2516','-71.0183','United States')

INSERT INTO CityEntitie VALUES('Al Fujayrah','25.1234','56.3375','United Arab Emirates')

INSERT INTO CityEntitie VALUES('Lagos de Moreno','21.3704','-101.93','Mexico')

INSERT INTO CityEntitie VALUES('Yishui','35.7904','118.62','China')

INSERT INTO CityEntitie VALUES('Usti Nad Labem','50.663','14.081','Czechia')

INSERT INTO CityEntitie VALUES('Livonia','42.3972','-83.3733','United States')

INSERT INTO CityEntitie VALUES('Lynn','42.4779','-70.9663','United States')

INSERT INTO CityEntitie VALUES('Malindi','-3.21','40.1','Kenya')

INSERT INTO CityEntitie VALUES('Plantation','26.126','-80.2617','United States')

INSERT INTO CityEntitie VALUES('Slidell','30.2882','-89.7826','United States')

INSERT INTO CityEntitie VALUES('Samandagi','36.1171','35.9333','Turkey')

INSERT INTO CityEntitie VALUES('Manzhouli','49.6','117.43','China')

INSERT INTO CityEntitie VALUES('Tatu?','-23.35','-47.86','Brazil')

INSERT INTO CityEntitie VALUES('Michurinsk','52.9','40.5','Russia')

INSERT INTO CityEntitie VALUES('Trelew','-43.25','-65.33','Argentina')

INSERT INTO CityEntitie VALUES('Nazran','43.233','44.783','Russia')

INSERT INTO CityEntitie VALUES('Daan','45.5','124.3','China')

INSERT INTO CityEntitie VALUES('Polatl?','39.5842','32.1472','Turkey')

INSERT INTO CityEntitie VALUES('Sheberghan','36.658','65.7383','Afghanistan')

INSERT INTO CityEntitie VALUES('Bath','51.3837','-2.35','United Kingdom')

INSERT INTO CityEntitie VALUES('Bafang','5.1704','10.18','Cameroon')

INSERT INTO CityEntitie VALUES('Darwin','-12.4254','130.85','Australia')

INSERT INTO CityEntitie VALUES('Nouméa','-22.2625','166.4443','New Caledonia')

INSERT INTO CityEntitie VALUES('Ho','6.6004','0.47','Ghana')

INSERT INTO CityEntitie VALUES('Kineshma','57.47','42.13','Russia')

INSERT INTO CityEntitie VALUES('Coronel','-37.03','-73.16','Chile')

INSERT INTO CityEntitie VALUES('Wukari','7.8704','9.78','Nigeria')

INSERT INTO CityEntitie VALUES('Rosenheim','47.8503','12.1333','Germany')

INSERT INTO CityEntitie VALUES('Toowoomba','-27.5645','151.9555','Australia')

INSERT INTO CityEntitie VALUES('Quibd?','5.6904','-76.66','Colombia')

INSERT INTO CityEntitie VALUES('Carson','33.8374','-118.2559','United States')

INSERT INTO CityEntitie VALUES('Terre Haute','39.4654','-87.3763','United States')

INSERT INTO CityEntitie VALUES('Sampit','-2.5329','112.95','Indonesia')

INSERT INTO CityEntitie VALUES('Foumban','5.7304','10.9','Cameroon')

INSERT INTO CityEntitie VALUES('Patos','-7.0196','-37.29','Brazil')

INSERT INTO CityEntitie VALUES('Blacksburg','37.23','-80.428','United States')

INSERT INTO CityEntitie VALUES('Portsmouth','43.058','-70.7826','United States')

INSERT INTO CityEntitie VALUES('Tulcea','45.1993','28.7967','Romania')

INSERT INTO CityEntitie VALUES('Fuan','27.0704','119.62','China')

INSERT INTO CityEntitie VALUES('Maizuru','35.4504','135.3333','Japan')

INSERT INTO CityEntitie VALUES('Villa Mar?a','-32.41','-63.26','Argentina')

INSERT INTO CityEntitie VALUES('Santiago de Compostela','42.8829','-8.5411','Spain')

INSERT INTO CityEntitie VALUES('Itaituba','-4.2586','-55.925','Brazil')

INSERT INTO CityEntitie VALUES('Miami Beach','25.8171','-80.1396','United States')

INSERT INTO CityEntitie VALUES('Santa Monica','34.0232','-118.4813','United States')

INSERT INTO CityEntitie VALUES('Dhangarhi','28.695','80.593','Nepal')

INSERT INTO CityEntitie VALUES('Tubar?o','-28.48','-49.02','Brazil')

INSERT INTO CityEntitie VALUES('Dover','43.1887','-70.8845','United States')

INSERT INTO CityEntitie VALUES('Calais','50.9504','1.8333','France')

INSERT INTO CityEntitie VALUES('Carmel','39.9658','-86.1461','United States')

INSERT INTO CityEntitie VALUES('Ciudad Guzman','19.7104','-103.46','Mexico')

INSERT INTO CityEntitie VALUES('Ere?li','37.5063','34.0517','Turkey')

INSERT INTO CityEntitie VALUES('Kohima','25.667','94.1166','India')

INSERT INTO CityEntitie VALUES('Hanford','36.326','-119.654','United States')

INSERT INTO CityEntitie VALUES('Danjiangkou','32.52','111.5','China')

INSERT INTO CityEntitie VALUES('Lafayette','39.9949','-105.0997','United States')

INSERT INTO CityEntitie VALUES('Edmond','35.6689','-97.4159','United States')

INSERT INTO CityEntitie VALUES('Bugulma','54.5543','52.7943','Russia')

INSERT INTO CityEntitie VALUES('Kuopio','62.8943','27.6949','Finland')

INSERT INTO CityEntitie VALUES('Kilosa','-6.8396','36.99','Tanzania')

INSERT INTO CityEntitie VALUES('Fishers','39.9589','-85.967','United States')

INSERT INTO CityEntitie VALUES('Cabinda','-5.5596','12.19','Angola')

INSERT INTO CityEntitie VALUES('Arezzo','43.4617','11.875','Italy')

INSERT INTO CityEntitie VALUES('Westminster','33.7523','-117.9938','United States')

INSERT INTO CityEntitie VALUES('Grahamstown','-33.2996','26.52','South Africa')

INSERT INTO CityEntitie VALUES('Novoaltaysk','53.3993','83.9588','Russia')

INSERT INTO CityEntitie VALUES('Lawton','34.6171','-98.4204','United States')

INSERT INTO CityEntitie VALUES('Tecoman','18.9204','-103.88','Mexico')

INSERT INTO CityEntitie VALUES('Florence','34.1785','-79.7857','United States')

INSERT INTO CityEntitie VALUES('Helsingborg','56.0505','12.7','Sweden')

INSERT INTO CityEntitie VALUES('Mons','50.446','3.939','Belgium')

INSERT INTO CityEntitie VALUES('Kendu Bay','-0.3596','34.64','Kenya')

INSERT INTO CityEntitie VALUES('Muriaé','-21.13','-42.39','Brazil')

INSERT INTO CityEntitie VALUES('Pingdu','36.7904','119.94','China')

INSERT INTO CityEntitie VALUES('Ni?de','37.976','34.694','Turkey')

INSERT INTO CityEntitie VALUES('Livermore','37.6861','-121.7608','United States')

INSERT INTO CityEntitie VALUES('L?leburgaz','41.4067','27.3552','Turkey')

INSERT INTO CityEntitie VALUES('Chauk','20.9085','94.823','Burma')

INSERT INTO CityEntitie VALUES('Cuauhtémoc','28.4257','-106.8696','Mexico')

INSERT INTO CityEntitie VALUES('Drammen','59.7572','10.1907','Norway')

INSERT INTO CityEntitie VALUES('Moncton','46.0833','-64.7667','Canada')

INSERT INTO CityEntitie VALUES('Klagenfurt','46.6203','14.31','Austria')

INSERT INTO CityEntitie VALUES('Lees Summit','38.9172','-94.3816','United States')

INSERT INTO CityEntitie VALUES('Menifee','33.6909','-117.1849','United States')

INSERT INTO CityEntitie VALUES('San Leandro','37.7071','-122.1601','United States')

INSERT INTO CityEntitie VALUES('Albany','31.5776','-84.1762','United States')

INSERT INTO CityEntitie VALUES('Phetchaburi','13.1133','99.9412','Thailand')

INSERT INTO CityEntitie VALUES('Gatchina','59.5707','30.1333','Russia')

INSERT INTO CityEntitie VALUES('Bowling Green','36.9721','-86.4367','United States')

INSERT INTO CityEntitie VALUES('Itanhaem','-24.18','-46.8','Brazil')

INSERT INTO CityEntitie VALUES('Edinburg','26.319','-98.1607','United States')

INSERT INTO CityEntitie VALUES('Kirovo-Chepetsk','58.5544','50.0444','Russia')

INSERT INTO CityEntitie VALUES('Suffolk','36.6953','-76.6398','United States')

INSERT INTO CityEntitie VALUES('Missoula','46.8685','-114.0094','United States')

INSERT INTO CityEntitie VALUES('Reconquista','-29.1395','-59.65','Argentina')

INSERT INTO CityEntitie VALUES('Mbabane','-26.3167','31.1333','Swaziland')

INSERT INTO CityEntitie VALUES('Barahona','18.2004','-71.1','Dominican Republic')

INSERT INTO CityEntitie VALUES('Buea','4.155','9.231','Cameroon')

INSERT INTO CityEntitie VALUES('Limerick','52.6647','-8.6231','Ireland')

INSERT INTO CityEntitie VALUES('Francistown','-21.17','27.5','Botswana')

INSERT INTO CityEntitie VALUES('Chino','33.9836','-117.6653','United States')

INSERT INTO CityEntitie VALUES('Yegoryevsk','55.3848','39.0294','Russia')

INSERT INTO CityEntitie VALUES('J?nk?ping','57.7713','14.165','Sweden')

INSERT INTO CityEntitie VALUES('Santana do Livramento','-30.88','-55.53','Brazil')

INSERT INTO CityEntitie VALUES('Auburn','32.6084','-85.4897','United States')

INSERT INTO CityEntitie VALUES('Vlorë','40.4774','19.4982','Albania')

INSERT INTO CityEntitie VALUES('Chester','53.2','-2.92','United Kingdom')

INSERT INTO CityEntitie VALUES('Fujin','47.2704','132.02','China')

INSERT INTO CityEntitie VALUES('Fall River','41.7136','-71.1015','United States')

INSERT INTO CityEntitie VALUES('Hosaina','7.5504','37.85','Ethiopia')

INSERT INTO CityEntitie VALUES('Tambacounda','13.7804','-13.68','Senegal')

INSERT INTO CityEntitie VALUES('Sumbawanga','-7.9596','31.62','Tanzania')

INSERT INTO CityEntitie VALUES('Valle de la Pascua','9.21','-66.02','Venezuela')

INSERT INTO CityEntitie VALUES('Norwalk','41.1144','-73.4215','United States')

INSERT INTO CityEntitie VALUES('Decatur','39.8556','-88.9337','United States')

INSERT INTO CityEntitie VALUES('Newton','42.3316','-71.2084','United States')

INSERT INTO CityEntitie VALUES('Rapid City','44.0716','-103.2205','United States')

INSERT INTO CityEntitie VALUES('Shwebo','22.5783','95.6929','Burma')

INSERT INTO CityEntitie VALUES('Z?rate','-34.0896','-59.04','Argentina')

INSERT INTO CityEntitie VALUES('Wangqing','43.3248','129.7343','China')

INSERT INTO CityEntitie VALUES('Rafaela','-31.25','-61.5','Argentina')

INSERT INTO CityEntitie VALUES('Muncie','40.1989','-85.395','United States')

INSERT INTO CityEntitie VALUES('Norrk?ping','58.5954','16.1787','Sweden')

INSERT INTO CityEntitie VALUES('Kirkland','47.6997','-122.2041','United States')

INSERT INTO CityEntitie VALUES('Montero','-17.3496','-63.26','Bolivia')

INSERT INTO CityEntitie VALUES('Chosica','-11.93','-76.71','Peru')

INSERT INTO CityEntitie VALUES('Chaman','30.925','66.4463','Pakistan')

INSERT INTO CityEntitie VALUES('Brovary','50.4943','30.7809','Ukraine')

INSERT INTO CityEntitie VALUES('Sugar Land','29.5965','-95.6293','United States')

INSERT INTO CityEntitie VALUES('Târgovi?te','44.938','25.459','Romania')

INSERT INTO CityEntitie VALUES('Nova Lima','-19.98','-43.85','Brazil')

INSERT INTO CityEntitie VALUES('Taldyqorghan','45','78.4','Kazakhstan')

INSERT INTO CityEntitie VALUES('Zhob','31.349','69.4386','Pakistan')

INSERT INTO CityEntitie VALUES('Qal at Bishah','20.0087','42.5987','Saudi Arabia')

INSERT INTO CityEntitie VALUES('State College','40.7909','-77.8568','United States')

INSERT INTO CityEntitie VALUES('Brusque','-27.13','-48.93','Brazil')

INSERT INTO CityEntitie VALUES('S?o Tomé','0.3334','6.7333','Sao Tome And Principe')

INSERT INTO CityEntitie VALUES('Coronel Oviedo','-25.45','-56.44','Paraguay')

INSERT INTO CityEntitie VALUES('Citrus Heights','38.6948','-121.288','United States')

INSERT INTO CityEntitie VALUES('Yozgat','39.818','34.815','Turkey')

INSERT INTO CityEntitie VALUES('Madinat ath Thawrah','35.8367','38.5481','Syria')

INSERT INTO CityEntitie VALUES('Ebolowa','2.9','11.15','Cameroon')

INSERT INTO CityEntitie VALUES('Saint John','45.267','-66.0767','Canada')

INSERT INTO CityEntitie VALUES('A?r?','39.7198','43.0513','Turkey')

INSERT INTO CityEntitie VALUES('Hawthorne','33.9147','-118.3476','United States')

INSERT INTO CityEntitie VALUES('Surigao','9.7843','125.4888','Philippines')

INSERT INTO CityEntitie VALUES('Jackson','42.2431','-84.4038','United States')

INSERT INTO CityEntitie VALUES('Yeysk','46.6988','38.2634','Russia')

INSERT INTO CityEntitie VALUES('?anakkale','40.1459','26.4064','Turkey')

INSERT INTO CityEntitie VALUES('Parepare','-4.0167','119.6333','Indonesia')

INSERT INTO CityEntitie VALUES('San Juan De Los Morros','9.901','-67.354','Venezuela')

INSERT INTO CityEntitie VALUES('Waukegan','42.3697','-87.8716','United States')

INSERT INTO CityEntitie VALUES('Buzuluk','52.7821','52.2618','Russia')

INSERT INTO CityEntitie VALUES('Makeni','8.8804','-12.05','Sierra Leone')

INSERT INTO CityEntitie VALUES('Uman','48.7543','30.2109','Ukraine')

INSERT INTO CityEntitie VALUES('Pergamino','-33.8996','-60.57','Argentina')

INSERT INTO CityEntitie VALUES('?ilina','49.2198','18.7494','Slovakia')

INSERT INTO CityEntitie VALUES('Gitarama','-2.0696','29.76','Rwanda')

INSERT INTO CityEntitie VALUES('OFallon','38.7851','-90.7176','United States')

INSERT INTO CityEntitie VALUES('Koidu','8.4405','-10.85','Sierra Leone')

INSERT INTO CityEntitie VALUES('Assis','-22.6596','-50.42','Brazil')

INSERT INTO CityEntitie VALUES('Changting','25.867','116.3167','China')

INSERT INTO CityEntitie VALUES('Koudougou','12.2505','-2.37','Burkina Faso')

INSERT INTO CityEntitie VALUES('Shumen','43.27','26.9294','Bulgaria')

INSERT INTO CityEntitie VALUES('Mishan','45.5504','131.88','China')

INSERT INTO CityEntitie VALUES('Nenjiang','49.18','125.23','China')

INSERT INTO CityEntitie VALUES('Ed Dueim','13.9904','32.3','Sudan')

INSERT INTO CityEntitie VALUES('Napa','38.2977','-122.3011','United States')

INSERT INTO CityEntitie VALUES('Lokossa','6.615','1.715','Benin')

INSERT INTO CityEntitie VALUES('Huaraz','-9.53','-77.53','Peru')

INSERT INTO CityEntitie VALUES('Dalton','34.7689','-84.9711','United States')

INSERT INTO CityEntitie VALUES('Whittier','33.9678','-118.0188','United States')

INSERT INTO CityEntitie VALUES('Nouadhibou','20.9','-17.056','Mauritania')

INSERT INTO CityEntitie VALUES('Tumaco','1.81','-78.81','Colombia')

INSERT INTO CityEntitie VALUES('Ruhengeri','-1.4996','29.63','Rwanda')

INSERT INTO CityEntitie VALUES('Redwood City','37.5026','-122.2252','United States')

INSERT INTO CityEntitie VALUES('Mount Pleasant','32.8533','-79.8269','United States')

INSERT INTO CityEntitie VALUES('Clifton','40.8631','-74.1575','United States')

INSERT INTO CityEntitie VALUES('Bitola','41.0391','21.3395','Macedonia')

INSERT INTO CityEntitie VALUES('Anderson','40.0874','-85.692','United States')

INSERT INTO CityEntitie VALUES('Olavarr?a','-36.9','-60.33','Argentina')

INSERT INTO CityEntitie VALUES('Ioanina','39.6679','20.8509','Greece')

INSERT INTO CityEntitie VALUES('Joplin','37.0766','-94.5016','United States')

INSERT INTO CityEntitie VALUES('Panshi','42.9426','126.0561','China')

INSERT INTO CityEntitie VALUES('Newport Beach','33.6151','-117.8669','United States')

INSERT INTO CityEntitie VALUES('Lorca','37.6886','-1.6985','Spain')

INSERT INTO CityEntitie VALUES('Shchekino','54.0143','37.5143','Russia')

INSERT INTO CityEntitie VALUES('Agua Prieta','31.3223','-109.563','Mexico')

INSERT INTO CityEntitie VALUES('Potiskum','11.7104','11.08','Nigeria')

INSERT INTO CityEntitie VALUES('Huehuetenango','15.3204','-91.47','Guatemala')

INSERT INTO CityEntitie VALUES('Chipata','-13.6296','32.64','Zambia')

INSERT INTO CityEntitie VALUES('Poitier','46.5833','0.3333','France')

INSERT INTO CityEntitie VALUES('Keffi','8.849','7.8736','Nigeria')

INSERT INTO CityEntitie VALUES('Usolye Sibirskoye','52.765','103.645','Russia')

INSERT INTO CityEntitie VALUES('Bloomington','44.8306','-93.3151','United States')

INSERT INTO CityEntitie VALUES('Helong','42.5348','129.0044','China')

INSERT INTO CityEntitie VALUES('Behbehan','30.5818','50.2615','Iran')

INSERT INTO CityEntitie VALUES('R?o Gallegos','-51.6333','-69.2166','Argentina')

INSERT INTO CityEntitie VALUES('Chumphon','10.5127','99.1872','Thailand')

INSERT INTO CityEntitie VALUES('Sokcho','38.2087','128.5912','Korea')

INSERT INTO CityEntitie VALUES('Versailles','48.8005','2.1333','France')

INSERT INTO CityEntitie VALUES('Alhambra','34.084','-118.1355','United States')

INSERT INTO CityEntitie VALUES('Paulo Afonso','-9.3307','-38.2657','Brazil')

INSERT INTO CityEntitie VALUES('Ituiutaba','-18.97','-49.46','Brazil')

INSERT INTO CityEntitie VALUES('C?ceres','-16.05','-57.51','Brazil')

INSERT INTO CityEntitie VALUES('Liepaga','56.51','21.01','Latvia')

INSERT INTO CityEntitie VALUES('Ballarat','-37.5596','143.84','Australia')

INSERT INTO CityEntitie VALUES('Louga','15.6104','-16.25','Senegal')

INSERT INTO CityEntitie VALUES('Jun?n','-34.5846','-60.9589','Argentina')

INSERT INTO CityEntitie VALUES('Puerto Limon','10','-83.0333','Costa Rica')

INSERT INTO CityEntitie VALUES('Lorient','47.7504','-3.3666','France')

INSERT INTO CityEntitie VALUES('Hoover','33.3763','-86.8058','United States')

INSERT INTO CityEntitie VALUES('Nanaimo','49.146','-123.9343','Canada')

INSERT INTO CityEntitie VALUES('Savannakhet','16.5376','104.773','Laos')

INSERT INTO CityEntitie VALUES('Largo','27.9088','-82.7713','United States')

INSERT INTO CityEntitie VALUES('Sumenep','-7.0049','113.8496','Indonesia')

INSERT INTO CityEntitie VALUES('Guider','9.9304','13.94','Cameroon')

INSERT INTO CityEntitie VALUES('Mission','26.204','-98.3251','United States')

INSERT INTO CityEntitie VALUES('Conroe','30.322','-95.4807','United States')

INSERT INTO CityEntitie VALUES('Johns Creek','34.0333','-84.2027','United States')

INSERT INTO CityEntitie VALUES('Linjiang','41.8363','126.936','China')

INSERT INTO CityEntitie VALUES('Lake Forest','33.6607','-117.6712','United States')

INSERT INTO CityEntitie VALUES('Trinidad','-14.8334','-64.9','Bolivia')

INSERT INTO CityEntitie VALUES('Yurga','55.7258','84.8854','Russia')

INSERT INTO CityEntitie VALUES('Bryan','30.6657','-96.3672','United States')

INSERT INTO CityEntitie VALUES('Porto Santana','-0.0396','-51.18','Brazil')

INSERT INTO CityEntitie VALUES('Port Huron','42.9822','-82.4387','United States')

INSERT INTO CityEntitie VALUES('Troy','42.5818','-83.1457','United States')

INSERT INTO CityEntitie VALUES('Mbarara','-0.5996','30.65','Uganda')

INSERT INTO CityEntitie VALUES('Anzhero Sudzhensk','56.08','86.04','Russia')

INSERT INTO CityEntitie VALUES('Bethlehem','-28.2196','28.3','South Africa')

INSERT INTO CityEntitie VALUES('Londonderry','55.0004','-7.3333','United Kingdom')

INSERT INTO CityEntitie VALUES('Madera','36.964','-120.0803','United States')

INSERT INTO CityEntitie VALUES('Peterborough','44.3','-78.3333','Canada')

INSERT INTO CityEntitie VALUES('Gisenyi','-1.6847','29.2629','Rwanda')

INSERT INTO CityEntitie VALUES('Oca?a','8.2404','-73.35','Colombia')

INSERT INTO CityEntitie VALUES('Ouidah','6.3604','2.09','Benin')

INSERT INTO CityEntitie VALUES('Re?i?a','45.297','21.8865','Romania')

INSERT INTO CityEntitie VALUES('Cod?','-4.4796','-43.88','Brazil')

INSERT INTO CityEntitie VALUES('Springfield','39.9304','-83.7961','United States')

INSERT INTO CityEntitie VALUES('Kampong Cham','12.0004','105.45','Cambodia')

INSERT INTO CityEntitie VALUES('Izmayil','45.3503','28.8374','Ukraine')

INSERT INTO CityEntitie VALUES('Buena Park','33.8572','-118.0046','United States')

INSERT INTO CityEntitie VALUES('Pleasanton','37.6664','-121.8805','United States')

INSERT INTO CityEntitie VALUES('Tulc?n','0.822','-77.732','Ecuador')

INSERT INTO CityEntitie VALUES('Antsiranana','-12.2765','49.3115','Madagascar')

INSERT INTO CityEntitie VALUES('Amasya','40.6537','35.833','Turkey')

INSERT INTO CityEntitie VALUES('Wiener Neustadt','47.816','16.25','Austria')

INSERT INTO CityEntitie VALUES('Arax?','-19.5796','-46.95','Brazil')

INSERT INTO CityEntitie VALUES('Cicero','41.8445','-87.7593','United States')

INSERT INTO CityEntitie VALUES('Mu?','38.749','41.4969','Turkey')

INSERT INTO CityEntitie VALUES('Nan','18.7868','100.7715','Thailand')

INSERT INTO CityEntitie VALUES('Pernik','42.61','23.0227','Bulgaria')

INSERT INTO CityEntitie VALUES('Bagamoyo','-6.4396','38.89','Tanzania')

INSERT INTO CityEntitie VALUES('Chukai','4.2332','103.4479','Malaysia')

INSERT INTO CityEntitie VALUES('Palmerston North','-40.3527','175.6072','New Zealand')

INSERT INTO CityEntitie VALUES('Nangong','37.3704','115.37','China')

INSERT INTO CityEntitie VALUES('Chimaltenango','14.662','-90.82','Guatemala')

INSERT INTO CityEntitie VALUES('Gie?en','50.5837','8.65','Germany')

INSERT INTO CityEntitie VALUES('Troitsk','54.1056','61.5702','Russia')

INSERT INTO CityEntitie VALUES('Bansk? Bystrica','48.7333','19.15','Slovakia')

INSERT INTO CityEntitie VALUES('Kanoya','31.3833','130.85','Japan')

INSERT INTO CityEntitie VALUES('Kilis','36.7204','37.12','Turkey')

INSERT INTO CityEntitie VALUES('Fier','40.73','19.573','Albania')

INSERT INTO CityEntitie VALUES('Polatsk','55.4894','28.786','Belarus')

INSERT INTO CityEntitie VALUES('Asela','7.9504','39.1399','Ethiopia')

INSERT INTO CityEntitie VALUES('Bolgatanga','10.7904','-0.85','Ghana')

INSERT INTO CityEntitie VALUES('Abomey','7.1904','1.99','Benin')

INSERT INTO CityEntitie VALUES('Alexandria','31.2923','-92.4702','United States')

INSERT INTO CityEntitie VALUES('Chililabombwe','-12.3696','27.82','Zambia')

INSERT INTO CityEntitie VALUES('Melbourne','28.1084','-80.6628','United States')

INSERT INTO CityEntitie VALUES('Qasserine','35.1804','8.83','Tunisia')

INSERT INTO CityEntitie VALUES('Presidencia Roque Saenz Pena','-26.79','-60.45','Argentina')

INSERT INTO CityEntitie VALUES('Agboville','5.9403','-4.28','Côte DIvoire')

INSERT INTO CityEntitie VALUES('Kaspiysk','42.8747','47.6244','Russia')

INSERT INTO CityEntitie VALUES('Luj?n','-34.5796','-59.11','Argentina')

INSERT INTO CityEntitie VALUES('Westland','42.3192','-83.3805','United States')

INSERT INTO CityEntitie VALUES('Bendigo','-36.76','144.28','Australia')

INSERT INTO CityEntitie VALUES('Watampone','-4.5328','120.3334','Indonesia')

INSERT INTO CityEntitie VALUES('Campana','-34.16','-58.96','Argentina')

INSERT INTO CityEntitie VALUES('Sukhumi','43.02','41.02','Georgia')

INSERT INTO CityEntitie VALUES('Lavras','-21.2496','-45.01','Brazil')

INSERT INTO CityEntitie VALUES('Béziers','43.3505','3.21','France')

INSERT INTO CityEntitie VALUES('Mountain View','37.4','-122.0796','United States')

INSERT INTO CityEntitie VALUES('Balqash','46.8532','74.9502','Kazakhstan')

INSERT INTO CityEntitie VALUES('Somerville','42.3908','-71.1013','United States')

INSERT INTO CityEntitie VALUES('Bistri?a','47.138','24.513','Romania')

INSERT INTO CityEntitie VALUES('Avaré','-23.11','-48.93','Brazil')

INSERT INTO CityEntitie VALUES('Asbest','57.023','61.458','Russia')

INSERT INTO CityEntitie VALUES('Formosa','-15.5395','-47.34','Brazil')

INSERT INTO CityEntitie VALUES('Pisco','-13.71','-76.22','Peru')

INSERT INTO CityEntitie VALUES('Cranston','41.7658','-71.4858','United States')

INSERT INTO CityEntitie VALUES('Salina Cruz','16.1671','-95.2','Mexico')

INSERT INTO CityEntitie VALUES('Farmington Hills','42.486','-83.3771','United States')

INSERT INTO CityEntitie VALUES('Hattiesburg','31.3072','-89.3168','United States')

INSERT INTO CityEntitie VALUES('Linares','-35.84','-71.59','Chile')

INSERT INTO CityEntitie VALUES('Lakewood','33.8471','-118.1221','United States')

INSERT INTO CityEntitie VALUES('Zomba','-15.39','35.31','Malawi')

INSERT INTO CityEntitie VALUES('Natitingou','10.3204','1.39','Benin')

INSERT INTO CityEntitie VALUES('Warwick','41.7062','-71.4334','United States')

INSERT INTO CityEntitie VALUES('Hobart','-42.85','147.295','Australia')

INSERT INTO CityEntitie VALUES('Williamsburg','37.2693','-76.7076','United States')

INSERT INTO CityEntitie VALUES('Saint Joseph','39.7598','-94.821','United States')

INSERT INTO CityEntitie VALUES('Abu Kamal','34.4504','40.9186','Syria')

INSERT INTO CityEntitie VALUES('Klin','56.3431','36.6987','Russia')

INSERT INTO CityEntitie VALUES('Auburn','47.3041','-122.211','United States')

INSERT INTO CityEntitie VALUES('Atakpamé','7.53','1.12','Togo')

INSERT INTO CityEntitie VALUES('Brooklyn Park','45.1112','-93.3505','United States')

INSERT INTO CityEntitie VALUES('Deerfield Beach','26.305','-80.1278','United States')

INSERT INTO CityEntitie VALUES('Bing?l','38.885','40.498','Turkey')

INSERT INTO CityEntitie VALUES('Tustin','33.7309','-117.8106','United States')

INSERT INTO CityEntitie VALUES('Necochea','-38.56','-58.75','Argentina')

INSERT INTO CityEntitie VALUES('Huacho','-11.11','-77.6199','Peru')

INSERT INTO CityEntitie VALUES('Alton','38.9033','-90.1523','United States')

INSERT INTO CityEntitie VALUES('Chino Hills','33.9508','-117.7254','United States')

INSERT INTO CityEntitie VALUES('Uvinza','-5.1196','30.39','Tanzania')

INSERT INTO CityEntitie VALUES('Kilifi','-3.6096','39.85','Kenya')

INSERT INTO CityEntitie VALUES('Mbalmayo','3.5204','11.5','Cameroon')

INSERT INTO CityEntitie VALUES('Lawrence','42.7003','-71.1626','United States')

INSERT INTO CityEntitie VALUES('Musan','42.2304','129.2304','Korea')

INSERT INTO CityEntitie VALUES('Texarkana','33.4487','-94.0815','United States')

INSERT INTO CityEntitie VALUES('Meiganga','6.5205','14.29','Cameroon')

INSERT INTO CityEntitie VALUES('Al Karak','31.1851','35.7047','Jordan')

INSERT INTO CityEntitie VALUES('Vorkuta','67.5','64.01','Russia')

INSERT INTO CityEntitie VALUES('Nabatiye et Tahta','33.3833','35.45','Lebanon')

INSERT INTO CityEntitie VALUES('Capenda-Camulemba','-9.4196','18.43','Angola')

INSERT INTO CityEntitie VALUES('Xigaze','29.25','88.8833','China')

INSERT INTO CityEntitie VALUES('Koktokay','47.0004','89.4666','China')

INSERT INTO CityEntitie VALUES('Ciudad Mante','22.7334','-98.95','Mexico')

INSERT INTO CityEntitie VALUES('New Rochelle','40.9305','-73.7836','United States')

INSERT INTO CityEntitie VALUES('Fort Myers','26.6195','-81.8302','United States')

INSERT INTO CityEntitie VALUES('Goodyear','33.2614','-112.3622','United States')

INSERT INTO CityEntitie VALUES('Prachin Buri','14.0572','101.3768','Thailand')

INSERT INTO CityEntitie VALUES('Khaskovo','41.9438','25.5633','Bulgaria')

INSERT INTO CityEntitie VALUES('Quillota','-32.88','-71.26','Chile')

INSERT INTO CityEntitie VALUES('Erdenet','49.0533','104.1183','Mongolia')

INSERT INTO CityEntitie VALUES('Kropotkin','45.4471','40.5821','Russia')

INSERT INTO CityEntitie VALUES('Springdale','36.1898','-94.1573','United States')

INSERT INTO CityEntitie VALUES('Itumbiara','-18.3996','-49.21','Brazil')

INSERT INTO CityEntitie VALUES('Beni Mazar','28.4904','30.81','Egypt')

INSERT INTO CityEntitie VALUES('Ouahigouya','13.5704','-2.42','Burkina Faso')

INSERT INTO CityEntitie VALUES('Pharr','26.1686','-98.1904','United States')

INSERT INTO CityEntitie VALUES('Shadrinsk','56.0837','63.6333','Russia')

INSERT INTO CityEntitie VALUES('Abaetetuba','-1.7245','-48.8849','Brazil')

INSERT INTO CityEntitie VALUES('Valdosta','30.8503','-83.2789','United States')

INSERT INTO CityEntitie VALUES('Alameda','37.767','-122.2673','United States')

INSERT INTO CityEntitie VALUES('Kadoma','-18.33','29.9099','Zimbabwe')

INSERT INTO CityEntitie VALUES('Parma','41.3842','-81.7286','United States')

INSERT INTO CityEntitie VALUES('New Braunfels','29.6995','-98.1153','United States')

INSERT INTO CityEntitie VALUES('Kokomo','40.464','-86.1277','United States')

INSERT INTO CityEntitie VALUES('Newark','40.0705','-82.425','United States')

INSERT INTO CityEntitie VALUES('Paysand?','-32.33','-58.08','Uruguay')

INSERT INTO CityEntitie VALUES('Inowroc?aw','52.7799','18.25','Poland')

INSERT INTO CityEntitie VALUES('Slatina','44.435','24.371','Romania')

INSERT INTO CityEntitie VALUES('Kupyansk','49.7218','37.5981','Ukraine')

INSERT INTO CityEntitie VALUES('Shulan','44.4091','126.9487','China')

INSERT INTO CityEntitie VALUES('Sagaing','21.88','95.962','Burma')

INSERT INTO CityEntitie VALUES('Hania','35.5122','24.0156','Greece')

INSERT INTO CityEntitie VALUES('Cheyenne','41.1406','-104.7926','United States')

INSERT INTO CityEntitie VALUES('Três Lagoas','-20.79','-51.72','Brazil')

INSERT INTO CityEntitie VALUES('Biel','47.1666','7.25','Switzerland')

INSERT INTO CityEntitie VALUES('Gualeguaych?','-33.02','-58.52','Argentina')

INSERT INTO CityEntitie VALUES('Ceuta','35.889','-5.307','Spain')

INSERT INTO CityEntitie VALUES('Dondo','-19.6196','34.73','Mozambique')

INSERT INTO CityEntitie VALUES('Flagstaff','35.1872','-111.6195','United States')

INSERT INTO CityEntitie VALUES('S?o Jo?o del Rei','-21.13','-44.25','Brazil')

INSERT INTO CityEntitie VALUES('St.-Jerome','45.7666','-74','Canada')

INSERT INTO CityEntitie VALUES('Plymouth','45.0224','-93.4615','United States')

INSERT INTO CityEntitie VALUES('Franklin','35.9214','-86.8524','United States')

INSERT INTO CityEntitie VALUES('Pingyi','35.5104','117.62','China')

INSERT INTO CityEntitie VALUES('Ume?','63.83','20.24','Sweden')

INSERT INTO CityEntitie VALUES('Zahlé','33.8501','35.9042','Lebanon')

INSERT INTO CityEntitie VALUES('Florence','34.83','-87.6658','United States')

INSERT INTO CityEntitie VALUES('Wa','10.0604','-2.5','Ghana')

INSERT INTO CityEntitie VALUES('Milpitas','37.4338','-121.8921','United States')

INSERT INTO CityEntitie VALUES('Folsom','38.6669','-121.1422','United States')

INSERT INTO CityEntitie VALUES('Kankakee','41.102','-87.8643','United States')

INSERT INTO CityEntitie VALUES('Lebanon','40.3412','-76.4228','United States')

INSERT INTO CityEntitie VALUES('Gangtok','27.3333','88.6166','India')

INSERT INTO CityEntitie VALUES('Perris','33.7899','-117.2233','United States')

INSERT INTO CityEntitie VALUES('Marsala','37.8054','12.4387','Italy')

INSERT INTO CityEntitie VALUES('Bellflower','33.888','-118.1271','United States')

INSERT INTO CityEntitie VALUES('Linkou','45.2819','130.2519','China')

INSERT INTO CityEntitie VALUES('Boynton Beach','26.5282','-80.0811','United States')

INSERT INTO CityEntitie VALUES('Watsonville','36.9205','-121.7706','United States')

INSERT INTO CityEntitie VALUES('Hakkâri','37.5744','43.7408','Turkey')

INSERT INTO CityEntitie VALUES('Numan','9.4604','12.04','Nigeria')

INSERT INTO CityEntitie VALUES('Kars','40.6085','43.0975','Turkey')

INSERT INTO CityEntitie VALUES('Anderson','34.5212','-82.6479','United States')

INSERT INTO CityEntitie VALUES('Ita?na','-20.06','-44.57','Brazil')

INSERT INTO CityEntitie VALUES('S?ke','37.7512','27.4103','Turkey')

INSERT INTO CityEntitie VALUES('Elizabethtown','37.703','-85.8772','United States')

INSERT INTO CityEntitie VALUES('Kayes','14.45','-11.44','Mali')

INSERT INTO CityEntitie VALUES('Wheeling','40.0755','-80.6951','United States')

INSERT INTO CityEntitie VALUES('Ende','-8.8623','121.6489','Indonesia')

INSERT INTO CityEntitie VALUES('San Carlos','9.658','-68.59','Venezuela')

INSERT INTO CityEntitie VALUES('Jiexiu','37.04','111.9','China')

INSERT INTO CityEntitie VALUES('Ovalle','-30.59','-71.2001','Chile')

INSERT INTO CityEntitie VALUES('S?o Mateus','-18.7296','-39.86','Brazil')

INSERT INTO CityEntitie VALUES('Butare','-2.5896','29.73','Rwanda')

INSERT INTO CityEntitie VALUES('Upland','34.1178','-117.6603','United States')

INSERT INTO CityEntitie VALUES('La Rochelle','46.1667','-1.15','France')

INSERT INTO CityEntitie VALUES('Thakhek','17.4112','104.8361','Laos')

INSERT INTO CityEntitie VALUES('Sarqan','45.4203','79.9149','Kazakhstan')

INSERT INTO CityEntitie VALUES('Karur','10.9504','78.0833','India')

INSERT INTO CityEntitie VALUES('Baytown','29.7607','-94.9628','United States')

INSERT INTO CityEntitie VALUES('Battle Creek','42.2985','-85.2296','United States')

INSERT INTO CityEntitie VALUES('Pori','61.4789','21.7749','Finland')

INSERT INTO CityEntitie VALUES('San Felipe','10.336','-68.746','Venezuela')

INSERT INTO CityEntitie VALUES('Porterville','36.0644','-119.0337','United States')

INSERT INTO CityEntitie VALUES('Oudtshoorn','-33.58','22.19','South Africa')

INSERT INTO CityEntitie VALUES('Loveland','40.4167','-105.0621','United States')

INSERT INTO CityEntitie VALUES('Layton','41.0771','-111.9622','United States')

INSERT INTO CityEntitie VALUES('Flower Mound','33.0344','-97.1147','United States')

INSERT INTO CityEntitie VALUES('Uttaradit','17.6316','100.0972','Thailand')

INSERT INTO CityEntitie VALUES('Hammond','41.617','-87.4908','United States')

INSERT INTO CityEntitie VALUES('Lake Jackson','29.0516','-95.4522','United States')

INSERT INTO CityEntitie VALUES('Jata?','-17.8796','-51.75','Brazil')

INSERT INTO CityEntitie VALUES('S?o Jo?o da Boa Vista','-21.98','-46.79','Brazil')

INSERT INTO CityEntitie VALUES('Davis','38.5552','-121.7365','United States')

INSERT INTO CityEntitie VALUES('Al Musayyib','32.7786','44.29','Iraq')

INSERT INTO CityEntitie VALUES('Zephyrhills','28.2407','-82.1797','United States')

INSERT INTO CityEntitie VALUES('Baldwin Park','34.0829','-117.972','United States')

INSERT INTO CityEntitie VALUES('Tucuru?','-3.68','-49.72','Brazil')

INSERT INTO CityEntitie VALUES('Honiara','-9.438','159.9498','Solomon Islands')

INSERT INTO CityEntitie VALUES('Masvingo','-20.0596','30.82','Zimbabwe')

INSERT INTO CityEntitie VALUES('Babahoyo','-1.7996','-79.54','Ecuador')

INSERT INTO CityEntitie VALUES('Tuban','-6.8995','112.05','Indonesia')

INSERT INTO CityEntitie VALUES('Sakhon Nakhon','17.1679','104.1479','Thailand')

INSERT INTO CityEntitie VALUES('Birobidzhan','48.7974','132.9508','Russia')

INSERT INTO CityEntitie VALUES('Altoona','40.5082','-78.4007','United States')

INSERT INTO CityEntitie VALUES('Gary','41.5906','-87.3472','United States')

INSERT INTO CityEntitie VALUES('Tamanrasset','22.785','5.5228','Algeria')

INSERT INTO CityEntitie VALUES('Tailai','46.3904','123.41','China')

INSERT INTO CityEntitie VALUES('Wyoming','42.8909','-85.7066','United States')

INSERT INTO CityEntitie VALUES('Saint Augustine','29.8979','-81.31','United States')

INSERT INTO CityEntitie VALUES('San Ramon','37.7625','-121.9365','United States')

INSERT INTO CityEntitie VALUES('Mackay','-21.1439','149.15','Australia')

INSERT INTO CityEntitie VALUES('Kamensk Shakhtinskiy','48.3318','40.2518','Russia')

INSERT INTO CityEntitie VALUES('Buynaksk','42.8335','47.113','Russia')

INSERT INTO CityEntitie VALUES('Bethlehem','40.6266','-75.3679','United States')

INSERT INTO CityEntitie VALUES('Cedar Park','30.5105','-97.8198','United States')

INSERT INTO CityEntitie VALUES('Arlington Heights','42.0956','-87.9825','United States')

INSERT INTO CityEntitie VALUES('Galway','53.2724','-9.0488','Ireland')

INSERT INTO CityEntitie VALUES('Nev?ehir','38.624','34.724','Turkey')

INSERT INTO CityEntitie VALUES('Campo Murao','-24.0496','-52.42','Brazil')

INSERT INTO CityEntitie VALUES('Union City','37.603','-122.0187','United States')

INSERT INTO CityEntitie VALUES('Kompong Chhnang','12.2505','104.6666','Cambodia')

INSERT INTO CityEntitie VALUES('Agrinio','38.6218','21.4077','Greece')

INSERT INTO CityEntitie VALUES('Bolingbrook','41.6903','-88.102','United States')

INSERT INTO CityEntitie VALUES('Anniston','33.6713','-85.8136','United States')

INSERT INTO CityEntitie VALUES('Oshkosh','44.0228','-88.5616','United States')

INSERT INTO CityEntitie VALUES('Idah','7.1104','6.7399','Nigeria')

INSERT INTO CityEntitie VALUES('Bombo','0.5833','32.5333','Uganda')

INSERT INTO CityEntitie VALUES('Gode','5.95','43.45','Ethiopia')

INSERT INTO CityEntitie VALUES('Red Deer','52.2666','-113.8','Canada')

INSERT INTO CityEntitie VALUES('Evanston','42.0463','-87.6942','United States')

INSERT INTO CityEntitie VALUES('Darhan','49.6167','106.35','Mongolia')

INSERT INTO CityEntitie VALUES('Cachoeira do Sul','-30.03','-52.91','Brazil')

INSERT INTO CityEntitie VALUES('Camarillo','34.223','-119.0323','United States')

INSERT INTO CityEntitie VALUES('Greenock','55.9333','-4.75','United Kingdom')

INSERT INTO CityEntitie VALUES('Toledo','39.867','-4.0167','Spain')

INSERT INTO CityEntitie VALUES('Camden','39.9361','-75.1073','United States')

INSERT INTO CityEntitie VALUES('Manokwari','-0.8711','134.0693','Indonesia')

INSERT INTO CityEntitie VALUES('Hilton Head Island','32.1896','-80.7499','United States')

INSERT INTO CityEntitie VALUES('Missouri City','29.5635','-95.5377','United States')

INSERT INTO CityEntitie VALUES('Shanxian','34.7904','116.08','China')

INSERT INTO CityEntitie VALUES('San Marcos','29.8734','-97.9382','United States')

INSERT INTO CityEntitie VALUES('Rochester Hills','42.6645','-83.1563','United States')

INSERT INTO CityEntitie VALUES('Schaumburg','42.0307','-88.0838','United States')

INSERT INTO CityEntitie VALUES('Karlstad','59.3671','13.4999','Sweden')

INSERT INTO CityEntitie VALUES('Kara Balta','42.8306','73.8857','Kyrgyzstan')

INSERT INTO CityEntitie VALUES('San Ram?n de la Nueva Or?n','-23.14','-64.32','Argentina')

INSERT INTO CityEntitie VALUES('Winchester','39.1735','-78.1746','United States')

INSERT INTO CityEntitie VALUES('Harrisonburg','38.4362','-78.8735','United States')

INSERT INTO CityEntitie VALUES('Standerton','-26.9395','29.24','South Africa')

INSERT INTO CityEntitie VALUES('Riberalta','-10.983','-66.1','Bolivia')

INSERT INTO CityEntitie VALUES('Prey Veng','11.484','105.324','Cambodia')

INSERT INTO CityEntitie VALUES('Cozumel','20.51','-86.95','Mexico')

INSERT INTO CityEntitie VALUES('Wausau','44.9615','-89.6436','United States')

INSERT INTO CityEntitie VALUES('Kiffa','16.62','-11.4','Mauritania')

INSERT INTO CityEntitie VALUES('Jonesboro','35.8212','-90.6795','United States')

INSERT INTO CityEntitie VALUES('Grand Bassam','5.2004','-3.75','Côte DIvoire')

INSERT INTO CityEntitie VALUES('Jackson','35.6533','-88.8352','United States')

INSERT INTO CityEntitie VALUES('Farah','32.3917','62.0968','Afghanistan')

INSERT INTO CityEntitie VALUES('Munchon','39.3813','127.2517','Korea')

INSERT INTO CityEntitie VALUES('Bawku','11.0604','-0.24','Ghana')

INSERT INTO CityEntitie VALUES('Rancho Cordova','38.5739','-121.2521','United States')

INSERT INTO CityEntitie VALUES('Sundsvall','62.4001','17.3167','Sweden')

INSERT INTO CityEntitie VALUES('Onitsha','6.1404','6.78','Nigeria')

INSERT INTO CityEntitie VALUES('Mandurah','-32.5235','115.7471','Australia')

INSERT INTO CityEntitie VALUES('Mpanda','-6.3596','31.05','Tanzania')

INSERT INTO CityEntitie VALUES('Skien','59.2','9.6','Norway')

INSERT INTO CityEntitie VALUES('Roanne','46.0333','4.0667','France')

INSERT INTO CityEntitie VALUES('Bonao','18.942','-70.409','Dominican Republic')

INSERT INTO CityEntitie VALUES('Cuamba','-14.7896','36.54','Mozambique')

INSERT INTO CityEntitie VALUES('C?l?ra?i','44.2063','27.3259','Romania')

INSERT INTO CityEntitie VALUES('Tatvan','38.5066','42.2816','Turkey')

INSERT INTO CityEntitie VALUES('General Roca','-39.02','-67.61','Argentina')

INSERT INTO CityEntitie VALUES('Southfield','42.4765','-83.2605','United States')

INSERT INTO CityEntitie VALUES('Tokar','18.4333','37.7333','Sudan')

INSERT INTO CityEntitie VALUES('Owensboro','37.7575','-87.1172','United States')

INSERT INTO CityEntitie VALUES('Apple Valley','34.5329','-117.2102','United States')

INSERT INTO CityEntitie VALUES('Nekemte','9.0905','36.53','Ethiopia')

INSERT INTO CityEntitie VALUES('Pasco','46.2503','-119.1298','United States')

INSERT INTO CityEntitie VALUES('Korosten','50.9504','28.65','Ukraine')

INSERT INTO CityEntitie VALUES('San Juan','18.807','-71.229','Dominican Republic')

INSERT INTO CityEntitie VALUES('Dabou','5.3204','-4.3899','Côte DIvoire')

INSERT INTO CityEntitie VALUES('Conway','35.0753','-92.4695','United States')

INSERT INTO CityEntitie VALUES('Lodi','38.1218','-121.2932','United States')

INSERT INTO CityEntitie VALUES('Guadalajara','40.6337','-3.1666','Spain')

INSERT INTO CityEntitie VALUES('Qu?ng Tr?','16.7504','107.2','Vietnam')

INSERT INTO CityEntitie VALUES('New Britain','41.6758','-72.7862','United States')

INSERT INTO CityEntitie VALUES('Tamazunchale','21.2704','-98.78','Mexico')

INSERT INTO CityEntitie VALUES('Georgievsk','44.1599','43.4699','Russia')

INSERT INTO CityEntitie VALUES('Mansfield','40.7656','-82.5275','United States')

INSERT INTO CityEntitie VALUES('Carlisle','54.88','-2.93','United Kingdom')

INSERT INTO CityEntitie VALUES('Lanxi','46.2664','126.276','China')

INSERT INTO CityEntitie VALUES('Waukesha','43.0087','-88.2464','United States')

INSERT INTO CityEntitie VALUES('Launceston','-41.4498','147.1302','Australia')

INSERT INTO CityEntitie VALUES('Morgantown','39.638','-79.9468','United States')

INSERT INTO CityEntitie VALUES('Keshan','48.0263','125.866','China')

INSERT INTO CityEntitie VALUES('Bacabal','-4.23','-44.8','Brazil')

INSERT INTO CityEntitie VALUES('Venado Tuerto','-33.7496','-61.97','Argentina')

INSERT INTO CityEntitie VALUES('Bourges','47.0837','2.4','France')

INSERT INTO CityEntitie VALUES('Goya','-29.14','-59.27','Argentina')

INSERT INTO CityEntitie VALUES('Montepuez','-13.1196','39','Mozambique')

INSERT INTO CityEntitie VALUES('As Suwayda','32.7004','36.5666','Syria')

INSERT INTO CityEntitie VALUES('Esbjerg','55.467','8.45','Denmark')

INSERT INTO CityEntitie VALUES('Pittsburg','38.019','-121.8969','United States')

INSERT INTO CityEntitie VALUES('Sumter','33.9392','-80.393','United States')

INSERT INTO CityEntitie VALUES('Chusovoy','58.2934','57.813','Russia')

INSERT INTO CityEntitie VALUES('Nizwa','22.9264','57.5314','Oman')

INSERT INTO CityEntitie VALUES('Pawtucket','41.8743','-71.3743','United States')

INSERT INTO CityEntitie VALUES('Lauderhill','26.1605','-80.2241','United States')

INSERT INTO CityEntitie VALUES('Cleveland','35.1817','-84.8707','United States')

INSERT INTO CityEntitie VALUES('Chalkida','38.464','23.6124','Greece')

INSERT INTO CityEntitie VALUES('Kalamata','37.0389','22.1142','Greece')

INSERT INTO CityEntitie VALUES('Andkhvoy','36.9317','65.1015','Afghanistan')

INSERT INTO CityEntitie VALUES('Vratsa','43.21','23.5625','Bulgaria')

INSERT INTO CityEntitie VALUES('Dothan','31.2336','-85.4068','United States')

INSERT INTO CityEntitie VALUES('Chernogorsk','53.8313','91.2227','Russia')

INSERT INTO CityEntitie VALUES('Porto Uni?o','-26.2396','-51.08','Brazil')

INSERT INTO CityEntitie VALUES('Redlands','34.0512','-117.171','United States')

INSERT INTO CityEntitie VALUES('Goiana','-7.5596','-35','Brazil')

INSERT INTO CityEntitie VALUES('Monastir','35.7307','10.7673','Tunisia')

INSERT INTO CityEntitie VALUES('Wenatchee','47.4338','-120.3285','United States')

INSERT INTO CityEntitie VALUES('?ank?r?','40.607','33.621','Turkey')

INSERT INTO CityEntitie VALUES('Upington','-28.46','21.23','South Africa')

INSERT INTO CityEntitie VALUES('Mardin','37.3115','40.7427','Turkey')

INSERT INTO CityEntitie VALUES('Kovel','51.2171','24.7166','Ukraine')

INSERT INTO CityEntitie VALUES('Sotik','-0.6796','35.12','Kenya')

INSERT INTO CityEntitie VALUES('Asti','44.93','8.21','Italy')

INSERT INTO CityEntitie VALUES('Zadar','44.1201','15.2623','Croatia')

INSERT INTO CityEntitie VALUES('Passaic','40.8574','-74.1282','United States')

INSERT INTO CityEntitie VALUES('Iju?','-28.3895','-53.9199','Brazil')

INSERT INTO CityEntitie VALUES('Anlu','31.27','113.67','China')

INSERT INTO CityEntitie VALUES('Salima','-13.7829','34.4333','Malawi')

INSERT INTO CityEntitie VALUES('Mamou','10.3804','-12.1','Guinea')

INSERT INTO CityEntitie VALUES('Comayagua','14.4604','-87.65','Honduras')

INSERT INTO CityEntitie VALUES('Wilmington','39.7415','-75.5413','United States')

INSERT INTO CityEntitie VALUES('Lynwood','33.924','-118.2017','United States')

INSERT INTO CityEntitie VALUES('Muyinga','-2.8523','30.3173','Burundi')

INSERT INTO CityEntitie VALUES('Maple Grove','45.1089','-93.4626','United States')

INSERT INTO CityEntitie VALUES('Pocatello','42.8716','-112.4656','United States')

INSERT INTO CityEntitie VALUES('South Jordan','40.5571','-111.9782','United States')

INSERT INTO CityEntitie VALUES('Weston','26.1006','-80.4057','United States')

INSERT INTO CityEntitie VALUES('Altamira','-3.1996','-52.21','Brazil')

INSERT INTO CityEntitie VALUES('Saraburi','14.5304','100.88','Thailand')

INSERT INTO CityEntitie VALUES('Debre Markos','10.34','37.72','Ethiopia')

INSERT INTO CityEntitie VALUES('Paracatu','-17.1996','-46.87','Brazil')

INSERT INTO CityEntitie VALUES('Georgetown','30.6664','-97.6937','United States')

INSERT INTO CityEntitie VALUES('Ali Bayramli','39.9323','48.9203','Azerbaijan')

INSERT INTO CityEntitie VALUES('Lethbridge','49.7005','-112.8333','Canada')

INSERT INTO CityEntitie VALUES('Mindelo','16.8838','-25','Cabo Verde')

INSERT INTO CityEntitie VALUES('Byumba','-1.5796','30.06','Rwanda')

INSERT INTO CityEntitie VALUES('Saint Gallen','47.423','9.362','Switzerland')

INSERT INTO CityEntitie VALUES('Scarborough','54.2804','-0.43','United Kingdom')

INSERT INTO CityEntitie VALUES('Tatab?nya','47.55','18.433','Hungary')

INSERT INTO CityEntitie VALUES('Janesville','42.6855','-89.0136','United States')

INSERT INTO CityEntitie VALUES('Volsk','52.0347','47.3743','Russia')

INSERT INTO CityEntitie VALUES('Baiquan','47.6018','126.0819','China')

INSERT INTO CityEntitie VALUES('North Richland Hills','32.8605','-97.218','United States')

INSERT INTO CityEntitie VALUES('Kastamonu','41.389','33.783','Turkey')

INSERT INTO CityEntitie VALUES('Union City','40.7674','-74.0323','United States')

INSERT INTO CityEntitie VALUES('Iguatu','-6.36','-39.3','Brazil')

INSERT INTO CityEntitie VALUES('Saint Charles','38.7958','-90.5159','United States')

INSERT INTO CityEntitie VALUES('Sunyani','7.336','-2.336','Ghana')

INSERT INTO CityEntitie VALUES('Châu ??c','10.7004','105.1167','Vietnam')

INSERT INTO CityEntitie VALUES('Belogorsk','50.9191','128.4637','Russia')

INSERT INTO CityEntitie VALUES('Karakol','42.492','78.3818','Kyrgyzstan')

INSERT INTO CityEntitie VALUES('Lisala','2.14','21.51','Congo (Kinshasa)')

INSERT INTO CityEntitie VALUES('Rongzhag','30.9504','101.9167','China')

INSERT INTO CityEntitie VALUES('Sheboygan','43.7447','-87.7322','United States')

INSERT INTO CityEntitie VALUES('Homestead','25.4664','-80.4472','United States')

INSERT INTO CityEntitie VALUES('Bristol','36.5572','-82.2144','United States')

INSERT INTO CityEntitie VALUES('Lobatse','-25.2196','25.68','Botswana')

INSERT INTO CityEntitie VALUES('Lima','40.7409','-84.1121','United States')

INSERT INTO CityEntitie VALUES('Trnava','48.3666','17.6','Slovakia')

INSERT INTO CityEntitie VALUES('Walnut Creek','37.9025','-122.0398','United States')

INSERT INTO CityEntitie VALUES('Woodbury','44.9057','-92.923','United States')

INSERT INTO CityEntitie VALUES('Totonicap?n','14.914','-91.358','Guatemala')

INSERT INTO CityEntitie VALUES('Ouezzane','34.8103','-5.57','Morocco')

INSERT INTO CityEntitie VALUES('Arba Minch','6.04','37.55','Ethiopia')

INSERT INTO CityEntitie VALUES('Rio Verde','21.93','-99.98','Mexico')

INSERT INTO CityEntitie VALUES('Paragominas','-2.9596','-47.49','Brazil')

INSERT INTO CityEntitie VALUES('Tuy H?a','13.082','109.316','Vietnam')

INSERT INTO CityEntitie VALUES('Guajara-Miram','-10.8','-65.3499','Brazil')

INSERT INTO CityEntitie VALUES('West Bend','43.4184','-88.1822','United States')

INSERT INTO CityEntitie VALUES('Mount Vernon','48.4202','-122.3116','United States')

INSERT INTO CityEntitie VALUES('Villa Carlos Paz','-31.42','-64.5','Argentina')

INSERT INTO CityEntitie VALUES('Jaboticabal','-21.25','-48.33','Brazil')

INSERT INTO CityEntitie VALUES('Bafia','4.7504','11.23','Cameroon')

INSERT INTO CityEntitie VALUES('Kolda','12.9104','-14.95','Senegal')

INSERT INTO CityEntitie VALUES('Arauca','7.0907','-70.7616','Colombia')

INSERT INTO CityEntitie VALUES('Vaslui','46.6333','27.7333','Romania')

INSERT INTO CityEntitie VALUES('Hasselt','50.964','5.484','Belgium')

INSERT INTO CityEntitie VALUES('Ongjin','37.9371','125.3571','Korea')

INSERT INTO CityEntitie VALUES('Hammond','30.506','-90.456','United States')

INSERT INTO CityEntitie VALUES('Azogues','-2.74','-78.84','Ecuador')

INSERT INTO CityEntitie VALUES('Giurgiu','43.93','25.84','Romania')

INSERT INTO CityEntitie VALUES('Daphne','30.6291','-87.8872','United States')

INSERT INTO CityEntitie VALUES('Potenza','40.642','15.799','Italy')

INSERT INTO CityEntitie VALUES('Ayr','55.4504','-4.6167','United Kingdom')

INSERT INTO CityEntitie VALUES('Jorhat','26.75','94.2167','India')

INSERT INTO CityEntitie VALUES('Mocuba','-16.8496','38.26','Mozambique')

INSERT INTO CityEntitie VALUES('Ragusa','36.93','14.73','Italy')

INSERT INTO CityEntitie VALUES('Liuhe','42.2789','125.7173','China')

INSERT INTO CityEntitie VALUES('G?ines','22.8361','-82.028','Cuba')

INSERT INTO CityEntitie VALUES('Mansfield','32.569','-97.1211','United States')

INSERT INTO CityEntitie VALUES('Timbuktu','16.7666','-3.0166','Mali')

INSERT INTO CityEntitie VALUES('Wum','6.4004','10.07','Cameroon')

INSERT INTO CityEntitie VALUES('Chulucanas','-5.0896','-80.17','Peru')

INSERT INTO CityEntitie VALUES('Tuymazy','54.6048','53.6943','Russia')

INSERT INTO CityEntitie VALUES('Decatur','34.573','-86.9905','United States')

INSERT INTO CityEntitie VALUES('Oranjestad','12.5304','-70.029','Aruba')

INSERT INTO CityEntitie VALUES('Al Ahmadi','29.0769','48.0838','Kuwait')

INSERT INTO CityEntitie VALUES('Delray Beach','26.455','-80.0905','United States')

INSERT INTO CityEntitie VALUES('Saywun','15.943','48.7873','Yemen')

INSERT INTO CityEntitie VALUES('Kamloops','50.6667','-120.3333','Canada')

INSERT INTO CityEntitie VALUES('Gaithersburg','39.1347','-77.213','United States')

INSERT INTO CityEntitie VALUES('Mount Vernon','40.9136','-73.8291','United States')

INSERT INTO CityEntitie VALUES('Palatine','42.1181','-88.043','United States')

INSERT INTO CityEntitie VALUES('G?vle','60.667','17.1666','Sweden')

INSERT INTO CityEntitie VALUES('Borisoglebsk','51.3687','42.0887','Russia')

INSERT INTO CityEntitie VALUES('Dubuque','42.5007','-90.7067','United States')

INSERT INTO CityEntitie VALUES('Bossier City','32.523','-93.6666','United States')

INSERT INTO CityEntitie VALUES('Sabanalarga','10.64','-74.92','Colombia')

INSERT INTO CityEntitie VALUES('LAquila','42.3504','13.39','Italy')

INSERT INTO CityEntitie VALUES('Buckeye','33.4318','-112.643','United States')

INSERT INTO CityEntitie VALUES('Rockville','39.0834','-77.1553','United States')

INSERT INTO CityEntitie VALUES('Broomfield','39.9541','-105.0527','United States')

INSERT INTO CityEntitie VALUES('T?rkmenba?y','40.023','52.9697','Turkmenistan')

INSERT INTO CityEntitie VALUES('Saldanha','-33.01','17.93','South Africa')

INSERT INTO CityEntitie VALUES('Victoria','28.8285','-96.985','United States')

INSERT INTO CityEntitie VALUES('Matara','5.949','80.5428','Sri Lanka')

INSERT INTO CityEntitie VALUES('Lodja','-3.4896','23.42','Congo (Kinshasa)')

INSERT INTO CityEntitie VALUES('Yorba Linda','33.889','-117.7714','United States')

INSERT INTO CityEntitie VALUES('Oturkpo','7.1904','8.13','Nigeria')

INSERT INTO CityEntitie VALUES('Ames','42.0261','-93.6279','United States')

INSERT INTO CityEntitie VALUES('Narathiwat','6.4318','101.8214','Thailand')

INSERT INTO CityEntitie VALUES('Artemisa','22.8134','-82.7619','Cuba')

INSERT INTO CityEntitie VALUES('Balsas','-7.52','-46.05','Brazil')

INSERT INTO CityEntitie VALUES('Daytona Beach','29.1995','-81.0982','United States')

INSERT INTO CityEntitie VALUES('Putrajaya','2.914','101.7019','Malaysia')

INSERT INTO CityEntitie VALUES('Calbayog','12.0672','124.6042','Philippines')

INSERT INTO CityEntitie VALUES('Redondo Beach','33.8574','-118.3766','United States')

INSERT INTO CityEntitie VALUES('Concepci?n del Uruguay','-32.48','-58.24','Argentina')

INSERT INTO CityEntitie VALUES('Garissa','-0.4396','39.67','Kenya')

INSERT INTO CityEntitie VALUES('Minxian','34.4362','104.0306','China')

INSERT INTO CityEntitie VALUES('Carbondale','37.722','-89.2238','United States')

INSERT INTO CityEntitie VALUES('Deva','45.8833','22.9167','Romania')

INSERT INTO CityEntitie VALUES('Khanty Mansiysk','61.0015','69.0015','Russia')

INSERT INTO CityEntitie VALUES('Ishim','56.1502','69.4498','Russia')

INSERT INTO CityEntitie VALUES('Matehuala','23.6604','-100.65','Mexico')

INSERT INTO CityEntitie VALUES('Hanover','39.8117','-76.9835','United States')

INSERT INTO CityEntitie VALUES('Juchitan','16.43','-95.02','Mexico')

INSERT INTO CityEntitie VALUES('Saratoga Springs','43.0674','-73.7775','United States')

INSERT INTO CityEntitie VALUES('DeKalb','41.9312','-88.7483','United States')

INSERT INTO CityEntitie VALUES('Soyo','-6.1296','12.37','Angola')

INSERT INTO CityEntitie VALUES('Casper','42.8421','-106.3208','United States')

INSERT INTO CityEntitie VALUES('Kenner','30.0109','-90.2549','United States')

INSERT INTO CityEntitie VALUES('South San Francisco','37.6536','-122.4197','United States')

INSERT INTO CityEntitie VALUES('Santa Inês','-3.66','-45.39','Brazil')

INSERT INTO CityEntitie VALUES('El Bayadh','33.6904','1.01','Algeria')

INSERT INTO CityEntitie VALUES('Oktyabrskiy','52.6636','156.2387','Russia')

INSERT INTO CityEntitie VALUES('Sherman','33.6266','-96.6195','United States')

INSERT INTO CityEntitie VALUES('Dimbokro','6.6505','-4.71','Côte DIvoire')

INSERT INTO CityEntitie VALUES('Petaluma','38.2423','-122.6267','United States')

INSERT INTO CityEntitie VALUES('Tejen','37.3786','60.496','Turkmenistan')

INSERT INTO CityEntitie VALUES('Kasese','0.2325','29.9883','Uganda')

INSERT INTO CityEntitie VALUES('Tzaneen','-23.8195','30.17','South Africa')

INSERT INTO CityEntitie VALUES('Jelgava','56.6527','23.7128','Latvia')

INSERT INTO CityEntitie VALUES('Lao Chi','22.5014','103.966','Vietnam')

INSERT INTO CityEntitie VALUES('Bayonne','40.6659','-74.1141','United States')

INSERT INTO CityEntitie VALUES('Palo Alto','37.3913','-122.1467','United States')

INSERT INTO CityEntitie VALUES('Puerto Maldonado','-12.6','-69.1833','Peru')

INSERT INTO CityEntitie VALUES('Mbaïki','3.8704','18','Central African Republic')

INSERT INTO CityEntitie VALUES('Weirton','40.406','-80.5671','United States')

INSERT INTO CityEntitie VALUES('Bay City','43.5903','-83.8886','United States')

INSERT INTO CityEntitie VALUES('Jaltipan','17.94','-94.74','Mexico')

INSERT INTO CityEntitie VALUES('Narva','59.3776','28.1603','Estonia')

INSERT INTO CityEntitie VALUES('Apatity','67.5731','33.393','Russia')

INSERT INTO CityEntitie VALUES('Kati','12.7504','-8.08','Mali')

INSERT INTO CityEntitie VALUES('Aleksin','54.5143','37.0944','Russia')

INSERT INTO CityEntitie VALUES('Kissidougou','9.1905','-10.12','Guinea')

INSERT INTO CityEntitie VALUES('Ban?','18.28','-70.331','Dominican Republic')

INSERT INTO CityEntitie VALUES('Las Heras','-32.825','-68.8017','Argentina')

INSERT INTO CityEntitie VALUES('Eagan','44.817','-93.1638','United States')

INSERT INTO CityEntitie VALUES('Los Lunas','34.8114','-106.7808','United States')

INSERT INTO CityEntitie VALUES('Nalut','31.8804','10.97','Libya')

INSERT INTO CityEntitie VALUES('Mansehra','34.3418','73.1968','Pakistan')

INSERT INTO CityEntitie VALUES('Corvallis','44.5697','-123.278','United States')

INSERT INTO CityEntitie VALUES('Rogers','36.3173','-94.1514','United States')

INSERT INTO CityEntitie VALUES('Lake Elsinore','33.6843','-117.3348','United States')

INSERT INTO CityEntitie VALUES('Kungur','57.4348','56.9543','Russia')

INSERT INTO CityEntitie VALUES('Klintsy','52.7652','32.2448','Russia')

INSERT INTO CityEntitie VALUES('Laguna Niguel','33.5275','-117.705','United States')

INSERT INTO CityEntitie VALUES('Neryungri','56.674','124.7104','Russia')

INSERT INTO CityEntitie VALUES('Huinan','42.6229','126.2614','China')

INSERT INTO CityEntitie VALUES('Lahij','13.0582','44.8838','Yemen')

INSERT INTO CityEntitie VALUES('Leninogorsk','54.5987','52.4487','Russia')

INSERT INTO CityEntitie VALUES('Pyongsan','38.3367','126.3866','Korea')

INSERT INTO CityEntitie VALUES('Grand Forks','47.9223','-97.0887','United States')

INSERT INTO CityEntitie VALUES('Burdur','37.7167','30.2833','Turkey')

INSERT INTO CityEntitie VALUES('Alba Lulia','46.077','23.58','Romania')

INSERT INTO CityEntitie VALUES('Mor?n','22.1099','-78.6275','Cuba')

INSERT INTO CityEntitie VALUES('Lesosibirsk','58.2433','92.4833','Russia')

INSERT INTO CityEntitie VALUES('North Little Rock','34.7814','-92.2378','United States')

INSERT INTO CityEntitie VALUES('Lahat','-3.8','103.5333','Indonesia')

INSERT INTO CityEntitie VALUES('Rockhampton','-23.3639','150.52','Australia')

INSERT INTO CityEntitie VALUES('Chabahar','25.3004','60.6299','Iran')

INSERT INTO CityEntitie VALUES('Alpharetta','34.0704','-84.2739','United States')

INSERT INTO CityEntitie VALUES('Polevskoy','56.4434','60.188','Russia')

INSERT INTO CityEntitie VALUES('Sodo','6.9','37.75','Ethiopia')

INSERT INTO CityEntitie VALUES('Tamarac','26.2058','-80.2547','United States')

INSERT INTO CityEntitie VALUES('Schenectady','42.8025','-73.9276','United States')

INSERT INTO CityEntitie VALUES('Great Falls','47.5022','-111.2995','United States')

INSERT INTO CityEntitie VALUES('Longview','46.146','-122.963','United States')

INSERT INTO CityEntitie VALUES('West Des Moines','41.5527','-93.7805','United States')

INSERT INTO CityEntitie VALUES('Phyarpon','16.2933','95.6829','Burma')

INSERT INTO CityEntitie VALUES('Rio Negro','-26.1','-49.79','Brazil')

INSERT INTO CityEntitie VALUES('Panaji','15.492','73.818','India')

INSERT INTO CityEntitie VALUES('Prince George','53.9167','-122.7667','Canada')

INSERT INTO CityEntitie VALUES('Shawnee','39.0158','-94.8076','United States')

INSERT INTO CityEntitie VALUES('Santo ?ngelo','-28.3','-54.28','Brazil')

INSERT INTO CityEntitie VALUES('Bani Walid','31.7704','13.99','Libya')

INSERT INTO CityEntitie VALUES('East Orange','40.7651','-74.2117','United States')

INSERT INTO CityEntitie VALUES('Masaka','-0.3296','31.73','Uganda')

INSERT INTO CityEntitie VALUES('Kumertau','52.7748','55.7843','Russia')

INSERT INTO CityEntitie VALUES('San Clemente','33.4498','-117.6103','United States')

INSERT INTO CityEntitie VALUES('Birni Nkonni','13.7904','5.2599','Niger')

INSERT INTO CityEntitie VALUES('Debre Birhan','9.6804','39.53','Ethiopia')

INSERT INTO CityEntitie VALUES('Békéscsaba','46.672','21.101','Hungary')

INSERT INTO CityEntitie VALUES('Cob?n','15.47','-90.38','Guatemala')

INSERT INTO CityEntitie VALUES('Michigan City','41.7098','-86.8705','United States')

INSERT INTO CityEntitie VALUES('Ji-Paran?','-10.8333','-61.967','Brazil')

INSERT INTO CityEntitie VALUES('Bor?s','57.7304','12.92','Sweden')

INSERT INTO CityEntitie VALUES('Solwezi','-12.1796','26.4','Zambia')

INSERT INTO CityEntitie VALUES('Jupiter','26.9199','-80.1127','United States')

INSERT INTO CityEntitie VALUES('Saki','41.1923','47.1705','Azerbaijan')

INSERT INTO CityEntitie VALUES('Krasnoturinsk','59.7948','60.4848','Russia')

INSERT INTO CityEntitie VALUES('Tan Tan','28.4304','-11.1','Morocco')

INSERT INTO CityEntitie VALUES('Wellington','26.6464','-80.2707','United States')

INSERT INTO CityEntitie VALUES('Rocklin','38.8075','-121.2488','United States')

INSERT INTO CityEntitie VALUES('Gurupi','-11.7196','-49.06','Brazil')

INSERT INTO CityEntitie VALUES('Rafha','29.6202','43.4948','Saudi Arabia')

INSERT INTO CityEntitie VALUES('Rocky Mount','35.9676','-77.8047','United States')

INSERT INTO CityEntitie VALUES('Fairbanks','64.8353','-147.6533','United States')

INSERT INTO CityEntitie VALUES('Feyzabad','37.1298','70.5792','Afghanistan')

INSERT INTO CityEntitie VALUES('Elmira','42.0938','-76.8097','United States')

INSERT INTO CityEntitie VALUES('Mazatenango','14.5304','-91.51','Guatemala')

INSERT INTO CityEntitie VALUES('Binga','2.3834','20.42','Congo (Kinshasa)')

INSERT INTO CityEntitie VALUES('Johnstown','40.3258','-78.9194','United States')

INSERT INTO CityEntitie VALUES('Blaine','45.1696','-93.2077','United States')

INSERT INTO CityEntitie VALUES('Puerto Madryn','-42.77','-65.04','Argentina')

INSERT INTO CityEntitie VALUES('Sammamish','47.6019','-122.0419','United States')

INSERT INTO CityEntitie VALUES('Parintins','-2.61','-56.74','Brazil')

INSERT INTO CityEntitie VALUES('Nepalganj','28.0503','81.6167','Nepal')

INSERT INTO CityEntitie VALUES('Eden Prairie','44.8488','-93.4595','United States')

INSERT INTO CityEntitie VALUES('Tikhoretsk','45.8531','40.1377','Russia')

INSERT INTO CityEntitie VALUES('Redmond','47.6762','-122.1166','United States')

INSERT INTO CityEntitie VALUES('Glens Falls','43.3109','-73.6459','United States')

INSERT INTO CityEntitie VALUES('Taloqan','36.73','69.54','Afghanistan')

INSERT INTO CityEntitie VALUES('Parkersburg','39.2623','-81.5419','United States')

INSERT INTO CityEntitie VALUES('Qinggang','46.69','126.1','China')

INSERT INTO CityEntitie VALUES('Arras','50.2833','2.7833','France')

INSERT INTO CityEntitie VALUES('Ciudad Hidalgo','19.68','-100.57','Mexico')

INSERT INTO CityEntitie VALUES('Zrenjanin','45.3786','20.3995','Serbia')

INSERT INTO CityEntitie VALUES('Mazabuka','-15.86','27.76','Zambia')

INSERT INTO CityEntitie VALUES('Skokie','42.0359','-87.74','United States')

INSERT INTO CityEntitie VALUES('Curvelo','-18.7596','-44.43','Brazil')

INSERT INTO CityEntitie VALUES('Col?n','22.7196','-80.9058','Cuba')

INSERT INTO CityEntitie VALUES('Kangar','6.433','100.19','Malaysia')

INSERT INTO CityEntitie VALUES('Ramla','31.9167','34.8667','Israel')

INSERT INTO CityEntitie VALUES('Tulare','36.1996','-119.34','United States')

INSERT INTO CityEntitie VALUES('Kristiansand','58.1666','8','Norway')

INSERT INTO CityEntitie VALUES('Lakeville','44.6776','-93.2521','United States')

INSERT INTO CityEntitie VALUES('Caçador','-26.77','-51.02','Brazil')

INSERT INTO CityEntitie VALUES('Kanchanaburi','14.0174','99.522','Thailand')

INSERT INTO CityEntitie VALUES('Haverhill','42.7838','-71.0871','United States')

INSERT INTO CityEntitie VALUES('Irecê','-11.3','-41.87','Brazil')

INSERT INTO CityEntitie VALUES('Svobodnyy','51.4062','128.1312','Russia')

INSERT INTO CityEntitie VALUES('Catal?o','-18.18','-47.95','Brazil')

INSERT INTO CityEntitie VALUES('Zarafshon','41.5822','64.2018','Uzbekistan')

INSERT INTO CityEntitie VALUES('Pico Rivera','33.9902','-118.0888','United States')

INSERT INTO CityEntitie VALUES('Samut Sakhon','13.536','100.274','Thailand')

INSERT INTO CityEntitie VALUES('Beloit','42.523','-89.0184','United States')

INSERT INTO CityEntitie VALUES('Kakamega','0.2904','34.73','Kenya')

INSERT INTO CityEntitie VALUES('Sefra','32.7604','-0.5799','Algeria')

INSERT INTO CityEntitie VALUES('Rzhev','56.2574','34.3275','Russia')

INSERT INTO CityEntitie VALUES('Cahul','45.9079','28.1944','Moldova')

INSERT INTO CityEntitie VALUES('Bhairawa','27.5333','83.3833','Nepal')

INSERT INTO CityEntitie VALUES('Pflugerville','30.4528','-97.6022','United States')

INSERT INTO CityEntitie VALUES('Molepolole','-24.4','25.51','Botswana')

INSERT INTO CityEntitie VALUES('Zal?u','47.175','23.063','Romania')

INSERT INTO CityEntitie VALUES('Vilhena','-12.7166','-60.1166','Brazil')

INSERT INTO CityEntitie VALUES('Valença','-13.3596','-39.08','Brazil')

INSERT INTO CityEntitie VALUES('Eastvale','33.9617','-117.5802','United States')

INSERT INTO CityEntitie VALUES('Port Orange','29.1084','-81.0136','United States')

INSERT INTO CityEntitie VALUES('Montebello','34.0155','-118.1108','United States')

INSERT INTO CityEntitie VALUES('Encinitas','33.0492','-117.2613','United States')

INSERT INTO CityEntitie VALUES('Medicine Hat','50.0333','-110.6833','Canada')

INSERT INTO CityEntitie VALUES('Knysna','-34.0329','23.0333','South Africa')

INSERT INTO CityEntitie VALUES('Balakhna','56.4943','43.5944','Russia')

INSERT INTO CityEntitie VALUES('Bossangoa','6.4837','17.45','Central African Republic')

INSERT INTO CityEntitie VALUES('Sopore','34.3','74.4667','India')

INSERT INTO CityEntitie VALUES('Belize City','17.4987','-88.1884','Belize')

INSERT INTO CityEntitie VALUES('Kasongo','-4.45','26.66','Congo (Kinshasa)')

INSERT INTO CityEntitie VALUES('Coffs Harbour','-30.3071','153.1123','Australia')

INSERT INTO CityEntitie VALUES('Machiques','10.0704','-72.5499','Venezuela')

INSERT INTO CityEntitie VALUES('Itapeva','-23.9796','-48.88','Brazil')

INSERT INTO CityEntitie VALUES('Beledweyne','4.74','45.2','Somalia')

INSERT INTO CityEntitie VALUES('Graaff Reinet','-32.3','24.54','South Africa')

INSERT INTO CityEntitie VALUES('Balboa','8.95','-79.5667','Panama')

INSERT INTO CityEntitie VALUES('Rowlett','32.9156','-96.5488','United States')

INSERT INTO CityEntitie VALUES('Monessen','40.1519','-79.8828','United States')

INSERT INTO CityEntitie VALUES('Sebring','27.477','-81.453','United States')

INSERT INTO CityEntitie VALUES('Bitlis','38.394','42.123','Turkey')

INSERT INTO CityEntitie VALUES('Lehi','40.4137','-111.8728','United States')

INSERT INTO CityEntitie VALUES('Guam?chil','25.4704','-108.09','Mexico')

INSERT INTO CityEntitie VALUES('Nkawkaw','6.5505','-0.78','Ghana')

INSERT INTO CityEntitie VALUES('Coon Rapids','45.1755','-93.3094','United States')

INSERT INTO CityEntitie VALUES('Belebey','54.1291','54.1187','Russia')

INSERT INTO CityEntitie VALUES('Tataouine','33','10.4667','Tunisia')

INSERT INTO CityEntitie VALUES('Surin','14.8868','103.4915','Thailand')

INSERT INTO CityEntitie VALUES('Kayes','-4.18','13.28','Congo (Brazzaville)')

INSERT INTO CityEntitie VALUES('La Habra','33.9278','-117.9513','United States')

INSERT INTO CityEntitie VALUES('Waltham','42.3889','-71.2423','United States')

INSERT INTO CityEntitie VALUES('Brentwood','37.9355','-121.7191','United States')

INSERT INTO CityEntitie VALUES('Singida','-4.8196','34.74','Tanzania')

INSERT INTO CityEntitie VALUES('Ankeny','41.728','-93.6031','United States')

INSERT INTO CityEntitie VALUES('San Luis Obispo','35.2671','-120.6689','United States')

INSERT INTO CityEntitie VALUES('Tanjungpandan','-2.75','107.65','Indonesia')

INSERT INTO CityEntitie VALUES('Springfield','44.0538','-122.9811','United States')

INSERT INTO CityEntitie VALUES('Council Bluffs','41.2369','-95.8517','United States')

INSERT INTO CityEntitie VALUES('Castle Rock','39.3761','-104.8534','United States')

INSERT INTO CityEntitie VALUES('Assen','53','6.55','Netherlands')

INSERT INTO CityEntitie VALUES('North Miami','25.9006','-80.1681','United States')

INSERT INTO CityEntitie VALUES('Chistopol','55.3648','50.6407','Russia')

INSERT INTO CityEntitie VALUES('Bambari','5.762','20.6672','Central African Republic')

INSERT INTO CityEntitie VALUES('Hamilton','39.3938','-84.5653','United States')

INSERT INTO CityEntitie VALUES('Tikhvin','59.6448','33.5144','Russia')

INSERT INTO CityEntitie VALUES('Sagua la Grande','22.809','-80.0711','Cuba')

INSERT INTO CityEntitie VALUES('Tup?','-21.93','-50.52','Brazil')

INSERT INTO CityEntitie VALUES('Madang','-5.2248','145.7853','Papua New Guinea')

INSERT INTO CityEntitie VALUES('Veszprém','47.091','17.911','Hungary')

INSERT INTO CityEntitie VALUES('Ferkessédougou','9.6004','-5.2','Côte DIvoire')

INSERT INTO CityEntitie VALUES('Labinsk','44.6348','40.7443','Russia')

INSERT INTO CityEntitie VALUES('Fernand?polis','-20.2696','-50.26','Brazil')

INSERT INTO CityEntitie VALUES('Apia','-13.8415','-171.7386','Samoa')

INSERT INTO CityEntitie VALUES('Mankato','44.1712','-93.9773','United States')

INSERT INTO CityEntitie VALUES('Zalaegerszeg','46.844','16.84','Hungary')

INSERT INTO CityEntitie VALUES('Noblesville','40.0353','-86.006','United States')

INSERT INTO CityEntitie VALUES('Bamian','34.8211','67.521','Afghanistan')

INSERT INTO CityEntitie VALUES('Bassar','9.261','0.789','Togo')

INSERT INTO CityEntitie VALUES('Moca','19.397','-70.523','Dominican Republic')

INSERT INTO CityEntitie VALUES('Iskitim','54.6509','83.2865','Russia')

INSERT INTO CityEntitie VALUES('Berbérati','4.25','15.78','Central African Republic')

INSERT INTO CityEntitie VALUES('Benevento','41.1337','14.75','Italy')

INSERT INTO CityEntitie VALUES('Chinhoyi','-17.3596','30.18','Zimbabwe')

INSERT INTO CityEntitie VALUES('Medenine','33.4','10.4167','Tunisia')

INSERT INTO CityEntitie VALUES('Troyes','48.3404','4.0834','France')

INSERT INTO CityEntitie VALUES('Vyska','55.3247','42.1644','Russia')

INSERT INTO CityEntitie VALUES('Gadsden','34.009','-86.0156','United States')

INSERT INTO CityEntitie VALUES('Pula','44.8687','13.8481','Croatia')

INSERT INTO CityEntitie VALUES('Sibay','52.7091','58.6387','Russia')

INSERT INTO CityEntitie VALUES('Cayenne','4.933','-52.33','French Guiana')

INSERT INTO CityEntitie VALUES('Ponta Delgada','37.7483','-25.6666','Portugal')

INSERT INTO CityEntitie VALUES('Moore','35.3294','-97.4758','United States')

INSERT INTO CityEntitie VALUES('Fengcheng','28.2004','115.77','China')

INSERT INTO CityEntitie VALUES('Burnsville','44.7648','-93.2795','United States')

INSERT INTO CityEntitie VALUES('Stralsund','54.3004','13.1','Germany')

INSERT INTO CityEntitie VALUES('National City','32.6654','-117.0983','United States')

INSERT INTO CityEntitie VALUES('Civitavecchia','42.1004','11.8','Italy')

INSERT INTO CityEntitie VALUES('Taylor','42.226','-83.2688','United States')

INSERT INTO CityEntitie VALUES('Malden','42.4305','-71.0576','United States')

INSERT INTO CityEntitie VALUES('Portim?o','37.1337','-8.5333','Portugal')

INSERT INTO CityEntitie VALUES('Gaalkacyo','6.77','47.43','Somalia')

INSERT INTO CityEntitie VALUES('Doral','25.8149','-80.3565','United States')

INSERT INTO CityEntitie VALUES('Coburg','50.2666','10.9666','Germany')

INSERT INTO CityEntitie VALUES('Marietta','33.9532','-84.5421','United States')

INSERT INTO CityEntitie VALUES('Monterey Park','34.0497','-118.1325','United States')

INSERT INTO CityEntitie VALUES('Yopal','5.347','-72.406','Colombia')

INSERT INTO CityEntitie VALUES('Coconut Creek','26.2803','-80.1842','United States')

INSERT INTO CityEntitie VALUES('Salsk','46.4775','41.542','Russia')

INSERT INTO CityEntitie VALUES('Cherbourg','49.6504','-1.65','France')

INSERT INTO CityEntitie VALUES('El Carmen de Bol?var','9.7204','-75.13','Colombia')

INSERT INTO CityEntitie VALUES('Rome','34.2662','-85.1862','United States')

INSERT INTO CityEntitie VALUES('Bouaflé','6.978','-5.748','Côte DIvoire')

INSERT INTO CityEntitie VALUES('Dublin','37.7161','-121.8964','United States')

INSERT INTO CityEntitie VALUES('Sonbong','42.3377','130.4027','Korea')

INSERT INTO CityEntitie VALUES('Tartagal','-22.55','-63.81','Argentina')

INSERT INTO CityEntitie VALUES('Canela','-29.36','-50.81','Brazil')

INSERT INTO CityEntitie VALUES('Nelson','-41.2926','173.2474','New Zealand')

INSERT INTO CityEntitie VALUES('Albany','44.6274','-123.0966','United States')

INSERT INTO CityEntitie VALUES('Cupertino','37.3168','-122.0465','United States')

INSERT INTO CityEntitie VALUES('San Fernando','-34.58','-70.99','Chile')

INSERT INTO CityEntitie VALUES('Shuya','56.8543','41.3643','Russia')

INSERT INTO CityEntitie VALUES('Sfintu-Gheorghe','45.868','25.793','Romania')

INSERT INTO CityEntitie VALUES('Tidore','0.6964','127.436','Indonesia')

INSERT INTO CityEntitie VALUES('Lokoja','7.8004','6.7399','Nigeria')

INSERT INTO CityEntitie VALUES('Langzhong','31.5759','105.9656','China')

INSERT INTO CityEntitie VALUES('Pirassununga','-21.99','-47.43','Brazil')

INSERT INTO CityEntitie VALUES('Khujayli','42.4047','59.4517','Uzbekistan')

INSERT INTO CityEntitie VALUES('Morristown','36.2043','-83.3001','United States')

INSERT INTO CityEntitie VALUES('Lakewood','47.1628','-122.5299','United States')

INSERT INTO CityEntitie VALUES('Banfora','10.6304','-4.76','Burkina Faso')

INSERT INTO CityEntitie VALUES('Zima','53.9331','102.0331','Russia')

INSERT INTO CityEntitie VALUES('Gardena','33.8944','-118.3073','United States')

INSERT INTO CityEntitie VALUES('Bristol','41.6812','-72.9407','United States')

INSERT INTO CityEntitie VALUES('Millcreek','40.6892','-111.8291','United States')

INSERT INTO CityEntitie VALUES('Mbanza-Congo','-6.2696','14.24','Angola')

INSERT INTO CityEntitie VALUES('Lankaran','38.754','48.8511','Azerbaijan')

INSERT INTO CityEntitie VALUES('Pirapora','-17.33','-44.93','Brazil')

INSERT INTO CityEntitie VALUES('Caratinga','-19.79','-42.14','Brazil')

INSERT INTO CityEntitie VALUES('Woodland','38.6712','-121.75','United States')

INSERT INTO CityEntitie VALUES('La Mesa','32.7703','-117.0204','United States')

INSERT INTO CityEntitie VALUES('Crotone','39.0833','17.1233','Italy')

INSERT INTO CityEntitie VALUES('U?ge','-7.62','15.05','Angola')

INSERT INTO CityEntitie VALUES('Taylorsville','40.6569','-111.9493','United States')

INSERT INTO CityEntitie VALUES('Benton Harbor','42.1159','-86.4488','United States')

INSERT INTO CityEntitie VALUES('West Allis','43.0068','-88.0296','United States')

INSERT INTO CityEntitie VALUES('Meriden','41.5367','-72.7943','United States')

INSERT INTO CityEntitie VALUES('Kotlas','61.2631','46.6631','Russia')

INSERT INTO CityEntitie VALUES('Gorno Altaysk','51.9613','85.9577','Russia')

INSERT INTO CityEntitie VALUES('Chapel Hill','35.9269','-79.039','United States')

INSERT INTO CityEntitie VALUES('Nkhotakota','-12.9163','34.3','Malawi')

INSERT INTO CityEntitie VALUES('Pontiac','42.6493','-83.2878','United States')

INSERT INTO CityEntitie VALUES('Linares','38.0833','-3.6334','Spain')

INSERT INTO CityEntitie VALUES('Itapetinga','-15.25','-40.25','Brazil')

INSERT INTO CityEntitie VALUES('Novi','42.4786','-83.4893','United States')

INSERT INTO CityEntitie VALUES('Saint Clair Shores','42.4921','-82.8957','United States')

INSERT INTO CityEntitie VALUES('Beckley','37.7878','-81.1841','United States')

INSERT INTO CityEntitie VALUES('S?o Borja','-28.6596','-56.01','Brazil')

INSERT INTO CityEntitie VALUES('V?xj?','56.8837','14.8167','Sweden')

INSERT INTO CityEntitie VALUES('Leticia','-4.22','-69.94','Colombia')

INSERT INTO CityEntitie VALUES('Béja','36.7304','9.19','Tunisia')

INSERT INTO CityEntitie VALUES('Drummondville','45.8833','-72.4834','Canada')

INSERT INTO CityEntitie VALUES('Truc Giang','10.235','106.375','Vietnam')

INSERT INTO CityEntitie VALUES('Carazinho','-28.29','-52.8','Brazil')

INSERT INTO CityEntitie VALUES('Dengzhou','32.6804','112.08','China')

INSERT INTO CityEntitie VALUES('Sanford','28.7891','-81.2757','United States')

INSERT INTO CityEntitie VALUES('San Felipe','-32.75','-70.72','Chile')

INSERT INTO CityEntitie VALUES('Santa Rosa','-27.8695','-54.46','Brazil')

INSERT INTO CityEntitie VALUES('Lappeenranta','61.0671','28.1833','Finland')

INSERT INTO CityEntitie VALUES('Kavala','40.9412','24.4018','Greece')

INSERT INTO CityEntitie VALUES('Gannan','47.9204','123.51','China')

INSERT INTO CityEntitie VALUES('Azua','18.454','-70.729','Dominican Republic')

INSERT INTO CityEntitie VALUES('Midland','43.6239','-84.2315','United States')

INSERT INTO CityEntitie VALUES('Viedma','-40.8','-63','Argentina')

INSERT INTO CityEntitie VALUES('Concepci?n','-23.4064','-57.4344','Paraguay')

INSERT INTO CityEntitie VALUES('Royal Oak','42.5084','-83.1539','United States')

INSERT INTO CityEntitie VALUES('Bangor','44.8322','-68.7906','United States')

INSERT INTO CityEntitie VALUES('Manacapuru','-3.2896','-60.62','Brazil')

INSERT INTO CityEntitie VALUES('Bartlett','35.2337','-89.8195','United States')

INSERT INTO CityEntitie VALUES('San Rafael','37.9905','-122.5222','United States')

INSERT INTO CityEntitie VALUES('Mt. Hagen','-5.8632','144.2168','Papua New Guinea')

INSERT INTO CityEntitie VALUES('San Francisco','-31.43','-62.09','Argentina')

INSERT INTO CityEntitie VALUES('White Plains','41.022','-73.7548','United States')

INSERT INTO CityEntitie VALUES('Kabinda','-6.1296','24.48','Congo (Kinshasa)')

INSERT INTO CityEntitie VALUES('Goldsboro','35.3777','-77.972','United States')

INSERT INTO CityEntitie VALUES('Mikhaylovka','50.0679','43.2175','Russia')

INSERT INTO CityEntitie VALUES('Solnechnogorsk','56.1807','36.9809','Russia')

INSERT INTO CityEntitie VALUES('Telêmaco Borba','-24.33','-50.62','Brazil')

INSERT INTO CityEntitie VALUES('Guanambi','-14.2296','-42.79','Brazil')

INSERT INTO CityEntitie VALUES('Bowie','38.9575','-76.7421','United States')

INSERT INTO CityEntitie VALUES('Tezpur','26.6338','92.8','India')

INSERT INTO CityEntitie VALUES('Huntington Park','33.98','-118.2167','United States')

INSERT INTO CityEntitie VALUES('Arcadia','34.1342','-118.0373','United States')

INSERT INTO CityEntitie VALUES('Lewiston','44.0915','-70.1681','United States')

INSERT INTO CityEntitie VALUES('Kamphaeng Phet','16.473','99.529','Thailand')

INSERT INTO CityEntitie VALUES('Columbus','39.2091','-85.918','United States')

INSERT INTO CityEntitie VALUES('Orland Park','41.6075','-87.8619','United States')

INSERT INTO CityEntitie VALUES('Arsenyev','44.1623','133.2823','Russia')

INSERT INTO CityEntitie VALUES('Casa Grande','32.907','-111.7624','United States')

INSERT INTO CityEntitie VALUES('Embu','-0.5196','37.45','Kenya')

INSERT INTO CityEntitie VALUES('Aketi','2.7405','23.78','Congo (Kinshasa)')

INSERT INTO CityEntitie VALUES('Staunton','38.1593','-79.0611','United States')

INSERT INTO CityEntitie VALUES('San Pedro','-24.2196','-64.87','Argentina')

INSERT INTO CityEntitie VALUES('Margate','26.2465','-80.2119','United States')

INSERT INTO CityEntitie VALUES('Chaiyaphum','15.804','102.0386','Thailand')

INSERT INTO CityEntitie VALUES('Bondoukou','8.0304','-2.8','Côte DIvoire')

INSERT INTO CityEntitie VALUES('Jefferson City','38.5677','-92.1757','United States')

INSERT INTO CityEntitie VALUES('Korçë','40.6167','20.7667','Albania')

INSERT INTO CityEntitie VALUES('San Andrés','12.5621','-81.6903','Colombia')

INSERT INTO CityEntitie VALUES('K?rklareli','41.743','27.226','Turkey')

INSERT INTO CityEntitie VALUES('Agen','44.2004','0.6333','France')

INSERT INTO CityEntitie VALUES('Des Plaines','42.0344','-87.9008','United States')

INSERT INTO CityEntitie VALUES('Rundu','-17.92','19.7499','Namibia')

INSERT INTO CityEntitie VALUES('Santee','32.8555','-116.9856','United States')

INSERT INTO CityEntitie VALUES('Ariquemes','-9.9396','-63.08','Brazil')

INSERT INTO CityEntitie VALUES('Hai Duong','20.942','106.331','Vietnam')

INSERT INTO CityEntitie VALUES('Ushuaia','-54.79','-68.31','Argentina')

INSERT INTO CityEntitie VALUES('Middletown','41.4458','-74.4228','United States')

INSERT INTO CityEntitie VALUES('Allanmyo','19.3783','95.2279','Burma')

INSERT INTO CityEntitie VALUES('Borovichi','58.3978','33.8974','Russia')

INSERT INTO CityEntitie VALUES('Angoche','-16.23','39.91','Mozambique')

INSERT INTO CityEntitie VALUES('Hot Springs','34.4902','-93.0498','United States')

INSERT INTO CityEntitie VALUES('Napier','-39.49','176.9265','New Zealand')

INSERT INTO CityEntitie VALUES('Medford','42.4234','-71.1087','United States')

INSERT INTO CityEntitie VALUES('Suileng','47.246','127.106','China')

INSERT INTO CityEntitie VALUES('Walla Walla','46.067','-118.3365','United States')

INSERT INTO CityEntitie VALUES('Lop Buri','14.804','100.6186','Thailand')

INSERT INTO CityEntitie VALUES('Qunghirot','43.0704','58.9','Uzbekistan')

INSERT INTO CityEntitie VALUES('Linares','24.8604','-99.57','Mexico')

INSERT INTO CityEntitie VALUES('Tiznit','29.7104','-9.74','Morocco')

INSERT INTO CityEntitie VALUES('Timba?ba','-7.4996','-35.32','Brazil')

INSERT INTO CityEntitie VALUES('Carson City','39.1512','-119.7474','United States')

INSERT INTO CityEntitie VALUES('Hendersonville','36.3063','-86.5997','United States')

INSERT INTO CityEntitie VALUES('Titusville','28.5727','-80.8193','United States')

INSERT INTO CityEntitie VALUES('Picos','-7.08','-41.44','Brazil')

INSERT INTO CityEntitie VALUES('Stepanakert','39.8156','46.752','Azerbaijan')

INSERT INTO CityEntitie VALUES('Kentau','43.5165','68.5199','Kazakhstan')

INSERT INTO CityEntitie VALUES('Cheremkhovo','53.1588','103.0739','Russia')

INSERT INTO CityEntitie VALUES('Greenwood','39.6019','-86.1073','United States')

INSERT INTO CityEntitie VALUES('Caborca','30.7161','-112.1642','Mexico')

INSERT INTO CityEntitie VALUES('Midwest City','35.463','-97.371','United States')

INSERT INTO CityEntitie VALUES('Manhattan','39.1882','-96.6053','United States')

INSERT INTO CityEntitie VALUES('Punta Alta','-38.88','-62.08','Argentina')

INSERT INTO CityEntitie VALUES('Saint Peters','38.7825','-90.6061','United States')

INSERT INTO CityEntitie VALUES('Arcoverde','-8.42','-37.07','Brazil')

INSERT INTO CityEntitie VALUES('Bragança','-1.05','-46.77','Brazil')

INSERT INTO CityEntitie VALUES('Taunton','41.9036','-71.0943','United States')

INSERT INTO CityEntitie VALUES('Al Mafraq','32.2833','36.2333','Jordan')

INSERT INTO CityEntitie VALUES('Puntarenas','9.9702','-84.8336','Costa Rica')

INSERT INTO CityEntitie VALUES('New Brunswick','40.487','-74.445','United States')

INSERT INTO CityEntitie VALUES('Mafetang','-29.8166','27.25','Lesotho')

INSERT INTO CityEntitie VALUES('General Pico','-35.6596','-63.77','Argentina')

INSERT INTO CityEntitie VALUES('Vaasa','63.1','21.6','Finland')

INSERT INTO CityEntitie VALUES('Roslavl','53.9509','32.8604','Russia')

INSERT INTO CityEntitie VALUES('Rodos','36.4412','28.2225','Greece')

INSERT INTO CityEntitie VALUES('Adrar','27.87','-0.29','Algeria')

INSERT INTO CityEntitie VALUES('Kitgum','3.3004','32.87','Uganda')

INSERT INTO CityEntitie VALUES('Cruzeiro do Sul','-7.63','-72.67','Brazil')

INSERT INTO CityEntitie VALUES('Los Andes','-32.8296','-70.6','Chile')

INSERT INTO CityEntitie VALUES('Serrinha','-11.6496','-39.01','Brazil')

INSERT INTO CityEntitie VALUES('Jijiga','9.3504','42.79','Ethiopia')

INSERT INTO CityEntitie VALUES('East Stroudsburg','41.0023','-75.1779','United States')

INSERT INTO CityEntitie VALUES('Vacaria','-28.4996','-50.94','Brazil')

INSERT INTO CityEntitie VALUES('Ridder','50.3554','83.5149','Kazakhstan')

INSERT INTO CityEntitie VALUES('Smyrna','33.8633','-84.5168','United States')

INSERT INTO CityEntitie VALUES('Tinley Park','41.567','-87.8051','United States')

INSERT INTO CityEntitie VALUES('Diamond Bar','33.9992','-117.8161','United States')

INSERT INTO CityEntitie VALUES('Eger','47.895','20.383','Hungary')

INSERT INTO CityEntitie VALUES('Puerto Barrios','15.7175','-88.5927','Guatemala')

INSERT INTO CityEntitie VALUES('Garz?n','2.2104','-75.65','Colombia')

INSERT INTO CityEntitie VALUES('Tikrit','34.597','43.677','Iraq')

INSERT INTO CityEntitie VALUES('Jana?ba','-15.7996','-43.31','Brazil')

INSERT INTO CityEntitie VALUES('Kaga Bandoro','6.9804','19.18','Central African Republic')

INSERT INTO CityEntitie VALUES('Bradenton','27.49','-82.5743','United States')

INSERT INTO CityEntitie VALUES('Magong','23.5667','119.5833','Taiwan')

INSERT INTO CityEntitie VALUES('Formiga','-20.46','-45.43','Brazil')

INSERT INTO CityEntitie VALUES('Alxa Zuoqi','38.839','105.6686','China')

INSERT INTO CityEntitie VALUES('Pol-e Khomri','35.9511','68.7011','Afghanistan')

INSERT INTO CityEntitie VALUES('Zouirat','22.7304','-12.4833','Mauritania')

INSERT INTO CityEntitie VALUES('Fountain Valley','33.7105','-117.9514','United States')

INSERT INTO CityEntitie VALUES('Pittsfield','42.4517','-73.2605','United States')

INSERT INTO CityEntitie VALUES('Richland','46.2826','-119.2938','United States')

INSERT INTO CityEntitie VALUES('Huntersville','35.4057','-80.873','United States')

INSERT INTO CityEntitie VALUES('Rotorua','-38.1317','176.2483','New Zealand')

INSERT INTO CityEntitie VALUES('Shoreline','47.7564','-122.3426','United States')

INSERT INTO CityEntitie VALUES('Hof','50.317','11.9167','Germany')

INSERT INTO CityEntitie VALUES('Paita','-5.09','-81.12','Peru')

INSERT INTO CityEntitie VALUES('Oak Lawn','41.7139','-87.7528','United States')

INSERT INTO CityEntitie VALUES('Nizhnyaya Tura','58.6436','59.7983','Russia')

INSERT INTO CityEntitie VALUES('Nova Viçosa','-17.88','-39.37','Brazil')

INSERT INTO CityEntitie VALUES('Novato','38.092','-122.5576','United States')

INSERT INTO CityEntitie VALUES('Bungoma','0.5704','34.56','Kenya')

INSERT INTO CityEntitie VALUES('Commerce City','39.8642','-104.8434','United States')

INSERT INTO CityEntitie VALUES('Lautoka','-17.6161','177.4666','Fiji')

INSERT INTO CityEntitie VALUES('Seres','41.086','23.5497','Greece')

INSERT INTO CityEntitie VALUES('Changling','44.27','123.99','China')

INSERT INTO CityEntitie VALUES('Hempstead','40.7043','-73.6193','United States')

INSERT INTO CityEntitie VALUES('Cartersville','34.1632','-84.8007','United States')

INSERT INTO CityEntitie VALUES('Itapipoca','-3.4995','-39.58','Brazil')

INSERT INTO CityEntitie VALUES('E?k','53.8337','22.35','Poland')

INSERT INTO CityEntitie VALUES('Dearborn Heights','42.3164','-83.2769','United States')

INSERT INTO CityEntitie VALUES('Umm Ruwaba','12.9104','31.2','Sudan')

INSERT INTO CityEntitie VALUES('Kingston','41.9295','-73.9968','United States')

INSERT INTO CityEntitie VALUES('Parachinar','33.8992','70.1008','Pakistan')

INSERT INTO CityEntitie VALUES('Halmstad','56.6718','12.8556','Sweden')

INSERT INTO CityEntitie VALUES('Estância','-11.2696','-37.45','Brazil')

INSERT INTO CityEntitie VALUES('Sayanogorsk','53.0894','91.4004','Russia')

INSERT INTO CityEntitie VALUES('?vora','38.56','-7.906','Portugal')

INSERT INTO CityEntitie VALUES('Berwyn','41.8433','-87.7909','United States')

INSERT INTO CityEntitie VALUES('Arqalyq','50.2418','66.8976','Kazakhstan')

INSERT INTO CityEntitie VALUES('Chicopee','42.1764','-72.5719','United States')

INSERT INTO CityEntitie VALUES('Gelendzhik','44.5748','38.0644','Russia')

INSERT INTO CityEntitie VALUES('Vyazma','55.2122','34.2918','Russia')

INSERT INTO CityEntitie VALUES('Melo','-32.3595','-54.18','Uruguay')

INSERT INTO CityEntitie VALUES('Maldonado','-34.91','-54.96','Uruguay')

INSERT INTO CityEntitie VALUES('Brive','45.1504','1.5333','France')

INSERT INTO CityEntitie VALUES('Hazleton','40.9504','-75.9724','United States')

INSERT INTO CityEntitie VALUES('Ithaca','42.4442','-76.5032','United States')

INSERT INTO CityEntitie VALUES('S?o Gabriel','-30.32','-54.32','Brazil')

INSERT INTO CityEntitie VALUES('Xinqing','48.2363','129.5059','China')

INSERT INTO CityEntitie VALUES('Placetas','22.3158','-79.6555','Cuba')

INSERT INTO CityEntitie VALUES('Wagga Wagga','-35.1222','147.34','Australia')

INSERT INTO CityEntitie VALUES('Nakhon Phanom','17.3945','104.7695','Thailand')

INSERT INTO CityEntitie VALUES('Conc?rdia','-27.23','-52.03','Brazil')

INSERT INTO CityEntitie VALUES('Highland','34.1113','-117.1653','United States')

INSERT INTO CityEntitie VALUES('Lake Havasu City','34.5006','-114.3115','United States')

INSERT INTO CityEntitie VALUES('S?o José de Ribamar','-2.55','-44.07','Brazil')

INSERT INTO CityEntitie VALUES('Kribi','2.9404','9.91','Cameroon')

INSERT INTO CityEntitie VALUES('Kettering','39.6957','-84.1496','United States')

INSERT INTO CityEntitie VALUES('Euless','32.8508','-97.0798','United States')

INSERT INTO CityEntitie VALUES('Hoboken','40.7453','-74.0279','United States')

INSERT INTO CityEntitie VALUES('Abancay','-13.6396','-72.89','Peru')

INSERT INTO CityEntitie VALUES('Tadmur','34.5504','38.2833','Syria')

INSERT INTO CityEntitie VALUES('Kalasin','16.428','103.509','Thailand')

INSERT INTO CityEntitie VALUES('Marzuq','25.9044','13.8972','Libya')

INSERT INTO CityEntitie VALUES('Karlovac','45.4872','15.5478','Croatia')

INSERT INTO CityEntitie VALUES('Palm Beach Gardens','26.8488','-80.1656','United States')

INSERT INTO CityEntitie VALUES('Watertown','43.9734','-75.9094','United States')

INSERT INTO CityEntitie VALUES('Grants Pass','42.4333','-123.3317','United States')

INSERT INTO CityEntitie VALUES('Blue Springs','39.0124','-94.2722','United States')

INSERT INTO CityEntitie VALUES('Caic?','-6.4596','-37.1','Brazil')

INSERT INTO CityEntitie VALUES('Paramount','33.8976','-118.1651','United States')

INSERT INTO CityEntitie VALUES('Tarbes','43.2333','0.0833','France')

INSERT INTO CityEntitie VALUES('West Haven','41.2739','-72.9671','United States')

INSERT INTO CityEntitie VALUES('Colton','34.0538','-117.3254','United States')

INSERT INTO CityEntitie VALUES('Nuevo Casas Grandes','30.4185','-107.9119','Mexico')

INSERT INTO CityEntitie VALUES('Juigalpa','12.11','-85.38','Nicaragua')

INSERT INTO CityEntitie VALUES('Caldwell','43.6458','-116.6591','United States')

INSERT INTO CityEntitie VALUES('Ningan','44.3313','129.4659','China')

INSERT INTO CityEntitie VALUES('Fond du Lac','43.772','-88.4396','United States')

INSERT INTO CityEntitie VALUES('Arroyo Grande','35.1241','-120.5845','United States')

INSERT INTO CityEntitie VALUES('Cathedral City','33.8363','-116.4642','United States')

INSERT INTO CityEntitie VALUES('Cape Girardeau','37.3108','-89.5596','United States')

INSERT INTO CityEntitie VALUES('Rosemead','34.0688','-118.0823','United States')

INSERT INTO CityEntitie VALUES('El Banco','9.0003','-73.98','Colombia')

INSERT INTO CityEntitie VALUES('Moquegua','-17.19','-70.94','Peru')

INSERT INTO CityEntitie VALUES('Chivilcoy','-34.9','-60.04','Argentina')

INSERT INTO CityEntitie VALUES('Pen?polis','-21.41','-50.08','Brazil')

INSERT INTO CityEntitie VALUES('Delano','35.767','-119.2637','United States')

INSERT INTO CityEntitie VALUES('Stonecrest','33.6843','-84.1373','United States')

INSERT INTO CityEntitie VALUES('Twin Falls','42.5645','-114.4611','United States')

INSERT INTO CityEntitie VALUES('Williamsport','41.2398','-77.0371','United States')

INSERT INTO CityEntitie VALUES('Ajaccio','41.9271','8.7283','France')

INSERT INTO CityEntitie VALUES('Krasnokamensk','50.0665','118.0265','Russia')

INSERT INTO CityEntitie VALUES('Mocambique','-15.0399','40.6822','Mozambique')

INSERT INTO CityEntitie VALUES('Eagle Pass','28.7118','-100.4832','United States')

INSERT INTO CityEntitie VALUES('Normal','40.522','-88.9877','United States')

INSERT INTO CityEntitie VALUES('Tacuaremb?','-31.71','-55.98','Uruguay')

INSERT INTO CityEntitie VALUES('Ash Shihr','14.7593','49.6092','Yemen')

INSERT INTO CityEntitie VALUES('West New York','40.7856','-74.0093','United States')

INSERT INTO CityEntitie VALUES('Leesburg','39.1058','-77.5544','United States')

INSERT INTO CityEntitie VALUES('Beaufort','32.4487','-80.7103','United States')

INSERT INTO CityEntitie VALUES('Parker','39.5083','-104.7755','United States')

INSERT INTO CityEntitie VALUES('Aveiro','40.641','-8.651','Portugal')

INSERT INTO CityEntitie VALUES('Illichivsk','46.3','30.6666','Ukraine')

INSERT INTO CityEntitie VALUES('Southaven','34.9514','-89.9787','United States')

INSERT INTO CityEntitie VALUES('Nuevitas','21.5456','-77.2644','Cuba')

INSERT INTO CityEntitie VALUES('Brunswick','31.145','-81.474','United States')

INSERT INTO CityEntitie VALUES('Andorra','42.5','1.5165','Andorra')

INSERT INTO CityEntitie VALUES('Revere','42.4191','-71.0035','United States')

INSERT INTO CityEntitie VALUES('Grapevine','32.9343','-97.0744','United States')

INSERT INTO CityEntitie VALUES('Ruteng','-8.6118','120.4698','Indonesia')

INSERT INTO CityEntitie VALUES('Chiquinquir?','5.6204','-73.8199','Colombia')

INSERT INTO CityEntitie VALUES('Bozeman','45.6828','-111.0548','United States')

INSERT INTO CityEntitie VALUES('Azul','-36.7796','-59.87','Argentina')

INSERT INTO CityEntitie VALUES('Chicoutimi','48.4333','-71.0667','Canada')

INSERT INTO CityEntitie VALUES('Mount Prospect','42.0653','-87.937','United States')

INSERT INTO CityEntitie VALUES('Severomorsk','69.0731','33.4231','Russia')

INSERT INTO CityEntitie VALUES('Jihlava','49.4004','15.5833','Czechia')

INSERT INTO CityEntitie VALUES('Elyria','41.3761','-82.1063','United States')

INSERT INTO CityEntitie VALUES('Bijar','35.8741','47.5937','Iran')

INSERT INTO CityEntitie VALUES('Vyshnniy Volochek','57.583','34.5631','Russia')

INSERT INTO CityEntitie VALUES('Markala','13.6704','-6.07','Mali')

INSERT INTO CityEntitie VALUES('B?c Giang','21.267','106.2','Vietnam')

INSERT INTO CityEntitie VALUES('Yevlax','40.6172','47.15','Azerbaijan')

INSERT INTO CityEntitie VALUES('San Pedro de las Colonias','25.7592','-102.9827','Mexico')

INSERT INTO CityEntitie VALUES('Upata','8.0204','-62.41','Venezuela')

INSERT INTO CityEntitie VALUES('Yucaipa','34.0336','-117.0426','United States')

INSERT INTO CityEntitie VALUES('Charikar','35.0183','69.1679','Afghanistan')

INSERT INTO CityEntitie VALUES('Pamplona','7.3904','-72.66','Colombia')

INSERT INTO CityEntitie VALUES('DeSoto','32.5992','-96.8633','United States')

INSERT INTO CityEntitie VALUES('Lenexa','38.9609','-94.8018','United States')

INSERT INTO CityEntitie VALUES('Brookhaven','33.8746','-84.3314','United States')

INSERT INTO CityEntitie VALUES('West Sacramento','38.5556','-121.5504','United States')

INSERT INTO CityEntitie VALUES('Buguruslan','53.663','52.433','Russia')

INSERT INTO CityEntitie VALUES('Ilo','-17.64','-71.34','Peru')

INSERT INTO CityEntitie VALUES('Lewiston','46.3935','-116.9934','United States')

INSERT INTO CityEntitie VALUES('Grand Island','40.9214','-98.3584','United States')

INSERT INTO CityEntitie VALUES('Bellevue','41.1535','-95.9357','United States')

INSERT INTO CityEntitie VALUES('Supham Buri','14.471','100.129','Thailand')

INSERT INTO CityEntitie VALUES('R?o Tercero','-32.1796','-64.12','Argentina')

INSERT INTO CityEntitie VALUES('Joensuu','62.6','29.7666','Finland')

INSERT INTO CityEntitie VALUES('Wheaton','41.8561','-88.1083','United States')

INSERT INTO CityEntitie VALUES('Coari','-4.08','-63.13','Brazil')

INSERT INTO CityEntitie VALUES('Katerini','40.2723','22.5025','Greece')

INSERT INTO CityEntitie VALUES('Registro','-24.49','-47.84','Brazil')

INSERT INTO CityEntitie VALUES('St.-Brieuc','48.5167','-2.7833','France')

INSERT INTO CityEntitie VALUES('Taxco','18.5704','-99.62','Mexico')

INSERT INTO CityEntitie VALUES('Qingan','46.8719','127.5118','China')

INSERT INTO CityEntitie VALUES('Tingo Mar?a','-9.2896','-75.99','Peru')

INSERT INTO CityEntitie VALUES('Camaqu?','-30.8396','-51.81','Brazil')

INSERT INTO CityEntitie VALUES('Tigard','45.4237','-122.7844','United States')

INSERT INTO CityEntitie VALUES('Turnovo','43.0862','25.6555','Bulgaria')

INSERT INTO CityEntitie VALUES('Banes','20.9629','-75.7186','Cuba')

INSERT INTO CityEntitie VALUES('Minnetonka','44.9332','-93.4617','United States')

INSERT INTO CityEntitie VALUES('Peabody','42.5335','-70.9724','United States')

INSERT INTO CityEntitie VALUES('Alexandroupoli','40.8486','25.8744','Greece')

INSERT INTO CityEntitie VALUES('City of Milford (balance)','41.2253','-73.0624','United States')

INSERT INTO CityEntitie VALUES('Mercedes','-34.66','-59.44','Argentina')

INSERT INTO CityEntitie VALUES('Arrecife','28.969','-13.5378','Spain')

INSERT INTO CityEntitie VALUES('Crate?s','-5.1656','-40.666','Brazil')

INSERT INTO CityEntitie VALUES('Palm Desert','33.7378','-116.3695','United States')

INSERT INTO CityEntitie VALUES('Livny','52.4248','37.6044','Russia')

INSERT INTO CityEntitie VALUES('Lompoc','34.6618','-120.4714','United States')

INSERT INTO CityEntitie VALUES('Pinellas Park','27.8588','-82.7076','United States')

INSERT INTO CityEntitie VALUES('Perth Amboy','40.5203','-74.2724','United States')

INSERT INTO CityEntitie VALUES('Port Shepstone','-30.7195','30.46','South Africa')

INSERT INTO CityEntitie VALUES('Lorica','9.2419','-75.816','Colombia')

INSERT INTO CityEntitie VALUES('Slobozia','44.57','27.382','Romania')

INSERT INTO CityEntitie VALUES('Krasnokamsk','58.0747','55.7443','Russia')

INSERT INTO CityEntitie VALUES('Siena','43.317','11.35','Italy')

INSERT INTO CityEntitie VALUES('Farmington','36.7555','-108.1823','United States')

INSERT INTO CityEntitie VALUES('Mongu','-15.2796','23.12','Zambia')

INSERT INTO CityEntitie VALUES('Puerto Ayacucho','5.6639','-67.6236','Venezuela')

INSERT INTO CityEntitie VALUES('Porirua','-41.1219','174.8524','New Zealand')

INSERT INTO CityEntitie VALUES('New Plymouth','-39.0556','174.0748','New Zealand')

INSERT INTO CityEntitie VALUES('Thongwa','16.7547','96.5193','Burma')

INSERT INTO CityEntitie VALUES('Jaén','-5.7096','-78.81','Peru')

INSERT INTO CityEntitie VALUES('Pursat','12.5337','103.9167','Cambodia')

INSERT INTO CityEntitie VALUES('Bundaberg','-24.8791','152.3509','Australia')

INSERT INTO CityEntitie VALUES('Glendora','34.1449','-117.8468','United States')

INSERT INTO CityEntitie VALUES('Troms?','69.6351','18.992','Norway')

INSERT INTO CityEntitie VALUES('Apple Valley','44.7458','-93.2006','United States')

INSERT INTO CityEntitie VALUES('Mérida','38.912','-6.338','Spain')

INSERT INTO CityEntitie VALUES('Chambersburg','39.9315','-77.6556','United States')

INSERT INTO CityEntitie VALUES('Andradina','-20.9096','-51.3799','Brazil')

INSERT INTO CityEntitie VALUES('Barra do Garças','-15.8796','-52.26','Brazil')

INSERT INTO CityEntitie VALUES('Fredericton','45.95','-66.6333','Canada')

INSERT INTO CityEntitie VALUES('Hamilton','32.2942','-64.7839','Bermuda')

INSERT INTO CityEntitie VALUES('Naryn','41.4263','75.9911','Kyrgyzstan')

INSERT INTO CityEntitie VALUES('Oak Park','41.8872','-87.7899','United States')

INSERT INTO CityEntitie VALUES('Basankusu','1.2337','19.8','Congo (Kinshasa)')

INSERT INTO CityEntitie VALUES('Whangarei','-35.7256','174.323','New Zealand')

INSERT INTO CityEntitie VALUES('Tayshet','55.9277','97.9877','Russia')

INSERT INTO CityEntitie VALUES('Placentia','33.8807','-117.8553','United States')

INSERT INTO CityEntitie VALUES('Nyagan','62.1465','65.3814','Russia')

INSERT INTO CityEntitie VALUES('Kimry','56.8691','37.3444','Russia')

INSERT INTO CityEntitie VALUES('Walvis Bay','-22.9575','14.5053','Namibia')

INSERT INTO CityEntitie VALUES('Batatais','-20.89','-47.59','Brazil')

INSERT INTO CityEntitie VALUES('Edina','44.8914','-93.3602','United States')

INSERT INTO CityEntitie VALUES('Chilliwack','49.1666','-121.95','Canada')

INSERT INTO CityEntitie VALUES('Usulut?n','13.346','-88.432','El Salvador')

INSERT INTO CityEntitie VALUES('Gwadar','25.139','62.3286','Pakistan')

INSERT INTO CityEntitie VALUES('Kentwood','42.8852','-85.5925','United States')

INSERT INTO CityEntitie VALUES('Burien','47.4762','-122.3393','United States')

INSERT INTO CityEntitie VALUES('Aliso Viejo','33.5792','-117.729','United States')

INSERT INTO CityEntitie VALUES('Maha Sarakham','16.184','103.298','Thailand')

INSERT INTO CityEntitie VALUES('Sierra Vista','31.563','-110.3153','United States')

INSERT INTO CityEntitie VALUES('Hoffman Estates','42.0638','-88.1463','United States')

INSERT INTO CityEntitie VALUES('Apopka','28.7015','-81.5308','United States')

INSERT INTO CityEntitie VALUES('Tucupita','9.0605','-62.06','Venezuela')

INSERT INTO CityEntitie VALUES('Emden','53.3667','7.2167','Germany')

INSERT INTO CityEntitie VALUES('Itacoatiara','-3.14','-58.44','Brazil')

INSERT INTO CityEntitie VALUES('Florissant','38.7996','-90.3269','United States')

INSERT INTO CityEntitie VALUES('Tefé','-3.36','-64.7','Brazil')

INSERT INTO CityEntitie VALUES('Jendouba','36.5','8.75','Tunisia')

INSERT INTO CityEntitie VALUES('Bloomsburg','41.0027','-76.4561','United States')

INSERT INTO CityEntitie VALUES('Tarma','-11.41','-75.73','Peru')

INSERT INTO CityEntitie VALUES('Tulun','54.5653','100.5654','Russia')

INSERT INTO CityEntitie VALUES('Plainfield','40.6154','-74.4157','United States')

INSERT INTO CityEntitie VALUES('Saint Cloud','28.2303','-81.2849','United States')

INSERT INTO CityEntitie VALUES('Vejle','55.709','9.535','Denmark')

INSERT INTO CityEntitie VALUES('Séguéla','7.9504','-6.67','Côte DIvoire')

INSERT INTO CityEntitie VALUES('Coral Gables','25.7037','-80.2715','United States')

INSERT INTO CityEntitie VALUES('Nyeri','-0.417','36.951','Kenya')

INSERT INTO CityEntitie VALUES('Jinotega','13.091','-86','Nicaragua')

INSERT INTO CityEntitie VALUES('Kyustendil','42.2843','22.6911','Bulgaria')

INSERT INTO CityEntitie VALUES('Cerritos','33.8677','-118.0686','United States')

INSERT INTO CityEntitie VALUES('Hinesville','31.8247','-81.6135','United States')

INSERT INTO CityEntitie VALUES('Kyshtym','55.7','60.5595','Russia')

INSERT INTO CityEntitie VALUES('Ye','15.2533','97.8679','Burma')

INSERT INTO CityEntitie VALUES('Campobasso','41.563','14.656','Italy')

INSERT INTO CityEntitie VALUES('Mahalapye','-23.1','26.82','Botswana')

INSERT INTO CityEntitie VALUES('Jutiapa','14.29','-89.9','Guatemala')

INSERT INTO CityEntitie VALUES('Mweka','-4.8396','21.57','Congo (Kinshasa)')

INSERT INTO CityEntitie VALUES('Phetchabun','16.419','101.159','Thailand')

INSERT INTO CityEntitie VALUES('New Bern','35.0955','-77.0724','United States')

INSERT INTO CityEntitie VALUES('Xanthi','41.1418','24.8836','Greece')

INSERT INTO CityEntitie VALUES('Passau','48.567','13.4666','Germany')

INSERT INTO CityEntitie VALUES('Turbo','8.1004','-76.74','Colombia')

INSERT INTO CityEntitie VALUES('Gbadolite','4.2904','21.0199','Congo (Kinshasa)')

INSERT INTO CityEntitie VALUES('Hua Hin','12.5697','99.9443','Thailand')

INSERT INTO CityEntitie VALUES('Apex','35.7248','-78.8659','United States')
INSERT INTO CityEntitie VALUES('Aragua?na','-7.19','-48.21','Brazil')
INSERT INTO CityEntitie VALUES('Arjona','10.26','-75.35','Colombia')

INSERT INTO CityEntitie VALUES('Summerville','33.0005','-80.1788','United States')

INSERT INTO CityEntitie VALUES('Sawahlunto','-0.6663','100.7833','Indonesia')

INSERT INTO CityEntitie VALUES('Trujillo','9.3804','-70.44','Venezuela')

INSERT INTO CityEntitie VALUES('Maan','30.192','35.736','Jordan')

INSERT INTO CityEntitie VALUES('Sisophon','13.5838','102.9833','Cambodia')

INSERT INTO CityEntitie VALUES('Collierville','35.0474','-89.699','United States')

INSERT INTO CityEntitie VALUES('Methuen','42.734','-71.1889','United States')

INSERT INTO CityEntitie VALUES('Lakewood','41.4824','-81.8008','United States')

INSERT INTO CityEntitie VALUES('Helena','46.5964','-112.0197','United States')

INSERT INTO CityEntitie VALUES('Ferre?afe','-6.63','-79.8','Peru')

INSERT INTO CityEntitie VALUES('Ad Nabk','34.017','36.7333','Syria')

INSERT INTO CityEntitie VALUES('North Bay','46.3','-79.45','Canada')

INSERT INTO CityEntitie VALUES('Siguiri','11.4171','-9.1666','Guinea')

INSERT INTO CityEntitie VALUES('Otradnyy','53.3778','51.3474','Russia')

INSERT INTO CityEntitie VALUES('Kampot','10.6171','104.1833','Cambodia')

INSERT INTO CityEntitie VALUES('Gogrial','8.5337','28.1167','South Sudan')

INSERT INTO CityEntitie VALUES('Poway','32.9871','-117.0201','United States')

INSERT INTO CityEntitie VALUES('Pa-an','16.85','97.6167','Burma')

INSERT INTO CityEntitie VALUES('Puzi','23.4611','120.2419','Taiwan')

INSERT INTO CityEntitie VALUES('Korogwe','-5.0896','38.54','Tanzania')

INSERT INTO CityEntitie VALUES('Kahemba','-7.2829','19','Congo (Kinshasa)')

INSERT INTO CityEntitie VALUES('Bairin Zuoqi','43.9837','119.1834','China')
