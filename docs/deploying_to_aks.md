# Manually Deploying the frontend Application to AKS
_Aim: To deploy the frontend application from to AKS._
> Note: You can apply the same steps to deploy the backend application to AKS

## Introduction
We can either deploy to Kubernetes _imperatively_ (by passing commands to `kubectl`) or _declaratively_ (by declaring the state that we would like Kubernetes to achieve and maintain).

In most cases, we would prefer to have a declarative configuration. This is where Kubernetes manifests come in.

## Defining our Deployment

A _Deployment_ provides declarative state for Pods and ReplicaSets, see the [Kubernetes documentation](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/).

A manifest like this [frontend Deployment & Service](k8/frontend-deployment-service.yaml):

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: frontend
  labels:
    app: frontend-app
spec:
  replicas: 2
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 1
      maxUnavailable: 0
  selector:
    matchLabels:
      service: frontend
  template:
    metadata:
      labels:
        app: frontend-app
        service: frontend
    spec:
      containers:
        - name: frontend
          image: registry.gitlab.com/youssefhammoud.dev/sonicast/frontend
          imagePullPolicy: Always
          ports:
            - containerPort: 80
              protocol: TCP
          env:
            - name: METEOMATICSUSERNAME
              value: titi_toto
            - name: METEOMATICSPASSWORD
              value: 98niDA2vVIqsW 
            - name: BACKENDURL
              value: http://backend/weatherforecast/                   
      imagePullSecrets:
        - name: regcred
```

Will do the following:

- Create a Kubernetes `Deployment` named `frontend`, indicated by the `.metadata.name` field
- The Deployment will create two replicated `Pods` (indicated by the `replicas` field)
- The `selector` field defines which `Pods` will be managed by the `Deployment`:

```yaml
  selector:
    matchLabels:
      service: frontend
```

- We ask the `Deployment` to select `Pods` that have the `app: frontend` label
- We label our `Pods` with the following `template`:

```yaml
  template:
    metadata:
      labels:
        app: frontend-app
        service: frontend
```

- Each of our Pods contains a single container (recall that a Kubernetes `Pod` is an abstraction that wraps one or more containers)
- We call this container `frontend`
```yaml
  template:
    ...
    spec:
      containers:
        - name: frontend
          image: nginx:latest
          imagePullPolicy: Always
          ports:
            - containerPort: 80
              protocol: TCP
```
## Creating a Service to Expose our Application

We will not be able to reach our application yet. While our Pods are running, they are only accessible from other Pods within the cluster.

To make an initial test of our application, we can expose our application via a simple `Service` of `type=LoadBalancer`. This does not permit sophisticated control of which traffic reaches which of our pods, but it is a sensible initial test.

```yaml
apiVersion: v1
kind: Service
metadata:
  name: frontend
  labels:
    app: frontend-app
    service: frontend
spec:
  type: LoadBalancer
  ports:
    - port: 80
      targetPort: 80
      protocol: TCP
  selector:
    service: frontend

```
This specification will:

- Create a new `Service` object named `frontend`
- Which targets TCP port 8080
- On any Pod with the "service: frontend" label

## Before deploy the service we need to create a secret
Please create a secret in order to authorize the AKS to pull images from a private docker registry like gitlab:
```
➜  kubectl create secret docker-registry regcred \
      --docker-server="registry.gitlab.com" \
      --docker-username="yourgitlabaccount@xxx.com" \
      --docker-password="yourpassword" \
      --docker-email="yourgitlabaccount@xxx.com" \
      -o yaml --dry-run=client | kubectl apply -f -
```

## Deploy our application (Deployment with service)

We apply our desired state configuration:

```
➜  kubectl apply -f deployment.yaml

deployment/frontend created
service/frontend created

```
And then `watch` as our two Pods are created (initially in `ContainerCreating` state, in `Running` state a few moments later).
```
➜  kubectl get pods --watch

NAME                                  READY   STATUS              RESTARTS   AGE
frontend-deployment-65cf4464dd-fs6bx   0/1   ContainerCreating      0        13s
frontend-deployment-65cf4464dd-j5dhh   0/1   ContainerCreating      0        13s
frontend-deployment-65cf4464dd-fs6bx   1/1   Running                0        16s
frontend-deployment-65cf4464dd-j5dhh   1/1   Running                0        17s
```
## Connecting to Our Application
We ask Kubernetes where our application lives:

```
➜  kubectl get services

NAME                      TYPE    CLUSTER-IP    EXTERNAL-IP    PORT(S)        AGE
frontend-service   LoadBalancer   10.0.10.62    50.123.45.67   80:31037/TCP   23h
```

```
➜  ~ curl -X GET http://50.123.45.67:32769
Congratulations !!
```
