# Delivery our app with docker in production mode

## Run The Project
You will need the following tools:

* [Docker Desktop](https://www.docker.com/products/docker-desktop)

##  Environment setup

The default configuration of access rights in mssql-server does not allow using `localhost` or` 127.0.0.1` to access the server.

This limitation does not pose a problem with docker-compose. But first each container is launched manually. In this case it is necessary to use the real IP address of the server. To simplify the use, an environment variable will be used.

Obtain the machine's IP address
```bash
ip a
```
Create an environment variable
```bash
export MY_IP=xxx.xxx.xxx.xxx
```

##  Clone the project

Clone the "Sonicast application"
``` bash
cd $HOME
git clone https://gitlab.com/youssefhammoud.dev/sonicast.git
cd sonicast
```
## Create mssql-server container

### Description

Official container images for Microsoft SQL Server on Linux for Docker Engine.

Caracteristics: 

- Base Image : `mcr.microsoft.com/mssql/server:2019-latest`
- configuration for mssql-server container
  - ACCEPT_EULA: "Y"
  - SA_PASSWORD: "Password@12345"

###  Start mssql-server container 

Start a mssql-server instance using the latest update for SQL Server 2019
```console
chmod -R +x database*
cd database
```
```console
docker run \
     -e 'ACCEPT_EULA=Y'  \
     -e 'SA_PASSWORD=Password@12345' \
     -v ${PWD}:/scripts \
     -p 1433:1433 \
     --name sql-server-database \ 
     -d mcr.microsoft.com/mssql/server:2019-latest \
     bash -c "cd scripts && ./entrypoint.sh"
```

###  Validation

Check the container status
```bash
-> docker ps -a
CONTAINER ID   IMAGE                          COMMAND                  CREATED          STATUS              PORTS                     NAMES
e97ea284c6fa   sql-server-database   "/opt/mssql/bin/perm…"   47 seconds ago   Up 41 seconds       0.0.0.0:1433->1433/tcp            sql-server-database

-> docker logs sql-server-database
```

Connect to mssql-server
```bash
-> docker exec -it sql-server-database /opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P Password@12345
-> SELECT * FROM INFORMATION_SCHEMA.TABLES;
   GO
```


## Create backend container

### Description

Containerise the backend application based in dotnet core framework

Caracteristics: 

- Image de base : `mcr.microsoft.com/dotnet/core/sdk:3.1-buster`
- configuration for the backend container
  - ConnectionStrings__DefaultConnection: "Data Source=${MY_IP},1433; Initial Catalog=CitiesDb; User Id=sa; Password=Password@12345"

###  Start backend container

Build the backend container using the corresponding
File [backend/Dockerfile](backend/Dockerfile).
```console
cd backend
docker build -t back .
```
Create backend container instance
```console
docker run \
     -e 'ConnectionStrings__DefaultConnection=Data Source=${MY_IP},1433; Initial Catalog=CitiesDb; User Id=sa; Password=Password@12345'  \
     -p 8001:80 \
     --name back \
     -d back
```

###  Validation

http://localhost:8001/weatherforecast/paris
```json
{"cityName":"Paris","latitude":48.8667,"longitude":2.3333,"country":"France"}
```

## The frontend container

### Description

Caracteristics: 

- Base Image : `php:7.2-apache`
- configuration for frontend container
  - METEOMATICSUSERNAME: "titi_toto"
  - METEOMATICSPASSWORD: "98niDA2vVIqsW"
  - BACKENDURL: "http://${MY_IP}:8001/weatherforecast/"

Build the frontend container using the corresponding
File [frontend/Dockerfile](frontend/Dockerfile).
```console
cd frontend
docker build -t front .
```
Create frontend container instance
```console
docker run \
     -e 'METEOMATICSUSERNAME=titi_toto'  \
     -e 'METEOMATICSPASSWORD=98niDA2vVIqsW'  \
     -e 'BACKENDURL=http://${MY_IP}:8001/weatherforecast/'  \
     -p 8000:80 \
     --name front \
     -d front
```
###  Validation

http://localhost:8000
```
-> docker ps -a
CONTAINER ID   IMAGE         COMMAND                  CREATED              STATUS              PORTS                            NAMES
a955eab104af   front       "docker-php-entrypoi…"   About a minute ago   Up About a minute   0.0.0.0:8000->80/tcp             front
```  

## Utilisation de docker-compose

### Launch the docker-compose File [docker-compose.yaml](docker-compose.yaml).
- Launch the services
```
docker-compose up -d
```
- Verification of the services
```bash
-> docker-compose ps
       Name                      Command               State           Ports         
-------------------------------------------------------------------------------------
back                  dotnet WeatherApp.dll            Up      0.0.0.0:8001->80/tcp  
front                 docker-php-entrypoint apac ...   Up      0.0.0.0:8000->80/tcp  
sql-server-database   /opt/mssql/bin/permissions ...   Up      0.0.0.0:1433->1433/tcp

```
- Test the application : Connexion via the url "http://localhost:8000/"
- Stop the services
```
docker-compose down
```

