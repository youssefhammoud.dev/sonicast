# Using Gitlab CI/CD to deploy an Image

## Introduction
In this section we will use Gitlab CI/CD to deploy our container image from Gitlab Container Registry to AKS.

## Deployment with Gitlab

Relatively speaking, asking Gitlab to deploy our application to AKS is simpler than building the image.

As before, we create a new job, `deploy`, part of our `deploy` stage:

```yaml
deploy:
  stage: deploy
```

We need an image that contains `kubectl`. We could roll our own, and may wish to do so in environments where security is paramount. For the purposes of this example, we will leverage Dockerhub:

```yaml
  image:
    name: youssefhammoud/k8s-kubectl
    entrypoint: [""]
```

All that remains is to instruct `kubectl` to create our deployments:

```yaml
  script:
    - echo ${CI_REGISTRY_IMAGE}/${FRONTEND_IMAGE_NAME}:$CI_COMMIT_SHORT_SHA
    #Create secret to authorise the cluster to pull images from the private registry
    - kubectl create secret docker-registry regcred --docker-server="$CI_REGISTRY" --docker-username="$CI_DEPLOY_USER" --docker-password="$CI_DEPLOY_PASSWORD" --docker-email="$GITLAB_USER_EMAIL" -o yaml --dry-run=client | kubectl apply -f -
    #Create a backend deployment with latest tag
    - kubectl apply -f ${KUBEHOME}/backend-deployment-service.yaml
    #Create a frontend backend with latest tag
    - kubectl apply -f ${KUBEHOME}/frontend-deployment-service.yaml
    # Perform rolling updates for the frontend application with the latest image
    - kubectl set image -n ${NAMESPACE} deployment/frontend frontend=${CI_REGISTRY_IMAGE}/${FRONTEND_IMAGE_NAME}:$CI_COMMIT_SHORT_SHA
    # Perform rolling updates for the backend application with the latest image
    - kubectl set image -n ${NAMESPACE} deployment/backend backend=${CI_REGISTRY_IMAGE}/${BACKEND_IMAGE_NAME}:$CI_COMMIT_SHORT_SHA
```

- First we apply our deployment and our services
- Second we create a secret object in order make sure that AKS can pull images from the private registry (Gitlab)
- Then, we ask that our frontend containers are updated to our latest build,  `${CI_REGISTRY_IMAGE}/${FRONTEND_IMAGE_NAME}:$CI_COMMIT_SHORT_SHA`
- Finally, we ask that our backend containers are updated to our latest build,  `${CI_REGISTRY_IMAGE}/${BACKEND_IMAGE_NAME}:$CI_COMMIT_SHORT_SHA`

## Additional Gitlab namespace
```
➜  kubectl get namespaces

NAME              STATUS   AGE
default           Active   2d20h
kube-node-lease   Active   2d20h
kube-public       Active   2d20h
kube-system       Active   2d20h
mynamespace       Active   2d20h
```

Our most recent deployment is in the `mynamespace` namespaces, because we have configured that previously
> Note: For more info, see the section `Required Configuration Parameters` in File [Connecting Gitlab To Aks](connecting_gitlab_to_aks.md)

```
➜  kubectl get pods -n mynamespace
NAME                        READY   STATUS    RESTARTS   AGE
backend-654fbdf758-mdn7z    1/1     Running   0          6m25s
backend-654fbdf758-rzssw    1/1     Running   0          6m25s
frontend-5c979ddf8b-j78zm   1/1     Running   0          6m25s
frontend-5c979ddf8b-z8dcn   1/1     Running   0          6m25s

```

## Viewing our Pipeline

We can see the execution of our Job:

![Gitlab Deployment](img/Gitlab_Deploy.PNG "Gitlab Deployment")


## Next
In the next section we will demonstrate Continuous Delivery with our new pipeline.

< Deploying a Container Image with Gitlab CI/CD | [ Demonstrating Continuous Delivery](continuous_delivery.md) >
