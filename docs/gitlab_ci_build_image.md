# Using Gitlab CI/CD to build an Image

## Introduction
In this section we will use Gitlab CI/CD to build our code, then push it to our docker registry.

## Defining CI/CD Stages

Gitlab's Pipelines are _Pipelines as Code_, which is a modern approach to Continuous Delivery.

To get started, we create a `.gitlab-ci.yml` file in our project's root.

Initially, we want to:

- Build our software
- Create our containers images and push it to our registry

In Gitlab terms, we will model these two requirements as separates _stages_:

```yaml
stages:
  - build
```


## The Build Code Job

### Building an image from a Dockerfile
We can do the build our images(backend and frontend) using the shared runner image `docker:20.10`
Here we see a generic function that take an `$IMAGE_NAME` as parameter
we defined the `$IMAGE_NAME (BACKEND_IMAGE_NAME,FRONTEND_IMAGE_NAME)` as variable as below:
```yaml
variables:
  BACKEND_IMAGE_NAME: backend
  FRONTEND_IMAGE_NAME: frontend
  KUBEHOME: ${CI_PROJECT_DIR}/k8
  NAMESPACE: mynamespace

```  
Our function is parameterised and it's able to build and push any containerized application that contains a `Dockerfile` inside their source code.
```yaml
.build_docker_image:
  image: docker:20.10
  services:
    - docker:20.10-dind
  before_script:
    # check variables
    - echo "CI_REGISTRY_IMAGE = $CI_REGISTRY_IMAGE"
    - echo "IMAGE_NAME = $IMAGE_NAME"
    # Create IMAGE Tag
    - export MY_IMAGE_REGISTRY=${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:$CI_COMMIT_SHORT_SHA
      # Connect to registry 
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    # check variable
    - echo "IMAGE_NAME = $IMAGE_NAME"
    - echo "MY_IMAGE_REGISTRY = $MY_IMAGE_REGISTRY"
    # build de l'image
    #   - copie du dockerfile et des ressources à la racine du projet
    #   - build de l'image
    - mv ${IMAGE_NAME}/* .
    - docker build -t ${MY_IMAGE_REGISTRY}  .
    # push dans la registry 
    - docker push ${MY_IMAGE_REGISTRY}
```

### Building the frontend image
This configuration will:
```yaml
build:frontend:
  extends: .build_docker_image
  stage: build
  variables:
    IMAGE_NAME: ${FRONTEND_IMAGE_NAME}
```
### Building the backend image
This configuration will:
```yaml
build:backend:
  extends: .build_docker_image
  stage: build
  variables:
    IMAGE_NAME: ${BACKEND_IMAGE_NAME}
```

- We pass the built in environment variable `$CI_REGISTRY_IMAGE` for the location of our project's container registry
- To ensure that the images we push have different tags, we refer to `$CI_COMMIT_SHORT_SHA`

> Note: The latter becomes important when we want to continuously deploy to Kubernetes - we need to have a way of asking Kubernetes to update to a newer, tagged image

## Viewing our Pipeline

If we commit code to our pipeline, we can now watch it build:

![Gitlab Pipeline](./img/Pipeline.PNG "Gitlab Pipeline")

And within the pipeline, we can see our Build Code job:

![Gitlab Build Code Job](./img/Build_Code_Job.PNG "Gitlab Build Code Job")

## Viewing our Images

We can also see our images in the Gitlab UI:

![Gitlab Container Registry](./img/container_registry.PNG "Gitlab Container Registry")
