# Connecting Gitlab to AKS
_Aim: To publish the “Sonicast” application from Gitlab to AKS._

# Introduction
In this section we will configure our AKS cluster in Gitlab, then use a docker image shell that support the  `kubectl` command to configure our  cluster and used as a deployment target (CD).

## Adding an Existing Kubernetes Cluster to Gitlab

In your Gitlab project, navigate to _Operations > Kubernetes > Add Existing Cluster_:

![gitlab kubernetes integration](./img/05_gitlab_kubernetes.png "Adding a Kubernetes Cluster to Gitlab")

Using this form, we will supply the credentials we need to reach our Kubernetes cluster (We will demonstrate how to obtain this information in the sub-sections below). [Gitlab's documentation](https://gitlab.com/help/user/project/clusters/index#adding-an-existing-kubernetes-cluster) provides additional information on the values for these fields.

## Required Configuration Parameters

| Required Field | Example |
| --- | --- |
| **Kubernetes cluster name** | `akspoccluster` |  
| **Environment scope** | `*` |
| **API URL** | https://akspocclus-my-resource-7b2354-c9e724cb.hcp.eastus.azmk8s.io:443 |
| **CA Certificate** | A PEM (plaintext) x509 certificate | 
| **Token** | The token for a Kubernetes `ServiceAccount` called `gitlab-admin` that has been granted the `cluster-admin` role | 
| **Project namespace (optional, unique)** | Can be left blank to ask Gitlab to create a Kubernetes namespace for the project , in our project we have added one with name: `mynamespace`| 
| **RBAC-enabled cluster** | Specified when creating the cluster | Yes |


### Kubernetes Cluster Name
Specified when creating the cluster.

Can be retrieved with the Azure CLI (see `Name`):

```
➜  az aks list --output table

Name                 Location    ResourceGroup       KubernetesVersion    ProvisioningState    Fqdn
-------------------  ----------  ------------------  -------------------  -------------------  -------------------------------------------------
akspoccluster  eastus      my-resource     1.18.14              Succeeded            akspocclus-my-resource-7b2354-c9e724cb.hcp.eastus.azmk8s.io
```

### Environment Scope
_Environment Scope_ is used to tell Gitlab how to use the Cluster (e.g. for Dev, Staging, Prod or for all workloads).

Although Gitlab documentation suggests that this as a Premium feature (hinting that, in the Community Edition, we should default to `*`), it actually appears to be mandatory. If we set `*`, Gitlab appears **not** to expose its environment variables (`$KUBE_CONFIG`, etc.) to our CI/CD pipeline.

For the purposes of this repo, we have declared `*`.


> Note: See [Troubleshooting Missing KUBECONFIG or KUBE_TOKEN](https://gitlab.com/help/user/project/clusters/index#troubleshooting-missing-kubeconfig-or-kube_token) (Gitlab Docs)


### API URL
The Kubernetes API server, visible as the `fqdn` in `az aks list --output table` (above). Remember to prefix with `https:`.

> Note that AKS API Server endpoints are public, regardless of the use of 'Advanced Networking'.


### CA Certificate
The AKS API Server endpoint is secured with a self-signed TLS certificate.
Gitlab must be provided with the CA cert so that it can trust it.

Following Gitlab's instructions:

- List the available secrets with `kubectl get secrets`; one should have name matching `default-token-xxxxx`

```
➜  kubectl get secrets

NAME                  TYPE                                  DATA   AGE
default-token-lp5k9   kubernetes.io/service-account-token   3      46m
```

- Using `kubectl get secret` and the name of this token, lookup the certificate in the config map, and decode:

```
kubectl get secret default-token-lp5k9 -o jsonpath="{['data']['ca\.crt']}" | base64 --decode

-----BEGIN CERTIFICATE-----
...
-----END CERTIFICATE-----

```

### Token
GitLab authenticates against Kubernetes using service tokens, which are scoped to a particular namespace.
The token used should belong to a service account with cluster-admin privileges.
The ".yaml" files referenced below can be found in the [`/k8`](/k8)  folder of this repository.

- Create a `ServiceAccount` called `gitlab-admin`:

`kubectl apply -f gitlab-admin-service-account.yaml`

- Grant `ClusterRole` `cluster-admin`:

`kubectl apply -f gitlab-admin-cluster-role-binding.yaml`


With the service created, we are now able to collect the token that gitlab will ask. Please do the following:

```
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep gitlab-admin | awk '{print $1}')
```

```
Type:  kubernetes.io/service-account-token

Data
====
ca.crt:     1720 bytes
namespace:  11 bytes
token:      <token string>
```

> Note: Please be ensure that you are aware of the security implications of doing this - Gitlab is being granted administrative access to your cluster. See [Gitlab's documentation](https://gitlab.com/help/user/project/clusters/index#security-implications) for a similar warning.

Copy the "token string", making sure not to copy any extraneous spaces.


### Project namespace

Kubernetes namespaces are a way to segregate projects within a single Kubernetes cluster. For this example we'll have Gitlab create one for us, but if you have set up a specific namespace in Kubernetes you can enter it here:

> Gitlab documentation notes that you don't have to fill it in; by leaving it blank, GitLab will create one for you


### RBAC-enabled cluster
Congrats ! we've created an RBAC enabled cluster.
