# Creating an AKS Cluster Using Azure CLI

## Prerequisites
- Before getting started, ensure that you have Azure CLI up and running with the latest version.
- Next, install “kubectl” so we can talk to the Kubernetes Cluster later.
- Enter the following command in Azure CLI.

```
$ az aks install-cli
```

### Create a Resource Group
Next, create a new Resource Group `my-resource` to deploy AKS into in Azure.

- The intent of creating a separate RG is to ensure we can clean up the deployment later if required.

Next command:
```
$ az ad sp create-for-rbac --skip-assignment
```
That will create a service principal and configure its access to Azure resources.

```
$ az group create --name my-resource --location eastus
```

- The output below shows a successful creation of the new Resource Group

```
{
  "id": "/subscriptions/7b2354a6-8e5b-47c7-b8eb-7e97b38f1079/resourceGroups/my-resource",
  "location": "eastus",
  "managedBy": null,
  "name": "my-resource",
  "properties": {
    "provisioningState": "Succeeded"
  },
  "tags": null,
  "type": "Microsoft.Resources/resourceGroups"
}
```

### Create an AKS cluster

- Next, start deploying the Kubernetes cluster

> Note: This operation will take a few minutes to complete.

```
$ az aks create \
    --resource-group my-resource \
    --name akspoccluster \
    --vm-set-type VirtualMachineScaleSets \
    --node-count 1 \
    --generate-ssh-keys \
    --load-balancer-sku standard
```

### Credentials

With the AKS cluster running, you can start deploying the sample application on it. To do that, we need to obtain the credentials for the cluster:

```
$ az aks get-credentials --resource-group my-resource --name akspoccluster
```
- You should see an output similar to the one below
```
Merged "akspoccluster" as current context in $HOME/.kube/config
```

Now you can check the nodes of the AKS cluster:

```
$ kubectl get nodes

NAME                       STATUS   ROLES   AGE   VERSION
aks-nodepool1-2460631-vm0  Ready    agent   73s   v1.18.14
```


#### Deploying an Application to the Cluster
Next, create a YAML deployment file for the test application.
A sample dep.yaml file is provided here to enable faster deployment testing.

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: azure-vote-back
spec:
  replicas: 1
  selector:
    matchLabels:
      app: azure-vote-back
  template:
    metadata:
      labels:
        app: azure-vote-back
    spec:
      containers:
      - name: azure-vote-back
        image: redis
        resources:
          requests:
            cpu: 100m
            memory: 128Mi
          limits:
            cpu: 250m
            memory: 256Mi
        ports:
        - containerPort: 6379
          name: redis
---
apiVersion: v1
kind: Service
metadata:
  name: azure-vote-back
spec:
  ports:
  - port: 6379
  selector:
    app: azure-vote-back
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: azure-vote-front
spec:
  replicas: 1
  selector:
    matchLabels:
      app: azure-vote-front
  template:
    metadata:
      labels:
        app: azure-vote-front
    spec:
      containers:
      - name: azure-vote-front
        image: microsoft/azure-vote-front:v1
        resources:
          requests:
            cpu: 100m
            memory: 128Mi
          limits:
            cpu: 250m
            memory: 256Mi
        ports:
        - containerPort: 80
        env:
        - name: REDIS
          value: "azure-vote-back"
---
apiVersion: v1
kind: Service
metadata:
  name: azure-vote-front
spec:
  type: LoadBalancer
  ports:
  - port: 80
  selector:
    app: azure-vote-front
```

Open the file in Notepad in case you wish to edit variables for the app itself.

Towards the end of the file, you will see the following syntax:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: azure-vote-front
spec:
  type: LoadBalancer
  ports:
  - port: 80
  selector:
    app: azure-vote-front
````

> To deploy the application, just type in your terminal
>```
>$ kubectl apply -f dep.yaml
>``` 

To access the application
```
$ kubectl get services

NAMESPACE     NAME               TYPE           CLUSTER-IP    EXTERNAL-IP     PORT(S)         AGE
default       azure-vote-back    ClusterIP      10.0.147.52   <none>          6379/TCP        33s
default       azure-vote-front   LoadBalancer   10.0.60.204   52.188.134.33   80:31831/TCP    32s
default       kubernetes         ClusterIP      10.0.0.1      <none>          443/TCP         4m44s
kube-system   kube-dns           ClusterIP      10.0.0.10     <none>          53/UDP,53/TCP   4m5s
kube-system   metrics-server     ClusterIP      10.0.42.15    <none>          443/TCP         4m5s

You can now access your application via http://52.188.134.33
```
