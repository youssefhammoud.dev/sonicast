# Demonstrating Continuous Delivery 

## Introduction
Now that our pipeline is in place, we can demonstrate it end-to-end:

- Make a code change (an easy way is to change the header color from File [style.css](frontend/style.css))
- Watch the pipeline complete
- Watch your change (Use `CTRL-F5` to force an update and prevent the caching browser)

Since this will happen relatively quickly, we will set it up in reverse:

## Watching our Existing Service

Locate our existing deployment with `kubectl`:

```
➜  kubectl get services -n mynamespace
NAME       TYPE           CLUSTER-IP    EXTERNAL-IP     PORT(S)        AGE
backend    NodePort       10.0.187.61   <none>          80:30077/TCP   2d20h
frontend   LoadBalancer   10.0.6.93     52.146.56.181   80:30043/TCP   2d20h

```
Check the URL http://52.146.56.181


## Making a Code Change

We can now edit the style.css:

```
#header-custom{
    line-height: 1.5rem;
    vertical-align: bottom;
    text-align: end;
    background: red;/*#5856D6*/
    color: white;
    margin-bottom: 2vh;
}
```

If we commit and push this:

```
➜  git add .

➜  git commit -m "Updated header color to red"

➜  git push origin master
```

## Watching our Pipeline

And then watch our pipeline:

![Gitlab Pipeline in Progress (Build)](img/Pipeline_Build.PNG "Gitlab Pipeline in Progress (Build)")

## Rolling Update

We can watch Kubernetes perform a rolling update of our Pods (scheduling new ones, removing old ones):

```
➜  kubectl get pods -n mynamespace --watch --selector app=frontend-app
```

Our Pods ("7wnq2" and "fp5w7") are initially running:
```
NAME                        READY   STATUS    RESTARTS   AGE
frontend-5b5f598576-7wnq2   1/1     Running   0          3m5s
frontend-5b5f598576-fp5w7   1/1     Running   0          3m10s
```

A new Pod ("g9mgq") is scheduled:
```
frontend-5b5f598576-g9mgq   0/1   Pending   0     1s
frontend-5b5f598576-g9mgq   0/1   Pending   0     1s
```

And then created:
```
frontend-5b5f598576--g9mgq   0/1   ContainerCreating   0     1s
frontend-5b5f598576--g9mgq   1/1   Running             0     8s
```

Once running, the Pod it is replacing ("7wnq2") is terminated:

```
frontend-5b5f598576-7wnq2   1/1   Terminating   0     7m36s
```

And then the process repeats for the other Pod in our ReplicaSet. "tw7xz" is created to replace "fp5w7"

```
frontend-5b5f598576-7wnq2   1/1     Running   0          3m5s
frontend-5b5f598576-fp5w7   1/1     Running   0          3m10s
frontend-5bd798d599-g9mgq   0/1     Pending   0          0s
frontend-5bd798d599-g9mgq   0/1     Pending   0          0s
frontend-5bd798d599-g9mgq   0/1     ContainerCreating   0          0s
frontend-5bd798d599-g9mgq   0/1     Terminating         0          0s
frontend-5bd798d599-g9mgq   0/1     Terminating         0          6s
frontend-5bd798d599-g9mgq   0/1     Terminating         0          6s

```

## Watching our Application

As the change rolls in, we initially see the old message gradually replaced by the new message.

We see that the change is not _quite_ seamless - there is a small period of downtime (failed connections). To help with this, we could look at using Kubernete Readiness Probes.
